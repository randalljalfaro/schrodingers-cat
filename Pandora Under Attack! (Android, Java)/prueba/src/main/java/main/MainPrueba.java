package main;

import com.google.gson.Gson;

import bohr.engine.net.Network;
import game.model.main.ClientGame;
import game.model.net.GameEvents;
import bohr.engine.admin.GeoPoint;

/**
 * Created by curso on 09/09/2015.
 */
public class MainPrueba {
    private static ClientGame clientGame = new ClientGame();
    private static Network net;

    public static void main(String[] args) {
        clientGame.getEngineState().setPort(8080);
        net = clientGame.getNetwork();
        net.openConnection();
        pruebaDeVotosYCompras();
        net.closeConnection();
        //createUsersClans();
        //pruebaUser();
        //pruebaClan();
        //pruebaAction();
        //pruebaResources();
        //pruebaRequest();
        //pruebaMessaging();

    }

    private static void pruebaMessaging() {
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                GameEvents.sendMessage(1, 1, "hola"))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                GameEvents.sendMessage(2, 1, "soy"))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                GameEvents.sendMessage(3, 1, "el jugador X ha entrado al clan"))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                GameEvents.sendMessage(4, 1, "peersona"))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                GameEvents.sendMessage(6, 2, "jsdnfl"))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                GameEvents.getAllMessaging(1))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                GameEvents.getAllMessaging(2))));
    }

    public static void pruebaResources() {

        /*System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                GameEvents.collect(1, 1, new GeoPoint()))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                GameEvents.getAllClans())));*/
        int i = 18;
        int id = 1;
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                GameEvents.collect(
                        i + 1, id, new GeoPoint()))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                GameEvents.collect(
                        i + 2, id, new GeoPoint()))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                GameEvents.collect(
                        i + 3, id, new GeoPoint()))));


        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                GameEvents.getAllResources(new GeoPoint()))));

        System.out.println(clientGame.getNetwork().
                sendEvent(new Gson().toJson(
                        GameEvents.getClan(
                                "Xyy"))));
    }

    public static void pruebaClan() {
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                GameEvents.createClan(
                        1, "De los pichudos",
                        1, new GeoPoint(10.54, -85.1665)))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                GameEvents.createClan(
                        1, "De los guapos",
                        1, new GeoPoint(11.54, -84.1725)))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                GameEvents.createClan(
                        1, "De los feos",
                        1, new GeoPoint(10.54554, -85.1665)))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                GameEvents.createClan(
                        1, "De los feos",
                        1, new GeoPoint(10.54554, -85.1665)))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                GameEvents.createClan(
                        1, "De los otros",
                        1, new GeoPoint(11.58654, -83.8665)))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                GameEvents.getAllClans())));
    }

    public static void pruebaUser() {
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(GameEvents.signUp("Randall1", "1233"))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(GameEvents.signUp("Randall1", "456"))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(GameEvents.signUp("Randall2", "12334"))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(GameEvents.login("Randall2", "12335"))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(GameEvents.login("Randall1", "456"))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(GameEvents.getAllUsers())));
    }

    public static void pruebaRequest() {
        int clan = 10;
        /*System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(GameEvents.sendRequest(clan, 1))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(GameEvents.sendRequest(clan, 2))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(GameEvents.sendRequest(clan, 3))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(GameEvents.sendRequest(clan, 4))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(GameEvents.sendRequest(clan, 5))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(GameEvents.sendRequest(clan, 6))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(GameEvents.sendRequest(clan, 7))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(GameEvents.sendRequest(clan, 8))));*/
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(GameEvents.sendRequest(1, 2))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(GameEvents.getAllbyClanRequests(clan))));
        /*System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(GameEvents.getAllbyUserRequests(2))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(GameEvents.responseRequest(1, false))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(GameEvents.responseRequest(2, true))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(GameEvents.responseRequest(3, false))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(GameEvents.getAllbyClanRequests(1))));*/
    }

    public static void pruebaAction() {

        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                GameEvents.buy(1, 1, 1)
        )));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                GameEvents.getAllActions(1)
        )));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                GameEvents.voteGameAction(1, 1, true)
        )));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                GameEvents.voteGameAction(1, 1, true)
        )));

        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                GameEvents.getAllActions(1)
        )));
    }

    public static void createUsersClans() {
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(GameEvents.signUp("Randall1", "1233"))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(GameEvents.signUp("Randall2", "456"))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(GameEvents.signUp("Randall3", "12334"))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(GameEvents.signUp("Randall4", "12335"))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(GameEvents.signUp("Randall5", "456"))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                GameEvents.createClan(
                        1, "De los pichudos",
                        1, new GeoPoint(10.54, -85.1665)))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                GameEvents.createClan(
                        1, "De los guapos",
                        1, new GeoPoint(11.54, -84.1725)))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                GameEvents.createClan(
                        1, "De los feos",
                        1, new GeoPoint(10.54554, -85.1665)))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                GameEvents.createClan(
                        1, "De los otros",
                        1, new GeoPoint(11.58654, -83.8665)))));
    }

    public static void pruebaDeVotosYCompras() {
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(GameEvents.signUp("Randall1", "1233"))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(GameEvents.signUp("Randall2", "456"))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(GameEvents.signUp("Randall3", "12334"))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(GameEvents.signUp("Randall4", "12335"))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(GameEvents.signUp("Randall5", "456"))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                GameEvents.createClan(
                        1, "De los pichudos",
                        1, new GeoPoint(10.54, -85.1665)))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(GameEvents.sendRequest(1, 2))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(GameEvents.sendRequest(1, 3))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(GameEvents.sendRequest(1, 4))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(GameEvents.sendRequest(1, 5))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(GameEvents.responseRequest(1, true))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(GameEvents.responseRequest(2, true))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(GameEvents.responseRequest(3, true))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(GameEvents.responseRequest(4, true))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                GameEvents.buy(1, 1, 1)
        )));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                GameEvents.voteGameAction(1, 1, true)
        )));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                GameEvents.voteGameAction(1, 2, true)
        )));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                GameEvents.voteGameAction(1, 3, true)
        )));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                GameEvents.voteGameAction(1, 4, true)
        )));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                GameEvents.voteGameAction(1, 5, true)
        )));


    }

}
