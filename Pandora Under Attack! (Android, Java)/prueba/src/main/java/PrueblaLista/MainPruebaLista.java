package PrueblaLista;

import bohr.engine.util.list.List;
import bohr.engine.util.list.Node;

public class MainPruebaLista {
    private static List list = new List();

    public static void main(String[] args) {
        System.out.println("------------");
        list.insert(33);
        //System.out.println(list.head.getData());
        list.insert(44);
        list.insert(41);
        list.insert(42);
        list.insert(43);
        list.insert(44);
        list.insert(45);
        list.insert(46);
        list.insert(47);
        list.insert(48);
        list.remove(5);
        buscar();
        getListRemove();
        recorrer();
        //getlist();
        System.out.println("-------------");
    }

    private static void getListRemove() {
        list.remove(48);
    }

    private static void getlist() {
        list.insert(33);
        System.out.println("*+*+*+*+*");
        recorrer();
    }

    public static void buscar() {
        list.search(33);
        System.out.println(list.search(48));
        System.out.println("***********");
    }

    public static void recorrer() {
        Node tmp = list.head;
        while (tmp != null) {
            System.out.println(tmp.getData());
            tmp = tmp.getNextNode();
        }
    }
}
