package bohr.engine.admin;

/**
 * Clase utillizada para las solicitudes del usuario
 *
 * @author Isaac Trejos
 * @author Luis Murillo
 * @author Kevin Arce
 * @author Randall Alfaro
 * Clase utillizada para
 */
public class UserRequest {
    private int id;
    private String username;
    private String password;

    /**
     * constructor
     *
     * @param iD ID del usuario
     * @param username Nombre del usuario
     * @param password Contraseña del usuarui
     */
    public UserRequest(int iD, String username, String password) {
        id = iD;
        this.username = username;
        this.password = password;
    }

    /**
     * Solicitud de parte del usuario
     */
    public UserRequest() {
        id = -1;
        this.username = "";
        this.password = "";
    }

    public void setID(int ID) {
        this.id = ID;
    }

    public int getID() {
        return id;
    }


    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
