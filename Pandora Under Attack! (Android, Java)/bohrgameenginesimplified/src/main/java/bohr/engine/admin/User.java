package bohr.engine.admin;

import java.net.Socket;

/**
 * Clase del usuario, todo sobre la creacion y sobre un usuario
 * Entradas: id, geoPoint,socket,username, password
 */
public class User {
    private int id;
    private GeoPoint geoPoint;
    private transient Socket socket;
    private String username;
    private String password;

    /**
     * Constructor
     * @param id ID entregada al usuario
     * @param socket Socket de conexion
     * @param username Nombre de usuario
     * @param password Contraseña del usuario
     */
    public User(int id, Socket socket, String username, String password) {
        this.id = id;
        this.geoPoint = null;
        this.socket = socket;
        this.username = username;
        this.password = password;
    }

    /**
     * @author Isaac Trejos
     * @author Luis Murillo
     * @author Kevin Arce
     * @author Randall Alfaro
     * Ubicacion del usuario
     */
    public User() {
        this.id = -1;
        this.geoPoint = new GeoPoint();
        this.socket = null;
        this.username = "";
        this.password = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public GeoPoint getGeoPoint() {
        return geoPoint;
    }

    public void setGeoPoint(GeoPoint geoPoint) {
        this.geoPoint = geoPoint;
    }

    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
