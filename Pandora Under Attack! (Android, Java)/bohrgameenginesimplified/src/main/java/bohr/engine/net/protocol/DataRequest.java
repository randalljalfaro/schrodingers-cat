package bohr.engine.net.protocol;

/**
 * @author Randall Alfaro
 * @author Kevin Arse
 * @author Luis Fernando
 * @author Isaac Trejos
 */

public class DataRequest<T> extends Request {
    private T data;


    public DataRequest(String type, String action) {
        super(type, action);
        this.data = null;
    }

    /**
     * Constructor
     * @param type tipo de solicitud
     * @param action
     * @param data dato que se utiliza
     */
    public DataRequest(String type, String action, T data) {
        super(type, action);
        this.data = data;
    }

    public DataRequest(T data) {
        super();
        this.data = data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public T getData() {
        return data;
    }



}
