package bohr.engine.util.list;

/**
 * * Created by Isaac and kevin and Luis Fernando on 14/09/2015.
 */

/**
 * Clase encargada de la creacion de los nodos utilizados en la lista
 * @author Isaac Trejos
 * @author Luis Murillo
 * @author Kevin Arce
 * @author Randall Alfaro
 * @param <T> es un objeto abstract encargada de la imformacion del nodo.
 */
    public class Node<T> {
        private T data;
        private Node<T> nextNode;

    /**
     * Constructor del nodo
     * @param data el dato que entra al nodo
     * @param nextNode el nodo anterior al nodo principal la cabeza
     */
        public Node(T data, Node<T> nextNode) {
            this.data = data;
            this.nextNode = nextNode;
        }

    /**
     * el nodo
     * @param data el dato
     */
        public Node(T data) {
            this.data = data;
            nextNode = null;
        }
        public void setData(T data) {
            this.data = data;
        }

    public void setNextNode(Node<T> nextNode) {
        this.nextNode = nextNode;
    }

    public T getData() {
        return data;
    }

    public Node<T> getNextNode() {
        return nextNode;
    }
}
