package game.model.entities.others;

import java.io.Serializable;

/**
 * Clase que almacena los datos de un mensaje
 *
 * @author Isaac Trejos
 * @author Luis Murillo
 * @author Kevin Arce
 * @author Randall Alfaro
 */
public class Message{
    private int id;
    private int clanId;
    private int userId;
    private String message;

    /**
     * Constructor de la clase
     *
     * @param id      id del mensaje
     * @param userId  id del usuario
     * @param clanId  id del clan
     * @param message id del mensaje
     */
    public Message(int id, int userId, int clanId, String message) {
        this.id = id;
        this.userId = userId;
        this.clanId = clanId;
        this.message = message;
    }

    /**
     * Constructor auxiliar de la clase
     */
    public Message() {
        this.id = -1;
        this.clanId = -1;
        this.userId = -1;
        this.message = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getClanId() {
        return clanId;
    }

    public void setClanId(int clanId) {
        this.clanId = clanId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
