package game.model.entities.others;

/**
 * Clase que almacena los datos de una solicitud
 *
 * @author Isaac Trejos
 * @author Luis Murillo
 * @author Kevin Arce
 * @author Randall Alfaro
 */

public class GameRequest {

    private int clan;
    private int user;
    private boolean response;
    private int idRequest;

    /**
     * Constructor auxiliar de la clase
     *
     */
    public GameRequest() {
        this.clan = -1;
        this.user = -1;
        this.idRequest = -1;
        this.response = false;
    }

    /**
     * Constructor de la clase
     *
     * @param clan       id del clan
     * @param user     id del usuario
     * @param response respuesta a la solicitud
     * @param idRequest    id de la solicitud
     *
     **/
    public GameRequest(int clan, int user, boolean response, int idRequest) {
        this.clan = clan;
        this.user = user;
        this.response = response;
        this.idRequest = idRequest;
    }

    public int getIdClan() {
        return clan;
    }

    public void setIdClan(int clan) {
        this.clan = clan;
    }

    public int getIdUser() {
        return user;
    }

    public void setIdUser(int user) {
        this.user = user;
    }

    public boolean isResponse() {
        return response;
    }

    public void setResponse(boolean response) {
        this.response = response;
    }

    public int getIdRequest() {
        return idRequest;
    }

    public void setIdRequest(int idRequest) {
        this.idRequest = idRequest;
    }
}