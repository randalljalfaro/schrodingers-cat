package game.model.entities.others;


/**
 * Clase que almacena los datos de un voto
 *
 * @author Isaac Trejos
 * @author Luis Murillo
 * @author Kevin Arce
 * @author Randall Alfaro
 */
public class Vote {
    private boolean response;
    private int userId;

    /**
     * Constructor de la clase
     *
     * @param userId      id del usuario
     * @param response  respuesta al voto
     */
    public Vote(int userId, boolean response) {
        this.response = response;
        this.userId = userId;
    }


    public boolean getResponse() {
        return response;
    }

    public void setResponse(boolean response) {
        this.response = response;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
