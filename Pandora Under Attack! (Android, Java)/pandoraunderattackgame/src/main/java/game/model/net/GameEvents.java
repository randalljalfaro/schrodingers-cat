package game.model.net;

import java.util.ArrayList;

import bohr.engine.admin.UserRequest;
import game.model.entities.others.GameRequest;
import game.model.entities.others.GameResource;
import bohr.engine.admin.GeoPoint;
import game.model.entities.clan.Clan;
import game.model.entities.others.Message;
import bohr.engine.net.protocol.DataRequest;
import bohr.engine.net.protocol.CommunicationProtocol;

/**
 * Clase encargado de los eventos generales del juego
 * @author Isaac Trejos
 * @author Luis Murillo
 * @author Kevin Arce
 * @author Randall Alfaro
 */
public class GameEvents {
    /**
     *Envia mensajes al servidor
     * @param user ID del usuario
     * @param clan ID del clan
     * @param message Mensaje que se decea enviar
     * @return la peticion del mensaje
     */
    public static DataRequest<Message> sendMessage(int user, int clan, String message) {
        return new DataRequest(
                CommunicationProtocol.GAME_MESSAGING_SERVICE, CommunicationProtocol.SEND,
                new Message(-1, user, clan, message));
    }

    /**
     * Leer todos los mensajer del servidor
     * @param clan ID de clan
     * @return el mensaje
     */
    public static DataRequest<Message> getAllMessaging(int clan) {
        return new DataRequest(
                CommunicationProtocol.GAME_MESSAGING_SERVICE, CommunicationProtocol.GET_ALL,
                new Message(-1, -1, clan, ""));
    }

    /**
     * Acción de comprar un arma.
     * @param user ID del usuario
     * @param clan ID del clan
     * @param type Tipo de arma que se desea comprar
     * @return Retorna la acción
     */
    public static DataRequest<GameAction> buy(int user, int clan, int type) {
        return new DataRequest(
                CommunicationProtocol.GAME_ACTION_SERVICE, CommunicationProtocol.BUY,
                new GameAction(user, clan, -1, 1, false));
    }

    /**
     * Resive todas las acciones
     * @param clan ID del clan
     * @return La accion del juego
     */
    public static DataRequest<ArrayList<GameAction>> getAllActions(int clan) {
        return new DataRequest(
                CommunicationProtocol.GAME_ACTION_SERVICE, CommunicationProtocol.GET_ALL,
                new GameAction(-1, clan, -1, -1, false));
    }

    /**
     * Accion de votación del juego
     * @param idDecisionAction La ID del hilo de desiciones
     * @param idUser ID del user
     * @param accepted True o False dependiendo de la votación
     * @return la accion de la votación
     */
    public static DataRequest<GameResource> voteGameAction(int idDecisionAction,
                                                           int idUser, boolean accepted) {
        return new DataRequest(
                CommunicationProtocol.GAME_ACTION_SERVICE, CommunicationProtocol.RESPONSE,
                new GameAction(idUser, -1, idDecisionAction, -1, accepted));
    }

    /**
     * Obtiene todas las solicitudes hacia un usuarion
     * @param user ID del usuario
     * @return solicitudes
     */
    public static DataRequest<GameResource> getAllbyUserRequests(int user) {
        return new DataRequest(
                CommunicationProtocol.GAME_REQUESTS_SERVICE, CommunicationProtocol.GET_ALL_BY_USER,
                new GameRequest(-1, user, false, -1));
    }

    /**
     * Obtiene todas las solicitudes enviadas a un clan
     * @param clan ID del clan
     * @return la solicitud
     */
    public static DataRequest<GameResource> getAllbyClanRequests(int clan) {
        return new DataRequest(
                CommunicationProtocol.GAME_REQUESTS_SERVICE, CommunicationProtocol.GET_ALL_BY_CLAN,
                new GameRequest(clan, -1, false, -1));
    }

    /**
     * Enviar soliicitud de mensaje
     * @param clan ID del clan
     * @param user ID del usuario
     * @return La solicitud
     */
    public static DataRequest<GameResource> sendRequest(int clan, int user) {
        return new DataRequest(
                CommunicationProtocol.GAME_REQUESTS_SERVICE, CommunicationProtocol.SEND,
                new GameRequest(clan, user, false, -1));
    }

    /**
     * Responde a la solicitud enresivida
     * @param request ID de la solicitud
     * @param accepted boolean True  o flase
     * @return retorna la solicitud
     */
    public static DataRequest<GameResource> responseRequest(int request, boolean accepted) {
        return new DataRequest(
                CommunicationProtocol.GAME_REQUESTS_SERVICE, CommunicationProtocol.RESPONSE,
                new GameRequest(-1, -1, accepted, request));
    }

    /**
     * Accion de entrar al clan
     * @param username Nombre del usuario
     * @param pass pase oo contraseña para acceder
     * @return Solicitud
     */
    public static DataRequest<UserRequest> signUp(String username, String pass) {
        return new DataRequest(
                CommunicationProtocol.USER_SERVICE, CommunicationProtocol.SIGN_UP,
                new UserRequest(-1, username, pass));
    }

    /**
     * Obtiene toda la lista de usuarios
     * @return solicitudes
     */
    public static DataRequest<UserRequest> getAllUsers() {
        return new DataRequest(
                CommunicationProtocol.USER_SERVICE, CommunicationProtocol.GET_ALL,
                new UserRequest());
    }

    /**
     * Entrar a la cuenta del usuario
     * @param username nombre del usuario
     * @param pass contraseña del usuario
     * @return Solicitud
     */
    public static DataRequest<UserRequest> login(String username, String pass) {
        return new DataRequest(
                CommunicationProtocol.USER_SERVICE, CommunicationProtocol.LOGIN,
                new UserRequest(-1, username, pass));
    }

    /**
     * Accion para crear un clan
     * @param type Tipo de reliquia
     * @param clanName Nombre del clan
     * @param owner Lider del clan
     * @param geoPoint Localización del clan
     * @return el clan
     */
    public static DataRequest<Clan> createClan(int type, String clanName,
                                               int owner, GeoPoint geoPoint) {
        return new DataRequest(
                CommunicationProtocol.CLAN_SERVICE, CommunicationProtocol.CREATE,
                new Clan(0, type, clanName, owner, geoPoint));
    }

    /**
     * Accion para obtener toda la lista de clanes
     * @return clanes
     */
    public static DataRequest<Clan> getAllClans() {
        return new DataRequest(
                CommunicationProtocol.CLAN_SERVICE, CommunicationProtocol.GET_ALL,
                new Clan());
    }

    /**
     * Acción para obtener un clan en especifico
     * @param name nombre del clan
     * @return clan
     */
    public static DataRequest<Clan> getClan(String name) {
        return new DataRequest(
                CommunicationProtocol.CLAN_SERVICE, CommunicationProtocol.GET,
                new Clan(-1, -1, name, -1, new GeoPoint()));
    }

    /**
     * Acción para obtener toda los recursos
     * @param geoPoint puntos de los recurso
     * @return el recurso
     */
    public static DataRequest<GameResourceRequest> getAllResources(GeoPoint geoPoint) {
        return new DataRequest(
                CommunicationProtocol.GAME_RESOURCE_SERVICE, CommunicationProtocol.GET_ALL,
                new GameResourceRequest(
                        new GameResource(geoPoint), -1, new GeoPoint()
                )
        );
    }

    /**
     * Accion para colectar recurso
     * @param idResource ID del recurso
     * @param idUser ID del usuario
     * @param userLocation Localización del recurso
     * @return recurso
     */
    public static DataRequest<GameResourceRequest> collect(
            int idResource, int idUser, GeoPoint userLocation) {
        return new DataRequest(
                CommunicationProtocol.GAME_RESOURCE_SERVICE, CommunicationProtocol.COLLECT,
                new GameResourceRequest(
                        new GameResource(idResource), idUser, userLocation
                )
        );
    }

}
