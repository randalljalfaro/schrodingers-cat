package game.model.net;

/**
 * Clase encargada de la acción del juego
 * @author Isaac Trejos
 * @author Luis Murillo
 * @author Kevin Arce
 * @author Randall Alfaro
 */
public class GameAction {
    int idUser;
    int idClan;
    int idDecisionAction;
    int type;
    boolean response;

    /**
     * Constructor de GamaAction
     * @param idUser
     * @param idClan
     * @param idDecisionAction
     * @param type
     * @param response
     */
    public GameAction(int idUser, int idClan, int idDecisionAction, int type, boolean response) {
        this.idUser = idUser;
        this.idClan = idClan;
        this.idDecisionAction = idDecisionAction;
        this.type = type;
        this.response = response;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public int getIdClan() {
        return idClan;
    }

    public void setIdClan(int idClan) {
        this.idClan = idClan;
    }

    public int getIdDecisionAction() {
        return idDecisionAction;
    }

    public void setIdDecisionAction(int idDecisionAction) {
        this.idDecisionAction = idDecisionAction;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public boolean isResponse() {
        return response;
    }

    public void setResponse(boolean response) {
        this.response = response;
    }
}
