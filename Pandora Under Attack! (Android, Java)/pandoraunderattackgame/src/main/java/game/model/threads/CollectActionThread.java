package game.model.threads;


import java.util.ArrayList;

import bohr.engine.admin.User;
import bohr.engine.util.Util;
import game.model.entities.clan.Clan;
import game.model.entities.clan.ClanMember;
import game.model.entities.others.GameResource;
import game.model.entities.others.MostQuantity;

/**
 * Clase que contiene los datos de un hilo encargada de la recolección de recursos
 *
 * @author Isaac Trejos
 * @author Luis Murillo
 * @author Kevin Arce
 * @author Randall Alfaro
 */
public class CollectActionThread {
    private Thread collectThread;
    private int time;
    //private CollectionActionState
    private GameResource resource;
    private ArrayList<User> usersCollecting;
    private ArrayList<Clan> clans;
    private ArrayList<MostQuantity> membersClanCollecting;
    private double radioRange;
    private int collectQuantity;

    /**
     * Constructor
     *
     * @param resource   Recursos
     * @param clans      Lista de clanes
     * @param radioRange Radio maximo para la recoleción de recursos
     */
    public CollectActionThread(final GameResource resource, final ArrayList<Clan> clans, double radioRange) {
        this.radioRange = radioRange;
        usersCollecting = new ArrayList<>();
        membersClanCollecting = new ArrayList<>();
        this.resource = resource;
        this.clans = clans;
        time = 5000;
        collectQuantity = 15;
        collectThread = new Thread() {
            @Override
            public void run() {
                while (usersCollecting.size() > 0 && resource.getQuantity() >= collectQuantity) {
                    /*  cleanUsersByDistance();
                    most();
                    updateRange();
                  if (membersClanCollecting.size() >= 2 && twoUpper()[0][0] > 2 * twoUpper()[1][0]) {//2*twoUpper()[1][0] o 2+twoUpper()[1][0]
                        distributeResourceByClan(twoUpper()[0][1]);
                        distributeResourceByUser(15/twoUpper()[0][0]);
                    }
                    else if (membersClanCollecting.size() ==1){
                        distributeResourceByClan(membersClanCollecting.get(0).getIdClan());
                        distributeResourceByUser(15 / membersClanCollecting.get(0).getQuantity());
                    }*/
                    for(Clan clan:clans){
                        clan.updateResources(resource.getType(), 15);
                    }
                    sleep();
                }
            }

            /**
             * Metodo para dormir el hilo
             */
            private void sleep() {
                try {
                    sleep(time);
                } catch (InterruptedException e) {
                }
            }
        };
    }

    /**
     * Metodo que actualiza la posición del miembro respecto al recurso
     */
    private void updateRange() {
        for (Clan c : clans) {
            for (ClanMember cM : c.getMembers()) {
                if (cM.getResourcesCount() > 300) {
                    cM.setRange(3);
                }
                if (cM.getResourcesCount() > 700) {
                    cM.setRange(2);
                }
            }
        }
    }

    /**
     * Metodo para la distribución de recursos a cada usuario
     *
     * @param quantity Cantidad de recurso que le entra al miembro
     */
    private void distributeResourceByUser(int quantity) {
        for (User uC : usersCollecting) {
            for (Clan c : clans) {
                for (ClanMember cM : c.getMembers()) {
                    if (uC.getId() == cM.getIdUser()) {
                        cM.setResourcesCount(cM.getResourcesCount() + quantity);
                    }
                }
            }
        }
    }

    /**
     * Metodo que distribuyen la cantidad de recusrsos a cada clan
     *
     * @param idClan ID del clan
     */
    private void distributeResourceByClan(int idClan) {
        for (Clan c : clans) {
            if (c.getId() == idClan) {
                resource.setQuantity(resource.getQuantity() - collectQuantity);
                c.updateResources(resource.getType(), collectQuantity);
            }
        }
    }

    /**
     * Metodo que restringe el radio maximo para la recoleción de recursos para el usuario
     */
    private void cleanUsersByDistance() {
        for (User user : usersCollecting) {
            if (!user.getSocket().isConnected() &&
                    !Util.harveline(user.getGeoPoint(), resource.getGeoPoint(), radioRange)) {
                usersCollecting.remove(user);
            }
        }
    }

    /**
     * Metodo que crea una lista de los usuarios que recolectan por clan para encontrar lla mayoria de miembros
     */
    private void most() {
        for (User uC : usersCollecting) {
            for (Clan c : clans) {
                for (ClanMember cM : c.getMembers()) {
                    if (cM.getIdUser() == uC.getId()) {
                        for (MostQuantity mQ : membersClanCollecting) {
                            if (c.getId() == mQ.getIdClan()) {
                                mQ.setQuantity(mQ.getQuantity() + 1);
                            } else {
                                membersClanCollecting.add(new MostQuantity(c.getId(), 1));
                            }
                        }
                    }
                }

            }
        }
    }

    /**
     * Metodo que saca los dos clanes que se encuentran mayor en la lista para comparar.
     *
     * @return El clan mayor
     */
    private int[][] twoUpper() {
        int[][] upper = new int[2][2];
        upper[0] = new int[]{0, 0};
        upper[1] = new int[]{0, 0};
        for (MostQuantity mQ : membersClanCollecting) {
            if (mQ.getQuantity() > upper[0][0]) {
                upper[1] = upper[0];
                upper[0] = new int[]{mQ.getQuantity(), mQ.getIdClan()};
            } else if (mQ.getQuantity() > upper[1][0]) {
                upper[1] = new int[]{mQ.getQuantity(), mQ.getIdClan()};
            }
        }
        return upper;
    }

    public Thread getCollectThread() {
        return collectThread;
    }

    public GameResource getResource() {
        return resource;
    }

    public void setResource(GameResource resource) {
        this.resource = resource;
    }

    public ArrayList<User> getUsersCollecting() {
        return usersCollecting;
    }

    public void setUsersCollecting(ArrayList<User> usersCollecting) {
        this.usersCollecting = usersCollecting;
    }

    public ArrayList<Clan> getClans() {
        return clans;
    }

    public void setClans(ArrayList<Clan> clans) {
        this.clans = clans;
    }
}


