package game.model.threads.decisionAction;

import java.util.ArrayList;
import java.util.Properties;

import bohr.engine.util.Util;
import bohr.engine.util.observable.Observable;
import game.model.entities.others.Vote;
import game.model.entities.clan.Clan;
import game.model.entities.clan.ClanMember;

/**
 * Clase que definne al clan, los rangos que puede obtener cada miembro del clan.
 * Las votaciones de los miebros respecto a su rango
 * @author Isaac Trejos
 * @author Luis Murillo
 * @author Kevin Arce
 * @author Randall Alfaro

 */
public abstract class DecisionActionThread extends Observable {
    private Thread decisionThread;
    private DecisionActionState state;
    private Clan clan;
    private ArrayList<Vote> votes;
    private int liderVote;
    private int coliderVote;
    private int puebloVote;
    private int esclavoVote;

    /**
     * Constructor
     * @param clan El clan en el cual se realiza la votacion
     * @param idThread Numero del hilo
     * @param type para que se esta realizando la votacion, para que acción.
     * @param liderVot Voto del lider
     * @param coliderVot Voto del colider
     * @param puebloVot Voto del pueblo
     * @param esclavoVot Voto del esclavo
     * @param seconds Tiempo de la votación
     */
    public DecisionActionThread(final Clan clan, int idThread, final int type, int liderVot, int coliderVot, int puebloVot, int esclavoVot, int seconds) {
        this.clan = clan;
        state = new DecisionActionState(clan.getId(), idThread, type, seconds);
        votes = new ArrayList<>();
        this.liderVote = liderVot;
        this.coliderVote = coliderVot;
        this.puebloVote = puebloVot;
        this.esclavoVote = esclavoVot;
        decisionThread = new Thread() {
            @Override
            public void run() {
                startCollectAction();
            }

            /**
             * Inicio de las votaciones de un clan.
             * Dependiendo de la cantidad de votos, en base al rango del que tiene cada miebro del clan, para al final tomar la decisión
             */
            private void startCollectAction() {
                while (state.getTime() < state.getSeconds()) {
                    try {
                        sleep(1000);
                        state.setTime(state.getTime() + 1);
                    } catch (InterruptedException e) {
                        System.out.println(e);
                    }
                }
                int pos = 0, neg = 0;
                for (Vote v : votes) {
                    if (v.getResponse()) {
                        for (ClanMember cM : clan.getMembers()) {
                            if (cM.getIdUser() == v.getUserId()) {
                                if (cM.getRange() == 1)
                                    pos += liderVote;
                                if (cM.getRange() == 2)
                                    pos += coliderVote;
                                if (cM.getRange() == 3)
                                    pos += puebloVote;
                                if (cM.getRange() == 4)
                                    pos += esclavoVote;

                            }

                        }
                    } else {
                        for (ClanMember cM : clan.getMembers()) {
                            if (cM.getIdUser() == v.getUserId()) {
                                if (cM.getRange() == 1)
                                    neg += liderVote;
                                if (cM.getRange() == 2)
                                    neg += coliderVote;
                                if (cM.getRange() == 3)
                                    neg += puebloVote;
                                if (cM.getRange() == 4)
                                    neg += esclavoVote;

                            }
                        }
                    }
                }
                neg += Math.abs(votes.size() - clan.getMembers().size());
                System.out.println("Votos ++= " + pos + "--" + "Votos --= " + neg);
                if (pos >= neg)
                    execAction(type);
            }
        };
        decisionThread.start();
    }

    protected abstract void execAction(int type);

    public ArrayList<Vote> getVotes() {
        return votes;
    }

    public void setVotes(ArrayList<Vote> votes) {
        this.votes = votes;
    }

    public DecisionActionState getState() {
        return state;
    }

    public void setState(DecisionActionState state) {
        this.state = state;
    }

    public Thread getDecisionThread() {
        return decisionThread;
    }

    public void setDecisionThread(Thread decisionThread) {
        this.decisionThread = decisionThread;
    }
}
