package game.model.threads.attackAction;


import java.util.ArrayList;

import bohr.engine.admin.User;
import game.model.entities.clan.Clan;
import game.model.entities.others.GameResource;
import game.model.entities.others.MostQuantity;

/**
 * Clase que contiene los datos de un hilo encargada de la recolección de recursos
 *
 * @author Isaac Trejos
 * @author Luis Murillo
 * @author Kevin Arce
 * @author Randall Alfaro
 */
public class AttackActionThread {
    private AttackActionState state;
    private Thread attackThread;
    private ArrayList<User> usersInRange;
    private ArrayList<Clan> clans;

    /**
     * Constructor
     *
     * @param clans      Lista de clanes
     * @param radioRange Radio maximo para la recoleción de recursos
     */
    public AttackActionThread(int idClanA, int idClanB, final ArrayList<Clan> clans, double radioRange) {
        state.setIdClanA(idClanA);
        state.setIdClanB(idClanB);
        usersInRange = new ArrayList<>();
        this.clans = clans;
        attackThread = new Thread() {
            @Override
            public void run() {
                while (state.getSeconds()<state.getTime()) {
                    //REVISAR LOS QUE ESTAN EN EL RANGO Y PREGUNTARLES A  CUAL CLAN APOYAN
                    sleep();
                }
                //AQUI DEBE INICIAR OTRO HILO CON TODA LA LOGICA QUE DURA 3 MINUTOS
            }

            /**
             * Metodo para destruir el hilo
             */
            private void sleep() {
                try {
                    sleep(1000);
                    state.setSeconds(state.getSeconds()+1);
                } catch (InterruptedException e) {
                }
            }
        };
    }
}