package game.model.threads;

import java.util.Properties;
import java.util.Random;

import bohr.engine.game.GameState;
import game.model.main.ServerGame;
import bohr.engine.admin.GeoPoint;

/**
 *Clase que controla los hilos del juego para la creacion de recursos en el mapa
 *
 * @author Isaac Trejos
 * @author Luis Murillo
 * @author Kevin Arce
 * @author Randall Alfaro
 */
public class ServerGameThread extends Thread {
    private ServerGame game;
    private int add;
    private GameState state;
    private int timer;
    private int amount;

    /**
     * Constructor
     * @param game
     */
    public ServerGameThread(ServerGame game) {
        this.game = game;
        add = 1;
        timer = 0;
        amount = 0;
        state = game.getGameState();
    }

    /**
     * Inicia el hilo encargado de la creacion de recursos
     */
    public void run() {
        while (!state.isFinished()) {
            if (timer % add == 0 && amount < 200) {
                GeoPoint geoPoint;
                //Máximo y mínimo deben venir de un java properties
                if (new Random().nextInt(100) < 35)
                    geoPoint = new GeoPoint(point(9, 10), -1 * point(83, 85));
                else
                    geoPoint = new GeoPoint(point(10, 10), -1 * point(84, 84));
                synchronized (game.getResources()) {
                    game.newResourse(geoPoint);
                }
                amount++;
            }
           /* if (amount == 99)
                System.out.println("*****" + game.getResources().size());*/
            sleep();
        }
    }

    //puntos random donde apareceran los recursos

    /**
     * Puntos random en donde aparecen los recursos
     * @param min minimo punto en el mapa
     * @param max maximo punto en el mapa
     * @return los puntos donde se crearan los recursos
     */
    private double point(int min, int max) {
        double intPart = new Random().nextInt((max - min) + 1) + min;
        double floatPart = new Random().nextInt(99999) / 100000.0;
        double point = intPart + floatPart;
        return point;
    }

    //metodo para contar los segundos y dormir el hilo

    /**
     * Metodo encargado de dormir y distruir los hilos
     */
    private void sleep() {
        try {
            super.sleep(1000);
            timer++;
            state.incSeconds();
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
    }
}
