package game.model.threads.decisionAction;

import java.util.Properties;

import bohr.engine.util.Util;
import game.model.entities.clan.Clan;

/**
 * Clase encargada de el estado de las votaciones para la
 * @author Isaac Trejos
 * @author Luis Murillo
 * @author Kevin Arce
 * @author Randall Alfaro
 */
public class DecisionActionState {
    private int clanId;
    private int type;
    private int actionId;
    private int time;
    private int seconds;

    public DecisionActionState(int clanId, int actionId, int time, int seconds, int type) {
        this.clanId = clanId;
        this.actionId = actionId;
        this.time = time;
        this.seconds = seconds;
        this.type = type;
    }

    /**
     * Constructor
     * @param clanId ID del clan
     * @param actionId la ID de la acción de la votación
     * @param type Tipo de la votación
     * @param seconds Tiempo que dura la votación
     */
    public DecisionActionState(int clanId, int actionId, int type, int seconds) {
        this.clanId = clanId;
        this.actionId = actionId;
        this.time = 0;
        this.seconds = seconds;
        this.type = type;
    }

    public DecisionActionState() {
        this.clanId = -1;
        this.actionId = -1;
        this.time = 0;
        this.seconds = 120;
        this.type = -1;
    }

    /**
     * Propiedades utilizadas en el app
     * @param properties propiedad
     */
    public void setProperties(Properties properties) {
        String strTimeToVote = properties.getProperty("timeToVote");
        if (strTimeToVote != null && Util.isInteger(strTimeToVote)) {
            setTimeToVote(Integer.valueOf(strTimeToVote));
        }
    }

    public void setTimeToVote(int time) {
        this.seconds = time;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getClanId() {
        return clanId;
    }

    public void setClanId(int clanId) {
        this.clanId = clanId;
    }

    public int getActionId() {
        return actionId;
    }

    public void setActionId(int actionId) {
        this.actionId = actionId;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public int getSeconds() {
        return seconds;
    }

    public void setSeconds(int seconds) {
        this.seconds = seconds;
    }
}
