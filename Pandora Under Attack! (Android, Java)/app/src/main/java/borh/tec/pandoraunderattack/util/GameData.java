package borh.tec.pandoraunderattack.util;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

import borh.tec.pandoraunderattack.R;
import game.model.entities.others.GameResource;
import bohr.engine.admin.GeoPoint;
import game.model.main.ClientGame;
import game.model.net.GameEvents;
import bohr.engine.net.protocol.DataResponse;

/**
 * Created by randall on 22/09/2015.
 */
public class GameData {
    public static void loadResourcesMarkers(ClientGame clientGame, GoogleMap mMap) {
        DataResponse<ArrayList<GameResource>> response = new Gson().fromJson(
                clientGame.getNetwork().
                        sendEvent(new Gson().toJson(
                                GameEvents.getAllResources(new GeoPoint()))),
                new TypeToken<
                        DataResponse<ArrayList<GameResource>>>() {
                }.getType());
        for (GameResource gr : response.getData()) {
            //String details = "Quantity: " + gr.getCost() + "\n";
            MarkerOptions marker = new MarkerOptions().position(
                    new LatLng(gr.getGeoPoint().getLatitude(),
                            gr.getGeoPoint().getLongitude())
            );
            marker.title(String.valueOf(gr.getId()));
            if (gr.getType() == 0) {
                marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.evil_eye));
            } else if (gr.getType() == 1) {
                marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.armor));
            } else if (gr.getType() == 2) {
                marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.unicorn));
            }
            mMap.addMarker(marker);
        }
        clientGame.setResources(response.getData());
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                new LatLng(10, -84), 12.0f));
    }
}
