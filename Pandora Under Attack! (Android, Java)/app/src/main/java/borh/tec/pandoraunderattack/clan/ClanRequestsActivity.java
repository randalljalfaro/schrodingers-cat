package borh.tec.pandoraunderattack.clan;

import android.app.ListActivity;
import android.content.Context;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

import borh.tec.pandoraunderattack.control.GameAPP;
import borh.tec.pandoraunderattack.R;
import game.model.main.ClientGame;
import game.model.net.GameEvents;
import game.model.entities.others.GameRequest;
import bohr.engine.net.protocol.DataResponse;

public class ClanRequestsActivity extends ListActivity {
    private ClientGame clientGame;

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        clientGame = ((GameAPP) getApplicationContext()).getClientGame();
        DataResponse<ArrayList<GameRequest>> response = new Gson().fromJson(
                clientGame.getNetwork().
                        sendEvent(new Gson().toJson(
                                GameEvents.getAllbyClanRequests(
                                        clientGame.getClan().getId()
                                ))),
                new TypeToken<
                        DataResponse<ArrayList<GameRequest>>>() {
                }.getType());
        /*GameRequest[] gRArray = new GameRequest[response.getData().size()];
        gRArray = response.getData().toArray(gRArray);*/
        GameRequestArrayAdapter adapter = new GameRequestArrayAdapter(this,
                response.getData());
        setListAdapter(adapter);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        GameRequest item = (GameRequest) getListAdapter().getItem(position);
        Toast.makeText(this, item.getIdRequest() + " selected", Toast.LENGTH_LONG).show();
    }

    public class GameRequestArrayAdapter extends ArrayAdapter<GameRequest> {
        private final Context context;
        private final ArrayList<GameRequest> values;

        public GameRequestArrayAdapter(Context context, ArrayList<GameRequest> values) {
            super(context, -1, values);
            this.context = context;
            this.values = values;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final GameRequest gameRequest = (GameRequest) getListAdapter().getItem(position);
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View rowView = inflater.inflate(R.layout.layout_clan_request, parent, false);
            TextView textView = (TextView) rowView.findViewById(R.id.txtUsername);
            Button btnAccept = (Button) rowView.findViewById(R.id.btnAccept);
            Button btnDecline = (Button) rowView.findViewById(R.id.btnDecline);
            textView.setText(String.valueOf(gameRequest.getIdRequest()));
            btnAccept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    requestClicked(rowView, true,
                            gameRequest.getIdRequest(), position);
                }
            });
            btnDecline.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    requestClicked(rowView, false,
                            gameRequest.getIdRequest(), position);
                }
            });
            return rowView;
        }

        private void requestClicked(final View rowView, boolean response,
                                    int responseId, final int position) {
            clientGame.getNetwork()
                    .sendEvent(
                            new Gson().toJson(
                                    GameEvents.responseRequest(
                                            responseId, response
                                    )));
            AlphaAnimation anim = new AlphaAnimation(1.0f, 0.0f);
            final GameRequestArrayAdapter _this = this;
            anim.setDuration(1000);
            anim.setRepeatCount(0);
            anim.setRepeatMode(Animation.REVERSE);
            anim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    rowView.setVisibility(View.GONE);
                    values.remove(position);
                    _this.notifyDataSetChanged();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            });
            rowView.startAnimation(anim);
        }
    }
    /*
        Toast.makeText(getApplicationContext(),
                "nbk"),
                Toast.LENGTH_LONG).show();
    }*/


}
