package borh.tec.pandoraunderattack.control;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import bohr.engine.admin.UserRequest;
import borh.tec.pandoraunderattack.R;
import borh.tec.pandoraunderattack.otros.Message;
import game.model.main.ClientGame;
import game.model.net.GameEvents;
import bohr.engine.admin.User;
import bohr.engine.net.protocol.DataResponse;
import bohr.engine.net.protocol.CommunicationProtocol;


public class LoginActivity extends AppCompatActivity {
    private ClientGame clientGame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initPolicy();
        clientGame = ((GameAPP) getApplicationContext()).getClientGame();
        clientGame.getEngineState().setServerHost("186.15.173.80");
        clientGame.getNetwork().openConnection();
        setContentView(R.layout.activity_login);
        clickListeners();
    }

    private void initPolicy() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    private void clickListeners() {
        Button btnLogin = (Button) findViewById(R.id.btnLogin);
        final Context context = this;
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (login()) {
                    goToMapsActivity();
                } else {
                    Message.showMessage("Username and/or incorrect password", context);
                }
            }
        });
        Button signIn = (Button) findViewById(R.id.btnSignIn);
        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (signIn()) {
                    Message.showMessage("User created", context);
                } else {
                    Message.showMessage("Username is been used by other user", context);
                }
            }
        });
    }


    private void goToMapsActivity() {
        startActivity(
                new Intent(LoginActivity.this, MapsActivity.class)
        );
    }

    /**
     * Metodo encargado de verificar que los datos para ingresar al sistema son correctos,
     * si son correctos agrega un objeto usuario al objeto ClientGame.EngineState
     *
     * @return True si los datos son correcto, False si no son correctos
     */
    private boolean login() {
        final TextView txtUsername = (TextView) findViewById(R.id.txtUsername);
        final TextView txtPassword = (TextView) findViewById(R.id.txtPassword);
        DataResponse<UserRequest> response =
                new Gson().fromJson(
                        clientGame.getNetwork().
                                sendEvent(new Gson().toJson(
                                        GameEvents.login(txtUsername.getText().toString(),
                                                txtPassword.getText().toString())
                                )),
                        new TypeToken<DataResponse<UserRequest>>() {
                        }.getType());
        if (response.getCode() == CommunicationProtocol.NOT_FOUND)
            return false;
        clientGame.getEngineState().setUser(
                new User(response.getData().getID(), null,
                        response.getData().getUsername(),
                        response.getData().getPassword())
        );
        return true;
    }

    /**
     * Metodo encargado de verificar que los datos para registrarse al sistema sean válidos y únicos
     *
     * @return True si el registro fue exitoso, False si no fue exitoso
     */
    private boolean signIn() {
        final TextView txtUsername = (TextView) findViewById(R.id.txtUsername);
        final TextView txtPassword = (TextView) findViewById(R.id.txtPassword);
        DataResponse<UserRequest> response =
                new Gson().fromJson(
                        clientGame.getNetwork().
                                sendEvent(new Gson().toJson(
                                        GameEvents.signUp(txtUsername.getText().toString(),
                                                txtPassword.getText().toString())
                                )),
                        new TypeToken<DataResponse<UserRequest>>() {
                        }.getType());
        if (response.getCode() == CommunicationProtocol.ALREADY_CREATED)
            return false;
        return true;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
