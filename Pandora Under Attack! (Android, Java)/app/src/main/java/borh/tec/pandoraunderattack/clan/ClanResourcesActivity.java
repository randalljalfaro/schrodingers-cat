package borh.tec.pandoraunderattack.clan;

import android.app.ListActivity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

import borh.tec.pandoraunderattack.control.GameAPP;
import borh.tec.pandoraunderattack.R;
import game.model.main.ClientGame;
import game.model.net.GameEvents;
import game.model.entities.others.GameResource;
import game.model.entities.clan.Clan;
import bohr.engine.net.protocol.DataResponse;

public class ClanResourcesActivity extends ListActivity {
    static boolean active;
    private ClientGame clientGame;
    private Thread updatingThread;

    @Override
    public void onStop() {
        super.onStop();
        active = false;
    }

    @Override
    public void onCreate(Bundle icicle) {
        active = true;
        super.onCreate(icicle);
        clientGame = ((GameAPP) getApplicationContext()).getClientGame();
        updateModelAdapter();
        initUpdateThread();
    }

    private void initUpdateThread() {
        updatingThread = new Thread() {
            @Override
            public void run() {
                while (active) {
                    synchronized (clientGame.getClan().getResources()) {
                        updateResourcesBindData();
                    }
                    try {
                        sleep(3000);
                    } catch (InterruptedException e) {
                    }
                }
            }
        };
        updatingThread.start();
    }

    private DataResponse<Clan> getUpdateResource() {
        return new Gson().fromJson(
                clientGame.getNetwork().
                        sendEvent(new Gson().toJson(
                                GameEvents.getClan(
                                        clientGame.getClan().getClanName()
                                ))),
                new TypeToken<DataResponse<Clan>>() {
                }.getType());
    }

    private void updateResourcesBindData() {
        DataResponse<Clan> response = getUpdateResource();
        if (response.getData() != null) {
            clientGame.getClan().setResources(response.getData().getResources());
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateModelAdapter();
                }
            });
        }
    }

    private void updateModelAdapter() {
        final GameRequestArrayAdapter adapter = new GameRequestArrayAdapter(this,
                clientGame.getClan().getResources());
        setListAdapter(adapter);

    }

    public class GameRequestArrayAdapter extends ArrayAdapter<GameResource> {
        private final Context context;

        public GameRequestArrayAdapter(Context context, ArrayList<GameResource> resources) {
            super(context, -1, resources);
            this.context = context;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final GameResource gameResource = (GameResource) getListAdapter().getItem(position);
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View rowView = inflater.inflate(R.layout.layout_clan_resource, parent, false);
            TextView textView = (TextView) rowView.findViewById(R.id.txtResourceDetails);
            ImageView imgView = (ImageView) rowView.findViewById(R.id.imgResource);
            if (gameResource.getType() == 0) {
                textView.setText(
                        "EVIL EYE: \n" + R.string.resource_evil_eye_detail + "\n" +
                                "Quantity available: " + gameResource.getQuantity()
                );
                imgView.setImageResource(R.drawable.evil_eye_copia);
            } else if (gameResource.getType() == 1) {
                textView.setText(
                        "ARMOR: \n" + R.string.resource_armor_detail + "\n" +
                                "Quantity available: " + gameResource.getQuantity());
                imgView.setImageResource(R.drawable.armor_copia);
            } else if (gameResource.getType() == 2) {
                textView.setText(
                        "UNICORN: \n" + R.string.resource_armor_detail + "\n" +
                                "Quantity available: " + gameResource.getQuantity());
                imgView.setImageResource(R.drawable.unicorn_copia);
            }
            return rowView;
        }

    }

}
