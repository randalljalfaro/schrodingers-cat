package borh.tec.pandoraunderattack.util;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.ScrollView;
import android.widget.TextView;

import borh.tec.pandoraunderattack.R;
import game.model.entities.clan.Clan;
import game.model.entities.others.GameResource;

/**
 * Created by randall on 22/09/2015.
 */
public class GameView {
    public static View getResourcesWindow(GameResource gameResource, LayoutInflater layoutInflater) {
        View resourceInfoView = layoutInflater.inflate(R.layout.layout_map_resource, null);
        TextView textView = (TextView) resourceInfoView.findViewById(R.id.txtResourceDetails);
        ImageView imgView = (ImageView) resourceInfoView.findViewById(R.id.imgResource);
        if (gameResource.getType() == 0) {
            textView.setText(
                    "EVIL EYE: \n" + "There is no sympathy for the devil.\n" +
                            "Quantity available: " + gameResource.getQuantity()
            );
            imgView.setImageResource(R.drawable.evil_eye_copia);
        } else if (gameResource.getType() == 1) {
            textView.setText(
                    "ARMOR: \n" + "Without an army you will lose any battle.\n" +
                            "Quantity available: " + gameResource.getQuantity());
            imgView.setImageResource(R.drawable.armor_copia);
        } else if (gameResource.getType() == 2) {
            textView.setText(
                    "UNICORN: \n" + "Be creative or die on the hands of the creative one`s.\n" + "\n" +
                            "Quantity available: " + gameResource.getQuantity());
            imgView.setImageResource(R.drawable.unicorn_copia);
        }
        return resourceInfoView;
    }

    public static PopupWindow getPopUpWindow(LayoutInflater layoutInflater,
                                             PopupWindow popupWindow,
                                             View parent, View content) {
        popupWindow.dismiss();
        //Window
        View pupupView = layoutInflater.inflate(
                R.layout.layout_scrollable, null);
        final PopupWindow newPopupWindow = new PopupWindow(pupupView);
        //Scroll
        ScrollView container = (ScrollView) pupupView.findViewById(R.id.container);
        //Adding content to container
        container.addView(content);
        //Evento para cerrar la ventana
        Button btnClose = (Button) pupupView.findViewById(R.id.btnClose);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                newPopupWindow.dismiss();
            }
        });
        newPopupWindow.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        newPopupWindow.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        newPopupWindow.setContentView(pupupView);
        newPopupWindow.showAsDropDown(parent);
        return newPopupWindow;
    }

    private static void showPopUp(PopupWindow pupPopupWindow, View parent, View pupupView) {
        //popUpResource.showAtLocation(parent, Gravity.NO_GRAVITY, 0, 0);
    }

    public static View getClanWindow(Clan clan, LayoutInflater layoutInflater) {
        View resourceInfoView = layoutInflater.inflate(R.layout.fragment_clan_menu, null);
        return resourceInfoView;
    }

    public static View getScrollabletWindow(LayoutInflater layoutInflater) {
        View resourceInfoView = layoutInflater.inflate(R.layout.layout_scrollable, null);
        return resourceInfoView;
    }
}
