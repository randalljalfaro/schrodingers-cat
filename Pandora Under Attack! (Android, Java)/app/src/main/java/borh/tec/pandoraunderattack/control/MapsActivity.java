package borh.tec.pandoraunderattack.control;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.content.res.Configuration;
import android.location.Location;
import android.location.LocationListener;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.PopupWindow;
import android.widget.ScrollView;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Marker;
import com.google.gson.Gson;

import java.io.Serializable;

import borh.tec.pandoraunderattack.R;
import borh.tec.pandoraunderattack.clan.ClanRegisterActivity;
import borh.tec.pandoraunderattack.clan.ClanRequestsActivity;
import borh.tec.pandoraunderattack.clan.ClanResourcesActivity;
import borh.tec.pandoraunderattack.util.GameData;
import borh.tec.pandoraunderattack.util.GameView;
import game.model.main.ClientGame;
import game.model.net.GameEvents;
import game.model.util.Search;
import game.model.entities.others.GameResource;
import bohr.engine.admin.GeoPoint;

public class MapsActivity extends FragmentActivity implements
        LocationListener, Serializable {

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private ClientGame clientGame;
    private PopupWindow popUpWindow;
    private View popUpView;
    private int markerType;
    private FrameLayout fragment_menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        markerType = 0;
        popUpView = null;
        popUpWindow = new PopupWindow(this);
        fragment_menu = (FrameLayout) findViewById(R.id.fragment_menu);
        clientGame = ((GameAPP) getApplicationContext()).getClientGame();
        initActivity();
    }

    private void initActivity() {
        setUpGameMenu();
        setUpMapIfNeeded();
        mapListeners();
    }

    ///*****************************************************
    //GAME MENU
    private void setUpGameMenu() {
        Fragment gameMenuFragment = new Fragment() {
            @Nullable
            @Override
            public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
                super.onCreateView(inflater, container, savedInstanceState);
                View view = inflater.inflate(R.layout.fragment_game_menu, container, false);
                container.removeAllViews();
                initListenersGameMenu(view);
                return view;
            }
        };
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment_menu, gameMenuFragment).commit();
    }

    private void initListenersGameMenu(View view) {
        Button btnClan = (Button) view.findViewById(R.id.btnClan);
        btnClan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (clientGame.getClan().getId() != -1)
                    setUpClanMenu();
                else startActivity(
                        new Intent(MapsActivity.this, ClanRegisterActivity.class));
            }
        });
    }

    ///*****************************************************
    //CLAN MENU
    private void setUpClanMenu() {
        Fragment clanMenuFragment = new Fragment() {
            @Nullable
            @Override
            public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
                super.onCreateView(inflater, container, savedInstanceState);
                container.removeAllViews();
                View view = inflater.inflate(R.layout.fragment_clan_menu, container, false);
                initListenersClanMenu(view);
                return view;
            }

        };
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment_menu, clanMenuFragment).commit();
    }

    private void initListenersClanMenu(final View view) {
        Button btnClanGameRequests = (Button) view.findViewById(R.id.btnClanGameRequests);
        btnClanGameRequests.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MapsActivity.this, ClanRequestsActivity.class));
            }
        });
        Button btnClanGameResources = (Button) view.findViewById(R.id.btnClanGameResources);
        btnClanGameResources.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MapsActivity.this, ClanResourcesActivity.class));
            }
        });
        Button btnClanChat = (Button) view.findViewById(R.id.btnClanChat);
        btnClanChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View content = getLayoutInflater().inflate(R.layout.layout_clan_chat, null);
                popUpWindow = GameView.getPopUpWindow(
                        getLayoutInflater(), popUpWindow, fragment_menu, content
                );
            }

        });
        Button btnClanShop = (Button) view.findViewById(R.id.btnClanShop);
        btnClanShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View content = getLayoutInflater().inflate(R.layout.layout_clan_shop, null);
                popUpWindow = GameView.getPopUpWindow(
                        getLayoutInflater(), popUpWindow, fragment_menu, content
                );
            }
        });
    }


    ///*****************************************************
    //MAP
    private void mapListeners() {
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                popUpWindow.dismiss();
                switch (markerType) {
                    case 0:
                        loadResourceWindow(marker);
                        break;
                }
                showPopUp();
                return true;
            }
        });
    }

    private void loadResourceWindow(Marker marker) {
        // Getting view from the layout file info_window_layout
        final GameResource gameResource =
                Search.getGameResourceById(Integer.valueOf(marker.getTitle()),
                        clientGame.getResources());
        View resourceInfoView = GameView.getResourcesWindow(gameResource,
                getLayoutInflater());
        Button btnCollect = (Button) resourceInfoView.findViewById(R.id.btnCollect);
        Button btnCancel = (Button) resourceInfoView.findViewById(R.id.btnCancel);
        btnCollect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clientGame.getClan().getId() == -1) {
                    Toast.makeText(getApplicationContext(),
                            "You should create or register to a clan in " +
                                    "order to collect any resource.",
                            Toast.LENGTH_LONG).show();
                } else {
                    clientGame.getNetwork().sendEvent(new Gson().toJson(
                            GameEvents.collect(
                                    gameResource.getId(),
                                    clientGame.getEngineState().getUser().getId(), new GeoPoint())));
                    popUpWindow.dismiss();
                }
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popUpWindow.dismiss();
            }
        });
        popUpView = resourceInfoView;
    }


    public void showPopUp() {
        popUpWindow = new PopupWindow(popUpView);
        popUpWindow.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        popUpWindow.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        popUpWindow.setContentView(popUpView);
        popUpWindow.showAsDropDown(fragment_menu);
        //popUpWindow.showAtLocation(parent, Gravity.NO_GRAVITY, 0, 0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        initActivity();
    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
                //confMap();
            }
        }
    }


    private void setUpMap() {
        mMap.clear();
        mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        switch (markerType) {
            case 0:
                GameData.loadResourcesMarkers(clientGame, mMap);
                break;
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);/*
        menuFragmentContainer.removeAllViews();
        switch (newConfig.orientation) {
            case Configuration.ORIENTATION_PORTRAIT:
                menuFragmentContent = getLayoutInflater()
                        .inflate(menuFragmentContentId, menuFragmentContainer.orie, true);
                break;
            case Configuration.ORIENTATION_LANDSCAPE:
                menuFragmentContent = getLayoutInflater()
                        .inflate(menuFragmentContentId, menuFragmentContainer, true);
                break;
        }*/
    }

    @Override
    public void onLocationChanged(Location location) {
        Toast.makeText(getApplicationContext(),
                "onLocationChanged: " + location.toString(),
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
        Toast.makeText(getApplicationContext(),
                "onStatusChanged: " + s,
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onProviderEnabled(String s) {
        Toast.makeText(getApplicationContext(),
                "onProviderEnabled: " + s, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onProviderDisabled(String s) {
        Toast.makeText(getApplicationContext(),
                "onProviderDisabled: " + s, Toast.LENGTH_LONG).show();
    }

}

