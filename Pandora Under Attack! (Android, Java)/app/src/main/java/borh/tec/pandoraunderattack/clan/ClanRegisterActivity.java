package borh.tec.pandoraunderattack.clan;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import bohr.engine.net.Network;
import borh.tec.pandoraunderattack.control.GameAPP;
import borh.tec.pandoraunderattack.R;
import game.model.main.ClientGame;
import game.model.net.GameEvents;
import bohr.engine.admin.GeoPoint;
import game.model.entities.clan.Clan;
import bohr.engine.net.protocol.DataResponse;
import bohr.engine.net.protocol.CommunicationProtocol;

public class ClanRegisterActivity extends AppCompatActivity implements LocationListener {
    private ClientGame clientGame;
    private Location loc;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loc = null;
        clientGame = ((GameAPP) getApplicationContext()).getClientGame();
        setContentView(R.layout.activity_clan_register);
        initActivity();
        clickListeners();
    }

    private void clickListeners() {
        Button btnCreate = (Button) findViewById(R.id.btnCreate);
        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (registerClan())
                    finish();
            }
        });
        Button btnCancel = (Button) findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    /**
     * Metodo encargado de verificar que los datos para ingresar un clan al sistema son correctos,
     * si son correctos agrega un objeto clan al objeto ClientGame
     *
     * @return True si los datos son correcto, False si no son correctos
     */
    private boolean registerClan() {
        final TextView txtUsername = (TextView) findViewById(R.id.txtClanName);
        final RadioButton rB = (RadioButton) findViewById(R.id.rBtnTypeB);
        final RadioButton rC = (RadioButton) findViewById(R.id.rBtnTypeC);
        int type = 0;
        if (rB.isChecked())
            type = 1;
        if (rC.isChecked())
            type = 2;
        String clanName = txtUsername.getText().toString();
        int owner = clientGame.getEngineState().getUser().getId();
        GeoPoint geoPoint = new GeoPoint(99, -99);
        DataResponse<Clan> response =
                new Gson().fromJson(
                        clientGame.getNetwork().
                                sendEvent(new Gson().toJson(
                                        GameEvents.createClan(
                                                type, clanName, owner, geoPoint
                                        )
                                )),
                        new TypeToken<DataResponse<Clan>>() {
                        }.getType());
        if (response.getCode() == CommunicationProtocol.ALREADY_CREATED)
            return false;
        clientGame.setClan(response.getData());
        pruebas(response);
        return true;
    }

    private void pruebas(DataResponse<Clan> response) {
        Network net = clientGame.getNetwork();
        net.sendEvent(new Gson().toJson(GameEvents.sendRequest(response.getData().getId(), 1)));
        net.sendEvent(new Gson().toJson(GameEvents.sendRequest(response.getData().getId(), 2)));
        net.sendEvent(new Gson().toJson(GameEvents.sendRequest(response.getData().getId(), 3)));
        net.sendEvent(new Gson().toJson(GameEvents.sendRequest(response.getData().getId(), 4)));
        net.sendEvent(new Gson().toJson(GameEvents.sendRequest(response.getData().getId(), 5)));
        net.sendEvent(new Gson().toJson(GameEvents.sendRequest(response.getData().getId(), 6)));
        net.sendEvent(new Gson().toJson(GameEvents.sendRequest(response.getData().getId(), 7)));
        net.sendEvent(new Gson().toJson(GameEvents.sendRequest(response.getData().getId(), 8)));
    }

    private void initActivity() {
        TextView txtLatitude = (TextView) findViewById(R.id.txtLatitude);
        TextView txtLongitude = (TextView) findViewById(R.id.txtLongitude);
        loc = getLocation();
        if (loc != null) {
            txtLatitude.setText(Double.valueOf(loc.getLatitude()).toString());
            txtLongitude.setText(Double.valueOf(loc.getLongitude()).toString());
        }
    }

    private Location getLocation() {
        LocationManager location = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Location loc = null;
        //Obtenemos la última posición conocida
        try {
            loc = location.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        } catch (SecurityException e) {

        }
        return loc;
    }


    @Override
    public void onLocationChanged(Location location) {
        TextView txtLatitude = (TextView) findViewById(R.id.txtLatitude);
        TextView txtLongitude = (TextView) findViewById(R.id.txtLongitude);
        if (location != null) {
            txtLatitude.setText(Double.valueOf(location.getLatitude()).toString());
            txtLongitude.setText(Double.valueOf(location.getLongitude()).toString());
        } else {
            txtLatitude.setText(Double.valueOf(-1).toString());
            txtLongitude.setText(Double.valueOf(0).toString());
        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}
