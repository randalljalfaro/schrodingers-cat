package model.dao;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Properties;

import bohr.engine.admin.User;
import bohr.engine.util.Util;
import game.model.entities.others.AttackResourse;
import game.model.entities.others.Vote;
import game.model.entities.clan.Clan;
import game.model.threads.attackAction.AttackActionState;
import game.model.threads.attackAction.AttackActionThread;
import game.model.threads.decisionAction.DecisionActionState;
import game.model.threads.decisionAction.DecisionActionThread;
import game.model.util.Search;
import model.dao.intefaces.ActionDAO;
import bohr.engine.net.protocol.DataResponse;
import bohr.engine.net.protocol.CommunicationProtocol;

/**
 * Clase que usa los metodos de la interfaz ActionDAO para asi encargarse de manejar las acciones posibles en el protocolo de las acciones
 *
 * @author Isaac Trejos
 * @author Luis Murillo
 * @author Kevin Arce
 * @author Randall Alfaro
 */
public class ActionDAOList implements ActionDAO {
    ArrayList<Clan> clans;
    ArrayList<User> users;
    ArrayList<DecisionActionThread> desicions;
    private int actionLastId;
    private int liderVote;
    private int coliderVote;
    private int puebloVote;
    private int esclavoVote;
    private int seconds;
    private int attacksInfo[][];

    /**
     * Constructor de la clase
     *
     * @param clans lista de los clanses
     * @param users lista de los usuarios
     */
    public ActionDAOList(ArrayList<Clan> clans, ArrayList<User> users) {
        actionLastId = 0;
        attacksInfo = new int[2][2];
        this.clans = clans;
        this.users = users;
        desicions = new ArrayList<>();
    }

    /**
     * Metodo encargado de comprar recursos de ataque
     *
     * @param idUser id del usuario
     * @param idClan id del clan.
     * @param type   tipo del recurso de ataque a comprar
     * @return respuesta del servidor; retorna la respuesta a la compra
     */
    @Override
    public DataResponse<DecisionActionState> buy(int idUser, int idClan, int type) {
        DataResponse<DecisionActionState> data =
                new DataResponse(new DecisionActionState());

        final AttackResourse aR = new AttackResourse(type, attacksInfo[type][0], attacksInfo[type][1]);
        final Clan clan = Search.getClanbyId(idClan, clans);
        User user = Search.getUserById(idUser, users);
        if (clan != null && user != null && aR.getCost() <= clan.quantityResourceByType(type)) {
            actionLastId++;
            DecisionActionThread d = new DecisionActionThread(clan, actionLastId, type, liderVote,
                    coliderVote, puebloVote, esclavoVote, seconds) {
                @Override
                protected void execAction(int type) {
                    if (type < 3) {
                        clan.getAttackResourses().add(aR);
                        clan.updateResources(aR.getType(), -1 * aR.getCost());
                    }
                    System.out.println("EJECUTANDO EL TIPO " + type);
                    System.out.println(new Gson().toJson(clan));
                }
            };
            desicions.add(d);
            data.setData(d.getState());
            return data;
        }
        data.setCode(CommunicationProtocol.NOT_FOUND);
        return data;
    }

    /**
     * Metodo encargado de la votacion de las acciones
     *
     * @param idUser   id del usuario
     * @param idAction id de la accion.
     * @param response tipo de respuesta.
     * @return respuesta del servidor.
     */
    @Override
    public DataResponse<DecisionActionState> vote(int idAction, int idUser, boolean response) {
        DataResponse<DecisionActionState> data =
                new DataResponse(new DecisionActionState());
        DecisionActionThread decisionAction =
                getDecisionActionThreadById(idAction);
        if (decisionAction != null) {
            Vote vote = getVoteByUserID(idUser, decisionAction.getVotes());
            if (vote == null) {
                decisionAction.getVotes().add(new Vote(idUser, response));
            } else {
                data.setCode(CommunicationProtocol.ALREADY_VOTED);
            }
            data.setData(decisionAction.getState());
        }
        return data;
    }

    /**
     * Metodo encargado de obtener los votos del clan
     *
     * @param idClan id del clan.
     * @return respuesta del servidor; retorna la lista de los votos.
     */
    @Override
    public DataResponse<ArrayList<DecisionActionState>> getAll(int idClan) {
        DataResponse<ArrayList<DecisionActionState>> data =
                new DataResponse(new ArrayList<>());
        for (DecisionActionThread d : desicions) {
            data.getData().add(d.getState());
        }
        return data;
    }

    /**
     * Metodo encargado del ataque
     *
     * @param idUser id del usuario
     * @param idClan id del clan.
     * @return respuesta del servidor.
     */
    @Override
    public DataResponse<AttackActionState> pandoraAttack(int idUser, int idClan) {
        DataResponse<AttackActionState> data =
                new DataResponse(new AttackActionState());
        final Clan clanAttacked = Search.getClanbyId(idClan, clans);
        User userAttacking = Search.getUserById(idUser, users);

        if (clanAttacked != null && userAttacking != null) {
            actionLastId++;
           /* AttackActionThread d = new AttackActionThread() {
                @Override
                protected void execAction(int type) {
                    if (type < 3) {
                        clan.getAttackResourses().add(aR);
                        clan.updateResources(aR.getType(), -1 * aR.getCost());
                    }
                    System.out.println("EJECUTANDO EL TIPO " + type);
                    System.out.println(new Gson().toJson(clan));
                }
            };
            desicions.add(d);
            data.setData(d.getState());
            */return data;
        }
        data.setCode(CommunicationProtocol.NOT_FOUND);
        return data;
    }

    private DecisionActionThread getDecisionActionThreadById(int idAction) {
        for (DecisionActionThread d : desicions) {
            if (d.getState().getActionId() == idAction) {
                return d;
            }
        }
        return null;
    }

    /**
     * Metodo encargado de obtener los votos de cada usuario
     *
     * @param userId id del usuario
     * @param votes  lista de votos por usuario
     * @return respuesta del servidor; retorna el voto del userId
     */
    private Vote getVoteByUserID(int userId, ArrayList<Vote> votes) {
        for (Vote vote : votes) {
            if (vote.getUserId() == userId) {
                return vote;
            }
        }
        return null;
    }

    /**
     * Método para establecer propiedades iniciales desde un objeto Properties
     *
     * @param properties
     */
    public void setProperties(Properties properties) {
        String strLider = properties.getProperty("liderVote");
        String strColider = properties.getProperty("coliderVote");
        String strPueblo = properties.getProperty("puebloVote");
        String strEsclavo = properties.getProperty("esclavoVote");
        String strTimeToVote = properties.getProperty("timeToVote");
        int liderV = 10;
        int coliderV = 5;
        int puebloV = 2;
        int esclavoV = 1;
        int seconds = 120;
        if (strTimeToVote != null && Util.isInteger(strTimeToVote)) {
            seconds = Integer.valueOf(strTimeToVote);
        }
        if (strLider != null && Util.isInteger(strLider)) {
            liderV = Integer.valueOf(strLider);
        }
        if (strColider != null && Util.isInteger(strColider)) {
            coliderV = Integer.valueOf(strColider);
        }
        if (strPueblo != null && Util.isInteger(strPueblo)) {
            puebloV = Integer.valueOf(strPueblo);
        }
        if (strEsclavo != null && Util.isInteger(strEsclavo)) {
            esclavoV = Integer.valueOf(strEsclavo);
        }

        this.seconds = seconds;
        this.liderVote = liderV;
        this.coliderVote = coliderV;
        this.puebloVote = puebloV;
        this.esclavoVote = esclavoV;

        String strWeaponCost = properties.getProperty("weaponCost");
        String strShieldCost = properties.getProperty("shieldCost");
        attacksInfo[0] = new int[]{1, 1500};
        attacksInfo[1] = new int[]{2, 3500};
        if (strWeaponCost != null && Util.isInteger(strWeaponCost)) {
            attacksInfo[0][1] = Integer.valueOf(strWeaponCost);
        }
        if (strShieldCost != null && Util.isInteger(strShieldCost)) {
            attacksInfo[1][1] = Integer.valueOf(strShieldCost);
        }
        System.out.println("liderVote=" + liderV);
        System.out.println("coliderVote=" + coliderV);
        System.out.println("puebloVote=" + puebloV);
        System.out.println("liderVote=" + liderV);
        System.out.println("esclavoVote=" + esclavoV);
        System.out.println("weaponCost=" + attacksInfo[0][1]);
        System.out.println("shieldCost=" + attacksInfo[1][1]);
        System.out.println("timeToVote=" + seconds);
    }

}
