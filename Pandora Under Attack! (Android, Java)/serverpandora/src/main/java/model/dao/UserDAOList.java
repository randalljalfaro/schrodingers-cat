package model.dao;

import java.net.Socket;
import java.util.ArrayList;

import bohr.engine.admin.GeoPoint;
import bohr.engine.admin.User;
import game.model.util.Search;
import model.dao.intefaces.UserDAO;
import bohr.engine.net.protocol.CommunicationProtocol;
import bohr.engine.net.protocol.DataResponse;

/**
 * Clase que usa los metodos de la interfaz UserDAOList para asi encargarse de manejar las acciones posibles en el protocolo de los usuarios
 *
 * @author Isaac Trejos
 * @author Luis Murillo
 * @author Kevin Arce
 * @author Randall Alfaro
 */
public class UserDAOList implements UserDAO {
    private ArrayList<User> users;
    private int lastId;

    /**
     * Constructor de la clase
     * @param players lista de los jugadores
     */
    public UserDAOList(ArrayList<User> players) {
        this.users = players;
        lastId = 0;
    }

    /**
     * Metodo encargado de registrar un jugador
     *
     * @param username nombre de usuario del jugador.
     * @param socket   .
     * @param password contraseña del jugador.
     * @return respuesta del servidor; retorna el usuario creado o ya hay un usuario creado con ese username y password.
     */
    @Override
    public DataResponse<User> signUp(Socket socket, String username, String password) {
        DataResponse<User> response = new DataResponse(new User());
        if (getPlayerByUserPassword(username, password) == null) {
            lastId++;
            User user = new User(lastId, socket, username, password);
            users.add(user);
            response.setData(user);
            return response;
        }
        response.setCode(CommunicationProtocol.ALREADY_CREATED);
        return response;
    }

    /**
     * Metodo encargado de iniciar un jugador
     *
     * @param username nombre de usuario del jugador.
     * @param socket   .
     * @param password contraseña del jugador.
     * @return respuesta del servidor; retorna el usuario o error.
     */
    @Override
    public DataResponse<User> login(Socket socket, String username, String password) {
        DataResponse<User> response = new DataResponse(new User());
        User user = null;
        if ((user = getPlayerByUserPassword(username, password)) != null) {
            user.setSocket(socket);
            response.setData(user);
            return response;
        }
        response.setCode(CommunicationProtocol.NOT_FOUND);
        return response;
    }

    /**
     * Metodo encargado de iniciar un jugador
     *
     * @param id id de usuario.
     * @return respuesta del servidor; retorna el usuario con el id de entrada.
     */
    @Override
    public DataResponse<User> get(int id) {
        DataResponse<User> response = new DataResponse(new User());
        User user = Search.getUserById(id, users);
        if (user != null)
            response.setData(user);
        return response;
    }

    /**
     * Metodo encargado de obtener todos los usuarios
     *
     * @return respuesta del servidor; retorna usuarios.
     */
    @Override
    public DataResponse<ArrayList<User>> getAll() {
        DataResponse<ArrayList<User>> response =
                new DataResponse(new ArrayList());
        for (User user : users) {
            response.getData().add(user);
        }
        return response;
    }

    /**
     * Metodo encargado de
     *
     * @param idPlayer id de jugador.
     * @return respuesta del servidor;
     */
    @Override
    public void listen(int idPlayer) {

    }

    /**
     * Metodo encargado de buscar un usuario por el nombre de usuario y contraseña.
     *
     * @param username id de jugador.
     * @param password id de jugador.
     * @return respuesta del servidor; retorna null o el usuario a buscar.
     */
    private User getPlayerByUserPassword(String username, String password) {
        for (User p : users) {
            if (p.getUsername().equals(username) && p.getPassword().equals(password)) {
                return p;
            }
        }
        return null;
    }
}
