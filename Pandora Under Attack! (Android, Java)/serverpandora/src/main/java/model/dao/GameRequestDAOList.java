package model.dao;

import java.util.ArrayList;

import bohr.engine.admin.User;
import game.model.entities.clan.Clan;
import game.model.entities.others.GameRequest;
import game.model.entities.clan.ClanMember;
import model.dao.intefaces.GameRequestDAO;
import bohr.engine.net.protocol.DataResponse;
import bohr.engine.net.protocol.CommunicationProtocol;

/**
 * Clase que usa los metodos de la interfaz GameRequestDAO para asi encargarse de manejar las acciones posibles en el protocolo de las solicitudes
 *
 * @author Isaac Trejos
 * @author Luis Murillo
 * @author Kevin Arce
 * @author Randall Alfaro
 */
public class GameRequestDAOList implements GameRequestDAO {
    private ArrayList<GameRequest> requests;
    private ArrayList<Clan> clans;
    private ArrayList<User> users;
    private int lastID;

    /**
     * Constructor de la clase
     *
     * @param clans    lista de los clanes existentes
     * @param users    lista de los usuarios existentes
     * @param requests lista de las solicitudes existentes
     */
    public GameRequestDAOList(ArrayList<GameRequest> requests, ArrayList<User> users, ArrayList<Clan> clans) {
        this.requests = requests;
        this.clans = clans;
        this.users = users;
        lastID = 0;
    }

    /**
     * Metodo encargado de crear solicitudes de un usuario a un clan
     *
     * @param clan iD de clan al que se le hace la solicitud
     * @param user id del usuario que hace la solicitud
     * @return respuesta del servidor; OK o Solicitud existente
     */
    public DataResponse<GameRequest> send(int clan, int user) {
        DataResponse<GameRequest> data =
                new DataResponse(new GameRequest());
        if (getRequestByUserClan(clan, user) == null) {
            lastID++;
            GameRequest gameRequest = new GameRequest(clan, user, false, lastID);
            requests.add(gameRequest);
            data.setData(gameRequest);
            return data;
        }
        data.setCode(CommunicationProtocol.ALREADY_CREATED);
        return data;
    }

    /**
     * Metodo encargado de hacer una lista con las solicitudes hechas por un usuario
     *
     * @param iDuser id del usuario que ha enviado las solicitudes
     * @return lista con las solicitudes hechas por un usuario
     */
    @Override
    public DataResponse<ArrayList<GameRequest>> getAllbyUser(int iDuser) {
        DataResponse<ArrayList<GameRequest>> all =
                new DataResponse(new ArrayList<>());
        all.setData(getRequestByUser(iDuser));
        return all;
    }

    /**
     * Metodo encargado de hacer una lista con las solicitudes recibidas por un clan
     *
     * @param idClan id del clan que quiere saber las solicitudes
     * @return lista con las solicitudes recibidas por un clan
     */
    @Override
    public DataResponse<ArrayList<GameRequest>> getAllbyClan(int idClan) {
        DataResponse<ArrayList<GameRequest>> all =
                new DataResponse(new ArrayList<>());
        all.setData(getRequestByClan(idClan));
        return all;
    }

    /**
     * Metodo encargado de responder las solicitudes
     *
     * @param accepted respuesta a la solicitud
     * @param request  numero de solicitud
     * @return
     */
    @Override
    public DataResponse<GameRequest> response(boolean accepted, int request) {
        DataResponse<GameRequest> response =
                new DataResponse(new GameRequest());
        for (GameRequest gameRequest : requests) {
            if (gameRequest.getIdRequest() == request) {
                gameRequest.setResponse(accepted);
                response.setData(gameRequest);
                if (gameRequest.isResponse())
                    for (Clan c : clans)
                        if (gameRequest.getIdClan() == c.getId())
                            c.getMembers().add(new ClanMember(gameRequest.getIdUser(), 0, 4));
            }
        }
        return response;
    }

    /**
     * Metodo encargado de verificar que un usuario ha o no ha hecho una solicitud a un clan
     *
     * @param clan iD de clan al que se le hace la solicitud
     * @param user id del usuario que hace la solicitud
     * @return solicitud hecha por el usuario o null
     */
    private GameRequest getRequestByUserClan(int user, int clan) {
        for (GameRequest p : requests) {
            if (p.getIdClan() == clan && p.getIdUser() == user) {
                return p;
            }
        }
        return null;
    }

    /**
     * Metodo que recorre la lista de las solicitudes para asi crear una lista de las solicitudes enviadas por un usuario
     *
     * @param user id del usuario que quiere saber las solicitudes enviadas
     * @return lista de las solicitudes enviadas por un usuario
     */
    private ArrayList<GameRequest> getRequestByUser(int user) {
        ArrayList<GameRequest> req = new ArrayList();
        for (GameRequest p : requests) {
            if (p.getIdUser() == user) {
                req.add(p);
            }
        }
        return req;
    }

    /**
     * Metodo que recorre la lista de las solicitudes para asi crear una lista de las solicitudes de un clan
     *
     * @param clan id del clan que quiere saber las solicitudes
     * @return lista con las solicitudes recibidas por un clan
     */
    private ArrayList<GameRequest> getRequestByClan(int clan) {
        ArrayList<GameRequest> req = new ArrayList();
        for (GameRequest p : requests) {
            if (p.getIdClan() == clan) {
                req.add(p);
            }
        }
        return req;
    }

}