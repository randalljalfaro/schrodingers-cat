package model.dao;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

import bohr.engine.admin.User;
import bohr.engine.util.observable.Observable;
import bohr.engine.util.observable.Observer;
import bohr.engine.admin.GeoPoint;
import game.model.entities.clan.Clan;
import game.model.entities.clan.ClanMember;
import game.model.entities.clan.Relic;
import game.model.util.Search;
import model.dao.intefaces.ClanDAO;
import bohr.engine.net.protocol.DataResponse;
import bohr.engine.net.protocol.CommunicationProtocol;

/**
 * Clase que usa los metodos de la interfaz ClanDAO para asi encargarse de manejar las acciones posibles en el protocolo de los clanes
 *
 * @author Isaac Trejos
 * @author Luis Murillo
 * @author Kevin Arce
 * @author Randall Alfaro
 */
public class ClanDAOList implements ClanDAO {
    private ArrayList<Clan> clans;
    private ArrayList<User> users;
    private ArrayList<Relic> relics;
    private int lastIdClan;

    /**
     * Constructor de la clase
     *
     * @param clans lista de los clanses
     */
    public ClanDAOList(ArrayList<Clan> clans, ArrayList<User> users, ArrayList<Relic> relics) {
        this.clans = clans;
        this.users = users;
        this.relics = relics;
        lastIdClan = 0;

    }

    /**
     * Metodo encargado de crear clanes
     *
     * @param gP    punto donde se pondra la reliquia
     * @param name  nombre del clan.
     * @param owner id del creador del clan
     * @param type  tipo de clan creado
     * @return respuesta del servidor; OK o Clan existente
     */
    @Override
    public DataResponse<Clan> create(String name, int type, GeoPoint gP, int owner) {
        DataResponse<Clan> data =
                new DataResponse(new Clan());
        if (getClansByName(name) == null) {
            lastIdClan++;
            Clan clan = new Clan(lastIdClan, type, name, owner, gP);
            clan.getMembers().add(new ClanMember(owner, 0, 1));
            relics.add(new Relic(gP, lastIdClan, clan));
            clans.add(clan);
            data.setData(clan);
            return data;
        }
        data.setCode(CommunicationProtocol.ALREADY_CREATED);
        return data;
    }

    @Override
    public DataResponse<Clan> get(String name) {
        DataResponse<Clan> response =
                new DataResponse(new Clan());
        Clan clan = Search.getClanByName(name, clans);
        if (clan != null)
            response.setData(clan);
        return response;
    }

    /**
     * Metodo encargado de hacer una lista con todos los clanes creados
     *
     * @return lista con todos los clanes creados
     */
    @Override
    public DataResponse<ArrayList<Clan>> getAll() {
        DataResponse<ArrayList<Clan>> all =
                new DataResponse(new ArrayList<>());
        for (Clan clan : clans) {
            all.getData().add(clan);
        }
        return all;
    }


    /**
     * Metodo encargado de revisar si existe algún clan con un nombre dado
     *
     * @param name nombre del clan a revisar
     * @return Clan existente con el nombre dado o null
     */
    private Clan getClansByName(String name) {
        for (Clan p : clans) {
            if (p.getClanName().equals(name)) {
                return p;
            }
        }
        return null;
    }

    @Override
    /**
     * Este metodo mantiene la comunicación se mantiene escuchando y leyendo las acciones
     */
    public void listen(final Socket playerSocket, final BufferedReader in, final PrintWriter out, int userId) {
        for (Clan clan : clans) {
            if (clan.getOwner() == userId) {
                addObserverToClan(clan, playerSocket, in, out);
            }
            for (ClanMember member : clan.getMembers()) {
                if (member.getIdUser() == userId)
                    addObserverToClan(clan, playerSocket, in, out);
            }
        }
    }

    private void addObserverToClan(final Clan clan, final Socket playerSocket,
                                   final BufferedReader in, final PrintWriter out) {
        clan.addObserver(new Observer() {
            @Override
            public void update(Observable obs, Object msg) {
                if (!playerSocket.isClosed()) {
                    out.println((String) msg);
                } else {
                    synchronized (clan) {
                        clan.deleteObserver(this);
                    }
                }
            }
        });
    }
}
