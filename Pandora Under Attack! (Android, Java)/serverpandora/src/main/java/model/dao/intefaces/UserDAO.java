package model.dao.intefaces;

import java.net.Socket;
import java.util.ArrayList;

import bohr.engine.admin.User;
import bohr.engine.net.protocol.DataResponse;

public interface UserDAO {
	public DataResponse<ArrayList<User>> getAll();

	public DataResponse<User> signUp(Socket socket, String username, String password);

	public DataResponse<User> login(Socket socket, String username, String password);

	public DataResponse<User> get(int id);
/*
	public JSONObject messageClients(String msg);

	public JSONObject messageClient(String msg, int sessionID);
*/
	public void listen(int idPlayer);

}
