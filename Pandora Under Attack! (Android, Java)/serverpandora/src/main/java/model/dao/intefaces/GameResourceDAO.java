package model.dao.intefaces;

import java.util.ArrayList;

import game.model.entities.others.GameResource;
import bohr.engine.admin.GeoPoint;
import game.model.net.GameResourceRequest;
import bohr.engine.net.protocol.DataResponse;

/**
 * Created by RJ
 */
public interface GameResourceDAO {
    /*public JSONArray get(int idClan);*/

    public DataResponse<ArrayList<GameResource>> getAll(GeoPoint geoPoint);

    public DataResponse<GameResourceRequest> collect(int idResource, int userId, GeoPoint userLocation);

}
