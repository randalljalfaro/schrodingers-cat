package model.dao.intefaces;


import java.util.ArrayList;

import game.model.threads.attackAction.AttackActionState;
import game.model.threads.decisionAction.DecisionActionState;
import bohr.engine.net.protocol.DataResponse;

public interface ActionDAO {
    public DataResponse<DecisionActionState> vote(int idAction, int idUser, boolean response);

    public  DataResponse<DecisionActionState> buy(int idUser, int idClan, int type);

    public DataResponse<AttackActionState> pandoraAttack(int idUser, int idClan);

    public DataResponse<ArrayList<DecisionActionState>> getAll(int idClan);
}
