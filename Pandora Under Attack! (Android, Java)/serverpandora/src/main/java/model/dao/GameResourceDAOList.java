package model.dao;


import java.util.ArrayList;
import java.util.Properties;

import bohr.engine.util.Util;
import bohr.engine.admin.User;
import game.model.entities.clan.Clan;
import game.model.entities.others.GameResource;
import bohr.engine.admin.GeoPoint;
import game.model.net.GameResourceRequest;
import game.model.threads.CollectActionThread;
///import model.dao.intefaces.DAOResponse;
import game.model.util.Search;
import model.dao.intefaces.GameResourceDAO;
import bohr.engine.net.protocol.DataResponse;

/**
 * Clase que usa los metodos de la interfaz GameResourceDAO para asi encargarse de manejar las acciones posibles en el protocolo de las recursos
 *
 * @author Isaac Trejos
 * @author Luis Murillo
 * @author Kevin Arce
 * @author Randall Alfaro
 */
public class GameResourceDAOList implements GameResourceDAO {
    private ArrayList<GameResource> resources;
    private ArrayList<CollectActionThread> collectActionThreads;
    private ArrayList<User> players;
    private ArrayList<Clan> clans;
    private double radioRange;

    /**
     * Constructor de la clase
     *
     * @param clans   lista de los clanses
     * @param players lista de los jugadores
     */
    public GameResourceDAOList(ArrayList<Clan> clans, ArrayList<User> players,
                               ArrayList<GameResource> resources) {
        this.clans = clans;
        this.players = players;
        this.resources = resources;
        collectActionThreads = new ArrayList<>();
        radioRange = (double) 500;
    }

    /**
     * Metodo encargado de obtener todos los recursos
     *
     * @param geoPoint punto en el mapa.
     * @return respuesta del servidor; retorna todos los recursos
     */
    @Override
    public DataResponse<ArrayList<GameResource>> getAll(GeoPoint geoPoint) {
        DataResponse<ArrayList<GameResource>> all =
                new DataResponse(new ArrayList<>());
        //System.out.print(_resources.size());
        synchronized (resources) {
            for (GameResource gR : resources) {
                //if (isNear(myPoint, gR.getGeoPoint()))
                all.getData().add(gR);
            }
        }
        return all;
    }

    /**
     * Metodo encargado de recolectar recursos
     *
     * @param idResource id del recurso.
     * @param userId     id del usuario.
     * @param geoPoint   punto en el mapa.
     * @return respuesta del servidor; retorna la cantidad recolectada.
     */
    @Override
    public DataResponse<GameResourceRequest> collect(int idResource, int userId, GeoPoint geoPoint) {
        DataResponse<GameResourceRequest> response =
                new DataResponse(new GameResourceRequest());
        CollectActionThread collT =
                getCollectActionThreadByResource(idResource);
        if (collT != null) {
            User userSearch = Search.getUserById(userId, collT.getUsersCollecting());
            //Si no está recolectando
            if (userSearch == null)
                collT.getUsersCollecting().add(userSearch);
            response.setData(new GameResourceRequest(
                    collT.getResource(), userId, geoPoint));
        } else {
            GameResource gR = Search.getGameResourceById(idResource, resources);
            if (gR != null) {
                collT = new CollectActionThread(gR, clans, radioRange);
                collT.getUsersCollecting().add(Search.getUserById(userId, players));
                collectActionThreads.add(collT);
                response.setData(new GameResourceRequest(
                        gR, userId, geoPoint));
                collT.getCollectThread().start();
            }
        }
        return response;
    }


    /**
     * Metodo encargado de obtener el hilo del collect
     *
     * @param idResource id del recurso.
     * @return respuesta del servidor; retorna hilo del colect o null
     */
    private CollectActionThread getCollectActionThreadByResource(int idResource) {
        for (CollectActionThread collT : collectActionThreads) {
            if (collT.getResource().getId() == idResource) {
                return collT;
            }
        }
        return null;
    }


    /**
     * Metodo encargado de recolectar agrega players un collect
     *
     * @param playerId id del jugador.
     * @return respuesta del servidor; retorna nuevo hilo de collec.
     */
    private ArrayList<CollectActionThread> getCollectActionThreadByPlayer(int playerId) {
        ArrayList<CollectActionThread> collects = new ArrayList<>();
        for (CollectActionThread collT : collectActionThreads) {
            for (User p : collT.getUsersCollecting()) {
                if (p.getId() == playerId) {
                    collects.add(collT);
                }
            }
        }
        return collects;
    }

    /**
     * Método para establecer propiedades iniciales desde un objeto Properties
     *
     * @param properties
     */
    public void setProperties(Properties properties) {
        String strRadioRange = properties.getProperty("radioRangeResource");
        if (strRadioRange != null && Util.isDouble(strRadioRange)) {
            radioRange = Float.valueOf(strRadioRange);
        }
        System.out.println("radioRangeResources=" + radioRange);
    }
}
