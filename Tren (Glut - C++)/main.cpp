#include <windows.h>
#include <GL/glut.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
/*
 tapones
 sube y baja
 izq y derecha
 tecla x mueve el eje x
 barra espacio tira tapones
*/
GLfloat spinX=0.0, spinY=0.0;
GLint Ejes = 0;

GLint Tecla = 0;
char idTecla;
GLint disparar = 0;
GLfloat Rotacion = 0;
GLfloat Elevacion = 0;
GLfloat Descenso = 0;
GLfloat Derecha = 0;
GLfloat Izquierda = 0;
GLfloat giroX = 0;
GLfloat giroY = 0;
GLfloat giroZ = 0;
GLfloat taponX1 = 0; // tapón de respado de 'taponX'
GLfloat taponX = 0; // tapón en x
GLfloat taponY = 0;
GLfloat taponZ = 0;
GLfloat BOX_HEIGHT = 10;

// puntos iniciales para el plan de vuelo
GLfloat ptX = 20.0f;
GLfloat ptY = 55.0f;
GLfloat ptZ = -69.0f;
GLint k = 0;
GLint planControl = 0;
char Direccion[50];
GLfloat Desplazamiento[50];

void dibujaHelicoptero(void);
void Cabina(void);
void dibujaHelice1(void);
void dibujaHelice2(void);
void dibujaEje(void);
void dibujaPatas(void);
void pinHelicePrinc(void);
void pinHeliceSeg(void);
void dibujaTapones(void);
void planDeVuelo(void);
void tren(void);

GLfloat arrayCopa[16][4] = {
    -80, 47,50, 8,
    -72, 50, 51, 8,
    -64, 51, 50, 8,
    -56, 50, 48, 8,
    -48, 48, 45, 8,
    -40, 45, 40, 8,
    -32, 40, 35, 8,
    -24, 35, 28, 8,
    -16, 28, 20, 8,
    -8, 20, 10, 8,
    0, 10, 3, 8,
    8, 3, 3, 40,
    48, 3, 10, 5,
    53, 10, 20, 5,
    58, 20, 50, 5,
    60, 1, 50, 3
};

void dibujaMaquina(void){
    GLUquadricObj *eje = gluNewQuadric();
    gluQuadricDrawStyle(eje, GLU_FILL);
    gluQuadricNormals(eje, GL_SMOOTH);
    glPushMatrix();
    glTranslatef(0, 10, -320);
    glScalef(1.3, 1.3, 1);

    glPushMatrix();
    glRotatef(0, 1, 0, 0);
    glScalef(1, 1, 2.5);
    gluCylinder(eje, 20, 20, 100, 50, 1);
    glPopMatrix();

    //AROS
    glPushMatrix();
    glColor4ub(224, 224, 224, 224);  //
    glRotatef(0, 1, 0, 0);
    glTranslatef(0, 0, 0);
    gluCylinder(eje, 20.1, 20.1, 2, 50, 1);
    glPopMatrix();

    glPushMatrix();
    glColor4ub(224, 224, 224, 224);  //
    glRotatef(0, 1, 0, 0);
    glTranslatef(0, 0, 50);
    gluCylinder(eje, 20.1, 20.1, 2, 50, 1);
    glPopMatrix();

    glPushMatrix();
    glColor4ub(224, 224, 224, 224);  //
    glRotatef(0, 1, 0, 0);
    glTranslatef(0, 0, 98);
    gluCylinder(eje, 20.1, 20.1, 2, 50, 1);
    glPopMatrix();

    //TAPA
    glPushMatrix();
    glColor4ub(100, 100, 100, 224);

    glPushMatrix();
    glRotatef(0, 1, 0, 0);
    glTranslatef(0, 0, -10);
    gluCylinder(eje, 10, 20.1, 2, 50, 1);
    glPopMatrix();

    glPushMatrix();
    glRotatef(0, 1, 0, 0);
    glTranslatef(0, 0, -8);
    gluCylinder(eje, 20.1, 20.1, 8, 50, 1);
    glPopMatrix();

    glPushMatrix();
    glRotatef(0, 1, 0, 0);
    glTranslatef(0, 0, -12);
    gluCylinder(eje, 1, 10, 10, 50, 1);
    glPopMatrix();

    glPopMatrix();
    gluDeleteQuadric(eje);
}
void dibujaCabina(void){
    GLUquadricObj *eje = gluNewQuadric();
    gluQuadricDrawStyle(eje, GLU_FILL);
    gluQuadricNormals(eje, GL_SMOOTH);
    glPushMatrix();
    glTranslatef(0, 0, 5);
    glScalef(1.3, 1.3, 1);

    // techo
    glPushMatrix();
    glRotatef(0, 1, 0, 0);
    glTranslatef(0, 45, -120);
    glScalef(0.9,0.1,3.5);
    gluCylinder(eje, 40, 40, 50, 50, 1);
    glPopMatrix();

    // Pilares
    glPushMatrix();
    glRotatef(90, 1, 0, 0);
    glTranslatef(-30, 25, -45);
    glScalef(0.1,1,3);
    gluCylinder(eje, 5, 5, 20, 50, 1);
    glPopMatrix();

     glPushMatrix();
    glRotatef(90, 1, 0, 0);
    glTranslatef(-30, -30, -45);
    glScalef(0.1,1,3);
    gluCylinder(eje, 5, 5, 20, 50, 1);
    glPopMatrix();

    glPushMatrix();
    glRotatef(90, 1, 0, 0);
    glTranslatef(-30, -100, -45);
    glScalef(0.1,1,3);
    gluCylinder(eje, 5, 5, 20, 50, 1);
    glPopMatrix();


    glPushMatrix();
    glRotatef(90, 1, 0, 0);
    glTranslatef(30, 25, -45);
    glScalef(0.1,1,3);
    gluCylinder(eje, 5, 5, 20, 50, 1);
    glPopMatrix();

    glPushMatrix();
    glRotatef(90, 1, 0, 0);
    glTranslatef(30, -30, -45);
    glScalef(0.1,1,3);
    gluCylinder(eje, 5, 5, 20, 50, 1);
    glPopMatrix();

    glPushMatrix();
    glRotatef(90, 1, 0, 0);
    glTranslatef(30, -100, -45);
    glScalef(0.1,1,3);
    gluCylinder(eje, 5, 5, 20, 50, 1);
    glPopMatrix();

    //Paredes
    glPushMatrix();
    glBegin(GL_POLYGON);
    glVertex3f(-80, -30, 80);
    glVertex3f(-80, -30, -80);
    glVertex3f(80, -30, -80);
    glVertex3f(80, -30, 80);
    glEnd();
    glPopMatrix();

    glPopMatrix();
}
void tren(void){
    glPushMatrix();

    glTranslatef(30, -40, 100);
    dibujaMaquina();
    dibujaCabina();

    glPopMatrix();
}

void dibujaHelicoptero(void){
    glPushMatrix();
    glTranslatef(ptX, ptY, ptZ);
    glRotatef(giroX, 1, 0, 0);
    glRotatef(giroY, 0, 1, 0);
    glRotatef(giroZ, 0, 0, 1);
    planDeVuelo();//---- !!!!!!!!!!!!!!!!!
    glTranslatef(0, Elevacion, 0);
    glTranslatef(0, Descenso, 0);
    glTranslatef(Derecha, 0, 0);
    glTranslatef(Izquierda, 0, 0);
    Cabina();
    dibujaHelice1();
    dibujaHelice2();
    dibujaEje();
    dibujaPatas();
    pinHelicePrinc();
    pinHeliceSeg();
    dibujaTapones();
    glPopMatrix();
}

void Cabina(void) {
    glPushMatrix();
    glTranslatef(-18, 0, 0);
    glColor4ub(0, 0, 255, 0);   //Rojo
    glRotatef(90, 0, 1, 0);
    glScalef(1, 1.2, 2);
    glutWireSphere(10, 10, 30);
    glPopMatrix();
}

void Cubo (void) {
     glColor4ub(0, 255, 0, 0);   //Verde
     glutWireCube (100.0);
}

void dibujaHelice1(void){
    glPushMatrix();
    glTranslatef(-18, 13, 0);
    glRotatef(90, 1, 0, 0);
    glRotatef(Rotacion, 0, 0, 1);

    glPushMatrix();
    glColor4ub(224, 224, 224, 224);  //
    glScalef(0.05, 1, 0.01);
    glutSolidCube(70);
    glPopMatrix();

    glPushMatrix();
    glRotatef(90, 0, 0, 1);
    glScalef(0.05, 1, 0.01);
    glutSolidCube(70);
    glPopMatrix();

    glPopMatrix();
}

void dibujaHelice2(void){
    glPushMatrix();
    glTranslatef(49, 0, 5);
    glRotatef(Rotacion, 0, 0, 1);

    glPushMatrix();
    glColor4ub(224, 224, 224, 0);  //
    glScalef(0.05, 1, 0.01);
    glutSolidCube(20);
    glPopMatrix();

    glPushMatrix();
    glRotatef(90, 0, 0, 1);
    glScalef(0.05, 1, 0.01);
    glutSolidCube(20);
    glPopMatrix();

    glPopMatrix();
}

void dibujaEje(void){
    GLUquadricObj *eje = gluNewQuadric();
    gluQuadricDrawStyle(eje, GLU_FILL);
    gluQuadricNormals(eje, GL_SMOOTH);

    glPushMatrix();
    glRotatef(90, 0, 1, 0);
    glColor4ub(0, 0, 255, 0);   //Rojo
    gluCylinder(eje, 5, 2, 50, 20, 1);
    glPopMatrix();
    gluDeleteQuadric(eje);
}

void dibujaPatas(){
    GLUquadricObj *pata1 = gluNewQuadric();
    GLUquadricObj *pata2 = gluNewQuadric();
    GLUquadricObj *base1pata1 = gluNewQuadric();
    GLUquadricObj *base2pata1 = gluNewQuadric();
    GLUquadricObj *base1pata2 = gluNewQuadric();
    GLUquadricObj *base2pata2 = gluNewQuadric();
    gluQuadricDrawStyle(pata1, GLU_FILL);
    gluQuadricDrawStyle(pata2, GLU_FILL);
    gluQuadricDrawStyle(base1pata1, GLU_FILL);
    gluQuadricDrawStyle(base2pata1, GLU_FILL);
    gluQuadricDrawStyle(base1pata2, GLU_FILL);
    gluQuadricDrawStyle(base2pata2, GLU_FILL);
    gluQuadricNormals(pata1, GL_SMOOTH);
    gluQuadricNormals(pata2, GL_SMOOTH);
    gluQuadricNormals(base1pata1, GL_SMOOTH);
    gluQuadricNormals(base2pata1, GL_SMOOTH);
    gluQuadricNormals(base1pata2, GL_SMOOTH);
    gluQuadricNormals(base2pata2, GL_SMOOTH);

    glPushMatrix();
    glTranslatef(-32, -18, -8);
    glRotatef(90, 0, 1, 0);
    glColor4ub(194, 194, 194, 0);   //Rojo
    gluCylinder(pata1, 1, 1, 30, 10, 1);
    glPopMatrix();

    glPushMatrix();
    glTranslatef(-32, -18, 8);
    glRotatef(90, 0, 1, 0);
    glColor4ub(194, 194, 194, 0);   //Rojo
    gluCylinder(pata1, 1, 1, 30, 10, 1);
    glPopMatrix();

    glPushMatrix();
    glTranslatef(-25, -18, 8);
    glRotatef(240, 1, 0, 0);
    glColor4ub(194, 194, 194, 0);   //Rojo
    gluCylinder(base1pata1, 1, 1, 9, 10, 1);
    glPopMatrix();

    glPushMatrix();
    glTranslatef(-10, -18, 8);
    glRotatef(240, 1, 0, 0);
    glColor4ub(194, 194, 194, 0);   //Rojo
    gluCylinder(base1pata1, 1, 1, 9, 10, 1);
    glPopMatrix();

    glPushMatrix();
    glTranslatef(-25, -18, -8);
    glRotatef(-60, 1, 0, 0);
    glColor4ub(194, 194, 194, 0);   //Rojo
    gluCylinder(base1pata2, 1, 1, 9, 10, 1);
    glPopMatrix();

    glPushMatrix();
    glTranslatef(-10, -18, -8);
    glRotatef(-60, 1, 0, 0);
    glColor4ub(194, 194, 194, 0);   //Rojo
    gluCylinder(base2pata2, 1, 1, 9, 10, 1);
    glPopMatrix();

    gluDeleteQuadric(pata1);
    gluDeleteQuadric(pata2);
    gluDeleteQuadric(base1pata1);
    gluDeleteQuadric(base2pata1);
    gluDeleteQuadric(base1pata2);
    gluDeleteQuadric(base2pata2);
}

void dibujarPiso(void){
    glPushMatrix();
    glBegin(GL_POLYGON);
   // glColor4ub(255, 0, 0, 0);
    glVertex3f(-80, -30, 80);
    glVertex3f(-80, -30, -80);
    glVertex3f(80, -30, -80);
    glVertex3f(80, -30, 80);
    glEnd();
    glPopMatrix();
}

void dibujarPiso2(void){
    glPushMatrix();
    glTranslatef(0, 38, 0);
    glBegin(GL_POLYGON);
    glColor4ub(0, 255, 0, 0);
    glVertex3f(-120, -70, 120);
    glVertex3f(-120, -70, -120);
    glVertex3f(120, -70, -120);
    glVertex3f(120, -70, 120);
    glEnd();
    glPopMatrix();
}

void pinHelicePrinc(void){
    GLUquadricObj *pin = gluNewQuadric();
    gluQuadricDrawStyle(pin, GLU_FILL);
    gluQuadricNormals(pin, GL_SMOOTH);
    glPushMatrix();
    glTranslatef(-18, 15, 0);
    glRotatef(90, 1, 0, 0);
    glColor4ub(255, 255, 153, 0);   //Rojo
    gluCylinder(pin, 1, 1, 5, 10, 1);
    glPopMatrix();
    gluDeleteQuadric(pin);
}

void pinHeliceSeg(void){
    GLUquadricObj *pin = gluNewQuadric();
    gluQuadricDrawStyle(pin, GLU_FILL);
    gluQuadricNormals(pin, GL_SMOOTH);
    glPushMatrix();
    glTranslatef(49, 0, -2);
    glRotatef(90, 0, 0, 1);
    glColor4ub(0, 0, 255, 0);   //Rojo
    gluCylinder(pin, 1, 1, 10, 10, 1);
    glPopMatrix();
    gluDeleteQuadric(pin);
}

void dibujaTapones(void){
    // tapones que se 'disparan'
    glPushMatrix();
    if(disparar == 1) glTranslatef(taponX, taponY, taponZ);

    glPushMatrix();
    glTranslatef(-32, -18, -8);
    glColor4ub(255, 25, 0, 0);
    glRotatef(90, 0, 1, 0);
    glScalef(.2, .2, .2);
    glutSolidSphere(10, 10, 30);
    glPopMatrix();

    glPushMatrix();
    glTranslatef(-32, -18, 8);
    glColor4ub(255, 25, 0, 0);
    glRotatef(90, 0, 1, 0);
    glScalef(.2, .2, .2);
    glutSolidSphere(10, 10, 30);
    glPopMatrix();

    glPopMatrix();

    // ------------------------------------

    glPushMatrix();
    glTranslatef(-32, -18, -8);
    glColor4ub(255, 0, 0, 0);
    glRotatef(90, 0, 1, 0);
    glScalef(.2, .2, .2);
    glutSolidSphere(10, 10, 30);
    glPopMatrix();

    glPushMatrix();
    glTranslatef(0, -18, -8);
    glColor4ub(255, 0, 0, 0);
    glRotatef(90, 0, 1, 0);
    glScalef(.2, .2, .2);
    glutSolidSphere(10, 10, 30);
    glPopMatrix();

    glPushMatrix();
    glTranslatef(0, -18, 8);
    glColor4ub(255, 0, 0, 0);
    glRotatef(90, 0, 1, 0);
    glScalef(.2, .2, .2);
    glutSolidSphere(10, 10, 30);
    glPopMatrix();

    glPushMatrix();
    glTranslatef(-32, -18, 8);
    glColor4ub(255, 0, 0, 0);
    glRotatef(90, 0, 1, 0);
    glScalef(.2, .2, .2);
    glutSolidSphere(10, 10, 30);
    glPopMatrix();
}

void planDeVuelo(void){
    if(Tecla == 1 && idTecla == 'p'){
        int i = 0;
        while(i < k){
            GLfloat limite = Desplazamiento[i];
            if(Direccion[i] == 'w' && ptY < limite){ptY += 0.1; break;}
            if(Direccion[i] == 's' && ptY < limite){ ptY -= 0.1; break;}
            if(Direccion[i] == 'a' && ptX < limite){ ptX -= 0.1; break;}
            if(Direccion[i] == 'd' && ptX < limite){ ptX += 0.1; break;}
            if(Direccion[i] == 'x' && giroX < limite){ giroX += 0.1; break;}
            if(Direccion[i] == 'y' && giroY < limite){ giroY += 0.1; break;}
            if(Direccion[i] == 't' && limite == 1){ disparar = 1;}
            if(Direccion[i] == 't' && limite == 0){disparar = 0;}
            //---------
            i++;
        }
        i = 0;
    }
}

void dibujaGlobo(){
    glPushMatrix();
    glTranslatef(30, 50, 0);
    glColor4ub(255, 0, 0, 0);   //Rojo
    glRotatef(90, 1, 0, 0);
    glScalef(0.8, 0.8, 1);
    glutSolidSphere(10, 100, 10);
    glPopMatrix();

    glPushMatrix();
    GLUquadricObj *cuerda = gluNewQuadric();
    gluQuadricDrawStyle(cuerda, GLU_FILL);
    gluQuadricNormals(cuerda, GL_SMOOTH);
    glRotatef(90, 1, 0, 0);
    glTranslatef(30, 0, -40);
    glColor4ub(255, 255, 153, 0);   //Rojo
    gluCylinder(cuerda, .5, .5, 30, 10, 1);
    glPopMatrix();

    gluDeleteQuadric(cuerda);

}

void display(void){
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();

   glRotatef(spinX, 1, 0, 0);
   glRotatef(spinY, 0, 1, 0);

   glPushMatrix();
    glScalef(1, -2, 1);
    glScalef(0.3, -0.2,0.2);
   glTranslatef(0, BOX_HEIGHT, 0);
    tren();
   dibujaHelicoptero();
   dibujaGlobo();
   glPopMatrix();

   glEnable(GL_STENCIL_TEST); //Enable using the stencil buffer
   glColorMask(0, 0, 0, 0); //Disable drawing colors to the screen
   glDisable(GL_DEPTH_TEST); //Disable depth testing
   glStencilFunc(GL_ALWAYS, 1, 1); //Make the stencil test always pass


   //Make pixels in the stencil buffer be set to 1 when the stencil test passes
   glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
   dibujarPiso();

   glColorMask(1, 1, 1, 1); //Enable drawing colors to the screen
   glEnable(GL_DEPTH_TEST); //Enable depth testing

    //Make the stencil test pass only when the pixel is 1 in the stencil buffer
   glStencilFunc(GL_EQUAL, 1, 1);
   glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP); //Make the stencil buffer not change

   glPushMatrix();
   glScalef(1, -2, 1);
   glScalef(0.3, -0.2,0.2);
   glTranslatef(0, BOX_HEIGHT-250, 0);
   //Aquí se dibujan los objetos que se van a reflejar
   dibujaHelicoptero();
   tren();
   dibujaGlobo();
   glPopMatrix();
   glDisable(GL_STENCIL_TEST); //Disable using the stencil buffer
  //Blend the floor onto the screen
   glEnable(GL_BLEND);
   glColor4ub(0, 0, 125, 125);
  //Aquí se dibuja el piso que refleja
   dibujarPiso();
   glDisable(GL_BLEND);
   dibujarPiso2();
   Sleep(8);

   glutSwapBuffers();
}

void spinDisplay(void){
    if(Tecla == 1){
        Rotacion += 50;
        if(idTecla == 'w'){
            if(Elevacion != 80) Elevacion += 0.5;
            else Descenso = 80;
        }
        if(idTecla == 's'){
            if(Descenso != -80) Descenso -= 0.5;
            else Elevacion = -80;
        }
        if(idTecla == 'a'){
            if(Derecha != -46) Derecha -= 0.5;
            else Izquierda = -46;
        }
        if(idTecla == 'd'){
            if(Izquierda != 46) Izquierda += 0.5;
            else Derecha = 46;
        }
        if(idTecla == 'x') giroX += 1;
        if(idTecla == 'y') giroY += 1;
        if(idTecla == 'z') giroZ += 1;

        if(idTecla == 32){
            if(taponX != -400) taponX -= 5;
            else taponX = -32;
        }

        if(idTecla == 'p' && disparar == 1){
            if(taponX != -400) taponX -= 5;
            else taponX = taponX1;
        }
        else{
            taponX = taponX1;
        }
    }
    else{
        Rotacion += 50;
        if(idTecla == 32) taponX = -32;
    }
    glutPostRedisplay(); //Vuelve a dibujar
}

void init(void){
  glClearColor (0.0, 0.0, 0.0, 0.0); //Colores para iniciar la ventana (Fondo)
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_LIGHTING);
  glEnable(GL_LIGHT0);
  glEnable(GL_NORMALIZE);
  glEnable(GL_COLOR_MATERIAL);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); // manejar transparencia
  //----------- lectura archivo
  FILE *myFile;
  // C:\Users\Estudiante\Desktop\helicoptero
  if((myFile=fopen("D:/Dropbox/II Ciclo - 2014/Graficos/Tren_/planDeVuelo.txt", "r")) == NULL){
    printf("Error al abrir el archivo... ");
    exit(0);
  }
  k = 0;
  while(fscanf(myFile, "%c %f", &Direccion[k], &Desplazamiento[k]) != EOF) k++;
  fclose(myFile);
}

void reshape(int w, int h){
   glViewport (0, 0, (GLsizei) w, (GLsizei) h);
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();

   glOrtho(-100.0, 100.0, -100.0, 100.0, -500.0, 500.0); //Izq, Der, Abajo, Arriba, Cerca, lejos
   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();
   gluPerspective(45.0, (float)w / (float)h, 100.0, 100.0);
}

void handleKeypress(unsigned char key, int x, int y) {
    idTecla = key;
    if(key == 27) exit(0); //Escape key
    else{
        if(key == 'h' || key == 'w' || key == 's' || key == 'a' || key == 'd'|| key == 'x' || key == 'y' || key == 'z' || key == 32 || key == 'p'){
            if (Tecla == 1) Tecla = 0;
            else Tecla = 1;
        }
    }
}

void mouseMotion(int x, int y){
     spinX = y;
     spinY = x;
}

void mouse(int button, int state, int x, int y){
   switch (button) {
      case GLUT_LEFT_BUTTON:
         if (state == GLUT_DOWN)
            glutIdleFunc(spinDisplay);
         break;

      case GLUT_RIGHT_BUTTON:
//      case GLUT_MIDDLE_BUTTON:
         if (state == GLUT_DOWN)
            glutIdleFunc(NULL);
         break;

      default:
         break;
   }
}

/*
 *  Request double buffer display mode.
 *  Register mouse input callback functions
 */
int main(int argc, char** argv)
{
   glutInit(&argc, argv);   //Inicializa la librería GLUT y negocia una sesión
                            //con el sistema de ventanas
     glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH | GLUT_STENCIL); //Activa el modo inicial de despliegue
                            //GLUT_DOUBLE doble buffer  GLUT_RGB = GLUT_RGBA

   glutInitWindowSize (1000, 1000); //Tamaño de la ventana 500 x 500

   glutInitWindowPosition (100, 100); //Posición de la ventana en la pantalla

   glutCreateWindow ("HELICOPTERO"); //Crea la ventana y le pone la etiqueta

   init ();  //Ejecuta el método "init"

   glutDisplayFunc(display);  //Ejecuta el método "display"
   glutKeyboardFunc(handleKeypress);
   glutReshapeFunc(reshape); //Ejecuta el método "reshape"
   glutMouseFunc(mouse); //Activa los controles del mouse
   glutMotionFunc(mouseMotion);
   glutMainLoop(); //Repite el main indefinidamente (¡Animación!)
   return 0;   /* ANSI C requires main to return int. */
}

