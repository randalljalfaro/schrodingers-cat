package control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import javax.swing.Action;
import javax.swing.JFileChooser;
import model.Backtracking;
import model.Matriz;
import model.Pieza;
import model.PosibleSolucion;
import view.P_Soluciones;

/**
 *
 * @author bohr
 */
public class Katamino {

    public static Backtracking initBacktracking(String name) {
        Archivos archivos = new Archivos();
        Stack<PosibleSolucion> pila = new Stack();
        int n = 6;
        int m = 4;
        List<Pieza> pozo = archivos.cargarArchivo(name);
        //archivos.getPiezas();
        pozo.stream().forEach(
                pieza -> {
                    pieza.getRotaciones_Unicas().stream().forEach(
                            rotacion -> {
                                Matriz matriz = new Matriz(n, m);
                                for (int x = 0; x < matriz.getTamN(); x++) {
                                    for (int y = 0; y < matriz.getTamM(); y++) {
                                        if (matriz.cabePieza(rotacion, x, y)) {
                                            Matriz copia_matriz = new Matriz(matriz);
                                            Pieza copia_pieza = new Pieza(rotacion);
                                            copia_matriz.ingresaPieza(copia_pieza, x, y);
                                            PosibleSolucion ps = new PosibleSolucion(copia_matriz);
                                            ps.setPozo(copia_pieza.getCodigo(), pozo);
                                            pila.add(ps);
                                        }
                                    }
                                }
                            }
                    );
                }
        );
        System.out.println("Ejecutando Backtracking");
        return new Backtracking(pila, pozo);
    }

    public static void initView(P_Soluciones padre, Backtracking backtracking) {
        if (padre != null) {
            padre.dispose();
        }
        P_Soluciones pantalla = new P_Soluciones();
        pantalla.pintarRespuesta(backtracking.buscarSolucion());
        pantalla.getbtnSiguiente().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pantalla.pintarRespuesta(backtracking.buscarSolucion());
            }
        });
        pantalla.getBtnCargarArchivo().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fch = new JFileChooser(new File("../Conjunto_de_piezas"));
                int returnVal = fch.showOpenDialog(null);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    System.out.println(fch.getSelectedFile().getAbsolutePath());
                    initView(pantalla, initBacktracking(fch.getSelectedFile().getAbsolutePath()));
                }

            }
        });
    }

    public static void main(String[] args) {

        initView(null,initBacktracking("../Conjunto_de_piezas/katamino_1.txt"));
    }
}
