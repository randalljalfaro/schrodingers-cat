package control;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import model.Pieza;

/**
 *
 * @author bohr
 */
public class Archivos {

    List<Pieza> piezas;

    public Archivos() {
        this.piezas = new ArrayList<>();
    }

    public List<Pieza> cargarArchivo(String dir_file) {
        BufferedReader in = null;
        try {
            in = new BufferedReader(new FileReader(dir_file));
            System.out.println("katamino_1.txt LEIDO");
            String line;
            int cod = 1;
            while ((line = in.readLine()) != null) {
                //System.out.println(line);
                //int x = 0;
                //int y = 0;
                String x = "0";
                String y = "0";
                int nums;
                for (nums = 0; nums < line.length() && line.charAt(nums) != ' '; nums++) {
                    x = x + line.charAt(nums);
                    //this.matriz[j][i] = line.charAt(i);
                }
                for (nums = nums + 1; nums < line.length() && line.charAt(nums) != ' '; nums++) {
                    y = y + line.charAt(nums);
                    //this.matriz[j][i] = line.charAt(i);
                }

                //System.out.println(x+" - "+y);
                int xNum = Integer.valueOf(x);
                int yNum = Integer.valueOf(y);

                //System.out.println(xNum + " - " + yNum);
                int m[][] = new int[xNum][yNum];
                for (int i = 0; i < xNum; i++) {
                    line = in.readLine();
                    for (int j = 0; j < yNum; j++) {
                        //System.out.print(line.charAt(j));
                        if (Integer.valueOf("" + line.charAt(j)) > 0) {
                            m[i][j] = cod;
                        } else {
                            m[i][j] = 0;
                        }
                        //this.matriz[j][i] = line.charAt(i);
                    }
                    //System.out.println();
                }
                for (int i = 0; i < xNum; i++) {
                    for (int j = 0; j < yNum; j++) {
                        System.out.print(m[i][j]);
                        //this.matriz[j][i] = line.charAt(i);
                    }
                    System.out.println();
                }
                System.out.println("COD: " + cod);
                piezas.add(new Pieza(m, xNum, yNum, cod));
                cod++;
                line = in.readLine();
            }
            in.close();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        //System.out.println(piezas.size());
        return piezas;
    }

    public List<Pieza> getPiezas() {

        int m1[][] = {
            {1, 1},
            {1, 1}
        };
        piezas.add(new Pieza(m1, 2, 2, 1));

        int m2[][] = {
            {2, 2},
            {0, 2}
        };
        piezas.add(new Pieza(m2, 2, 2, 2));

        int m3[][] = {
            {3, 0, 0},
            {3, 3, 3}
        };
        piezas.add(new Pieza(m3, 2, 3, 3));

        int m4[][] = {
            {4, 4, 4, 4}
        };
        piezas.add(new Pieza(m4, 1, 4, 4));

        int m5[][] = {
            {5, 5},
            {5, 5}
        };
        piezas.add(new Pieza(m5, 2, 2, 5));
        int m6[][] = {
            {6, 6, 6},
            {6, 6, 0}
        };
        piezas.add(new Pieza(m6, 2, 3, 6));

        int m7[][] = {
            {7, 7, 7}
        };
        piezas.add(new Pieza(m7, 1, 3, 7));
        /*  
         int m8[][] = {
         {8, 8, 8},
         {0, 8, 0}
         };
         piezas.add(new Pieza(m8, 2, 3, 8));
         int m9[][] = {
         {9, 9, 9},
         {0, 9, 0},
         {9, 9, 0}
         };
         piezas.add(new Pieza(m9, 3, 3, 7));
         */
        return piezas;
    }

}
