package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 *
 * @author bohr
 */
public class PosibleSolucion {

    private final Matriz matriz;
    private final List<Pieza> pozo;

    public PosibleSolucion(Matriz matriz) {
        this.matriz = matriz;
        this.pozo = new ArrayList<>();
    }

    public void setPozo(int codigo, List<Pieza> pozoAux) {
        //Copiar todos en el nuevo pozo de cada uno, menos ellos mismos
        pozoAux.stream().forEach((pCopy) -> {
            if (codigo != pCopy.getCodigo()) {
                Pieza copy = new Pieza(pCopy);
                this.pozo.add(copy);
            }
        });
    }

    private void getRotEnTodasPos(Pieza rotacion, Stack<PosibleSolucion> pila) {
        for (int x = 0; x < matriz.getTamN(); x++) {
            for (int y = 0; y < matriz.getTamM(); y++) {
                if (matriz.cabePieza(rotacion, x, y)) {
                    Matriz copia_matriz = new Matriz(matriz);
                    Pieza copia_pieza = new Pieza(rotacion);
                    copia_matriz.ingresaPieza(copia_pieza, x, y);
                    PosibleSolucion ps = new PosibleSolucion(copia_matriz);
                    ps.setPozo(copia_pieza.getCodigo(), pozo);
                    pila.add(ps);
                }

            }
        }
    }

    public void pushHijosEnPila(Stack<PosibleSolucion> pila) {
        pozo.stream().forEach(
                pieza -> {
                    pieza.getRotaciones_Unicas().stream().forEach(
                            rotacion -> {
                                getRotEnTodasPos(rotacion, pila);
                            }
                    );
                }
        );
    }

    public boolean esSolucion() {
        return matriz.estaLlena();
    }

    public Matriz getMatriz() {
        return matriz;
    }

    public List<Pieza> getPozo() {
        return pozo;
    }

}
