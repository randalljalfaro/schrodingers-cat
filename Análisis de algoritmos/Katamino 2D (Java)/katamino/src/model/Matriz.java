package model;

/**
 *
 * @author bohr
 */
public class Matriz {

    private final int[][] matriz;
    private final int tamN;
    private final int tamM;

    public Matriz(Matriz m) {
        this.matriz = new int[m.getTamN()][m.getTamM()];
        for (int i = 0; i < m.getTamN(); i++) {
            for (int j = 0; j < m.getTamM(); j++) {
                this.matriz[i][j] = m.getMatriz()[i][j];
            }
        }
        this.tamM = m.getTamM();
        this.tamN = m.getTamN();
    }

    public Matriz(int n, int m) {
        this.tamM = m;
        this.tamN = n;
        matriz = new int[n][m];
        this.iniciarMatriz();
    }

    private void iniciarMatriz() {
        for (int n = 0; n < this.tamN; n++) {
            for (int m = 0; m < this.tamM; m++) {
                this.matriz[n][m] = 0;
            }
        }
    }
    
    public boolean cabePieza(Pieza pieza, int x, int y) {
        for (int i = 0; i < pieza.getTamX(); i++) {
            for (int j = 0; j < pieza.getTamY(); j++) {
                //System.out.println((x + i) + " - " + (y + j));
                if (x + i >= this.getTamN() || y + j >= this.getTamM()
                        || this.matriz[x + i][y + j] != 0) {
                    if (pieza.getEspacio()[i][j] != 0) {
                        return false;
                    }
                }
            }
        }
        return true;
    }
    
    public void ingresaPieza(Pieza pieza, int x, int y) {
        for (int i = 0; i < pieza.getTamX(); i++) {
            for (int j = 0; j < pieza.getTamY(); j++) {
                if (pieza.getEspacio()[i][j] != 0) {
                    this.matriz[x + i][y + j] = pieza.getEspacio()[i][j];
                }
            }
        }
    }
    
    public void imprimir() {
        for (int n = 0; n < this.tamN; n++) {
            for (int m = 0; m < this.tamM; m++) {
                System.out.print(this.matriz[n][m]);
            }
            System.out.println("");
        }
    }

    public boolean estaLlena() {
        for (int x = 0; x < this.tamN; x++) {
            for (int y = 0; y < this.tamM; y++) {
                if(this.matriz[x][y]==0) return false;
            }
        }
        return true;
    }
    public boolean compara(int[][] m, int tM, int tN) {
        if(this.tamN!=tN || this.tamM!=tM) return false;
        for (int x = 0; x < this.tamN; x++) {
            for (int y = 0; y < this.tamM; y++) {
                if(this.matriz[x][y] != m[x][y]) return false;
            }
        }
        return true;
    }
    public int[][] getMatriz() {
        return matriz;
    }

    public int getTamN() {
        return tamN;
    }

    public int getTamM() {
        return tamM;
    }

}
