package model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author bohr
 */
public class Pieza {
    private final int[][] espacio;
    private final int tamX;
    private final int tamY;
    private final int codigo;

    public Pieza(int[][] espacio, int tamX, int tamY, int codigo) {
        this.espacio = espacio;
        this.tamX = tamX;
        this.tamY = tamY;
        this.codigo = codigo;
    }

    public Pieza(Pieza p) {
        this.espacio = p.getEspacio();
        this.tamX = p.getTamX();
        this.tamY = p.getTamY();
        this.codigo = p.getCodigo();
    }

    public int getCodigo() {
        return codigo;
    }

    public int[][] getEspacio() {
        return espacio;
    }

    public int getTamX() {
        return tamX;
    }

    public int getTamY() {
        return tamY;
    }

    public List<Pieza> getRotaciones_Unicas() {
        List<Pieza> rotaciones = this.getRotaciones();
        List<Pieza> r_unicas = new ArrayList<>();
        for (int i = 0; i < rotaciones.size(); i++) {
            Pieza p1 = rotaciones.get(i);
            for (int j = i + 1; j < rotaciones.size(); j++) {
                Pieza p2 = rotaciones.get(j);
                if (equivalentes(p1, p2)) {
                    rotaciones.remove(p2);
                    j--;
                }
            }
            r_unicas.add(p1);
        }
        return r_unicas;
    }

    private boolean equivalentes(Pieza p1, Pieza p2) {
        if (p1.getTamX() == p2.getTamX() && p1.getTamY() == p2.getTamY()) {
            for (int x = 0; x < p1.getTamX(); x++) {
                for (int y = 0; y < p1.getTamY(); y++) {
                    if (p1.getEspacio()[x][y] != p2.getEspacio()[x][y]) {
                        return false;
                    }
                }
            }
            return true;
        } else {
            return false;
        }
    }

    private List<Pieza> getRotaciones() {
        List<Pieza> rotaciones = new ArrayList<>();
        Pieza p = this;
        for (int i = 0; i <= 3; i++) {
            Pieza p_inversa = getPiezaInversa(p);
            rotaciones.add(p_inversa);
            Pieza p_rotacion = getPiezaRotacion(p);
            rotaciones.add(p_rotacion);
            p = p_rotacion;
        }
        return rotaciones;
    }

    private Pieza getPiezaInversa(Pieza p) {
        int xT = p.getTamX();
        int yT = p.getTamY();
        int[][] inversa = new int[yT][xT];
        for (int x = 0; x < xT; x++) {
            for (int y = 0; y < yT; y++) {
                inversa[y][x] = p.espacio[x][y];
            }
        }
        return new Pieza(inversa, yT, xT, codigo);
    }

    private Pieza getPiezaRotacion(Pieza p) {
        int xT = p.getTamX();
        int yT = p.getTamY();
        int[][] rotacion = new int[yT][xT];
        for (int x = 0; x < xT; x++) {
            for (int y = 0; y < yT; y++) {
                rotacion[yT - y - 1][x] = p.espacio[x][y];
            }
        }
        return new Pieza(rotacion, yT, xT, codigo);
    }
    
    public void imprimir() {
        for (int i = 0; i < tamX; i++) {
            for (int j = 0; j < tamY; j++) {
                System.out.print(espacio[i][j]);
            }
            System.out.println("");
        }
        System.out.println("---");
    }
}
