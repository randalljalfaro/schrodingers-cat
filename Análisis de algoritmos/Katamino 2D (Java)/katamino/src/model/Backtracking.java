package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 *
 * @author bohr
 */
public class Backtracking {

    Stack<PosibleSolucion> pila;
    List<Pieza> pozo;
    PosibleSolucion solucion = null;
    PosibleSolucion ultimaSolucion = null;
    Stack<PosibleSolucion> pila_soluciones = new Stack<>();
    List<PosibleSolucion> lista_soluciones = new ArrayList<>();

    public Backtracking(Stack<PosibleSolucion> pila, List<Pieza> pozo) {
        this.pila = pila;
        this.pozo = pozo;
    }

    public PosibleSolucion buscarSolucion() {
        PosibleSolucion ps = new PosibleSolucion(new Matriz(0, 0));
        while (!pila.empty()) {
            ps = pila.pop();
            if (ps.esSolucion()) {
                //ESTOS ES PARA ELIMINAR RESPUESTAS REPETIDAS
                //NO SÉ PORQUÉ SE DAN
                if (ultimaSolucion == null) {
                    ultimaSolucion = ps;
                    lista_soluciones.add(ps);
                    return ultimaSolucion;
                } else if (esDistinto(ps)) {
                    ultimaSolucion = ps;
                    lista_soluciones.add(ps);
                    return ultimaSolucion;
                }
            } else {
                ps.pushHijosEnPila(pila);
            }
        }
        return ps;
    }

    private boolean esDistinto(PosibleSolucion ps) {
        return lista_soluciones.stream().filter(s -> {
            return s.getMatriz().compara(
                    ps.getMatriz().getMatriz(),
                    ps.getMatriz().getTamM(),
                    ps.getMatriz().getTamN());
        }).count()==0;
    }

    public void imprimirPila() {
        while (!pila.empty()) {
            System.out.println("******************");
            PosibleSolucion ps = pila.pop();
            ps.getMatriz().imprimir();
            System.out.println("******************");
        }
    }

    public void imprimirSoluciones() {
        int cR = 0;
        while (!pila_soluciones.empty()) {
            System.out.println("******************");
            PosibleSolucion ps = pila_soluciones.pop();
            //BORRA LAS REPETIDAS
            int r = 0;
            for (int i = 0; i < pila_soluciones.size(); i++) {
                PosibleSolucion psC = pila_soluciones.get(i);
                if (ps.getMatriz().compara(psC.getMatriz().getMatriz(),
                        psC.getMatriz().getTamM(), psC.getMatriz().getTamN())) {
                    pila_soluciones.remove(pila_soluciones.get(i));
                    r++;
                }
            }
            System.out.println("Repetidos: " + r);
            ps.getMatriz().imprimir();
            cR++;
            System.out.println("******************");
        }
        System.out.println("Cant. de respuestas netas: " + cR);
    }

    public Stack<PosibleSolucion> getPila_soluciones() {
        return pila_soluciones;
    }

    public void limpiar_repetidos() {
        int cR = 0;
        for (int x = 0; x < pila_soluciones.size(); x++) {
            System.out.println("******************");
            PosibleSolucion ps = pila_soluciones.get(x);
            //BORRA LAS REPETIDAS
            int r = 0;
            for (int i = 0; i < pila_soluciones.size(); i++) {
                PosibleSolucion psC = pila_soluciones.get(i);
                if (ps.getMatriz().compara(psC.getMatriz().getMatriz(),
                        psC.getMatriz().getTamM(), psC.getMatriz().getTamN())) {
                    pila_soluciones.remove(pila_soluciones.get(i));
                    r++;
                }
            }
            System.out.println("Repetidos: " + r);
            cR++;
            System.out.println("******************");
        }
        System.out.println("Cant. de respuestas netas: " + cR);
    }

}
