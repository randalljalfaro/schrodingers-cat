/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.Color;
import java.awt.Component;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import model.Matriz;
import model.PosibleSolucion;

/**
 *
 * @author bohr
 */
public final class P_Soluciones extends javax.swing.JFrame {

    /**
     * Creates new form P_Soluciones
     *
     * @param ps
     */
    List<Color> colors = Arrays.asList(
            Color.RED,
            Color.GREEN,
            Color.WHITE,
            Color.ORANGE,
            Color.MAGENTA,
            Color.BLUE,
            Color.BLACK,
            Color.PINK,
            Color.YELLOW,
            Color.CYAN
    );

    int[][] mCod;

    public class CellRenderer
            extends DefaultTableCellRenderer {

        public Component getTableCellRendererComponent(JTable table,
                Object value,
                boolean isSelected,
                boolean hasFocus,
                int row,
                int column) {
            Component c
                    = super.getTableCellRendererComponent(table, value,
                            isSelected, hasFocus,
                            row, column);

            c.setBackground(colors.get(mCod[row][column]));
            return c;
        }
    }

    public P_Soluciones() {
        initComponents();
        setTitle("Buscando una solución...");
        setVisible(true);
    }

    public void pintarRespuesta(PosibleSolucion ps) {
        setTitle("Solución");
        Matriz m = ps.getMatriz();
        mCod = m.getMatriz();
        String[] cols = new String[m.getTamM()];
        for (int col = 0; col < m.getTamM(); col++) {
            cols[col] = "";
        }
        Object[][] rows = new Object[m.getTamN()][m.getTamM()];
        for (int x = 0; x < m.getTamN(); x++) {
            for (int y = 0; y < m.getTamM(); y++) {
                rows[x][y] = m.getMatriz()[x][y];
            }
        }
        DefaultTableModel model = new DefaultTableModel(rows, cols);
        table_katamino.setModel(model);
        CellRenderer mtcr = new CellRenderer();
        mtcr.setHorizontalAlignment(SwingConstants.CENTER);
        table_katamino.setDefaultRenderer(Object.class, mtcr);
    }

    public JButton getbtnSiguiente() {
        return btnSiguiente;
    }

    public JButton getBtnCargarArchivo() {
        return btnCargarArchivo;
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        table_katamino = new javax.swing.JTable();
        btnSiguiente = new javax.swing.JButton();
        btnCargarArchivo = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        table_katamino.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        table_katamino.setEditingColumn(0);
        table_katamino.setEditingRow(0);
        table_katamino.setRowHeight(60);
        jScrollPane1.setViewportView(table_katamino);

        btnSiguiente.setText("Siguiente");

        btnCargarArchivo.setText("Cargar archivo");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnCargarArchivo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnSiguiente, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(25, 25, 25))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 466, Short.MAX_VALUE)
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 492, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSiguiente, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE)
                    .addComponent(btnCargarArchivo))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCargarArchivo;
    private javax.swing.JButton btnSiguiente;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable table_katamino;
    // End of variables declaration//GEN-END:variables
}
