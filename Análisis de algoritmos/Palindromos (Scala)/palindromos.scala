/*****************ALGORITMO*************************/
def matrizPalindromo(palabra:String) = {
	val pAlDerecho = "*"+palabra
	val pAlrevez = "*"+palabra.reverse
	
	
	def crearFila(y:Int, matriz:List[List[Int]]) = {
		(1 to pAlDerecho.size-1).toList.foldLeft(List(0)){
			(fila:List[Int], x:Int) =>
				(x, y) match {
					case (_,0) => (0)::fila
					case _ => revisarCaracter(x,y,matriz)::fila
				}
		}
	}
	
	def revisarCaracter(x:Int, y:Int, matriz:List[List[Int]]) = 
		(pAlDerecho(x), pAlrevez(y)) match {
			case (a,b) if (a.equals(b)) => {
				val filaAnterior = matriz(matriz.length-y-1)
				filaAnterior(filaAnterior.length-x)+1
			}
			case _ => 0
		}
		
	(0 to pAlrevez.size-1).toList.foldLeft(List(List(-1))){
		(matriz:List[List[Int]], y:Int) => crearFila(y,matriz)::matriz
	}
}
def esPalindromo(cadena:String):Boolean = {
	for (i <- 0 to cadena.length-1) {
		if (i<cadena.length-1-i && cadena(i)!=cadena(cadena.length-1-i)){
			//println("noesPalindromo>"+cadena+" : "+cadena(i)+"!="+cadena(cadena.length-1-i))
			return false
		}
	}
	//println("esPalindromo")
	return true
}
/***************************************************/

/*************IMPRESION_DE_RESPUESTA****************/
def imprimirPalindromos(palabra:String, matriz:List[List[Int]]) = {
	println("-------------------------------------")
	matriz.foreach(l => println(l))
	println("")
	println("Palindromos: ")
	matriz.foreach(
		fila => for (i <- 1 to fila.length-1; if fila(i) > 1)  yield {
			val subCadena = palabra.substring(i-fila(i), i)
			if (esPalindromo(subCadena)) println(subCadena)
		}
	)
	println("-------------------------------------")
}
/***************************************************/

/*****************EJECUCION*************************/
val msgSalida = "Digite 0 para salir o cualquier otra letra/n_mero para continuar: "
print(msgSalida)
while (Console.readLine()!="0"){
	print("Palabra a revisar: ")
	val palabra = Console.readLine
	imprimirPalindromos(
		palabra,
		matrizPalindromo(palabra).reverse.tail.map(l => l.reverse)
	)
	print(msgSalida)
}
/***************************************************/