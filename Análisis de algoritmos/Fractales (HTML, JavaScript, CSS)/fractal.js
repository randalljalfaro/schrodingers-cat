$(function() {
    //alert("FRACTALES");
    ejecutar_koch(0);
    init();
});
//********************************************************************************//
//KOCH
function ejecutar_koch(cantidad) {
    c = document.getElementById("graf_fractal_koch");
    ctx = document.getElementById("graf_fractal_koch").getContext("2d");
    //alert("ejecutar_koch");
	ctx.beginPath();
    koch(cantidad, 200.0, 220.0, 200.0, 900.0);
    koch(cantidad, 900.0, 220.0, 200.0, 220.0);
    koch(cantidad, 900.0, 900.0, 900.0, 220.0);
    koch(cantidad, 200.0, 900.0, 900.0, 900.0);
	ctx.closePath();
	ctx.stroke();
};
function koch(cant, x1_2, y1_2, x2_2, y2_2) {
    var sin60 = Math.sin(3.14 / 3.);
    var dx = (x2_2 - x1_2) / 3.;
    var dy = (y2_2 - y1_2) / 3.;
    var new_x = x1_2 + 3 * dx / 2. - dy * sin60;
    var new_y = y1_2 + 3 * dy / 2. + dx * sin60;
    //alert(cant);

    if (cant <= 0) {
        ctx.moveTo(parseInt(x1_2), parseInt(y1_2));
        ctx.lineTo(parseInt(x2_2), parseInt(y2_2));
        //ctx.stroke();
    } else {
        koch(cant - 1, x1_2, y1_2, x1_2 + dx, y1_2 + dy);
        koch(cant - 1, x1_2 + dx, y1_2 + dy, new_x, new_y);
        koch(cant - 1, new_x, new_y, x2_2 - dx, y2_2 - dy);
        koch(cant - 1, x2_2 - dx, y2_2 - dy, x2_2, y2_2);
    }
};
//********************************************************************************//
//PINE TREE
function ejecutar_pine_tree(cantidad, id){
	c = document.getElementById(id);
    ctx = document.getElementById(id).getContext("2d");
    //alert("ejecutar_koch");
	ctx.beginPath();
	//alert(cantidad);
	pine_tree(cantidad, 550, 1000, -90);
	ctx.closePath();
	ctx.stroke();
};
function pine_tree(cant, x1, y1, angle){
	if (cant != 0){
		var deg_to_rad = Math.PI / 180.0;
		var depth = 9;
		var x2 = x1 + (Math.cos(angle * deg_to_rad) * cant * 10.0);
		var y2 = y1 + (Math.sin(angle * deg_to_rad) * cant * 10.0);
		ctx.moveTo(x1, y1);
		ctx.lineTo(x2, y2);
		//alert(cant);
		pine_tree(cant - 1, x2, y2, angle - 20);
		pine_tree(cant - 1, x2, y2, angle + 20);
	}
};
//*******************************************************************************
//MANDRELBROT_SET
function ejecutar_mandelbrot_set(){
	var canvas = document.getElementById('graf_fractal_mandelbrot_set');
	var ctx = canvas.getContext('2d');
	var xr = ctx.canvas.width;
	var yr = ctx.canvas.height;
	//var maxIt = 256;
	ctx.putImageData(mandelbrot_set(xr, yr), 0, 0);
};

function mandelbrot_set(xr, yr){
	var imgd = ctx.createImageData(xr, yr);
	var pix = imgd.data;
	var xmin = -2.0; var xmax = 1.0;
	var ymin = -1.5; var ymax = 1.5;
	var x = 0.0; var y = 0.0;
	var zx = 0.0; var zx0 = 0.0; var zy = 0.0;
	var zx2 = 0.0; var zy2 = 0.0;

	for (var ky = 0; ky < yr; ky++)
	{
		y = ymin + (ymax - ymin) * ky / yr;
		for(var kx = 0; kx < xr; kx++)
		{
			x = xmin + (xmax - xmin) * kx / xr;
			zx = x; zy = y;
			for(var i = 0; i < maxIt; i++)
			{
				zx2 = zx * zx; zy2 = zy * zy;
				if(zx2 + zy2 > 4.0) break;
				zx0 = zx2 - zy2 + x;
				zy = 2.0 * zx * zy + y;
				zx = zx0;
			}
			
			var p = (xr * ky + kx) * 4;
			pix[p] = i ;
			pix[p + 1] = i;
			pix[p + 2] = i;
			pix[p + 3] = 255;
			
		}
	}
	return imgd;
}
//*******************************************************************************
function ejecutar_sierpinski(cantidad, id){
	canvas = document.getElementById(id);
	ctx = canvas.getContext('2d');
	var xr = canvas.width;
	var yr = canvas.height;
	
	var altTriang = parseInt(yr * Math.sqrt(3.0) / 2.0);
    var p1 = {'x':0, 'y':altTriang};
    var p2 = {'x':parseInt(yr/2), 'y':0};
    var p3 = {'x':yr, 'y':altTriang};
	
	ctx.beginPath();
    sierpinski(cantidad, p1, p2, p3);
	ctx.closePath();
	ctx.stroke();
};
function mediatriz(p1, p2){
	return {'x':(p1.x+p2.x)/2, 'y':(p1.y+p2.y)/2};
};
function sierpinski(level, p1, p2, p3){
	 if (level == 1) {
			//alert(p1.x+ " - "+p1.y);
			ctx.moveTo(p1.x, p1.y);
			ctx.lineTo(p2.x, p2.y);
			
			ctx.moveTo(p2.x, p2.y);
			ctx.lineTo(p3.x, p3.y);
			
			ctx.moveTo(p3.x, p3.y);
			ctx.lineTo(p1.x, p1.y);
        } else {
            var p4 = mediatriz(p1, p2);
            var p5 = mediatriz(p2, p3);
            var p6 = mediatriz(p1, p3);

            // recurse on 3 triangular areas
            sierpinski(level - 1, p1, p4, p6);
            sierpinski(level - 1, p4, p2, p5);
            sierpinski(level - 1, p6, p5, p3);
        }
};
//*******************************************************************************
function ejecutar_barnsley(cantidad, id){
	canvas = document.getElementById(id);
	ctx = canvas.getContext('2d');
	ctx.fillStyle = 'green';
	ctx.translate(ctx.canvas.width/2,ctx.canvas.height/2);
	barnsley(cantidad);
	//ctx.closePath();
	//ctx.stroke();
};


function barnsley(cant){
	var x = 0;
	var y = 0;
	for (var i=0;i<cant;i++) {
		var rand = Math.random()*100;
		if (rand < 1) {
			var xn = x;
			var yn = y;
			var zx = 0;
			var zy = 0.16 * yn;
			drawPoint(zx,zy);
		} else if (rand < 86) {
			var xn = x;
			var yn = y;
			var zx = 0.85 * xn + 0.04 * yn;
			var zy = -0.04 * xn + 0.85 * yn + 1.6;
			drawPoint(zx,zy);
		} else if (rand < 94) {
			var xn = x;
			var yn = y;
			var zx = 0.2 * xn - 0.26 * yn;
			var zy = 0.23 * xn + 0.22 * yn + 1.6;
			drawPoint(zx,zy);
		} else {
			var xn = x;
			var yn = y;
			var zx = -0.15 * xn + 0.28 * yn;
			var zy = 0.26 * xn + 0.24 * yn + 0.44;
			drawPoint(zx,zy);
		}
	}
	function drawPoint(xn,yn) {
		x = xn;
		y = yn;
		ctx.fillRect(xn*20,-yn*20,1,1);
	}
}
//*******************************************************************************
function init() {
	var fractales = [["Fractal de Koch", "fractal_koch"], ["Árbol fractal (Pino)", "fractal_pine_tree"],
		["Fractal de Mandelbrot", "fractal_mandelbrot_set"], ["Fractal de Sierpinski", "fractal_sierpinski"],
		["Fractal de Barnsley (helecho)","fractal_barnsley"]];
	$.each(fractales, function(i, v) {
        var option = $("<option></option>");
        option.text(v[0]).val(v[1]);
        $("#sel_fractal").append(option);
    });
	$("#sel_fractal").change(function() {
		$("section").addClass("invisible");
		$("#"+$("#sel_fractal").val()).removeClass("invisible");
	});
	
	
	//*********************************************************************
	//INIT SELECTS ITERACIONES
	
	eleccion([0, 1, 2, 3, 4, 5,6,7,8,9,10,11], "#sel_fractal_koch", "#lbl_fractal_koch",
		'#graf_fractal_koch', function() {
            ejecutar_koch($("#sel_fractal_koch").val());
            $("#lbl_fractal_koch").text("Graficado...");
        });
	
	//*********************************************************************
	eleccion([0, 1, 2, 3, 4, 5,6,7,8,9,10,11,12,13,14,15], "#sel_fractal_pine_tree", 
		"#lbl_fractal_pine_tree", '#graf_fractal_pine_tree', function() {
            ejecutar_pine_tree($("#sel_fractal_pine_tree").val(), "graf_fractal_pine_tree");
            $("#lbl_fractal_pine_tree").text("Graficado...");
        });
	
	//*********************************************************************
	var options = [];
	for(var i=200; i<=256; i++){
		options[i-200]=i;
	};
	eleccion(options, "#sel_fractal_mandelbrot_set", 
		"#lbl_fractal_mandelbrot_set", '#graf_fractal_mandelbrot_set', function() {
			maxIt = $("#sel_fractal_mandelbrot_set").val();
			ejecutar_mandelbrot_set();
            $("#lbl_fractal_mandelbrot_set").text("Graficado...");
        });
	
	//*********************************************************************
	eleccion([0, 1, 2, 3, 4, 5,6,7,8,9,10,11,12,13,14,15], "#sel_fractal_sierpinski", 
		"#lbl_fractal_sierpinski", '#graf_fractal_sierpinski', function() {
            ejecutar_sierpinski($("#sel_fractal_sierpinski").val(),'graf_fractal_sierpinski');
            $("#lbl_fractal_sierpinski").text("Graficado...");
        });
	
	//*********************************************************************
	eleccion([1000, 10000, 50000, 100000, 300000, 500000], "#sel_fractal_barnsley", 
		"#lbl_fractal_barnsley", '#graf_fractal_barnsley', function() {
            ejecutar_barnsley($("#sel_fractal_barnsley").val(),'graf_fractal_barnsley');
            $("#lbl_fractal_barnsley").text("Graficado...");
        });
	
	//*********************************************************************
	
	function eleccion(options, sel, lbl, graf, f){
		$.each(options, function(i, v) {
			var option = $("<option></option>");
			option.text(v).val(v);
			$(sel).append(option);
		});
		$(sel).change(function() {
			$(lbl).text("Graficando...");
			$(graf)[0].width = $(graf)[0].width;
			$(graf)[0].height = $(graf)[0].height;
			setTimeout(f, 25);
		});
	}
};