package control;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;

/**
 *
 * @author bohr
 */
public final class Logaritmo {

    private final double base;
    private final double argumento;
    private final static double error = (double) 0.002;
    DecimalFormat df = new DecimalFormat("#.#####");

    public Logaritmo(double base, double argumento) {
		System.out.println("Se realiza la búsqueda del lado negativo y positivo");
        this.base = base;
        this.argumento = argumento;
        printResult(calcular(true));
        printResult(calcular(false));
    }

    public Logaritmo() {
        this.base = 0;
        this.argumento = 0;
    }

    private void printResult(double c) {
        String result = "0.0";
        try {
            result = String.valueOf(df.format(c));
        } catch (NumberFormatException ex) {
            result = String.valueOf(c);
        }
        System.out.println("RESULTADO -> ln(" + argumento + ")"
                + "[base " + base + "] = " + result);
    }

    public double calcular(boolean signo) {
        //Inicia con la mitad del valor del argumento
        double logaritmo = argumento;
        int iteraciones;
        boolean encontrado = false;
        if (signo) {
            logaritmo *= -1;
        }
        double algoritmo_aux = 0;
        //System.out.println("ARGUMENTO : " + argumento);
        for (iteraciones = 0; !encontrado; iteraciones++) {
            //System.out.println("*********************************");
            //System.out.println("Logaritmo temporal -> " + logaritmo);
            algoritmo_aux = logaritmo;
            logaritmo = eval(logaritmo);
            //Si el valor del logaritmo no cambia es que ya se encuentra
            //dentro del rango con el margen de error
            if (algoritmo_aux == logaritmo) {
                encontrado = true;
            }
            //System.out.println("*********************************");
        }
        System.out.println("Iteraciones para encontrar respuesta: " + iteraciones);
        return algoritmo_aux;
    }
    public double eval(double logaritmo) {
        double potencia = Math.pow(base, logaritmo);
        if ((potencia >= argumento - error)
                && (potencia <= argumento + error)) {
            return logaritmo;
        } else {
            double result = 0;
            if (potencia < argumento - error) {
                result = logaritmo + logaritmo / 2;
            } else if (potencia > argumento + error) {
                result = logaritmo - logaritmo / 2;
            }
            //Devuelve un acercamiento ya que no entra en el rango
            //con los margenes de error
            return result;
        }
    }

    public static void main(String[] args) {
        String base = "";
        String argumento = "";
        String terminar = "";
		String msj = "Escriba el número '0' para terminar u "
                        + "cualquier otra tecla para continuar "
                        + "y la tecla 'ENTER'";
        BufferedReader bfr = new BufferedReader(
                new InputStreamReader(System.in)
        );
        while (!terminar.equals("0")) {
            try {
				System.out.println(msj);
				System.out.println("");
                System.out.println("Decimales usan puntos (ej 3.14)");
                System.out.println("lg(a)[base b] => ");
                System.out.println("a = ");
                argumento = bfr.readLine();
                System.out.println("b = ");
                base = bfr.readLine();
                new Logaritmo(
                        (double) Double.valueOf(base),
                        (double) Double.valueOf(argumento)
                );
                terminar = bfr.readLine();
                System.out.println("");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        //new Logaritmo();
    }

}
