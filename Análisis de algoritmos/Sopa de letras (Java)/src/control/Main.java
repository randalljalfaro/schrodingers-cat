package control;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import model.SopaDeLetras;
import view.P_Sopadeletras;

/**
 *
 * @author bohr
 */
public class Main {

    public static void main(String[] args) {
        BufferedReader in = null;
        try {
            in = new BufferedReader(new FileReader("sopas/sopadeletras3.txt"));
            String line = in.readLine();
            String x = "0";
            String y = "0";
            int nums;
            for (nums = 0; nums < line.length() && line.charAt(nums) != ' '; nums++) {
                x = x + line.charAt(nums);
                //this.matriz[j][i] = line.charAt(i);
            }
            for (nums = nums + 1; nums < line.length() && line.charAt(nums) != ' '; nums++) {
                y = y + line.charAt(nums);
                //this.matriz[j][i] = line.charAt(i);
            }

            //System.out.println(x+" - "+y);
            int xNum = Integer.valueOf(x);
            int yNum = Integer.valueOf(y);
            in.close();

            SopaDeLetras sopa = new SopaDeLetras(xNum, yNum);
            P_Sopadeletras p = new P_Sopadeletras(sopa);
            p.setVisible(true);
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

}
