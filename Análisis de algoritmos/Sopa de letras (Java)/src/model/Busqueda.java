/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author bohr
 */
public class Busqueda {

    private int[][] matrizRespuestas;
    private SopaDeLetras sopa;
    private ArrayList<ArrayList<List<Integer>>> respuestas;
    private int cont = 1;

    public Busqueda(SopaDeLetras sopa) {
        this.sopa = sopa;
        System.out.println(sopa.getY());
        this.matrizRespuestas = new int[sopa.getX()][sopa.getY()];
        for (int i = 0; i < sopa.getX(); i++) {
            for (int j = 0; j < sopa.getY(); j++) {
                matrizRespuestas[i][j] = 0;
            }
        }
        this.respuestas = new ArrayList<>();
    }

    private class Buscar extends Thread {

        String palabra;

        public Buscar(String palabra) {
            this.palabra = palabra;
        }

        @Override
        public void run() {
            for (int i = 0; i < sopa.getX(); i++) {
                for (int j = 0; j < sopa.getY(); j++) {
                    busquedaDiagonal(palabra, i, j,1,1);
                    busquedaDiagonal(palabra, i, j,-1,-1);
                    busquedaDiagonal(palabra, i, j,-1,1);
                    busquedaDiagonal(palabra, i, j,1,-1);
                    busquedaHorizontal(palabra, i, j);
                    busquedaVertical(palabra, i, j);
                }
            }
        }

    }

    public void buscar(String palabra) {
        new Buscar(palabra).start();
    }

    private void busquedaDiagonal(String palabra, int x, int y,
            int coefX, int coefY) {
        //System.out.println("Búsqueda diagonal: "+palabra);
        int pos = 0;
        ArrayList<List<Integer>> posicionPalabra = new ArrayList<>();
        for (int i = x, j = y;
                pos < palabra.length() && i >= 0 && j >= 0
                && i < sopa.getX() && j < sopa.getY();
                i++, j++, pos++) {
            //System.out.println("- "+palabra.charAt(pos)+ " - " + sopa.getMatriz()[i][j]);
            if (palabra.charAt(pos) != sopa.getMatriz()[i][j]) {
                pos = palabra.length() + 1;
            } else {
                Integer[] l = {i, j};
                posicionPalabra.add(Arrays.asList(l));
            }
        }

        if (pos == palabra.length()) {
            System.out.println("ENCONTRADA : " + palabra);
            respuestas.add(posicionPalabra);
            //System.out.println(matrizRespuestas.length);
            posicionPalabra.stream().forEach(l -> {
                //System.out.println("- "+l.get(0)+" / "+l.get(1)+" -");
                matrizRespuestas[l.get(0)][l.get(1)] = cont;
            });
            //cont++;
        }
    }

    private void busquedaHorizontal(String palabra, int x, int y) {
        //System.out.println("Búsqueda horizontal: "+palabra);
        int pos = 0;
        ArrayList<List<Integer>> posicionPalabra = new ArrayList<>();
        for (int j = y;
                pos < palabra.length() && j < sopa.getY();
                j++, pos++) {
            //System.out.println("- "+palabra.charAt(pos)+ " - " + sopa.getMatriz()[x][j]);
            if (palabra.charAt(pos) != sopa.getMatriz()[x][j]) {
                pos = palabra.length() + 1;
            } else {
                Integer[] l = {x, j};
                posicionPalabra.add(Arrays.asList(l));
            }
        }

        if (pos == palabra.length()) {
            System.out.println("ENCONTRADA : " + palabra);
            respuestas.add(posicionPalabra);
            posicionPalabra.stream().forEach(l -> {
                //System.out.println("- "+l.get(0)+" / "+l.get(1)+" -");
                matrizRespuestas[l.get(0)][l.get(1)] = cont;
            });
            //cont++;
        }
    }

    private void busquedaVertical(String palabra, int x, int y) {
        //System.out.println("Búsqueda horizontal: "+palabra);
        int pos = 0;
        ArrayList<List<Integer>> posicionPalabra = new ArrayList<>();
        for (int i = x;
                pos < palabra.length() && i < sopa.getX();
                i++, pos++) {
            //System.out.println("- "+palabra.charAt(pos)+ " - " + sopa.getMatriz()[x][j]);
            if (palabra.charAt(pos) != sopa.getMatriz()[i][y]) {
                pos = palabra.length() + 1;
            } else {
                Integer[] l = {i, y};
                posicionPalabra.add(Arrays.asList(l));
            }
        }

        if (pos == palabra.length()) {
            System.out.println("ENCONTRADA : " + palabra);
            respuestas.add(posicionPalabra);
            posicionPalabra.stream().forEach(l -> {
                //System.out.println("- "+l.get(0)+" / "+l.get(1)+" -");
                matrizRespuestas[l.get(0)][l.get(1)] = cont;
            });
            //cont++;
        }
    }

    public ArrayList<ArrayList<List<Integer>>> getRespuestas() {
        return respuestas;
    }

    public int[][] getMatrizRespuestas() {
        return matrizRespuestas;
    }

}
