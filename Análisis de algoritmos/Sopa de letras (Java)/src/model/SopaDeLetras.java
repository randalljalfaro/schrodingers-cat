/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author bohr
 */
public class SopaDeLetras {

    private char[][] matriz;
    private int x;
    private int y;
    private ArrayList<String> palabras;
    private Busqueda busqueda;

    public SopaDeLetras(int x, int y) {
        matriz = new char[x][y];
        palabras = new ArrayList<>();
        this.x = x;
        this.y = y;
        busqueda = new Busqueda(this);
        initMatriz();
        cargarArchivo();
        buscarPalabras();
    }

    private void initMatriz() {
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                matriz[i][j] = ' ';
            }
        }
    }

    public final void cargarArchivo() {
        BufferedReader in = null;
        try {
            in = new BufferedReader(new FileReader("sopas/sopadeletras3.txt"));
            String line = in.readLine();
            int j = 0;
            while ((line = in.readLine()) != null && !"".equals(line)) {
                for (int i = 0; i < line.length(); i++) {
                    this.matriz[j][i] = line.charAt(i);
                }
                j++;
            }
            while ((line = in.readLine()) != null && !"".equals(line)) {
                palabras.add(line);
                j++;
            }
            in.close();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void buscarPalabras() {
        palabras.stream().forEach(p -> {
            busqueda.buscar(p);
        });
    }

    public char[][] getMatriz() {
        return matriz;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int[][] getRespuestasBusqueda() {
        return busqueda.getMatrizRespuestas();
    }

}
