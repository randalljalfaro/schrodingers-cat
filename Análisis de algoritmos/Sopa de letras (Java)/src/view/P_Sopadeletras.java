/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.Color;
import java.awt.Component;
import java.util.Arrays;
import java.util.List;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import model.SopaDeLetras;

/**
 *
 * @author bohr
 */
public final class P_Sopadeletras extends javax.swing.JFrame {
    
    SopaDeLetras sopa;
    public P_Sopadeletras(SopaDeLetras sopa) {
        initComponents();
        this.sopa = sopa;
        pintarSopa(sopa);
    }

    public void pintarSopa(SopaDeLetras sopa) {
        char[][] m = sopa.getMatriz();
        String[] cols = new String[sopa.getY()];
        for (int col = 0; col < sopa.getY(); col++) {
            cols[col] = "";
        }
        Object[][] rows = new Object[sopa.getX()][sopa.getY()];
        for (int x = 0; x < sopa.getX(); x++) {
            for (int y = 0; y < sopa.getY(); y++) {
                rows[x][y] = m[x][y];
            }
        }

        DefaultTableModel model = new DefaultTableModel(rows, cols);
        table_sopa.setModel(model);
        CellRenderer mtcr = new CellRenderer();
        mtcr.setHorizontalAlignment(SwingConstants.CENTER);
        table_sopa.setDefaultRenderer(Object.class, mtcr);
    }
    public class CellRenderer
            extends DefaultTableCellRenderer {

        public Component getTableCellRendererComponent(JTable table,
                Object value,
                boolean isSelected,
                boolean hasFocus,
                int row,
                int column) {
            Component c
                    = super.getTableCellRendererComponent(table, value,
                            isSelected, hasFocus,
                            row, column);
            c.setBackground(colors.get(sopa.getRespuestasBusqueda()[row][column]));
            return c;
        }
    }
    List<Color> colors = Arrays.asList(
            Color.WHITE,
            Color.GREEN,
            Color.YELLOW,
            Color.ORANGE,
            Color.MAGENTA,
            Color.BLUE,
            Color.PINK,
            Color.CYAN,
            Color.BLACK
    );

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        table_sopa = new javax.swing.JTable();
        btnSubirSopa = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        table_sopa.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        table_sopa.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        table_sopa.setEditingColumn(0);
        table_sopa.setEditingRow(0);
        table_sopa.setIntercellSpacing(new java.awt.Dimension(0, 0));
        table_sopa.setRowHeight(60);
        jScrollPane1.setViewportView(table_sopa);

        btnSubirSopa.setText("Subir sopa de letras");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1175, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnSubirSopa)
                        .addGap(25, 25, 25))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 636, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnSubirSopa, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSubirSopa;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable table_sopa;
    // End of variables declaration//GEN-END:variables
}
