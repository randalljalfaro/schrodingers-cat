
def movidas(k:Int, n:Int, m:Int) = 
	(1 to k).toList.foldLeft(List("INICIO")){
		(jug:List[String], num:Int) =>
			num match {
				case 1 			=> ("g")::jug
				case x if (x<n) => evaluar_movida(List(x-1), jug)::jug
				case `n`		=> ("g")::jug
				case x if (x<m) => evaluar_movida(List(x-1,x-n), jug)::jug
				case `m`		=> ("g")::jug
				case x 			=> evaluar_movida(List(x-1,x-n,x-m), jug)::jug
			}
	}


def evaluar_movida(pos:List[Int], mov:List[String]):String ={
	def compara(r:String, p:String)={
		if (r.equals("p")) "p"
		else if (p=="g") "p" else "g"
	}
	
	pos.foldLeft(""){
		(r:String, p:Int) =>  compara(r, mov(mov.size-p-1))
	}
}
/****************************************************/


/*****************EJECUCION*************************/
val msgSalida = "Digite 0 para terminar o cualquier otro num. para continuar: "
print(msgSalida)
while (Console.readInt()!=0){
	print("\nCantidad total de tablas: ")
	val cant = Console.readInt()
	println("\n Cantidad de tablas que se pueden sacar")
	print("Cantidad 1: ")
	val n = Console.readInt()
	print("Cantidad 2: ")
	val m = Console.readInt()
	val algoritmoTablas = movidas(cant, n, m)
	
	/*****************IMPRESION DE EJECUCIÓN************/
	val mov = algoritmoTablas.reverse.tail
	println(respuestaEjecucion(mov))
	val primerMov = if (mov(mov.size-1).equals("g")) "GANA" else "PIERDE"
	println("\n****************************************\n" +
			"Si juega de manera óptima en cada móvida,\n" +
			"el que mueva primero " + primerMov +
			"\n****************************************\n")
	/****************************************************/
	
	print(msgSalida)
}
/****************************************************/


/**********FUNCION_IMPRESION_EJECUCION**************/
def respuestaEjecucion(l:List[String]) = l.foldLeft((0,"(")){
		(s:(Int,String), j:String) => {
			val conteo = s._1.+(1)
			(conteo,s._2+"{"+(conteo)+" "+j+"} , ")
		}
		
	}._2+")"
/****************************************************/






