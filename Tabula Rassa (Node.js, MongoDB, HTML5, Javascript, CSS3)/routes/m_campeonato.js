var mongoTeams;
try{
   mongoTeams = require('../model/teams.js');
} catch(e){
   // Por ejemplo si la DB no esta disponible
   console.log(e);
   mongoTeams ={};
};

function allTeams(action){
	mongoTeams.done = function(){
		action.resp.sendJson(mongoTeams.getAllTeams());
	};
    mongoTeams.db();
}
exports.allTeams = allTeams;

/************************************************************/

var mongoCampeonatos;
try{
   mongoCampeonatos = require('../model/campeonatos.js');
} catch(e){
   // Por ejemplo si la DB no esta disponible
   console.log(e);
   mongoCampeonatos ={};
};

function nuevoCampeonato(action){
	var req = action.req;
	var resp = action.resp;
	
	req._req.on('end', function() {
		mongoCampeonatos.done = function(){
			mongoCampeonatos.nuevoCampeonato(
				req.body, resp
			);
		}
		mongoCampeonatos.db();
	});
}
exports.nuevoCampeonato = nuevoCampeonato;

function allCampeonatos(action){
	mongoCampeonatos.done = function(){
		action.resp.sendJson(mongoCampeonatos.getAllCampeonatos());
	};
    mongoCampeonatos.db();
}
exports.allCampeonatos = allCampeonatos;