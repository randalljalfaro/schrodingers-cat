//Base de datos
//Tabla de campeonato
//Por el momento (campeonatos, jornadas, equipo)

var MongoClient = require('mongodb').MongoClient;
var dbName = "campeonato_nacional";
var server = "localhost";
var mongoPort = "27017";
var connectionString = "mongodb://"+server+":"+mongoPort+"/"+dbName;
var collectionName = "equipos";

var dbConnection = function(obj){
  try{
     obj.client.connect(connectionString, function(err, db){
	 if(!err && db) obj._obj.db = db;
	 else obj.error = err;
	 obj.db_done();
	});
  }catch(e){
    throw e;
  };
};

var loadTeamsCollection = function(obj){
  try{
    obj._obj.db.collection(collectionName, function(err, teams) {
	  if(!err) obj._obj.teams = teams;
	  else obj.error = err;
	  obj.collect_done();
	});
  } catch(e){
    throw e;
  }
};


module.exports={
				client:MongoClient,
                db:function(){
				    dbConnection(this);
				},
                error:null, 
				done:null,
                _obj:{
					db:null,    // objeto de conexion a la db
                    teams:null, // objeto de acceso a colleccion
                    array:[]	  // array con toda la coleccion			  
			    },
				db_done: function(){
				   //console.log("Db done ");
				   if(this.error){
				     this.errorHandler();
				     return;
				   };
				   loadTeamsCollection(this);
				},
				collect_done:function(){
				   if(this.error){
				     this.errorHandler();
				     return;
				   };
				   // Consulta todos y los fuerza en un array
				   this.asArray();
				},
				asArray: function(){ 
				    _this = this;
				    if(this._obj.teams){
					    this._obj.teams.find().toArray(function(err, array){
							if(err) throw err;
							_this._obj.array = array;
							/*
							Aqui termina la coneccion y ejecuta la funcion 
							que se ha seteado desde afuera
							*/
							_this.done();
					    });
					};
				},
				//QUERIES
				getAllTeams:function(){
				   return this._obj.array;
				}
				
};