//Base de datos
//Tabla de campeonato
//Por el momento (campeonatos, jornadas, equipo)

var MongoClient = require('mongodb').MongoClient;
var dbName = "campeonato_nacional";
var server = "localhost";
var mongoPort = "27017";
var connectionString = "mongodb://"+server+":"+mongoPort+"/"+dbName;
var collectionName = "campeonatos";

var dbConnection = function(obj){
  try{
     obj.client.connect(connectionString, function(err, db){
	 if(!err && db) obj._obj.db = db;
	 else obj.error = err;
	 obj.db_done();
	});
  }catch(e){
    throw e;
  };
};

var loadCampeonatosCollection = function(obj){
  try{
    obj._obj.db.collection(collectionName, function(err, campeonatos) {
	  if(!err) obj._obj.campeonatos = campeonatos;
	  else obj.error = err;
	  obj.collect_done();
	});
  } catch(e){
    throw e;
  }
};


module.exports={
				client:MongoClient,
                db:function(){
				    dbConnection(this);
				},
                error:null, 
				done:null,
                _obj:{
					db:null,    // objeto de conexion a la db
                    campeonatos:null, // objeto de acceso a colleccion
                    array:[]	  // array con toda la coleccion			  
			    },
				db_done: function(){
				   //console.log("Db done ");
				   if(this.error){
				     this.errorHandler();
				     return;
				   };
				   loadCampeonatosCollection(this);
				},
				collect_done:function(){
				   if(this.error){
				     this.errorHandler();
				     return;
				   };
				   // Consulta todos y los fuerza en un array
				   this.asArray();
				},
				asArray: function(){ 
				    _this = this;
				    if(this._obj.campeonatos){
					    this._obj.campeonatos.find().toArray(function(err, array){
							if(err) throw err;
							_this._obj.array = array;
							/*
							Aqui termina la coneccion y ejecuta la funcion 
							que se ha seteado desde afuera
							*/
							_this.done();
					    });
					};
				},
				//QUERIES
				getAllCampeonatos:function(){
				   return this._obj.array;
				},
				nuevoCampeonato : function(campeonato, resp){
					function uniqueID(c){
						if(c.periodo && c.a_o) {
							return (c.periodo+"_"+c.a_o);
						}
						return false;
					}
					//-------------------------------------------
					function validation(campeonato){
						campeonato._id = uniqueID(campeonato);
						return uniqueID(campeonato);
					}
					//-------------------------------------------
					if(validation(campeonato)){
						this.save(this._obj.campeonatos, campeonato, resp)
					}
					else resp.sendJson({_id:null, error:err});
				},
				
				//BASIC QUERIES
				save : function (collection, document, resp){
					if(collection){
					   collection.save(document,function(err, records){
							if(err) {
								resp.sendJson({_id:null, error:err});
								throw err;
							}
							else{
								//Si no se agrego un _ID, y se provee en records['_id']
								if(records['_id']) resp.sendJson({_id:records['_id']});
								//Si se agrego un propio _ID, viene en el documento
								else resp.sendJson({_id:document._id});
							}
					   });
					};
				}
				
};