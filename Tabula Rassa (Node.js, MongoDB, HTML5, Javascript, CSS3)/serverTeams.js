time = require("framework_time");
viewCampeonato = require("./routes/v_campeonato.js");
modelCampeonato = require("./routes/m_campeonato.js");

/*
La aplicacion es un servidor http.
Tiene un router, que maneja
el path y control
*/
app = time();

//Configuracion de la aplicacion

//Donde buscar los views y el contenido estatico (images/css/js)
app.set('views', __dirname + '/views');
app.set('static', __dirname + '/public');

//Enrutamiento
app.route.get('/tabla_rassa.html', viewCampeonato.tabla_rassa);

app.route.get('/equipos', modelCampeonato.allTeams);

app.route.get('/campeonatos', modelCampeonato.allCampeonatos);
app.route.post('/nuevoCampeonato', modelCampeonato.nuevoCampeonato);
//app.route.post('/eliminarCampeonato', modelCampeonato.eliminarCampeonato);
//app.route.get('/search/equipo', teams.getTeam);

app.listenHttp(8888);
