/*****************Controller***************************/

function Controller(model, view){
	this.model = model;
	this.view = view;
	var _this=this;
	
	//Escuchar cambios desde la vista
	this.view.dialog.teamUpdateView.attach(function (sender,args) {
        _this.updateEquipo(args.eq1,args.golesEq1,args.eq2,args.golesEq2);
    });
}

Controller.prototype = {
	updateEquipo : function(team1,sc1,team2,sc2){
		//Update del model
		var t = model.teamsList;
		t.updateEquipo(t.getIndexEquipo(team1),sc1,sc2);
		t.updateEquipo(t.getIndexEquipo(team2),sc2,sc1);
		t.detectChanges();
	}
}
