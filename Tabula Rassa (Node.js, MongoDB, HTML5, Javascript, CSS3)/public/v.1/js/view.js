/*****************VIEW***************************/
//background-image: url('../images/fondoEstadio.jpeg');
function View(model){
	this.tabla = new TablaEquipos(model.teamsList);
	this.dialog = new Dialog();
	this.panelViewTabs = $("#panel_view");
	this.functionsMenu = $("#functions_menu");
	this.panelViewTabs.tabs();
	this.functionsMenu.menu();
}
View.prototype ={
	showTabla : function(){
		this.tabla.showTablaCampeonato();
	},
	showDialog : function(eq1, eq2){
		this.dialog.agregarMarcador(eq1,eq2);
	}
}

//Componentes visuales
/****************Tabla****************************/
function TablaEquipos(teamsList){
	this.teamsList = teamsList;
	this.table = $('#table_equipos');
	//Escuchar cambios del model
	var _this = this;
	this.teamsList.teamUpdate.attach(function(sender,args){
		_this.updateRowEquipo(args.teamIndex, args.team);
	});
}
TablaEquipos.prototype = {
	showTablaCampeonato : function(){
		this.limpiartbody();
		this.table.removeClass('notRender');
		this.llenarTabla(this.teamsList.teams);
		$('#panel_view_table').append(this.table);
	},
	llenarTabla : function(){
		var _this = this;
		forEachIndex([0,1,2,3], function(i){
			_this.insertEquipointoRow(i, $('#tbody_equipos'),
									_this.teamsList.teams[i].toArray());
		});
		/*forEachIndex(_this.teamsList.teams, function(i){
			_this.insertEquipointoRow(i, $('#tbody_equipos'),
									_this.teamsList.teams[i].toArray());
		});*/
	},
	insertEquipointoRow : function (rowIndex, tb, infoEquipo){ 
		var changeIndex=0; var teamIndex=1; var ptIndex=9;
		function teamCell(c, team){
			//eleccionDeElemento(c);
			c.addClass('class', 't_equipo');
			c.text(team);
		}
		function posCell(c,pos){
			var divClass = 't_pos_real ';
			if(pos<=4) divClass += 'area_clasificacion ';
			divClass += 'roundBorder';
			c.html("<div class='" + divClass + "'>" + pos + "</div>");
			c.attr('class', 't_num_pos');
		}
		function changeCell(c,change){
			if(change == "up")
				c.html("<img src = 'images/up.gif' class='image'>");
			else if(change == "down")
				c.html("<img src = 'images/down.png' class='image'>");
			else c.html("&nbsp");
		}
		function ptsCell(c,pts){
			cell.attr('class', 't_pts');
			cell.text(pts);
		}
		
		var tr = $('<tr></tr>').attr('id', rowIndex);
		var cell = $('<td></td>');
		posCell(cell, rowIndex+1);
		tr.append(cell);
		for(var j=1; j<=infoEquipo.length; j++){
			var cell = $('<td></td>');
			var posInfo =j-1;
			
			if(posInfo == teamIndex) teamCell(cell, infoEquipo[posInfo]);
			else if(posInfo==ptIndex) ptsCell(cell, infoEquipo[posInfo]);
			else if(posInfo==changeIndex) changeCell(cell,infoEquipo[posInfo]);
			else cell.text(infoEquipo[posInfo]);
			
			tr.append(cell);
		}
		$('#tbody_equipos').append(tr);
	},
	updateRowEquipo : function (rowIndex,teamObj){
		var tbody= document.getElementById("tbody_equipos");
		var id="#"+rowIndex;
		var f=this.insertEquipointoRow;
	
		$(id).fadeOut(2000,function(){
			tbody.deleteRow(rowIndex);
			$(id).fadeOut();
			f(rowIndex,tbody.insertRow(rowIndex), teamObj.toArray());
			$(id).fadeIn(2000);
		});
	},
	limpiartbody : function(){
		var tbody= document.getElementById("tbody_equipos");
		for(;tbody.childElementCount;)	 
		   tbody.removeChild(tbody.children[0]);
	}
}

/****************Dialogo****************************/
function Dialog(){
	this.teamUpdateView = new Event(this);
	this.init();
	this.llenarSelectGoles();
	
}
Dialog.prototype = {
	agregarMarcador : function(eq1,eq2){
		$("#lbl_eq1").text(eq1);
		$("#lbl_eq2").text(eq2);
		$( "#dialog" ).dialog("open");
	},
	resetMarcador : function (){
		document.getElementById("eq1_id").options[0].selected = true;
		document.getElementById("eq2_id").options[0].selected = true;
	},
	llenarSelectGoles : function (){
		var eq1Goles = document.getElementById("eq1_id");
		var eq2Goles = document.getElementById("eq2_id");
		for(i=0;i<=10;i++){
			var option="<option value='0'>"+i+"</option>";
			eq1Goles.innerHTML += option;
			eq2Goles.innerHTML += option;
		}
	},
	init : function(){
		var resetSel = resetSelection;
		var resetMarc = this.resetMarcador;
		var eventUpdate = this.teamUpdateView;
		$("#dialog").dialog({
			autoOpen: false,
			height:250,
			width: 350,
			modal: true,
			buttons: {
				"Accept":function(){
					var selectGolesEq1 = document.getElementById("eq1_id");	
					var selectGolesEq2 = document.getElementById("eq2_id");
					var golesEq1=selectGolesEq1.options[selectGolesEq1.selectedIndex].text;
					var golesEq2=selectGolesEq2.options[selectGolesEq2.selectedIndex].text;
					var eq1 = document.getElementById("lbl_eq1").innerHTML;
					var eq2 = document.getElementById("lbl_eq2").innerHTML;
					//Notificar cambio al model
					eventUpdate.notify({ eq1 : eq1, golesEq1 : golesEq1,
										eq2 : eq2, golesEq2 : golesEq2 });
					$( this ).dialog("close");
				},
				"Reset": function(){
					resetMarc();
				},
				"Cancel": function() {
					$( this ).dialog("close");
				}
			},
			close: function() {
				resetSel();
				resetMarc();
				$( this ).dialog( "close" );
			},
			open:function() {
			}
		});
	}
}
