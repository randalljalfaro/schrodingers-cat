
var cantEquipos = 0;
var equiposElegidos = new Array();

function resetSelection(){
	if(equiposElegidos.length>0){
		equiposElegidos[0].classList.remove('selectedTeam');
		equiposElegidos[1].classList.remove('selectedTeam');
		cantEquipos=0;
		equiposElegidos = new Array();
	}
}

function countIfNotSelected(element){
	element.classList.add('selectedTeam');
	equiposElegidos[cantEquipos]=element;
	cantEquipos++;	
	if(equiposElegidos.length==2){
		if(equiposElegidos[0].innerHTML==equiposElegidos[1].innerHTML){
			cantEquipos--;
		}
	}
}

