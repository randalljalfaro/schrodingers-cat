/*****************MODEL***************************/
function Model(){
	this.teamsList = new TeamsList(getDataEquipos());
}

//Contenedor de 'objetos' Team
function TeamsList(jsonEquipos){
	this.teams = this.toTeamArray(jsonEquipos.results.teams);
	this.teamsPos = [];
	this.teamUpdate = new Event(this);
	this.teams.sort();
	this.fillActualPos();
}
TeamsList.prototype ={
	sort : function (){
		var t = this.teams;
		forEachIndex(t, function(i){
			for(j=i; j< t.length; j++){
				if(t[j].pts > t[i].pts){
					var aux=t[i];
					t[i]=t[j];
					t[j]=aux;
				}
			}
			t[i].index = i;
		});
	},
	fillActualPos : function(){
		var _this = this;
		this.teamsPos = [];
		forEachIndex(_this.teams, function(i){
			_this.teamsPos.push([i, _this.teams[i].team, _this.teams[i].pj]);
		});
	},
	getOldIndexEquipo : function(team){
		var _this = this;
		var index = -1;
		forEachIndex(_this.teamsPos, function(i){
			if(_this.teamsPos[i][1]==team) index=i;
		});
		return index;
	},
	detectChanges : function(){
		var _this = this;
		var teamChanged = [];
		this.sort();
		
		//Busca los equipos que cambiaron de posicion
		forEachIndex(_this.teams, function(i){
			//Equipos puede subir o bajar de posición
			if(_this.teams[i].team != _this.teamsPos[i][1]) {
				var oldIndex = _this.getOldIndexEquipo(_this.teams[i].team);
				if(i > oldIndex) _this.teams[i].change = "down";
				else if(i < oldIndex) _this.teams[i].change = "up";
				teamChanged.push([i, _this.teams[i]]);
			}
			//Equipos pueden estar en la misma pos, pero con más partidos
			else if(_this.teams[i].pj != _this.teamsPos[i][2]  )
					teamChanged.push([i, _this.teams[i]]);
			//Equipos que no cambiaron pero tenían up - down
			else if(_this.teams[i].change!=""){
					_this.teams[i].change = "";
					teamChanged.push([i, _this.teams[i]]);
			}
		});
		
		//Notifica a la vista sobre equipos que cambiaron de pos
		function notify(i, team){
			_this.teamUpdate.notify({ teamIndex : i,
										team : team});
		}
		forEachIndex( teamChanged, function(i){
			notify(teamChanged[i][0], teamChanged[i][1]);
		});
		//Reset de la lista que lleva registro de la 
		//lista antes de algun cambio
		this.fillActualPos();
	},
	//Covertidor de un Array de 'team' a uno del 'objeto Team'
	toTeamArray : function (teams){
		if(!teams){alert("toArrayTeam(teams): teams=>Sin definir");}
		forEachIndex(teams, function(i){
			var t = teams[i];
			teams[i]=new Team(t.team,t.won, t.tie, t.lost, t.gf, t.gc);
		});
		return teams;
	},
	updateEquipo : function(teamIndex,gol1,gol2){
		var t = this.teams;
		var _this = this;
		forEachIndex(t, function(i){
			if(i==teamIndex){
				t[i].updatewithScore(gol1,gol2);
				i=t.length;
			}
		});
	},
	getIndexEquipo : function(name){
		var index=-1;
		var t = this.teams;
		forEachIndex(t, function(i){
			if(t[i].team==name) {
				index=i;
				i=t.length;
			}
		});
		return index;
	},
	getEquipobyIndex : function(j){
		var team= new Team("",0,0,0,0,0);
		var t = this.teams;
		forEachIndex(t, function(i){
			if(i==j) {
				team=t[i];
				i=t.length;
			}
		});
		return team;
	}
}

//Objeto Team
function Team(name,pg,pe,pp,gf,gc){
	this.team=name;
	this.won=pg; 
	this.tie=pe; 
	this.lost=pp; 
	this.gf=gf; 
	this.gc=gc;
	this.dg=signedNumber(parseInt(this.gf)-parseInt(this.gc));
	this.pj = parseInt(this.won)+parseInt(this.tie)+parseInt(this.lost);
	this.pts = parseInt(this.won*3) + parseInt(this.tie);
	this.change="";
}
Team.prototype = {
	toArray : function(){
		return [this.change, this.team, this.pj, this.won, 
			this.tie, this.lost, this.gf, this.gc, this.dg, this.pts];
	},
	updatewithScore : function(gol1,gol2){
		if(gol1>gol2) this.won=this.won+1;
		else if(gol1<gol2) this.lost=this.lost+1;
		else if(gol1==gol2) this.tie=this.tie+1;
		this.gf=parseInt(this.gf)+parseInt(gol1);
		this.gc=parseInt(this.gc)+parseInt(gol2);
		this.dg=signedNumber(parseInt(this.gf)-parseInt(this.gc));
		this.pj = parseInt(this.won)+parseInt(this.tie)+parseInt(this.lost);
		this.pts = parseInt(this.won*3) + parseInt(this.tie);
	}
}

//Traer los equipos en un json
var JSON_DATA_URL="json/tabla_campeonato.json";
//var JSON_DATA_URL="json/equiposMayorOchenta.json";
function getDataEquipos(){
	return loadJSONData(JSON_DATA_URL, function(){ 
		return {results:{title:"Error Data Not Available"},teams:[]};
	});
}

