//Array con el modelo de equipos
window.onload=function(){
	//Model
	model=new Model();
	
	//View
	view = new View(model);
	//view.showTabla();
	//Controller
	control = new Controller(model,view);
};
//Patrón observador para los eventos
function Event(sender) {
    this._sender = sender;
    this._listeners = [];
}
Event.prototype = {
    attach : function (listener) {
        this._listeners.push(listener);
    },
    notify : function (args) {
        var index;

        for (index = 0; index < this._listeners.length; index += 1) {
            this._listeners[index](this._sender, args);
        }
    }
};

//SELECCION DE EQUIPOS PARA UPDATE
function eleccionDeElemento(element){
	element.onclick = function(){
		countIfNotSelected(element);
		if(cantEquipos==2)
			view.showDialog(equiposElegidos[0].innerHTML,
						equiposElegidos[1].innerHTML);
	}
}