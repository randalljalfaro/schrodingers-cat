function loadJSONData(url, handler){
    var http_request = new XMLHttpRequest();
	http_request.open("GET", url, false);
	try{
	  http_request.send();
	}catch(e){
		 return handler(e);
	}
	return JSON.parse(http_request.responseText);
}
