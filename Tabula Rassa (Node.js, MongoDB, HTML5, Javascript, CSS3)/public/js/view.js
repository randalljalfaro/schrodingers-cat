/*****************VIEW***************************/
function View(){
	this.functMenu = $("#funciones_tabs");
	this.tabEquipos = $("#tabEquipos");
	this.panelView = new PanelView();
	this.dialog = new Dialog();
	this.init();
}

View.prototype = {
	init : function(){
		this.functMenu.menu();
		this.tabMenu();
	 },
	tabMenu : function(){
		var campTab = $("div#wrapper_tabs > ul > li");
		campTab.hover(
			function () { 
				$('ul', this).fadeIn();
			},
			function () { 
				$('ul', this).fadeOut();
			}
		);
	},
	fillTeamsTab : function(equipos){
		var nombreEquiposTab = $("<ul></ul>");
		$.each(equipos, function(_id, equipo){
			nombreEquiposTab.append("<li><a>"+_id+"</a></li>");
		});
		this.tabEquipos.append(nombreEquiposTab);
	},
	dialogCampeonato : function(nuevoCampEvent, opc_periodos, opc_rango){
		var dialog = this.dialog;
		dialog.title("Crear campeonatos");
		var idDedicado = "dedicado"; 
		var idPeriodo = "periodo";
		var idA_o = "a_o";
		//---------------------------------------------------------
		var periodos = dialog.select(idPeriodo);
		$.each(opc_periodos, function(i, p){
				periodos.append(dialog.option(p));
			}
		);
		//---------------------------------------------------------
		var inDedicado = dialog.input(idDedicado);
		//--------------------------------------------------------
		var rangoA_os = dialog.input(idA_o).attr('readonly',true);
		//---------------------------------------------------------
		var dialogComponents = [dialog.label("Año: "), rangoA_os,
		dialog.label("Dedicado: "), inDedicado,
		dialog.label("Período: "), periodos];
		dialog.fill(dialogComponents);
		//---------------------------------------------------------
		rangoA_os.spinner();
		rangoA_os.spinner({max: opc_rango.max, min: opc_rango.min});
		rangoA_os.spinner('value' , opc_rango.min);
		//---------------------------------------------------------
		dialog.init({
			buttons: {
				"Crear": function() {
					nuevoCampEvent.trigger('nuevoCampeonato',{
						dedicado : $("#"+idDedicado).val(),
						periodo : $("#"+idPeriodo).val(),
						a_o : $("#"+idA_o).spinner('value')
					});
					$( this ).dialog( "close" );
				},
				Cancel: function() {
					$( this ).dialog( "close" );
				}
			}
		});
		
	},
	dialogGeneraJornada : function(genJornadaEvent, opc_campeonatos){
		var dialog = this.dialog;
		dialog.title("Generar jornada");
		var idCamp = "campeonatos";
		/*---------------------------------------------------------*/
		var campeonatos = dialog.select(idCamp);
		$.each(opc_campeonatos, function(i, c){
				var text = c.periodo + " - " + c.a_o;
				campeonatos.append(dialog.option(text, i));
			}
		);
		/*---------------------------------------------------------*/
		dialog.fill([dialog.label("Campeonato: "),campeonatos]);
		/*---------------------------------------------------------*/
		dialog.init({
			buttons: {
				"Generar": function() {
					genJornadaEvent.trigger('generarJornada',{
						_idCampeonato : $("#"+idCamp+" > option:selected").val()
					});
					$( this ).dialog( "close" );
				},
				Cancel: function() {
					$( this ).dialog( "close" );
				}
			}
		});
	},
	dialogPartido : function(nuevoPartido,eq1, eq2){
		var dialog = this.dialog;
		var idEq1 = eq1;
		var idEq2 = eq2;
		var idGol = "goles";
		var idNombre = "nombre";
		//--------------------------------------------------------
		var rangoGolesEq1 = dialog.input(idEq1+idGol).attr('readonly',true);
		var rangoGolesEq2 = dialog.input(idEq2+idGol).attr('readonly',true);
		//--------------------------------------------------------
		dialog.fill([dialog.label(eq1+" (Casa) : "), rangoGolesEq1,
					dialog.label(eq2+" (Visita) : "), rangoGolesEq2]);
		//--------------------------------------------------------
		rangoGolesEq1.spinner();
		rangoGolesEq1.spinner({max: 10, min: 0});
		rangoGolesEq1.spinner('value' , 0);
		rangoGolesEq2.spinner();
		rangoGolesEq2.spinner({max: 10, min: 0});
		rangoGolesEq2.spinner('value' , 0);
		//---------------------------------------------------------
		dialog.init({
			buttons: {
				"Aceptar": function() {
					nuevoPartido.trigger('nuevoPartido',{
						casa :{
							equipo:idEq1,
							goles:parseInt($('#'+idEq1+idGol).val())
						},
						visita :{
							equipo:idEq2,
							goles:parseInt($('#'+idEq2+idGol).val())
						}
					});
					$( this ).dialog( "close" );
				},
				Cancel: function() {
					$( this ).dialog( "close" );
				}
			}
		});
	}
}
//----------------------------------------------------------------------
function PanelView(){
	this.panel = $('#panel_view');
	this.tablaCamp = new TablaCampeonato();
	this.selectCamp = $('#selectCamp');
	this.chart = new Chart();
}

PanelView.prototype = {
	limpiar : function(){
		this.panel.children().addClass('notRender');
	},
	todosCampeonatos : function(campeonatos, restriccion){
		var _this = this;
		this.limpiar();
		var selectCamp = this.selectCamp
							.html("")
							.removeClass('notRender');
		selectCamp.unbind('change');
		selectCamp.on('change', function(){
			_this.tablaCamp.limpiartbody();
			_this.tablaCamp.mostrar(campeonatos[this.value],restriccion,1);
		});
		
		
		this.refreshSelEquipo(campeonatos);
		this.tablaCamp.mostrar(campeonatos[selectCamp.val()],restriccion);
		this.panel.append(selectCamp);
	},
	mostrarGrafica : function(campeonatos,grafica,equipos){
		var _this = this;
		this.limpiar();
		var selectCamp = this.selectCamp;
		selectCamp.unbind('change');
		selectCamp.on('change', function(){
			$('#chart').html('');
			_this.chart.campeonato = campeonatos[this.value];
			_this.chart[grafica](equipos);
		});
		
		this.refreshSelEquipo(campeonatos);
		this.chart.campeonato = campeonatos[selectCamp.val()];
		this.chart[grafica](equipos);
		this.panel.append(selectCamp);
	},
	refreshSelEquipo : function(campeonatos){
		var selectCamp = this.selectCamp
							.html("")
							.removeClass('notRender');
		$.each(campeonatos, function(_id, camp){
			var txtCamp = camp.periodo+" "+camp.a_o;
			var optCamp = $("<option></option>").val(_id).text(txtCamp);
			selectCamp.append(optCamp);
		});
	}
}

//----------------------------------------------------------------------
function TablaCampeonato(){
	this.campeonato;
	this.tabla_Pos = $('#tabla_equipos');
	this.posiciones = [];
	this.seleccionados = {
		callback: null,
		equipos : [],
		activo : false
	};
	
}
TablaCampeonato.prototype = {
	init : function(campeonato){
		var _this = this;
		var cont = 0;
		this.campeonato = campeonato;
		this.posiciones = [];
		if(campeonato.puntos){
			$.each(campeonato.puntos, function(_idEquipo){
				var camp = campeonato.puntos[_idEquipo]
				camp['_idEquipo'] = _idEquipo;
				_this.posiciones[cont] = camp;
				cont++;
			});
		}
	},
	mostrar : function(campeonato, array, igual){
		if(!igual){
			this.seleccionados = {
				callback : null,
				equipos : [],
				activo : false
			};
		}
		this.init(campeonato);
		this.limpiartbody();
		this.ordenar('pt',1);
		this.llenarTabla(array);
		this.tabla_Pos.removeClass('notRender');

	}, 
	limpiartbody : function(){
		$("#tbody_equipos").html("");
	},
	equipoRow : function (i, equipo, puntos){ 
		function posCell(pos){
			var divClass = 't_pos_real ';
			if(pos<=4) divClass += 'area_clasificacion ';
			divClass += 'roundBorder';
			return $('<td></td>').attr('class', 't_num_pos')
								.html("<div class='" + divClass + "'>" + pos + "</div>");
		}
		function equipoCell(equipo){
			return $('<td></td>')
				.attr('class', 't_equipo')
				.text(equipo)
				.attr('id',equipo);
		}
		function regularCell(text){
			return $('<td></td>').text(text);
		}
		function ptsCell(pts){
			return $('<td></td>').attr('class', 't_pts').text(pts);
		}
		
		var equipoC = equipoCell(equipo);
		var cells = [];
		if(puntos.pj && puntos.pj>0){
			cells = [
				posCell(i), "&nbsp;", equipoC, 
				regularCell(puntos.pj), regularCell(puntos.pg), 
				regularCell(puntos.pe), regularCell(puntos.pp), 
				regularCell(puntos.gf), regularCell(puntos.gc), 
				regularCell(puntos.dg), ptsCell(puntos.pt)
			]
		}else{
			cells = [
				posCell(i), "&nbsp;", equipoC, 
				regularCell(0), regularCell(0), 
				regularCell(0), regularCell(0), 
				regularCell(0), regularCell(0), 
				regularCell(0), ptsCell(0)
			]
		}
		
		var tr = $('<tr></tr>').attr('id', parseInt(i));
		$.each(cells, function(i, cell){
			tr.append(cell);
		});
		
		var _this = this;
		
		equipoC.on('click',function(){
			if(_this.seleccionados.activo){
				var id = this.id;
				var esta = false;
				for(i in _this.seleccionados.equipos){
					if(_this.seleccionados.equipos[i]==id) esta=true;
				}
				if(!esta){
					_this.seleccionados.equipos.push(id);
				}
				equipoC.addClass('selectedTeam');
				
				//alert(_this.seleccionados.equipos );
				if(_this.seleccionados.equipos.length >= 2){
					if(_this.seleccionados.callback)
						_this.seleccionados.callback(_this.seleccionados.equipos);
					$.each(_this.seleccionados.equipos, function(i, e){
						$('#'+e).removeClass('selectedTeam');
					});
					_this.seleccionados.equipos= [];
				}//else alert(_this.seleccionados.equipos.length);
			}
		});
		
		return tr;
	},
	llenarTabla : function(array){
		var pos = this.posiciones;
		var _this = this;
		function llenar(equipos){
			$.each(equipos, function(i, e){
				var p = e.p || i;
				//alert("llenar: "+p);
				var id = e._idEquipo || e;
				var row = _this.equipoRow(p+1, id, e).addClass('trEquipo');
				$('#tbody_equipos').append(row);
				row.fadeOut(1);
				row.fadeIn(2000);
			});
		}
		if(pos.length > 0){
			_this.seleccionados.activo =true;
			if(array){
				var aux = [];
				$.each(array, function(i, p){
					//alert(pos[p].p);
					aux.push(pos[p]);
				});
				_this.ordenar('pt', 1, aux);
				llenar(aux);
			}
			else llenar(pos);
		}else{
			_this.seleccionados.activo =false;
			llenar(_this.campeonato.equipos);
		}
	},
	ordenar : function(byThis, tipo, array){
		var pos = array || this.posiciones;
		var _this = this;
		function cambiar(i, j){
			var aux = pos[i];
			pos[i] = pos[j];
			pos[j] = aux;
		}
		function desempate(i, j, tipo){
			//p = prioridad
			var p = ['pt','pg','dg','gf','gc','nombre'];
			for(var z=0; z<p.length; z++){
				
				var byThis = p[z];
				if(tipo == 0 && pos[i][byThis] > pos[j][byThis]){
					//alert("Desempate "+byThis+ " : " + pos[i][byThis] +" - "+ pos[j][byThis]);
					cambiar(i,j);
				}
				else if(tipo == 1 && pos[i][byThis] < pos[j][byThis]){
					//alert("Desempate "+byThis+ " : " + pos[i][byThis] +" - "+ pos[j][byThis]);
					cambiar(i,j);
				}
			}
		}
		function asc(i, j, byThis){
			if(pos[i][byThis] > pos[j][byThis]){
				cambiar(i,j);
			}
			else if(pos[i][byThis] == pos[j][byThis])desempate(i, j, 0);
		}
		function desc(i, j, byThis){
			if(pos[i][byThis] < pos[j][byThis]){
				cambiar(i,j);
			}else if(pos[i][byThis] == pos[j][byThis]){
				desempate(i, j, 1);
			}
		}
		for(var i = 0; i < pos.length-1; i++){
			for(var j = i+1; j < pos.length; j++){
				if(tipo == 0) asc(i,j, byThis);
				else if(tipo == 1)desc(i,j, byThis);
			}
		}
		if(!array){
			for(var i = 0; i < pos.length; i++){
				pos[i].p = i;
			}
		}
		
	}
}
//----------------------------------------------------------------------
function Chart(campeonato){
	this.campeonato = campeonato;
	this.selectCamp = $('#selectCamp');
}
Chart.prototype = {
	drawBarChart : function(equipos) {
		var campeonato = this.campeonato;
		google.load("visualization", "1", {packages:["corechart"], 'callback':drawIt});
		function drawIt(){
			var puntos = campeonato.puntos;
			var arrayData = [['Equipo', 'PG','PE','PP']];
			if(campeonato.partidosJugados>0){
				$.each(puntos, function(_id, p){
					arrayData.push([_id, p.pg, p.pe, p.pp]);
				});
			}else{
				$.each(equipos,function(_id, e){
					arrayData.push([_id, 0, 0, 0]);
				});
			}
			
			
			var title = "Partidos ganados, empatados y perdidos";
			var infoCamp = "Campeonato: "+campeonato.periodo+" - "+campeonato.a_o+'\n';
			var options = {
			  title: infoCamp+title,
			  vAxis: {title: 'Equipo',  titleTextStyle: {color: 'red'}},
			  width:650,
              height:300
			};
			var chart = new google.visualization.BarChart(document.getElementById('chart'));
			var data = google.visualization.arrayToDataTable(arrayData);
			chart.draw(data, options);
			$('#chart').addClass('chart').removeClass('notRender');
		}
      },
	drawBubbleChart : function(equipos){
		var campeonato = this.campeonato;
		google.load("visualization", "1", {packages:["corechart"], 'callback':drawIt});
		function drawIt(){
			var arrayData = [['ID', 'Goles en contra','Goles a favor', 'Equipo', 'Puntaje']];
			var puntos = campeonato.puntos;
			if(campeonato.partidosJugados>0){
				$.each(puntos, function(_id, p){
					var nombre ="";
					$.each(equipos[_id].nombre.split(" "),function(i, n){
						if(!nombre=="") nombre= nombre+"\n"+n;
						else nombre=n;
					});
					arrayData.push([_id, p.gc, p.gf, nombre, p.pt]);
				});
			}else{
				$.each(equipos,function(_id, e){
					var nombre = "";
					$.each(e.nombre.split(" "),function(i, n){
						if(!nombre=="") nombre= nombre+"\n"+n;
						else nombre=n;
					});
					arrayData.push([_id, 0, 0, nombre, 0]);
				});
			}
			
			var title = "Relación entre los goles a favor, los goles en contra y el puntaje";
			var infoCamp = "Campeonato: "+campeonato.periodo+" - "+campeonato.a_o+'\n';
			var options = {
				title: infoCamp+title,
				hAxis: {title: 'Goles en contra'},
				vAxis: {title: 'Goles a favor'},
				bubble: {textStyle: {fontSize: 11}},
			    width:650,
                height:300
			};
			var chart = new google.visualization.BubbleChart(document.getElementById('chart'));
			var data = google.visualization.arrayToDataTable(arrayData);
			chart.draw(data, options);
			$('#chart').addClass('chart').removeClass('notRender');
		}
	},
	
}
//----------------------------------------------------------------------
function Dialog(title){
	this.dialog = $("#dialog-form > form > fieldset");
	if(title) this.title(title);
}
Dialog.prototype = {
	init : function(main){
		main.modal = true;
		this.dialog.dialog(main);
		this.dialog.dialog("open");
	},
	title : function(title){
		$("#dialog-form").attr('title', title);
	},
	select : function(id){
		return $("<select></select>").attr('id', id);
	},
	option : function(option, val){
		var value = val;
		if(!val) value = option;
		return $("<option></option>").text(option).attr('value', value);
	},
	label : function(label){
		return $("<label></label>").text(label);
	},
	input : function(id){
		return $("<input></input>").attr('id', id);
	},
	fill : function(componentsList){
		var dialog = this.dialog;
		this.clean();
		$.each(componentsList, function(i, c){
			dialog.append(c);
		});
	},
	clean : function(){
		this.dialog.html("");
	}
}

