/*****************Controller***************************/

function Controller(model, view){
	this.model = model;
	this.view = view;
	
	this.init();
}

Controller.prototype = {
	init : function(){
		this.view.fillTeamsTab(this.model.contEquipos.equipos);
	},
	actualizarCampeonato : function(_idcamp, partido){
		var campeonato = this.model.contCamp.campeonatos[_idcamp];
		campeonato.actualizarTabla(partido);
		view.panelView.limpiar();
		view.panelView.tablaCamp.mostrar(campeonato,false,1);
		view.panelView.refreshSelEquipo(this.model.contCamp.campeonatos);
	},
	agregarCampeonato : function(campeonato){
		var model = this.model;
		var _id = camp_unique_ID(campeonato);
		campeonato._id = _id;
		campeonato.equipos = model.contEquipos.IDList();
		campeonato.jornadas = [];
		if(model.contCamp.campeonatos[_id]) {
			alert("Campeonato ya existe");
			//_this.view.dialog.error("Campeonato ya existe");
		}
		else{
			//alert("POST /nuevoCampeonato");
			function handler(postResp){
				if(postResp.error){
					alert("Error : MongoDB en respuesta a POST [" +postResp.error + " ]");
					//_this.view.dialog.error("Error guardando campeonato : ");
				}
				else{
					//Se guarda en el modelo del cliente con el id que dio mongo
					var _id = model.contCamp.agregar(_id,
											campeonato.dedicado,
											campeonato.periodo,
											campeonato.a_o,
											campeonato.equipos,
											campeonato.jornadas);
					//alert("Guardado en model y en BD: "+ camp_unique_ID(model.contCamp.campeonatos[_id]));
				}
			}
			function error(req, e, msg){
				alert("Error : POST /nuevoCampeonato [ " + msg + " ]");
				//_this.view.dialog.error("ajax nuevoCampEvent Error:"+msg);
			}
			post("/nuevoCampeonato",campeonato,	handler,error);
		}
	},
	generarJornada : function(formJornada, param){
		var model = this.model;
		var _idCamp = formJornada._idCampeonato;
		var campeonato = model.contCamp.campeonatos[_idCamp];
		var jornada  = validJornada(campeonato);
		alert(_idCamp);
		function validJornada(camp) {
			if(camp) {
				/*if(camp.fullJornadas()) {
					alert("Jornadas Completas");
					return false;
				}*/
				return camp.generarPartidos();
			}
			alert("Campeonato no válido");
			return false;
		}
		
		if(jornada) {
			alert("Guardar jornada número: ")//+jornada.numero);
			/*var j=1;
			$.each(jornada.partidos, function(i, val){
				alert(j+ " : "+val.toString());
				j++;
			});*/
			//alert(this.view.dialog.dialog.html(jornada.toString());
			/*$.post("generarJornada",jsonJornada, 
				function(campeonatoResp,status){
					if(campeonatoResp.error){
						alert("Error guardando campeonato : " +campeonatoResp.error);
					}
					else{
						control.agregarCampeonato(campeonatoResp._id, jsonCamp);
					}
				});*/
		}
		/*}else{
			alert("Error al generar jornada ");
		}*/
	}
}
