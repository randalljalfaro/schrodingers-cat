//Array con el modelo de equipos
$(function(){
	//Model
	model = new Model();
	//View
	view = new View();
	//Controller
	control = new Controller(model,view);
	
	init();
});

function init(){
	//FUNCIONES
	tabla();
	graficas();
	//TABS
	campeonatos();
	jornadas();
}
function tabla(){
	function mostrarTabla(array){
		var tabla = view.panelView.tablaCamp;
		
		var campeonatos = model.contCamp.campeonatos;
		view.panelView.todosCampeonatos(campeonatos,array);
		
		tabla.seleccionados.callback = function(equipos){
			function handler(eventSender, formPartido){
				var _idcamp = view.panelView.selectCamp.val();
				control.actualizarCampeonato(_idcamp, formPartido);
			}
			var nuevoPartido = $({});
			nuevoPartido.on('nuevoPartido', handler);
			view.dialogPartido(nuevoPartido,equipos[0],equipos[1]);
		}
	}
	$("#ver_camp").click(function(){
		mostrarTabla();
	});
	$("#clasificados").click(function(){
		mostrarTabla([0,1,2,3]);
	});
	$("#descienden").click(function(){
		mostrarTabla([11, 10]);
	});
}
function graficas(){
	function mostrarGrafica(grafica){
		var campeonatos = model.contCamp.campeonatos;
		var equipos = model.contEquipos.equipos;
		view.panelView.mostrarGrafica(campeonatos,grafica, equipos);
	}
	$("#graficoBarra").click(function(){
		mostrarGrafica('drawBarChart');
	});
	$("#graficoBurbujas").click(function(){
		mostrarGrafica('drawBubbleChart');
	});
}
function campeonatos(){
	//CREAR CAMPEONATO
	$("li#crearCamp").click( function(){
		function handler(eventSender, formCampeonatoJSON){
			control.agregarCampeonato(formCampeonatoJSON);
		}
		
		var periodos = ["Invierno","Verano"];
		var rangoA_o = {max: 2020,min: 2014};
		
		var nuevoCampEvent = $({});
		nuevoCampEvent.on('nuevoCampeonato', handler);
		view.dialogCampeonato(nuevoCampEvent, periodos, rangoA_o);
	});
}
function jornadas(){
	//GENERAR JORNADA
	$("li#genJornada").click( function(){
		function handler(eventSender, formJornadaJSON){
			control.generarJornada(formJornadaJSON);
		}
		
		var genJornadaEvent = $({});
		genJornadaEvent.on('generarJornada', handler);
		view.dialogGeneraJornada(genJornadaEvent, 
								model.contCamp.campeonatos);
	});
}
//--------------------------------------------------
