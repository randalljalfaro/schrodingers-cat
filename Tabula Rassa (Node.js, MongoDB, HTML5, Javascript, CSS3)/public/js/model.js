/*****************MODEL****************************************/
function getDataEquipos(){
	return loadJSONData("/equipos", function(){ 
		return {};
	});
}
function getDataCampeonatos(){
	return loadJSONData("/campeonatos", function(){ 
		return {};
	});
}
function camp_unique_ID(campeonato){
	return (campeonato.periodo+"_"+campeonato.a_o);
}
function part_unique_ID(partido){
	return (partido.casa.equipo+"_"+partido.visita.equipo);
}
//---------------------------------------------------------------
function Model(){
	this.contEquipos = new Equipos({});
	this.contEquipos.init(getDataEquipos());
	this.contCamp = new Campeonatos({});
	this.contCamp.init(getDataCampeonatos());
}
//-------------------------------------------------------------
function Equipo(nombre,logo,sitioWeb){
	this.nombre = nombre;
	this.logo = logo;
	this.sitioWeb = sitioWeb;
}

function Equipos(equipos){
	this.equipos = equipos;
}
Equipos.prototype = {
	init : function (equipos){
		var _this = this;
		if(!equipos) alert("init: equipos => Sin definir");
		else{
			$.each(equipos, function(index, e){
				_this.agregar(e._id,e.nombre,e.logo,e.sitioWeb);
			});
		}
	},
	agregar : function(id,nombre,logo,sitioWeb){
		this.equipos[id] = new Equipo(nombre,logo,sitioWeb);
	},
	IDList : function(){
		var result = [];
		var equipos = this.equipos;
		$.each(equipos, function(_id){
			result.push(_id);
		});
		return result;
	},
	getFromIDArray : function(idArray){
		var result = [];
		var equipos = this.equipos;
		$.each(idArray, function(i,_id){
			var equipo = equipos[_id];
			if(equipo) result.push(equipo);
		});
		return result;
	}
}

//-------------------------------------------------------------

function Campeonato(dedicado, periodo, a_o,  equipos, jornadas){
	this.dedicado = dedicado;
	this.periodo = periodo;
	this.a_o = a_o;
	this.equipos = equipos;
	this.jornadas = jornadas;
	this.puntos = {};
	this.partidosJugados = 0;
	this.init();
}
Campeonato.prototype = {
	init : function(){
		var _this = this;
		var jornadas = this.jornadas;
		var pos = this.posiciones;
		$.each(jornadas, function(j, jornada){
			$.each(jornada, function(p, partido){
				_this.actualizarTabla(partido);
			});
		});
		//alert(JSON.stringify(this.puntos));
	},
	actualizarTabla : function(partido){
		var casa = partido.casa;
		var visita = partido.visita;
		//alert(JSON.stringify(partido));
		var puntos = this.puntos;
		function actualizarEquipo(equipo, gf, gc){
			var pt = 0; var pg = 0; 
			var pp = 0; var pe = 0;
			if(gf > gc) {
				pt = 3;
				pg++;
			}
			else if(gc == gf) {
				pt = 1;
				pe++;
			}
			else pp++;
			
			if(puntos[equipo]){
				var e = puntos[equipo];
				puntos[equipo] = {
					'pj': e.pj+1,
					'pg': e.pg+pg,
					'pe' : e.pe+pe,
					'pp' : e.pp+pp,
					'gf' : e.gf+gf,
					'gc' : e.gc+gc,
					'dg' : e.gf-e.gc,
					'pt' : e.pt+pt
				}
			}
			else{
				puntos[equipo] = {
					'pj': 1,
					'pg': pg,
					'pe' : pe,
					'pp' : pp,
					'gf' : gf,
					'gc' : gc,
					'dg' : gf-gc,
					'pt' : pt
				}
			}
		}
			
		actualizarEquipo(casa.equipo, casa.goles, visita.goles);
		actualizarEquipo(visita.equipo, visita.goles, casa.goles);
		this.partidosJugados++;
		//partido
	},
	generarPartidos : function(array){
		var pool  = {};
		var equipos = this.equipos;
		//------------------------------------
		var end = equipos.length-1;
		for(var i=0; i<=end; i++){
			for(var j=i+1; j<=end; j++){
				var casa = {'equipo':equipos[i], 'goles':0};
				var visita = {'equipo':equipos[j], 'goles':0};
				var partido = {'casa':casa,'visita':visita};
				pool[part_unique_ID(partido)] = partido;
			}
		}
		//------------------------------------
		if(array){
			$.each(array, function(i, p){
				var _id = part_unique_ID(p);
				if(pool[_id]) delete pool[_id];
			});
		}
		alert(JSON.stringify(pool));
		return pool;
	}
}
function Campeonatos(campeonatos){
	this.campeonatos = campeonatos;
}
Campeonatos.prototype = {
	init : function (campeonatos){
		var _this = this;
		if(!campeonatos) alert("init: campeonatos => Sin definir");
		else{
			$.each(campeonatos, function(i, c){
				_this.agregar(c._id, c.dedicado, c.periodo, c.a_o,  c.equipos, c.jornadas);
			});
		}
	},
	agregar : function(_id, dedicado, periodo, a_o,  equipos, jornadas){
		this.campeonatos[_id] = new Campeonato(dedicado, periodo, a_o,  equipos, jornadas);
	},
	campeonatoActual : function(){
		var camps = this.campeonatos;
		for(var c in camps){
			return camps[c];
		}
		return false;
	}
}






//----------------------------------------------------------------

/*


function Partido(casa, visitante){
	this.casa = casa.team;
	this.visitante = visitante.team;
	this.golesCasa = casa.goles;
	this.golesVisitante = visitante.goles;
}
Partido.prototype = {
	unique_ID : function(partido){
		if(partido) return (partido.casa.name+"_"+partido.visitante.name);
		else return (this.casa.name+"_"+this.visitante.name);
	},
	toString : function(){
		var casa = this.casa.name;
		var golesC = this.golesCasa;
		var visitante = this.visitante.name;
		var golesV = this.golesVisitante;
		var string = casa+"  "+golesC+" - "+visitante+"  "+golesV;
		return string;
	},
	juegaEquipo : function(equipo){
		if(this.casa.name == equipo.name || 
				this.visitante.name == equipo.name)
			return true;
		return false
	}
}

function Jornada(numero, fecha, partidos){
	this.numero = numero;
	this.fecha = fecha;
	this.equipos = {};
	this.partidos = {};
}
Jornada.prototype = {
	toString : function(){
		return JSON.stringify(this.partidos);
	}
}

Campeonato.prototype = {
	init : function(){
		this.generarPartidosDisponibles();
	},
	generarPartidosDisponibles : function(){
		var pool  = this.partidosDisponibles;
		var equipos = this.equipos;
		var maxEquipos = this.maxEquipos;
		var listEquipos = [];
		var i = 0;
		for(var equipo in equipos){
			listEquipos[i] = equipos[equipo];
			i++;
		}
		//------------------------------------
		var end = listEquipos.length-1;
		for(var i=0; i<=end; i++){
			for(var j=i+1; j<=end; j++){
				var casa = {'team':listEquipos[i], 'goles':0};
				var visita = {'team':listEquipos[j], 'goles':0};
				var partido = new Partido(casa,visita);
				pool[partido.unique_ID()] = partido;
			}
		}
		//alert("generarPartidosDisponibles: "+this.partidosDisponibles.length);
	},
	
	generarJornadaAleatoria : function(){
		var _this = this;
		var equipos = this.equipos;
		var pool  = this.partidosDisponibles;
		if(this.cantPartDisp!=0){
			var num = _this.cantJornadas+1;
			var jornada = new Jornada(num, new Date());
			//alert("JORNADA : "+num);
			for(var equipo in equipos){
				var partido = _this.partidoDisp(equipos[equipo], jornada);
				if(partido){
					var casa = partido.casa;
					var visitante = partido.visitante;
					var unique_ID = partido.unique_ID();
					//Goles al azar en rango 0 - 5
					partido.golesCasa = Math.floor(Math.random()*5); 
					partido.golesVisitante = Math.floor(Math.random()*5);
					//Registrar equipos en la jornada
					jornada.partidos[unique_ID] = partido;
					jornada.equipos[casa.name] = casa.name;
					jornada.equipos[visitante.name] = visitante.name;
					
					//Eliminar partido del pool
					delete pool[unique_ID]; 
					//Modificar el contador de partidos
					_this.cantPartDisp--;
				}
			}
			_this.cantJornadas++;
			return jornada;
		}
		alert("Campeonato completo");
		return false;
	},
	partidoDisp : function(equipo, j){
		var pool  = this.partidosDisponibles;
		for(var p in pool){
			var partido = pool[p];
			if(partido.juegaEquipo(equipo) && 
				!j.equipos[partido.casa.name] && 
					!j.equipos[partido.visitante.name]){
				return partido;
			}
		}
	},

	maxJornadas : function(){
		var cantPartJorn = 6;
		return (this.maxPartidos())/cantPartJorn;
	},
	maxPartidos : function(){
		var cantEquipos = this.cantEquipos;
		return (cantEquipos*(cantEquipos-1))/2;
	},
	agregarJornada : function(id, numero, fecha, partidos){
		alert('agregarJornada : '+id+ numero+ fecha);
		this.cantJornadas++;
		this.primerVuelta.push(new Jornada(id, numero, fecha, partidos));
	}
}


*/