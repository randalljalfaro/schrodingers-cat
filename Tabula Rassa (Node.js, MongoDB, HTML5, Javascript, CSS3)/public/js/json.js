function loadJSONData(url, handler){
    var http_request = new XMLHttpRequest();
	http_request.open("GET", url, false);
	try{
	  http_request.send();
	}catch(e){
		 return handler(e);
	}
	return JSON.parse(http_request.responseText);
}

function post(url,jsonData,done,errorHandler){
	$.ajax({
		type: "POST",
		url: url,
		data: JSON.stringify(jsonData),
		error: errorHandler
	}).done(function(jsonResponse){
		done(jsonResponse);
	});
}