package progra03.proyecto01.control;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import progra03.proyecto01.modelo.*;
import progra03.proyecto01.vista.*;

public class ControladorPrimario {

    private PantallaPerfilEmpleado pantallaPerfilEmpleado;
    private PantallaPerfilEmpleado pantallaPerfilCompanero;
    private PantallaInicioEmpleado pantallaLoginEmpleado;
    private PantallaRegistroEmpleado pantallaRegistroEmpleado;
    private PantallaBuscarEmpleado pantallaBuscarEmpleado;
    private PantallaRegistroEmpleado pantallaInfoEditableEmpleado;
    private Usuario usr;
    private ContenedorUsuarios contenedorUsuarios;
    private static final String TITULO_ERROR = "Mensaje de Error";
    

    public ControladorPrimario() {
        System.out.println("    - Iniciando Controlador");
        inicializarComponenetes();
    }

    private void inicializarComponenetes() {
        this.contenedorUsuarios = new ContenedorUsuarios();
        mostrarPantallaInicio();

    }

    private void listenersLogin() {

        this.pantallaLoginEmpleado.agregarAccionNuevoUsuario(new ImplementacionInNuevoUsuario());
        this.pantallaLoginEmpleado.agregarAccionEntrar(new ImplementacionInEntrar());
    }
    //1)Pantalla INICIO 1.1)Accion de Etiqueta Nuevo usuario

    public class ImplementacionInNuevoUsuario extends MouseAdapter {

        @Override
        public void mousePressed(MouseEvent evento) {
            if (SwingUtilities.isLeftMouseButton(evento)) {
                mostrarPantallaRegistro();
            }
        }
    }
    //2)PANTALLA DE REGISTRO para etiqueta Nuevo Usuario

    private void mostrarPantallaRegistro() {
        pantallaLoginEmpleado.dispose();
        pantallaRegistroEmpleado = new PantallaRegistroEmpleado("Registro de empleado");

        //2)PANTALLA DE REGISTRO 2.2)accion de guardar
        pantallaRegistroEmpleado.agregarAcionCerrar(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent we) {
                mostrarPantallaInicio();
            }
        });

        //Listener boton Guardar
        pantallaRegistroEmpleado.agregarAccionGuardarRegistro(new ImplementacionInAccionGuardar());
    }
    //2)PANTALLA DE REGISTRO 2.1)accion de guardar

    public class ImplementacionInAccionGuardar extends MouseAdapter {

        @Override
        public void mousePressed(MouseEvent evento) {
            if (SwingUtilities.isLeftMouseButton(evento)) {

                if (pantallaRegistroEmpleado.requisitosRegistro()) {
                    registrarUsuario(pantallaRegistroEmpleado.getUsuario());
                    pantallaRegistroEmpleado.dispose();
                     accionGuardarEnTexto();
                    mostrarPantallaInicio();
                }
            }
        }
    }

    public void registrarUsuario(Usuario user) {
        this.contenedorUsuarios.agregarUserRequisitosRegistro(user);
    }
    //1)Pantalla INICIO de nuevo

    private void mostrarPantallaInicio() {

        this.pantallaLoginEmpleado = new PantallaInicioEmpleado();
        listenersLogin();
        accionImportarDesdeTexto();
        accionImportarCompaneros();
        accionImportarPublicaciones();

    }
    //1)Pantalla INICIO 1.2)Accion de boton entrar

    public class ImplementacionInEntrar extends MouseAdapter {

        @Override
        public void mousePressed(MouseEvent evento) {
            if (SwingUtilities.isLeftMouseButton(evento)) {
                //Solo si sta el usuario registrado muestra el perfil
                if (contenedorUsuarios.estaUsuario(pantallaLoginEmpleado.getCorreo(), pantallaLoginEmpleado.getClave())) {
                    //Variable auxiliar con los datos del que se logueó
                    loguearUsuario();
                    mostrarPantallaPerfilInicio();
                    pantallaLoginEmpleado.dispose();
                } else {
                    JOptionPane.showMessageDialog(pantallaLoginEmpleado, "Cuenta o contraseña incorrecta");
                }
            }
        }
    }
    //3)Pantalla PERFIL 

    private void mostrarPantallaPerfilInicio() {
        this.pantallaPerfilEmpleado = new PantallaPerfilEmpleado("Perfil Empleado ",
                contenedorUsuarios.getUsuarioCuenta(this.pantallaLoginEmpleado.getCorreo(),
                this.pantallaLoginEmpleado.getClave()));
        //Se agrega la pantalla perfil del usuario registrado
        this.contenedorUsuarios.agregarObservadorActualizacionPerfil(pantallaPerfilEmpleado);
        listenersPantallaPerfil();
    }
    //3)Pantalla PERFIL 

    private void listenersPantallaPerfil() {
        //3)Pantalla PERFIL 3.1)Accion de cerrar
        //this.pantallaPerfilCompanero.agregarAcionCerrar(new ImplementacionAccionGuardarEnTexto());
        this.pantallaPerfilEmpleado.agregarAcionCerrar(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent we) {
                int salirPerfil = JOptionPane.showConfirmDialog(pantallaPerfilEmpleado, "¿Desea salir de su perfil?");
                switch (salirPerfil) {
                    case JOptionPane.YES_OPTION: {
                        accionGuardarEnTexto();
                        accionGuardarCompaneros();
                        accionGuardarPublicaciones();
                        mostrarPantallaInicio();
                        break;
                    }
                    case JOptionPane.CANCEL_OPTION:
                        mostrarPantallaPerfil();
                        break;
                    case JOptionPane.CLOSED_OPTION:
                        mostrarPantallaPerfil();
                        break;
                    case JOptionPane.NO_OPTION:
                        mostrarPantallaPerfil();
                        break;
                }
            }
        });
        this.pantallaPerfilEmpleado.agregarAccionMasInfoContacto(new ImplementacionInInfoEditableEmpleado());
        this.pantallaPerfilEmpleado.agregarAccionVerPerfilCompanero(new ImplementacionInVerPerfilCompanero());
        this.pantallaPerfilEmpleado.agregarAccionBuscarEmpleado(new ImplementacionInBuscarEmpleado());
        this.pantallaPerfilEmpleado.agregarAccionPublicar(new ImplementacionPublicacion());
    }
    //3)Pantalla PERFIL 3.1.1)Accion de guardar al cerrar

    private void accionGuardarEnTexto() {
        File archivoExport = this.pantallaLoginEmpleado.archivoExportTexto();

        if (archivoExport != null) {
            contenedorUsuarios.exportarATexto(archivoExport);
        }
    }

    private void accionImportarDesdeTexto() {
        try {
            File archivoImport = this.pantallaLoginEmpleado.archivoImportTexto();
            if (archivoImport != null) {
                contenedorUsuarios.importarDesdeTexto(archivoImport);
            }
        } catch (RuntimeException ex) {
            System.out.println("No hay ninguna base de datos de usuarios");
        }


    }

    private void accionGuardarCompaneros() {
        File archivoExport = this.pantallaLoginEmpleado.archivoExportCompaneros();

        if (archivoExport != null) {
            contenedorUsuarios.exportarATextoCompaneros(archivoExport);
        }


    }

    private void accionImportarCompaneros() {
        try {
            File archivoImport = this.pantallaLoginEmpleado.archivoImportCompaneros();

            if (archivoImport != null) {
                contenedorUsuarios.importarDesdeTextoCompaneros(archivoImport);
            }
        } catch (RuntimeException ex) {
            System.out.println("No hay ninguna base de datos de compañeros");
        }
    }

    private void accionGuardarPublicaciones() {
        File archivoExport = this.pantallaLoginEmpleado.archivoExportPublicaciones();

        if (archivoExport != null) {
            contenedorUsuarios.exportarATextoPublicacion(archivoExport);
        }
    }

    private void accionImportarPublicaciones() {
        try {
            File archivoImport = this.pantallaLoginEmpleado.archivoImportPublicaciones();
            if (archivoImport != null) {
                contenedorUsuarios.importarDesdeTextoPublicaciones(archivoImport);
            }
        } catch (RuntimeException ex) {
            System.out.println("No hay ninguna base de datos de publicaciones");
        }
    }
    //3)Pantalla PERFIL 3.2)Accion de boton mas

    public class ImplementacionInInfoEditableEmpleado extends MouseAdapter {

        @Override
        public void mousePressed(MouseEvent evento) {
            if (SwingUtilities.isLeftMouseButton(evento)) {
                mostrarPantallaInfoEditableEmpleado();
            }
        }
    }

    //4)Pantalla INFO. EDITABLE
    private void mostrarPantallaInfoEditableEmpleado() {
        ocultarPerfil();
        this.pantallaInfoEditableEmpleado = new PantallaRegistroEmpleado("Información Contacto Empleado",
                this.usr, 1);

        //4)Pantalla INFO. EDITABLE 4.1)Accion Aceptar(Edición de usuario)
        pantallaInfoEditableEmpleado.agregarAccionGuardarRegistro(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent evento) {
                if (SwingUtilities.isLeftMouseButton(evento)) {
                    if (pantallaInfoEditableEmpleado.requisitosRegistro()) {
                        contenedorUsuarios.modificarInfoUsuario(
                                usr, pantallaInfoEditableEmpleado.getUsuario());
                        pantallaInfoEditableEmpleado.dispose();
                        mostrarPantallaPerfil();
                    }
                }
            }
        });
        //4)Pantalla INFO. EDITABLE 4.1)Accion Cerrar
        pantallaInfoEditableEmpleado.agregarAcionCerrar(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent we) {
                pantallaPerfilEmpleado.setVisible(true);
            }
        });
    }
    //3)Pantalla PERFIL 3.3)Accion de etiqueta Buscar Empleado

    public class ImplementacionInBuscarEmpleado extends MouseAdapter {

        @Override
        public void mousePressed(MouseEvent evento) {
            if (SwingUtilities.isLeftMouseButton(evento)) {
                mostrarPantallaBuscarEmpleado();
            }
        }
    }
    //5)Pantalla BuscarEmpleado

    private void mostrarPantallaBuscarEmpleado() {
        ocultarPerfil();
        this.pantallaBuscarEmpleado = new PantallaBuscarEmpleado(this.contenedorUsuarios.getListaUsuario());
        listenersBuscarEmpleado();
    }
    //5)Pantalla BuscarEmpleado 

    private void listenersBuscarEmpleado() {
        //5)Pantalla BuscarEmpleado 5.1)Accion Cerrar
        this.pantallaBuscarEmpleado.agregarAcionCerrar(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent we) {
                pantallaBuscarEmpleado.dispose();
                mostrarPantallaPerfil();
            }
        });
        //5)Pantalla BuscarEmpleado 5.2)Accion Agregar
        this.pantallaBuscarEmpleado.agregarAccionAgregar(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent evento) {
                if (SwingUtilities.isLeftMouseButton(evento)) {
                    contenedorUsuarios.agregarCompaneros(usr.getNombre(), pantallaBuscarEmpleado.getNombreSeleccionado());
                }
            }
        });
    }
    //3)Pantalla PERFIL 3.3)Accion etiqueta Perfil Compañero

    public class ImplementacionInVerPerfilCompanero extends MouseAdapter {

        @Override
        public void mousePressed(MouseEvent evento) {
            if (SwingUtilities.isLeftMouseButton(evento)) {
                mostrarPantallaPerfilCompanero(
                        contenedorUsuarios.getUsuarioNombre(
                        pantallaPerfilEmpleado.getNombreSeleccionado()));
            }
        }
    }
    //6)Pantalla PERFIL COMPAÑERO

    private void mostrarPantallaPerfilCompanero(Usuario user) {
        ocultarPerfil();
        //Se crea un perfil con un user por parametro que sería el compañero
        //el final es necesario para poder comunicarme dentro del adaptador de la accion del botón mas y poder hacerlo visible
        pantallaPerfilCompanero = new PantallaPerfilEmpleado("Perfil Compañero ", user);
        final Usuario userAux = user;

        //6)Pantalla PERFIL COMPAÑERO 6.1)Accion Cerrar
        pantallaPerfilCompanero.agregarAcionCerrar(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent we) {
                mostrarPantallaPerfil();
            }
        });
        //6)Pantalla PERFIL COMPAÑERO 6.2)Accion MasInfo...
        pantallaPerfilCompanero.agregarAccionMasInfoContacto(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent evento) {
                if (SwingUtilities.isLeftMouseButton(evento)) {
                    //Se crea una ventana con la info de ese usuario y se ocula la ventana del perfil del compañero
                    pantallaPerfilCompanero.setVisible(false); //Se hace final perfilCompañero para poder llamar sus metodos en la clase anónima
                    //7)Pantalla MAS INFO COMPAÑERO 
                    final PantallaRegistroEmpleado pantallaMasInfoContactoCompanero = new PantallaRegistroEmpleado("Información Contacto Compañero", userAux, 2);
                    //7)Pantalla MAS INFO COMPAÑERO 7.1)Accion Cerrar
                    pantallaMasInfoContactoCompanero.agregarAcionCerrar(new WindowAdapter() {
                        @Override
                        public void windowClosing(WindowEvent we) {
                            pantallaPerfilCompanero.setVisible(true);
                        }
                    });
                    //7)Pantalla MAS INFO COMPAÑERO 7.1)Accion boton Volver
                    pantallaMasInfoContactoCompanero.agregarAccionSalir(new MouseAdapter() {
                        @Override
                        public void mousePressed(MouseEvent evento) {
                            if (SwingUtilities.isLeftMouseButton(evento)) {
                                pantallaMasInfoContactoCompanero.dispose();
                                pantallaPerfilCompanero.setVisible(true);
                            }
                        }
                    });
                }
            }
        });
    }
    //3)Pantalla PERFIL 3.4)Accion publicar

    public class ImplementacionPublicacion extends MouseAdapter {

        @Override
        public void mousePressed(MouseEvent evento) {
            if (SwingUtilities.isLeftMouseButton(evento)) {
                Date fechaPublicacion = new Date();
                Calendar calendario = new GregorianCalendar();
                Publicacion p = new Publicacion(usr.getNombre(), pantallaPerfilEmpleado.getTextoPublicacionPerfil(),
                        fechaPublicacion,calendario);
                contenedorUsuarios.enviarPublicacion(p);
                pantallaPerfilEmpleado.setVisible(false);
                pantallaPerfilEmpleado.setVisible(true);
            }
        }
    }

    private void loguearUsuario() {
        this.usr = contenedorUsuarios.getUsuarioCuenta(
                pantallaLoginEmpleado.getCorreo(), pantallaLoginEmpleado.getClave());
    }

    private void ocultarPerfil() {
        this.pantallaPerfilEmpleado.setVisible(false);
    }

    private void mostrarPantallaPerfil() {
        this.pantallaPerfilEmpleado.setVisible(true);
    }

    public void mostrarMensaje(String titulo, String mensaje, int tipo) {
        JOptionPane.showMessageDialog(
                this.pantallaPerfilEmpleado, mensaje, titulo, tipo);
    }
}
