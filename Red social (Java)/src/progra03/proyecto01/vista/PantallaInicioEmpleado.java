package progra03.proyecto01.vista;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.MouseListener;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileNotFoundException;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.filechooser.FileNameExtensionFilter;

public class PantallaInicioEmpleado extends javax.swing.JFrame {
    private JLabel etiquetaBienvenida, etiquetaNuevoUsuario, etiquetaCuenta, etiquetaClave;
    private JTextField areaTextoCuenta, areaTextoClave;
    private JPasswordField textoClave;        
    private JButton botonEntra;
    private JFileChooser fileChooser;
    
    public PantallaInicioEmpleado() {
    	System.out.println("        - Iniciando Pantalla Inicio");
        inicializarComponenetes();
        inicializarDiseno();
        inicializarVista();
     iniciarAtributosTexto();
    }
    private void iniciarAtributosTexto(){
        fileChooser= new JFileChooser();
        fileChooser.setFileFilter(new FileNameExtensionFilter("Archivos de texto", "txt"));
        fileChooser.setAcceptAllFileFilterUsed(true);
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
    }
    private void inicializarComponenetes(){
        this.etiquetaBienvenida=new JLabel("¡Bienvenido a Sozialen!");
        this.etiquetaNuevoUsuario=new JLabel("<html><u>Nuevo Usuario</u></html>");
        this.etiquetaCuenta=new JLabel("Cuenta:");
        this.etiquetaClave=new JLabel("Password:");
        this.areaTextoCuenta= new JTextField("micuenta@sozialen.com");
        this.textoClave=new JPasswordField();
        this.textoClave.setEchoChar('*');
        this.botonEntra= new JButton("Entrar");
        this.botonEntra.setSize(200,20);
        
        this.etiquetaBienvenida.setSize(100,400);
        Dimension dimensionEtiquetasTop=new Dimension(200,20);
        this.etiquetaNuevoUsuario.setPreferredSize(dimensionEtiquetasTop);
        this.etiquetaCuenta.setPreferredSize(dimensionEtiquetasTop);
        this.etiquetaClave.setPreferredSize(dimensionEtiquetasTop);
        
        this.etiquetaNuevoUsuario.setHorizontalAlignment(SwingConstants.RIGHT);
        this.etiquetaCuenta.setHorizontalAlignment(SwingConstants.RIGHT);
        this.etiquetaClave.setHorizontalAlignment(SwingConstants.RIGHT);
        
        Dimension dimensionAreaTexto=new Dimension(200,30);        
        this.textoClave.setPreferredSize(dimensionAreaTexto);
        this.areaTextoCuenta.setPreferredSize(dimensionAreaTexto);
        
        //Personalización
        this.etiquetaBienvenida.setFont(new Font("Verdana", Font.BOLD, 20));
        this.etiquetaBienvenida.setForeground(Color.BLUE);
        this.etiquetaNuevoUsuario.setForeground(Color.BLUE); 
        
        this.etiquetaNuevoUsuario.setFocusable(true);
    }

    private void inicializarDiseno(){
        JPanel contenedorIntermedio=new JPanel(new BorderLayout());
        
        JPanel panelBorder = new JPanel(new BorderLayout());
        
        JPanel panelBienvenida = new JPanel(new FlowLayout());
        panelBienvenida.add(this.etiquetaBienvenida);
        panelBorder.add(panelBienvenida, BorderLayout.NORTH);

       
        JPanel panelGridCentral = new JPanel(new GridLayout(5,0));
        
        //Separador
        panelGridCentral.add(new JSeparator());
        
        JPanel panelFlowCuenta = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        JPanel panelFlowClave = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        JPanel panelFlowEntra = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        JPanel panelFlowNuevoUsuario = new JPanel(new FlowLayout(FlowLayout.RIGHT)); 
        
        panelFlowCuenta.add(this.etiquetaCuenta);
        panelFlowCuenta.add(this.areaTextoCuenta);
        panelFlowClave.add(this.etiquetaClave);
        panelFlowClave.add(this.textoClave);
        panelFlowEntra.add(this.botonEntra);
        panelFlowNuevoUsuario.add(this.etiquetaNuevoUsuario);
        
        panelGridCentral.add(panelFlowCuenta);
        panelGridCentral.add(panelFlowClave);
        panelGridCentral.add(panelFlowEntra);
        panelGridCentral.add(panelFlowNuevoUsuario);
        
        panelBorder.add(panelGridCentral, BorderLayout.CENTER);

        contenedorIntermedio.add(panelBorder, BorderLayout.CENTER);
        setContentPane(contenedorIntermedio);
    }
       
    private final void inicializarVista(){
        setTitle("Proyecto Programado 01 - Randall Alfaro G.");
        setSize(450,350);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);
    }
    public String getCorreo(){
        return this.areaTextoCuenta.getText();
    }
    public String getClave(){
        char[] pass = textoClave.getPassword();
        String passString = new String(pass);
        return passString;
    }
    public void agregarAccionNuevoUsuario(MouseListener mouseListener){
		this.etiquetaNuevoUsuario.addMouseListener(mouseListener);
	}
	
   public void removerAccionNuevoUsuario(MouseListener mouseListener){
		this.etiquetaNuevoUsuario.removeMouseListener(mouseListener);
	}
   public void agregarAccionEntrar(MouseListener mouseListener){
		this.botonEntra.addMouseListener(mouseListener);
	}
	
   public void removerAccionEntrar(MouseListener mouseListener){
		this.botonEntra.removeMouseListener(mouseListener);
	}
   public void agregarAccionCerrar(WindowListener wListener){
		this.addWindowListener(wListener);
	}
	
   public void removerAccionCerrar(WindowListener wListener){
		this.removeWindowListener(wListener);
	}
   
  public File archivoExportTexto(){    
      File archivo = new File("usuarios.txt");
      
        /*fileChooser.setSelectedFile(new File("export.txt"));
        if (fileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
            archivo = fileChooser.getSelectedFile();
        }*/

        return archivo;
    }

    public File archivoImportTexto() {
        File archivo= null;
        
        try{
            archivo = new File("usuarios.txt");
        }
        catch (RuntimeException ex)
    {
      System.out.println("No hay ninguna base de datos");
    }
            return archivo;
        
    }
    public File archivoExportCompaneros(){    
      File archivo = new File("compañeros.txt");
      
        /*fileChooser.setSelectedFile(new File("export.txt"));
        if (fileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
            archivo = fileChooser.getSelectedFile();
        }*/

        return archivo;
    }

    public File archivoImportCompaneros() {

        File archivo = new File("compañeros.txt");
        
        /*if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            archivo = fileChooser.getSelectedFile();
        }*/

        return archivo;
    }
    public File archivoExportPublicaciones(){    
      File archivo = new File("publicaciones.txt");

        /*fileChooser.setSelectedFile(new File("export.txt"));
        if (fileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
            archivo = fileChooser.getSelectedFile();
        }*/

        return archivo;
    }

    public File archivoImportPublicaciones() {

        File archivo = new File("publicaciones.txt");
        
        /*if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            archivo = fileChooser.getSelectedFile();
        }*/

        return archivo;
    }
}
