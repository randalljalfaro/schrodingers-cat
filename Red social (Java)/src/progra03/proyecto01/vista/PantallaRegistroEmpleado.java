package progra03.proyecto01.vista;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.MouseListener;
import java.awt.event.WindowListener;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.BorderFactory;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
//import progra03.proyecto01.modelo.File;
import progra03.proyecto01.modelo.Usuario;
import progra03.validacion.texto.ValidacionTexto;
import progra03.validacion.texto.ValidarTexto;


public final class PantallaRegistroEmpleado extends javax.swing.JFrame{
    private JLabel etiquetaInfoGeneral, etiquetaNombreCompleto, etiquetaCorreo;
    private JLabel etiquetaClave, etiquetaReescribirClave, etiquetaSkype;
    private JLabel etiquetaHotmail, etiquetaPais, etiquetaTelefono, etiquetaPuesto;
    private JLabel etiquetaFechaIngreso, etiquetaGmail;
    private JLabel etiquetaPlataformas, etiquetaInformacionLaboral, imagenAvatar;
    private JTextField textoNombre, textoCorreo;
    private JTextField textoSkype, textoGmail, textoHotmail, textoTelefono, textoFechaIngreso;
    private JButton botonGuardar, botonVolver;
    private JComboBox listaPaises, listaPuestos;
    private JPasswordField textoClave, textoReescribirClave;
    int diseno;
    private JFileChooser fileChooser;
    ValidacionTexto validar;
    
    public PantallaRegistroEmpleado(String title) {
    	System.out.println("        - Iniciando Pantalla Registro");
        inicializarComponentes(0);
        inicializarDiseno1();
        inicializarVista(title);
        inicializarListeners();
    }
    
    public PantallaRegistroEmpleado(String title, Usuario usr, int diseno) {
    	System.out.println("        - Iniciando Pantalla Registro");
        inicializarComponentes(diseno);
        switch(diseno){
        case 1:
        	inicializarDiseno2(usr);
        	break;
        case 2:
        	inicializarDiseno3(usr);
        	break;
        }
        inicializarVista(title);
        inicializarListeners();
    }
    public void inicializarListeners(){
        validar = new ValidarTexto();
        validar.validarTelefono(this.textoTelefono); 
        validar.validarNombre(textoNombre);
        validar.validarCorreo(textoCorreo);
        validar.validarCorreo(textoGmail);
        validar.validarCorreo(textoHotmail);
    }
    
    private void inicializarComponentes(int diseno){
        Date fecha =new Date();
        SimpleDateFormat fechaConvierteATexto = new SimpleDateFormat("dd-MM-yyyy");
        String fechaTexto=fechaConvierteATexto.format(fecha);
    	this.fileChooser=new JFileChooser();
    	this.diseno=diseno;
    	this.imagenAvatar=new JLabel();
        this.etiquetaCorreo=new JLabel("Correo Electrónico");
        this.etiquetaClave=new JLabel("Clave:");
        this.etiquetaTelefono=new JLabel("Telefono:");
        this.etiquetaSkype=new JLabel("Skype:");
        this.etiquetaGmail=new JLabel("Gmail:");
        this.etiquetaReescribirClave=new JLabel("Reescribir Clave:");
        this.etiquetaPuesto=new JLabel("Puesto Actual:");
        this.etiquetaPais=new JLabel("Pais:");
        this.etiquetaNombreCompleto=new JLabel("Nombre Completo:");
        this.etiquetaInfoGeneral=new JLabel("Información General");
        this.etiquetaInformacionLaboral=new JLabel("Infomación Laboral");
        this.etiquetaPlataformas=new JLabel("Otras Plataformas de Comunicación");
        this.etiquetaHotmail=new JLabel("Hotmail:"); 
        this.etiquetaFechaIngreso=new JLabel("Fecha de Ingreso");
        this.textoTelefono=new JTextField();
        
        this.textoSkype=new JTextField();
        this.textoReescribirClave=new JPasswordField();
        this.textoReescribirClave.setEchoChar('*');
        this.textoNombre=new JTextField();
        this.textoHotmail=new JTextField("@hotmail.com");
        this.textoGmail=new JTextField("@gmail.com");
        this.textoFechaIngreso=new JTextField(fechaTexto);
        this.textoFechaIngreso.setEditable(false);
        this.textoCorreo=new JTextField("@sozialen.com");
        this.textoClave= new JPasswordField();
        this.textoClave.setEchoChar('*');
        this.botonGuardar=new JButton("Guardar");
        this.botonGuardar.setSize(10, 40);
        this.botonVolver=new JButton("Volver");
        this.botonVolver.setSize(10, 40);
                
        Dimension dimensionAreaTexto=new Dimension(200,20);      
        this.textoTelefono.setPreferredSize(dimensionAreaTexto);
        this.textoSkype.setPreferredSize(dimensionAreaTexto);
        this.textoReescribirClave.setPreferredSize(dimensionAreaTexto);
        this.textoNombre.setPreferredSize(dimensionAreaTexto);
        this.textoHotmail.setPreferredSize(dimensionAreaTexto);
        this.textoGmail.setPreferredSize(dimensionAreaTexto);
        this.textoFechaIngreso.setPreferredSize(dimensionAreaTexto);
        this.textoCorreo.setPreferredSize(dimensionAreaTexto);
        this.textoClave.setPreferredSize(dimensionAreaTexto);
        
        Dimension dimensionEtiquetas=new Dimension(60,15);        
        this.etiquetaSkype.setPreferredSize(dimensionEtiquetas);
        this.etiquetaGmail.setPreferredSize(dimensionEtiquetas);
        this.etiquetaHotmail.setPreferredSize(dimensionEtiquetas);
        
        Dimension dimensionEtiquetasTop=new Dimension(300,40);        
        this.etiquetaInfoGeneral.setPreferredSize(dimensionEtiquetasTop);
        this.etiquetaInformacionLaboral.setPreferredSize(dimensionEtiquetasTop);
        this.etiquetaPlataformas.setPreferredSize(dimensionEtiquetasTop);
        this.etiquetaInfoGeneral.setHorizontalAlignment(SwingConstants.RIGHT);
        this.etiquetaInformacionLaboral.setHorizontalAlignment(SwingConstants.RIGHT);
        this.etiquetaPlataformas.setHorizontalAlignment(SwingConstants.LEFT);
        
        this.etiquetaInfoGeneral.setForeground(Color.BLUE);
        this.etiquetaInfoGeneral.setFont(new Font("Verdana", Font.BOLD, 12));
        this.etiquetaInformacionLaboral.setForeground(Color.BLUE);
        this.etiquetaInformacionLaboral.setFont(new Font("Verdana", Font.BOLD, 12));
        this.etiquetaPlataformas.setForeground(Color.BLUE);
        this.etiquetaPlataformas.setFont(new Font("Verdana", Font.BOLD, 12));
        
        iniciarComboBoxesPaisPuesto();
    }
    
    private void iniciarComboBoxesPaisPuesto(){
        String[] paisStrings = { "Costa Rica", "Alemania", "México", "Estados Unidos"};
         this.listaPaises= new JComboBox(paisStrings);
         this.listaPaises.setSelectedIndex(0);
         String[] puestoStrings = { "Analista en sistema", "Asegurador de Calidad", "Administrador de Base de Datos", "Administrador de Proyectos"};
         this.listaPuestos= new JComboBox(puestoStrings);
          this.listaPuestos.setSelectedIndex(0);
    }
    
    private void inicializarDiseno1(){
    	JPanel contenedorIntermedio=new JPanel(new BorderLayout());
    	//Imagen
        Dimension dimensionImagen=new Dimension(150,150);
        JPanel panelImagen=new JPanel(new BorderLayout());
        panelImagen.setPreferredSize(dimensionImagen);
        panelImagen.setBorder(BorderFactory.createLineBorder(Color.black, 1));
        
        //Parte Superior
        JPanel panelBorderArriba=new JPanel(new BorderLayout());
        JPanel panelGridInfoGeneral=new JPanel(new GridLayout(4,1));
        JPanel panelFlowInfoGeneral=new JPanel(new FlowLayout(SwingConstants.RIGHT));
        panelFlowInfoGeneral.add(this.etiquetaInfoGeneral);
        panelBorderArriba.add(panelFlowInfoGeneral, BorderLayout.NORTH);
        JPanel panelFlowNombre=new JPanel(new FlowLayout(SwingConstants.RIGHT));
        panelFlowNombre.add(this.etiquetaNombreCompleto);
        panelFlowNombre.add(this.textoNombre);
        JPanel panelFlowCorreo=new JPanel(new FlowLayout(SwingConstants.RIGHT));
        panelFlowCorreo.add(this.etiquetaCorreo);
        panelFlowCorreo.add(this.textoCorreo);
        JPanel panelFlowClave=new JPanel(new FlowLayout(SwingConstants.RIGHT));
        panelFlowClave.add(this.etiquetaClave);
        panelFlowClave.add(this.textoClave);
        JPanel panelFlowReClave=new JPanel(new FlowLayout(SwingConstants.RIGHT));
        panelFlowReClave.add(this.etiquetaReescribirClave);
        panelFlowReClave.add(this.textoReescribirClave);
        panelGridInfoGeneral.add(panelFlowNombre);
        panelGridInfoGeneral.add(panelFlowCorreo);
        panelGridInfoGeneral.add(panelFlowClave);
        panelGridInfoGeneral.add(panelFlowReClave);
        JPanel panelGridArriba=new JPanel(new GridLayout(1,2));
        panelGridArriba.add(panelImagen);
        panelGridArriba.add(panelGridInfoGeneral);
        panelBorderArriba.add(panelGridArriba, BorderLayout.CENTER);
        panelBorderArriba.add(panelFlowInfoGeneral, BorderLayout.NORTH);        
        contenedorIntermedio.add(panelBorderArriba, BorderLayout.NORTH);
        
        //Parte Inferior
        //Parte Izquierda
        JPanel panelBorderAbajo=new JPanel(new BorderLayout());
        JPanel panelBorderAbajo1=new JPanel(new BorderLayout());
        JPanel panelFlowSkype=new JPanel(new FlowLayout(SwingConstants.LEFT));
        panelFlowSkype.add(this.etiquetaSkype);
        panelFlowSkype.add(this.textoSkype);
        JPanel panelFlowGmail=new JPanel(new FlowLayout(SwingConstants.LEFT));
        panelFlowGmail.add(this.etiquetaGmail);
        panelFlowGmail.add(this.textoGmail);
        JPanel panelFlowHotmail=new JPanel(new FlowLayout(SwingConstants.LEFT));
        panelFlowHotmail.add(this.etiquetaHotmail);
        panelFlowHotmail.add(this.textoHotmail);
        JPanel panelGridPlataform=new JPanel(new GridLayout(4,1));
        panelGridPlataform.add(panelFlowSkype);
        panelGridPlataform.add(panelFlowGmail);
        panelGridPlataform.add(panelFlowHotmail);
        panelBorderAbajo1.add(this.etiquetaPlataformas, BorderLayout.NORTH);
        panelBorderAbajo1.add(panelGridPlataform, BorderLayout.WEST);
        panelBorderAbajo.add(panelBorderAbajo1, BorderLayout.WEST);
        //Parte Derecha
        JPanel panelBorderAbajo2=new JPanel(new BorderLayout());
        panelBorderAbajo2.add(this.etiquetaInformacionLaboral, BorderLayout.NORTH);
        JPanel panelFlowPais=new JPanel(new FlowLayout(SwingConstants.RIGHT));
        panelFlowPais.add(this.etiquetaPais);
        panelFlowPais.add(this.listaPaises);
        JPanel panelFlowTel=new JPanel(new FlowLayout(SwingConstants.RIGHT));
        panelFlowTel.add(this.etiquetaTelefono);
        panelFlowTel.add(this.textoTelefono);
        JPanel panelFlowPuesto=new JPanel(new FlowLayout(SwingConstants.RIGHT));
        panelFlowPuesto.add(this.etiquetaPuesto);
        panelFlowPuesto.add(this.listaPuestos);
        JPanel panelFlowFecha=new JPanel(new FlowLayout(SwingConstants.RIGHT));
        panelFlowFecha.add(this.etiquetaFechaIngreso);
        panelFlowFecha.add(this.textoFechaIngreso);
        
        JPanel GridInfoLaboral=new JPanel(new GridLayout(5,1));
        GridInfoLaboral.add(panelFlowPais);
        GridInfoLaboral.add(panelFlowTel);
        GridInfoLaboral.add(panelFlowPuesto);
        GridInfoLaboral.add(panelFlowFecha);
        JPanel panelFlowBotonGuardar=new JPanel(new FlowLayout(SwingConstants.RIGHT));
            panelFlowBotonGuardar.add(this.botonGuardar);
            GridInfoLaboral.add(panelFlowBotonGuardar);
        panelBorderAbajo2.add(GridInfoLaboral, BorderLayout.CENTER);

        panelBorderAbajo.add(panelBorderAbajo2, BorderLayout.CENTER);
        //
        contenedorIntermedio.add(panelBorderAbajo, BorderLayout.CENTER);
    	
        setContentPane(contenedorIntermedio);
    }
    
    private void inicializarDiseno2(Usuario usr){
    	//Personalizar
    	mostrarDatos(usr);
    	//cargarImagen(usr.getUrlAvatar());
    	
    	JPanel contenedorIntermedio=new JPanel(new BorderLayout());
    	//Imagen
        Dimension dimensionImagen=new Dimension(150,150);
        JPanel panelImagen=new JPanel(new FlowLayout(SwingConstants.WEST));
        panelImagen.setPreferredSize(dimensionImagen);
        panelImagen.add(this.imagenAvatar);
        
        //Parte Superior
        JPanel panelBorderArriba=new JPanel(new BorderLayout());
        JPanel panelGridInfoGeneral=new JPanel(new GridLayout(4,1));
        JPanel panelFlowInfoGeneral=new JPanel(new FlowLayout(SwingConstants.RIGHT));
        panelFlowInfoGeneral.add(this.etiquetaInfoGeneral);
        panelBorderArriba.add(panelFlowInfoGeneral, BorderLayout.NORTH);
        JPanel panelFlowNombre=new JPanel(new FlowLayout(SwingConstants.RIGHT));
        panelFlowNombre.add(this.etiquetaNombreCompleto);
        panelFlowNombre.add(this.textoNombre);
        JPanel panelFlowCorreo=new JPanel(new FlowLayout(SwingConstants.RIGHT));
        panelFlowCorreo.add(this.etiquetaCorreo);
        panelFlowCorreo.add(this.textoCorreo);
        JPanel panelFlowClave=new JPanel(new FlowLayout(SwingConstants.RIGHT));
        panelFlowClave.add(this.etiquetaClave);
        panelFlowClave.add(this.textoClave);
        JPanel panelFlowReClave=new JPanel(new FlowLayout(SwingConstants.RIGHT));
        panelFlowReClave.add(this.etiquetaReescribirClave);
        panelFlowReClave.add(this.textoReescribirClave);
        panelGridInfoGeneral.add(panelFlowNombre);
        panelGridInfoGeneral.add(panelFlowCorreo);
        panelGridInfoGeneral.add(panelFlowClave);
        panelGridInfoGeneral.add(panelFlowReClave);
        JPanel panelGridArriba=new JPanel(new GridLayout(1,2));
        panelGridArriba.add(panelImagen);
        panelGridArriba.add(panelGridInfoGeneral);
        panelBorderArriba.add(panelGridArriba, BorderLayout.CENTER);
        panelBorderArriba.add(panelFlowInfoGeneral, BorderLayout.NORTH);        
        contenedorIntermedio.add(panelBorderArriba, BorderLayout.NORTH);
        //Parte Inferior
        //Parte Izquierda
        JPanel panelBorderAbajo=new JPanel(new BorderLayout());
        JPanel panelBorderAbajo1=new JPanel(new BorderLayout());
        JPanel panelFlowSkype=new JPanel(new FlowLayout(SwingConstants.LEFT));
        panelFlowSkype.add(this.etiquetaSkype);
        panelFlowSkype.add(this.textoSkype);
        JPanel panelFlowGmail=new JPanel(new FlowLayout(SwingConstants.LEFT));
        panelFlowGmail.add(this.etiquetaGmail);
        panelFlowGmail.add(this.textoGmail);
        JPanel panelFlowHotmail=new JPanel(new FlowLayout(SwingConstants.LEFT));
        panelFlowHotmail.add(this.etiquetaHotmail);
        panelFlowHotmail.add(this.textoHotmail);
        JPanel panelGridPlataform=new JPanel(new GridLayout(4,1));
        panelGridPlataform.add(panelFlowSkype);
        panelGridPlataform.add(panelFlowGmail);
        panelGridPlataform.add(panelFlowHotmail);
        panelBorderAbajo1.add(this.etiquetaPlataformas, BorderLayout.NORTH);
        panelBorderAbajo1.add(panelGridPlataform, BorderLayout.WEST);
        panelBorderAbajo.add(panelBorderAbajo1, BorderLayout.WEST);
        //Parte Derecha
        JPanel panelBorderAbajo2=new JPanel(new BorderLayout());
        panelBorderAbajo2.add(this.etiquetaInformacionLaboral, BorderLayout.NORTH);
        JPanel panelFlowPais=new JPanel(new FlowLayout(SwingConstants.RIGHT));
        panelFlowPais.add(this.etiquetaPais);
        panelFlowPais.add(this.listaPaises);
        JPanel panelFlowTel=new JPanel(new FlowLayout(SwingConstants.RIGHT));
        panelFlowTel.add(this.etiquetaTelefono);
        panelFlowTel.add(this.textoTelefono);
        JPanel panelFlowPuesto=new JPanel(new FlowLayout(SwingConstants.RIGHT));
        panelFlowPuesto.add(this.etiquetaPuesto);
        panelFlowPuesto.add(this.listaPuestos);
        JPanel panelFlowFecha=new JPanel(new FlowLayout(SwingConstants.RIGHT));
        panelFlowFecha.add(this.etiquetaFechaIngreso);
        panelFlowFecha.add(this.textoFechaIngreso);
        JPanel GridInfoLaboral=new JPanel(new GridLayout(5,1));
        GridInfoLaboral.add(panelFlowPais);
        GridInfoLaboral.add(panelFlowTel);
        GridInfoLaboral.add(panelFlowPuesto);
        GridInfoLaboral.add(panelFlowFecha);
        
            JPanel panelFlowBotonGuardar=new JPanel(new FlowLayout(SwingConstants.RIGHT));
            panelFlowBotonGuardar.add(this.botonGuardar);
            GridInfoLaboral.add(panelFlowBotonGuardar);
       
        panelBorderAbajo2.add(GridInfoLaboral, BorderLayout.CENTER);

        panelBorderAbajo.add(panelBorderAbajo2, BorderLayout.CENTER);
        //
        contenedorIntermedio.add(panelBorderAbajo, BorderLayout.CENTER);
    	
        setContentPane(contenedorIntermedio);
    }
    
    private void inicializarDiseno3(Usuario usr){
    	//Personalizar
    	mostrarDatos(usr);
    	//cargarImagen(usr.getUrlAvatar());
    	noEditable();
    	
    	JPanel contenedorIntermedio=new JPanel(new BorderLayout());
    	//Imagen
    	Dimension dimensionImagen=new Dimension(150,150);
        JPanel panelImagen=new JPanel(new FlowLayout(SwingConstants.WEST));
        panelImagen.setPreferredSize(dimensionImagen);
        panelImagen.add(this.imagenAvatar);
        
        //Parte Superior
        JPanel panelBorderArriba=new JPanel(new BorderLayout());
        JPanel panelGridInfoGeneral=new JPanel(new GridLayout(3,1));
        JPanel panelFlowInfoGeneral=new JPanel(new FlowLayout(SwingConstants.RIGHT));
        panelFlowInfoGeneral.add(this.etiquetaInfoGeneral);
        panelBorderArriba.add(panelFlowInfoGeneral, BorderLayout.NORTH);
        JPanel panelFlowNombre=new JPanel(new FlowLayout(SwingConstants.RIGHT));
        panelFlowNombre.add(this.etiquetaNombreCompleto);
        panelFlowNombre.add(this.textoNombre);
        JPanel panelFlowCorreo=new JPanel(new FlowLayout(SwingConstants.RIGHT));
        panelFlowCorreo.add(this.etiquetaCorreo);
        panelFlowCorreo.add(this.textoCorreo);
        JPanel panelFlowClave=new JPanel(new FlowLayout(SwingConstants.RIGHT));
        panelFlowClave.add(this.etiquetaClave);
        panelFlowClave.add(this.textoClave);
        panelGridInfoGeneral.add(panelFlowNombre);
        panelGridInfoGeneral.add(panelFlowCorreo);
        panelGridInfoGeneral.add(panelFlowClave);
        JPanel panelGridArriba=new JPanel(new GridLayout(1,2));
        panelGridArriba.add(panelImagen);
        panelGridArriba.add(panelGridInfoGeneral);
        panelBorderArriba.add(panelGridArriba, BorderLayout.CENTER);
        panelBorderArriba.add(panelFlowInfoGeneral, BorderLayout.NORTH);        
        contenedorIntermedio.add(panelBorderArriba, BorderLayout.NORTH);
        
        //Parte Inferior
        //Parte Izquierda
        JPanel panelBorderAbajo=new JPanel(new BorderLayout());
        JPanel panelBorderAbajo1=new JPanel(new BorderLayout());
        JPanel panelFlowSkype=new JPanel(new FlowLayout(SwingConstants.LEFT));
        panelFlowSkype.add(this.etiquetaSkype);
        panelFlowSkype.add(this.textoSkype);
        JPanel panelFlowGmail=new JPanel(new FlowLayout(SwingConstants.LEFT));
        panelFlowGmail.add(this.etiquetaGmail);
        panelFlowGmail.add(this.textoGmail);
        JPanel panelFlowHotmail=new JPanel(new FlowLayout(SwingConstants.LEFT));
        panelFlowHotmail.add(this.etiquetaHotmail);
        panelFlowHotmail.add(this.textoHotmail);
        JPanel panelGridPlataform=new JPanel(new GridLayout(4,1));
        panelGridPlataform.add(panelFlowSkype);
        panelGridPlataform.add(panelFlowGmail);
        panelGridPlataform.add(panelFlowHotmail);
        panelBorderAbajo1.add(this.etiquetaPlataformas, BorderLayout.NORTH);
        panelBorderAbajo1.add(panelGridPlataform, BorderLayout.WEST);
        panelBorderAbajo.add(panelBorderAbajo1, BorderLayout.WEST);
        //Parte Derecha
        JPanel panelBorderAbajo2=new JPanel(new BorderLayout());
        panelBorderAbajo2.add(this.etiquetaInformacionLaboral, BorderLayout.NORTH);
        JPanel panelFlowPais=new JPanel(new FlowLayout(SwingConstants.RIGHT));
        panelFlowPais.add(this.etiquetaPais);
        panelFlowPais.add(this.listaPaises);
        JPanel panelFlowTel=new JPanel(new FlowLayout(SwingConstants.RIGHT));
        panelFlowTel.add(this.etiquetaTelefono);
        panelFlowTel.add(this.textoTelefono);
        JPanel panelFlowPuesto=new JPanel(new FlowLayout(SwingConstants.RIGHT));
        panelFlowPuesto.add(this.etiquetaPuesto);
        panelFlowPuesto.add(this.listaPuestos);
        JPanel panelFlowFecha=new JPanel(new FlowLayout(SwingConstants.RIGHT));
        panelFlowFecha.add(this.etiquetaFechaIngreso);
        panelFlowFecha.add(this.textoFechaIngreso);
        JPanel GridInfoLaboral=new JPanel(new GridLayout(5,1));
        GridInfoLaboral.add(panelFlowPais);
        GridInfoLaboral.add(panelFlowTel);
        GridInfoLaboral.add(panelFlowPuesto);
        GridInfoLaboral.add(panelFlowFecha);
        
            JPanel panelFlowBotonVolver=new JPanel(new FlowLayout(SwingConstants.RIGHT));
            panelFlowBotonVolver.add(this.botonVolver);
            GridInfoLaboral.add(panelFlowBotonVolver);
            
        panelBorderAbajo2.add(GridInfoLaboral, BorderLayout.CENTER);

        panelBorderAbajo.add(panelBorderAbajo2, BorderLayout.CENTER);
        //
        contenedorIntermedio.add(panelBorderAbajo, BorderLayout.CENTER);
    	
        setContentPane(contenedorIntermedio);
    }
    
    private void mostrarDatos(Usuario usr){
    	this.textoCorreo.setText(usr.getCorreo());
    	this.textoFechaIngreso.setText(usr.getFechaIngreso());
    	this.textoGmail.setText(usr.getGmail());
    	this.textoHotmail.setText(usr.getHotmail());
    	this.textoNombre.setText(usr.getNombre());
    	this.textoSkype.setText(usr.getSkype());
    	this.textoTelefono.setText(usr.getTelefono());
    	
    	switch(usr.getPais().charAt(0)){
    	case 'C':
    		this.listaPaises.setSelectedIndex(0);
    		break;
    	case 'A':
    		this.listaPaises.setSelectedIndex(1);
    		break;
    	case 'M':
    		this.listaPaises.setSelectedIndex(2);
    		break;
    	case 'E':
    		this.listaPaises.setSelectedIndex(3);
    		break;
    	}
    	switch(usr.getPuesto().charAt(1)){
    	case 'n':
    		this.listaPuestos.setSelectedIndex(0);
    		break;
    	case 's':
    		this.listaPuestos.setSelectedIndex(1);
    		break;
    	case 'd':
    		if(usr.getPuesto().charAt(17)=='B'){
    			this.listaPuestos.setSelectedIndex(2);
    		}else{
    			this.listaPuestos.setSelectedIndex(3);
    		}
    		break;
    	}
    }
    public void noEditable(){
    	this.textoClave.setEditable(false);
    	this.textoTelefono.setEditable(false);
    	this.textoSkype.setEditable(false);
    	this.textoNombre.setEditable(false);
    	this.textoHotmail.setEditable(false);
    	this.textoGmail.setEditable(false);
    	this.textoFechaIngreso.setEditable(false);
    	this.textoCorreo.setEditable(false);
    	this.listaPuestos.setEnabled(false);
    	this.listaPaises.setEnabled(false);
    }

    public void cargarImagen(String url){
    	Dimension dimensionImagen=new Dimension(150,150); 
    	this.imagenAvatar.setIcon(new ImageIcon(getClass().getResource(url)));
    	this.imagenAvatar.setPreferredSize(dimensionImagen);
    	this.imagenAvatar.setBorder(BorderFactory.createLineBorder(Color.black, 1));
    }
    
    private void inicializarVista(String title){
        setTitle(title);
        setSize(700,500);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setVisible(true);
    }
    public Usuario getUsuario(){
        Usuario user= new Usuario();
        user.setNombre(this.textoNombre.getText());
        user.setCorreo(this.textoCorreo.getText());
        char[] pass = textoClave.getPassword();
        String passString = new String(pass);
        user.setClave(passString);
        user.setSkype(this.textoSkype.getText());
        user.setHotmail(this.textoHotmail.getText());
        user.setGmail(this.textoGmail.getText());
        user.setPais(this.listaPaises.getSelectedItem().toString());
        user.setTelefono(this.textoTelefono.getText());
        user.setPuesto(this.listaPuestos.getSelectedItem().toString());
        user.setFechaIngreso(this.textoFechaIngreso.getText());
        return user;
    }
    public boolean requisitosRegistro(){
        if(vacio()){
            JOptionPane.showMessageDialog(this, "Necesita completar la información requerida");
            return false;
        }
            
        if(!camposContrasenasIguales()){
            JOptionPane.showMessageDialog(this, "Las contraseñas deben coincidir");
            return false;
        }
        return true;
    }
    private boolean vacio(){
        if(this.textoNombre.getText().isEmpty()) {
            return true;
        }
        if (this.textoClave.getPassword().length==0) {
            return true;
        }
        if(this.textoCorreo.getText().isEmpty()) {
            return true;
        }
        if(this.textoFechaIngreso.getText().isEmpty()) {
            return true;
        }
        return false;
    }
    private boolean camposContrasenasIguales(){
        JPasswordField input = (JPasswordField) this.textoClave;
        char[] password = input.getPassword();
        JPasswordField input2 = (JPasswordField) this.textoReescribirClave;
        char[] password2 = input2.getPassword();
        if(password.length!=password2.length) {
            return false;
        }
        for(int i=0; i<password.length;i++){
            if (password[i]!=password2[i]){
                return false;
            }
        }
        return true;
    }
    
    public void agregarAccionGuardarRegistro(MouseListener mouseListener){
        this.botonGuardar.addMouseListener(mouseListener);
    }
    public void removerAccionGuardarRegistro(MouseListener mouseListener){
        this.botonGuardar.removeMouseListener(mouseListener);
    }
    public void agregarAccionSalir(MouseListener mouseListener){
        this.botonVolver.addMouseListener(mouseListener);
    }
    public void removerAccionSalir(MouseListener mouseListener){
        this.botonVolver.removeMouseListener(mouseListener);
    }
    public void agregarAcionCerrar(WindowListener windowListener){
        this.addWindowListener(windowListener);
    }
    public void removerAccionGuardarRegistro(WindowListener windowListener){
        this.addWindowListener(windowListener);
    }
    public void elegirImagen(){
    	File archivo = null;
		if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
        	archivo = fileChooser.getSelectedFile();
        	
		}
		this.cargarImagen(archivo.getAbsolutePath());
    }
    

}

