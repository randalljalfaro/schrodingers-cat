package progra03.proyecto01.vista;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.MouseListener;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import progra03.proyecto01.modelo.Usuario;

public class PantallaBuscarEmpleado extends JFrame{
	
	JButton botonAgregar;
        JList listaEmpleados;
	
	public PantallaBuscarEmpleado(ArrayList<Usuario> empleados){
		System.out.println("        - Iniciando Pantalla Buscar Empleado");
		inicializarComponentes();
		inicializarDiseno(empleados);
		inicializarVista();
	}
	
	private final void inicializarComponentes(){
		this.botonAgregar=new JButton("Agregar");
	}
	
	private final void inicializarDiseno(ArrayList<Usuario> empleados){
		JPanel contenedorIntermedio=new JPanel(new BorderLayout());
		//ScrollBar Empleados
       /* Dimension dimensionEmpleados=new Dimension(200,100);
        DefaultTableModel dtm=new DefaultTableModel(0,1);
        for(int i=0; i<empleados.size(); i++){
        	String[] buffer={empleados.get(i).getNombre()};
        	dtm.addRow(buffer);
        }
        JTable tablaEmpleados=new JTable(dtm);
        tablaEmpleados.setRowHeight(30);
        tablaEmpleados.setTableHeader(null);
        JScrollPane panelScrollEmpleados=new JScrollPane();
        panelScrollEmpleados.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        panelScrollEmpleados.setPreferredSize(dimensionEmpleados);
        panelScrollEmpleados.setViewportView(tablaEmpleados);
        */
        Vector c= new Vector();
        for(int i=0; i<empleados.size(); i++){
            c.add(empleados.get(i).getNombre());
        }
        listaEmpleados = new JList();
        JScrollPane acrossScrollBar;
        listaEmpleados.setListData(c);
        listaEmpleados.setBorder(BorderFactory.createLineBorder(Color.black));
        listaEmpleados.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        listaEmpleados.setLayoutOrientation(JList.VERTICAL);

        acrossScrollBar= new JScrollPane(listaEmpleados);
        acrossScrollBar.setPreferredSize(new Dimension(250, 80));
        
        
        JPanel panelBorderCentro=new JPanel(new BorderLayout());
        panelBorderCentro.add(acrossScrollBar, BorderLayout.CENTER);
        JPanel panelFlowBoton=new JPanel(new FlowLayout(SwingConstants.RIGHT));
        panelFlowBoton.add(this.botonAgregar);
        panelBorderCentro.add(panelFlowBoton, BorderLayout.SOUTH);
        
        contenedorIntermedio.add(panelBorderCentro);
        
        setContentPane(contenedorIntermedio);
	}
	
    private final void inicializarVista(){
        setTitle("Buscar Empleados");
        setSize(400,200);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setVisible(true);
    }
   
    public void agregarAcionCerrar(WindowListener windowListener){
        this.addWindowListener(windowListener);
    }
    public void removerAccionCerrar(WindowListener windowListener){
        this.addWindowListener(windowListener);
    }
    public void agregarAccionAgregar(MouseListener mouseListener){
        this.botonAgregar.addMouseListener(mouseListener);
    }
    public void removerAccionAgregar(MouseListener mouseListener){
        this.botonAgregar.addMouseListener(mouseListener);
    }
    public String getNombreSeleccionado(){
        return this.listaEmpleados.getSelectedValue().toString();
    }
}
