package progra03.proyecto01.vista;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.MouseListener;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import progra03.proyecto01.modelo.Publicacion;
import progra03.proyecto01.modelo.Usuario;
import progra03.proyecto01.modelo.observador.EventoActualizacionPerfilUsuario;
import progra03.proyecto01.modelo.observador.ObservadorActualizacionPerfilUsuario;


public class PantallaPerfilEmpleado extends javax.swing.JFrame implements ObservadorActualizacionPerfilUsuario{
    
    JTextField textoPublicacion;
    JButton botonPublicar;
    JLabel etiquetaNuevaPublicacion,etiquetaPublicaciones,etiquetaCompaneros,etiquetaNombre;
    JLabel etiquetaPuesto,etiquetaPais,etiquetaMas,etiquetaVerPerfil,etiquetaBuscar, imagenAvatar;
    JTable tablePublicaciones;
    JList<String> listaCompaneros;
    Vector vectorListaCompaneros; 
    JPanel  panelCompletoPublicaciones;
    JPanel  panelCompletoPublicaciones2;
    JScrollPane panelScrollPublicaciones;
    JPanel contenedorIntermedio;
    public PantallaPerfilEmpleado(){
        this("Vacio", new Usuario());
    }
    public PantallaPerfilEmpleado(String title, Usuario usr) {
        System.out.println("        - Iniciando Pantalla "+title);
        inicializarComponenetes(usr.getNombre(), usr.getPais(), usr.getPuesto(), usr.getUrlAvatar());
        inicializarDiseno(usr.getCompaneros(), usr.getPublicaciones());
        inicializarVista(title);
    }

    private void inicializarComponenetes(String nombreUsr, String paisUsr, String puestoUsr, String urlAvatar){
    	this.imagenAvatar=new JLabel();
        this.botonPublicar=new JButton("Publicar"); 
        this.textoPublicacion=new JTextField();
        this.etiquetaBuscar=new JLabel("<html><u>Buscar</u></html>");
        this.etiquetaCompaneros=new JLabel("Mis Compañeros");
        this.etiquetaMas=new JLabel("<html><u>Mas...</u></html>");
        this.etiquetaNombre=new JLabel(nombreUsr);
        this.etiquetaNuevaPublicacion=new JLabel("Nueva Publicación");
        this.etiquetaPais=new JLabel(paisUsr);
        this.etiquetaPuesto=new JLabel(puestoUsr);
        this.etiquetaVerPerfil=new JLabel("<html><u>Ver Perfil</u></html>");
        this.etiquetaPublicaciones=new JLabel("Publicaciones");
        
        Dimension dimensionEtiquetasTop=new Dimension(200,20);
        this.etiquetaPais.setPreferredSize(dimensionEtiquetasTop);
        this.etiquetaPuesto.setPreferredSize(dimensionEtiquetasTop);
        this.etiquetaMas.setPreferredSize(dimensionEtiquetasTop);
        this.etiquetaNombre.setPreferredSize(dimensionEtiquetasTop);
        this.etiquetaPais.setHorizontalAlignment(SwingConstants.LEFT);
        this.etiquetaPuesto.setHorizontalAlignment(SwingConstants.LEFT);
        this.etiquetaMas.setHorizontalAlignment(SwingConstants.LEFT);
        this.etiquetaNombre.setHorizontalAlignment(SwingConstants.LEFT);
        this.textoPublicacion.setPreferredSize(dimensionEtiquetasTop);
        
        Dimension dimensionEtiquetasLow=new Dimension(150,15);        
        this.etiquetaNuevaPublicacion.setPreferredSize(dimensionEtiquetasLow);
        this.etiquetaPublicaciones.setPreferredSize(dimensionEtiquetasLow);
        this.etiquetaCompaneros.setHorizontalAlignment(SwingConstants.LEFT);
        this.etiquetaNuevaPublicacion.setHorizontalAlignment(SwingConstants.LEFT);
        this.etiquetaPublicaciones.setHorizontalAlignment(SwingConstants.LEFT);
        this.etiquetaCompaneros.setPreferredSize(dimensionEtiquetasLow);
        
        Dimension dimensionEtiquetasLower=new Dimension(70,25); 
        this.etiquetaVerPerfil.setPreferredSize(dimensionEtiquetasLower);
        this.etiquetaBuscar.setHorizontalAlignment(SwingConstants.RIGHT);
        this.etiquetaVerPerfil.setHorizontalAlignment(SwingConstants.LEFT);
        Dimension dimensionEtiquetaBuscar=new Dimension(50,10); 
        this.etiquetaBuscar.setPreferredSize(dimensionEtiquetaBuscar);
        
        this.etiquetaMas.setForeground(Color.BLUE);
        this.etiquetaVerPerfil.setForeground(Color.BLUE);
        this.etiquetaBuscar.setForeground(Color.BLUE);
        //cargarImagen(urlAvatar
        listaCompaneros = new JList();
        vectorListaCompaneros= new Vector();
        tablePublicaciones=new JTable();
        
        this.panelCompletoPublicaciones=new JPanel(new GridLayout());
        this.contenedorIntermedio=new JPanel(new BorderLayout());
    }
    
    private void inicializarDiseno(ArrayList<String> companeros, ArrayList<Publicacion> publicaciones){
        
        //Imagen
        Dimension dimensionImagen=new Dimension(150,150);
        JPanel panelImagen=new JPanel(new BorderLayout());
        panelImagen.setPreferredSize(dimensionImagen);
        panelImagen.add(this.imagenAvatar, BorderLayout.CENTER);
        
        //ScrollBar Compañeros
        
        for(int i=0; i<companeros.size(); i++){
            vectorListaCompaneros.add(companeros.get(i));
        }
        listaCompaneros.setListData(vectorListaCompaneros);
        listaCompaneros.setBorder(BorderFactory.createLineBorder(Color.black));
        listaCompaneros.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        listaCompaneros.setLayoutOrientation(JList.VERTICAL);

         JScrollPane panelScrollCompaneros = new JScrollPane(listaCompaneros);
        //Panel de publicaciones
       Dimension dimensionPublicacion=new Dimension(400,200);
            Dimension dimensionImagenPublicacion=new Dimension(100,100);
            
            this.panelCompletoPublicaciones.setSize(new Dimension(400,200));
       for(int i=0; i<publicaciones.size(); i++){
           this.panelCompletoPublicaciones.add(getPanelBorderPublicacion(publicaciones.get(i).getFecha(),
                    publicaciones.get(i).getNombrePublicador(),
                    publicaciones.get(i).getTextoPublicacion()));
           }
            panelScrollPublicaciones=new JScrollPane();
            panelScrollPublicaciones.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
            panelScrollPublicaciones.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
           panelScrollPublicaciones.setSize(new Dimension(400,200));
            panelScrollPublicaciones.setViewportView(this.panelCompletoPublicaciones);
            this.panelCompletoPublicaciones2=new JPanel(new GridLayout(1,1));
            this.panelCompletoPublicaciones2.add(panelScrollPublicaciones);
            this.panelCompletoPublicaciones2.setSize(new Dimension(400,200));
            //Parte Superior
        JPanel panelFlowArriba=new JPanel(new FlowLayout(FlowLayout.LEFT));
        JPanel panelGridArriba=new JPanel(new GridLayout(4,1));
        panelGridArriba.add(this.etiquetaNombre);
        panelGridArriba.add(this.etiquetaPuesto);
        panelGridArriba.add(this.etiquetaPais);
        panelGridArriba.add(this.etiquetaMas);
        panelFlowArriba.add(panelImagen);
        panelFlowArriba.add(panelGridArriba);
        contenedorIntermedio.add(panelFlowArriba, BorderLayout.NORTH);
        
        //Parte Inferior
        JPanel panelBorderAbajo=new JPanel(new BorderLayout());
        //Izquierda
        JPanel panelBorderAbajo1=new JPanel(new BorderLayout());
        JPanel panelBorderAbajo12=new JPanel(new BorderLayout());
        panelBorderAbajo12.add(this.etiquetaCompaneros, BorderLayout.NORTH);
        panelBorderAbajo12.add(panelScrollCompaneros, BorderLayout.CENTER);
        JPanel panelFlowVerBuscar=new JPanel(new FlowLayout(SwingConstants.RIGHT));
        panelFlowVerBuscar.add(this.etiquetaVerPerfil);
        panelFlowVerBuscar.add(this.etiquetaBuscar);
        panelBorderAbajo12.add(panelFlowVerBuscar, BorderLayout.SOUTH);
        panelBorderAbajo1.add(panelBorderAbajo12, BorderLayout.CENTER);
        panelBorderAbajo1.add(new JSeparator(SwingConstants.VERTICAL),BorderLayout.EAST);
        panelBorderAbajo.add(panelBorderAbajo1, BorderLayout.WEST);
        //Derecha
        JPanel panelBorderAbajo2=new JPanel(new BorderLayout());
        JPanel panelGridAbajo2=new JPanel(new GridLayout(4,1));
        panelGridAbajo2.add(this.etiquetaNuevaPublicacion);
        panelGridAbajo2.add(this.textoPublicacion);
        JPanel panelFlowBotonPublicar=new JPanel(new FlowLayout(SwingConstants.RIGHT));
        panelFlowBotonPublicar.add(this.botonPublicar);
        panelGridAbajo2.add(panelFlowBotonPublicar);
        panelGridAbajo2.add(this.etiquetaPublicaciones);
        panelBorderAbajo2.add(panelGridAbajo2, BorderLayout.NORTH);
        panelBorderAbajo2.add(this.panelCompletoPublicaciones2, BorderLayout.CENTER);
        panelBorderAbajo.add(panelBorderAbajo2, BorderLayout.CENTER);
        
        contenedorIntermedio.add(panelBorderAbajo, BorderLayout.CENTER);
        contenedorIntermedio.add(new JSeparator(), BorderLayout.SOUTH);
        
        setContentPane(contenedorIntermedio);
    }

    public void cargarImagen(String url){
    	Dimension dimensionImagen=new Dimension(150,150); 
    	this.imagenAvatar.setIcon(new ImageIcon(getClass().getResource(url)));
    	this.imagenAvatar.setPreferredSize(dimensionImagen);
    	this.imagenAvatar.setBorder(BorderFactory.createLineBorder(Color.black, 1));
    }
    
    private void inicializarVista(String title){
        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setSize(600,500);
        setLocationByPlatform(true);
        setTitle(title);
        setVisible(true);
    }
    
    
    public void agregarAccionMasInfoContacto(MouseListener mouseListener){
		this.etiquetaMas.addMouseListener(mouseListener);
	}
	
   public void removerAccionMasInfoContacto(MouseListener mouseListener){
		this.etiquetaMas.removeMouseListener(mouseListener);
	}
   public void agregarAccionVerPerfilCompanero(MouseListener mouseListener){
		this.etiquetaVerPerfil.addMouseListener(mouseListener);
	}
	
   public void removerAccionVerPerfilCompanero(MouseListener mouseListener){
		this.etiquetaVerPerfil.removeMouseListener(mouseListener);
	}
   public void agregarAcionCerrar(WindowListener windowListener){
        this.addWindowListener(windowListener);
    }
    public void removerAccionCerrar(WindowListener windowListener){
        this.removeWindowListener(windowListener);
    }
    public void agregarAccionBuscarEmpleado(MouseListener mouseListener){
		this.etiquetaBuscar.addMouseListener(mouseListener);
	}
	
   public void removerAccionBuscarEmpleado(MouseListener mouseListener){
		this.etiquetaBuscar.removeMouseListener(mouseListener);
	}
   public void agregarAccionPublicar(MouseListener mListener){
       this.botonPublicar.addMouseListener(mListener);
   }
   public void removerAccionPublicar(MouseListener mListener){
       this.botonPublicar.removeMouseListener(mListener);
   }
   public String getNombreSeleccionado(){
        return this.listaCompaneros.getSelectedValue().toString();
    }
   
   @Override
    public void manejarEdicionPerfil(EventoActualizacionPerfilUsuario erp) {
        this.etiquetaNombre.setText(erp.getUsuarioEditado().getNombre());
        this.etiquetaPais.setText(erp.getUsuarioEditado().getPais());
        this.etiquetaPuesto.setText(erp.getUsuarioEditado().getPuesto());
    }
   @Override
    public void manejarAdicionCompanero(EventoActualizacionPerfilUsuario erp) {
         vectorListaCompaneros.add(erp.getNombreNuevoCompanero());
         listaCompaneros.setListData(vectorListaCompaneros);
    }
   
   @Override
    public void manejarPublicacion(EventoActualizacionPerfilUsuario erp) {
       this.panelCompletoPublicaciones.add(getPanelBorderPublicacion(erp.getPublicacion().getFecha(),
               erp.getPublicacion().getNombrePublicador(),
               erp.getPublicacion().getTextoPublicacion()));
       
       this.panelCompletoPublicaciones.repaint();
       this.panelScrollPublicaciones.repaint();
       this.contenedorIntermedio.repaint();
   }
    private JPanel getPanelBorderPublicacion(String fecha, String publicador, String publicacion){
            Dimension dimensionPublicacion=new Dimension(500,400);
            Dimension dimensionImagenPublicacion=new Dimension(100,100);
            JLabel etiquetaFecha= new JLabel(fecha);
            JLabel etiquetaNombrePublica=new JLabel(publicador);
            JLabel etiquetaPublicacion=new JLabel(publicacion);
            JPanel panelFlowNombreFecha=new JPanel(new FlowLayout());
            panelFlowNombreFecha.add(etiquetaNombrePublica);
            panelFlowNombreFecha.add(etiquetaFecha);
            JPanel panelFlowPublicacion=new JPanel(new FlowLayout());
            panelFlowPublicacion.add(etiquetaPublicacion);
            JPanel panelGridPublicacion=new JPanel (new GridLayout(2,1));
            panelGridPublicacion.add(panelFlowNombreFecha);
            panelGridPublicacion.add(panelFlowPublicacion);
            panelGridPublicacion.setSize(new Dimension(500,300));
            JLabel etiquetaImagen=new JLabel("IMAGEN");
            JPanel panelImagenPublicacion=new JPanel(new BorderLayout());
            panelImagenPublicacion.setSize(dimensionImagenPublicacion);
            panelImagenPublicacion.add(etiquetaImagen);
            JPanel panelBorderPublicacion= new JPanel(new BorderLayout());
            panelBorderPublicacion.add(panelGridPublicacion, BorderLayout.CENTER);
            panelBorderPublicacion.add(panelImagenPublicacion, BorderLayout.WEST);
            panelBorderPublicacion.setSize(dimensionPublicacion);
            panelBorderPublicacion.setBorder(BorderFactory.createLineBorder(Color.black));
            return panelBorderPublicacion;
    }

    public String getTextoPublicacionPerfil(){
        return this.textoPublicacion.getText();
    }
}
