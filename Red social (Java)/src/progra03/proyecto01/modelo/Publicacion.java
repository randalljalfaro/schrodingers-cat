package progra03.proyecto01.modelo;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
public class Publicacion {
    
    String nombrePublicador, publicacion, fechaTexto;
    Date fecha;
    public Publicacion(){
         nombrePublicador=""; 
         publicacion=""; 
         fechaTexto="";
         this.fecha=new Date();
    }
    public Publicacion(String nombrePublicador, String publicacion, Date fecha,Calendar calendario ){
        calendario = new GregorianCalendar();
        int hora =calendario.get(Calendar.HOUR_OF_DAY);
        int minutos = calendario.get(Calendar.MINUTE);
        int segundos = calendario.get(Calendar.SECOND);
        
        this.nombrePublicador=nombrePublicador;
        this.fecha=fecha;
        SimpleDateFormat fechaConvierteATexto = new SimpleDateFormat("dd-MM-yyyy");
        String fechaHora= fechaConvierteATexto.format(fecha)+"/"+
                hora+":"+minutos+":"+segundos;
        this.fechaTexto=fechaHora;
        this.publicacion=publicacion;
    }
    
    public void setNombrePublicador(String nombrePublicador){
        this.nombrePublicador=nombrePublicador;
    }
    public void setPublicacion(String publicacion){
        this.publicacion=publicacion;
    }
    public void setFecha(String fecha){
        this.fechaTexto=fecha;
    }
    public String getNombrePublicador(){
        return this.nombrePublicador;
    }
    public String getTextoPublicacion(){
        return this.publicacion;
    }
    public String getFecha(){
        return this.fechaTexto;
    }
}
