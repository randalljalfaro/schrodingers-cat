
package progra03.proyecto01.modelo.observador;
import java.util.ArrayList;
import progra03.proyecto01.modelo.*;
public class EventoActualizacionPerfilUsuario {
    private String nuevoCompanero;
    private Usuario usuarioEditado;
    private Usuario usuarioPublica;
    Publicacion publicacion;

    public EventoActualizacionPerfilUsuario(Usuario userEditado) {
        this.usuarioEditado=userEditado;
    }
    public EventoActualizacionPerfilUsuario(String nuevoCompanero) {
        this.nuevoCompanero=nuevoCompanero;
    }
    public Usuario getUsuarioEditado() {
        return this.usuarioEditado;
    }
    public String getNombreNuevoCompanero(){
        return this.nuevoCompanero;
    }
    public EventoActualizacionPerfilUsuario(Publicacion publicacion) {
        this.publicacion=publicacion;
    }
    public Publicacion getPublicacion(){
        return this.publicacion;
    }
    

    /*public EventoActualizacionPerfilUsuario(Usuario personaRegistrada, Usuario personaEditada, int indexPersonaEditada, int indexPersonaEliminada) {
        this.personaRegistrada = personaRegistrada;
        this.personaEditada = personaEditada;
        this.indexPersonaEditada = indexPersonaEditada;
        this.indexPersonaEliminada = indexPersonaEliminada;
    }

    public void setIndexPersonaEditada(int indexPersonaEditada) {
        this.indexPersonaEditada = indexPersonaEditada;
    }

    public void setPersonaEditada(Usuario personaEditada) {
        this.personaEditada = personaEditada;
    }

    public int getIndexPersonaEditada() {
        return indexPersonaEditada;
    }

    

    public Persona getPersonaRegistrada() {
        return personaRegistrada;
    }

    public void setPersonaRegistrada(Usuario personaRegistrada) {
        this.personaRegistrada = personaRegistrada;
    }

    public void setIndexPersonaEliminada(int indexPersonaEliminada) {
        this.indexPersonaEliminada = indexPersonaEliminada;
    }

    public int getIndexPersonaEliminada() {
        return indexPersonaEliminada;
    }
    * */
}
