package progra03.proyecto01.modelo.observador;

public interface ObservadorActualizacionPerfilUsuario {
    public void manejarPublicacion(EventoActualizacionPerfilUsuario erp);

    public void manejarAdicionCompanero(EventoActualizacionPerfilUsuario erp);

    public void manejarEdicionPerfil(EventoActualizacionPerfilUsuario erp);
}
