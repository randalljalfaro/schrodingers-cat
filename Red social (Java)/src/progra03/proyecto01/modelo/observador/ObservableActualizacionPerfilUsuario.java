
package progra03.proyecto01.modelo.observador;
public interface ObservableActualizacionPerfilUsuario {
    public void agregarObservadorActualizacionPerfil(ObservadorActualizacionPerfilUsuario orp);
    public void removerObservadorActualizacionPerfil(ObservadorActualizacionPerfilUsuario orp);
    public void notificarEdicionPerfilUsuario(EventoActualizacionPerfilUsuario erp);
    public void notificarAdicionCompanero(EventoActualizacionPerfilUsuario erp);
    public void notificarPublicacion(EventoActualizacionPerfilUsuario erp);
}
