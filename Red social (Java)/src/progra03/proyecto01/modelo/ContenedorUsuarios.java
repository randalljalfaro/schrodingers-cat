
package progra03.proyecto01.modelo;
import java.io.File;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import progra03.persistencia.texto.*;
import progra03.proyecto01.modelo.observador.EventoActualizacionPerfilUsuario;
import progra03.proyecto01.modelo.observador.ObservableActualizacionPerfilUsuario;
import progra03.proyecto01.modelo.observador.ObservadorActualizacionPerfilUsuario;

public class ContenedorUsuarios implements ObservableActualizacionPerfilUsuario{
    private static final String SEPARADOR_USUARIO = " # ";
    private static final String SEPARADOR_ATRIBUTOS_USUARIO = " - ";
    private static final String SEPARADOR_COMPANEROS_USUARIO = " * ";
    private static final String SEPARADOR_NOMBRES_COMPANEROS_USUARIO = " + ";
    private static final String SEPARADOR_PUBLICACION = " * ";
    private static final String SEPARADOR_ATRIBUTOS_PUBLICACION = " + ";
    private static final int POSICION_NOMBRE = 0;
    private static final int POSICION_CORREO = 1;
    private static final int POSICION_CLAVE=2;
    private static final int POSICION_SKYPE=3;
    private static final int POSICION_GMAIL=4;
    private static final int POSICION_HOTMAIL=5;
    private static final int POSICION_PAIS=6;
    private static final int POSICION_TELEFONO=7;
    private static final int POSICION_PUESTO=8;
    private static final int POSICION_FECHA_INGRESO=9;
    private static final int POSICION_IMAGEN=10;
    
    private ArrayList<Usuario> usuarios;
    private ArrayList<ObservadorActualizacionPerfilUsuario> observadores;
    private ArrayList<Publicacion> publicaciones;
    
    int contadorUsuarios;
    
    public ContenedorUsuarios(ArrayList<Usuario> usuarios){
        this.usuarios=usuarios;
        this.observadores=new ArrayList<>();
        this.contadorUsuarios=0;
    }
    public ContenedorUsuarios(){
        this.usuarios=new ArrayList<>();
        this.observadores=new ArrayList<>();
        this.publicaciones= new ArrayList<>();
        this.contadorUsuarios=0;
        
    }
    public void  agregarUserRequisitosRegistro(Usuario user){
        boolean registrado=false;
        if(!this.estaUsuarioRegistrado(user)){
            agregarUsuario(user);
            registrado=true;
        }
        if(!registrado){
           JOptionPane.showMessageDialog(null, "Usuario " + user.getNombre() +
                   " o el correo "+ user.getCorreo()+" se encuentra registrado.");
        }
        else{
            JOptionPane.showMessageDialog(null, "Usuario " + user.getNombre() + " se registró exitosamente");
        }
    }
    public void agregarUsuario(Usuario user){
        this.usuarios.add(user);
        }
    public void removerUsuario(Usuario user){
        this.usuarios.remove(user);
    }
    public ArrayList<Usuario> getListaUsuario(){
        return this.usuarios;
    }
    
    public Usuario getUsuarioNombre(String nombre){
        Usuario userAux=new Usuario();
        for(int i=0; i<this.usuarios.size(); i++ ){
            if(this.usuarios.get(i).getNombre().equals(nombre)){
                userAux=this.usuarios.get(i);
                i=this.usuarios.size();
            }    
        }
        return userAux;
    }
    public boolean estaUsuarioNombre(String nombre){
        for(int i=0; i<this.usuarios.size(); i++ ){
            if(this.usuarios.get(i).getNombre().equals(nombre)){
                return true;
            }    
        }
        return false;
    }
    public boolean estaPublicacion(Publicacion p){
        for(int i=0; i<this.publicaciones.size(); i++ ){
            if(this.publicaciones.get(i).getTextoPublicacion().equals(p.getTextoPublicacion())){
                return true;
            }    
        }
        return false;
    }
    
    public void agregarCompaneros(String nombre, String nombre2){
        if(estaUsuarioNombre(nombre2)&&estaUsuarioNombre(nombre))
        {
        for(int i=0; i<this.usuarios.size(); i++ ){
            if(this.usuarios.get(i).getNombre().equals(nombre)){
                //Solo si no se ha agregado antes
                
                if(!this.usuarios.get(i).estaCompanero(nombre2)
                        &&!this.usuarios.get(i).getNombre().equals(nombre2)){
                    
                    this.usuarios.get(i).addCompanero(nombre2);
                    notificarAdicionCompanero(new EventoActualizacionPerfilUsuario(nombre2));
                }
            }  
            
            if(this.usuarios.get(i).getNombre().equals(nombre2)){
                if(!this.usuarios.get(i).estaCompanero(nombre)
                        &&!this.usuarios.get(i).getNombre().equals(nombre)){
                    this.usuarios.get(i).addCompanero(nombre);
                }
            }
        }
        } 
    }
    
    
    public Usuario getUsuarioCuenta(String cuenta, String clave){
        Usuario userAux=new Usuario();
        for(int i=0; i<this.usuarios.size(); i++ ){
            
            if(this.usuarios.get(i).getCorreo().equals(cuenta)&&
                   this.usuarios.get(i).getClave().equals(clave) ){
                userAux=this.usuarios.get(i);
                i=this.usuarios.size();
            }    
        }
        return userAux;
    }
    
    public boolean estaUsuario(String cuenta, String clave){
        for(int i=0; i<this.usuarios.size(); i++ ){
            
            if(this.usuarios.get(i).getCorreo().equals(cuenta)&&
                   this.usuarios.get(i).getClave().equals(clave) ){
               return true;
            }
        }
        return false;
    }
    //debe haber alguna variable única (correo, nombre, o nuevo atributo ID)  **provicional - nombre
    public boolean estaUsuarioRegistrado(Usuario user){
        for(int i=0; i<this.usuarios.size(); i++ ){
            if(comparaNombreUsuario(this.usuarios.get(i),user)||
                    comparaCorreoUsuario(this.usuarios.get(i),user)){
               return true;
            }
        }
        return false;
    }
     
    public void modificarInfoUsuario(Usuario userInfoAnterior, Usuario userNuevainfo){
        //Ya que este método para saber cual usuario modificó la info debe comparar con la informacion
        //anterior ya que pudo haber cambiado el correo o la clave, así que requiere el anterior
        //Se puede usar para modificar otras informaciones
        notificarEdicionPerfilUsuario(new EventoActualizacionPerfilUsuario(userNuevainfo));
        
        for(int i=0; i<this.usuarios.size(); i++){
           if(!comparaNombreUsuario(this.usuarios.get(i),userInfoAnterior)) {
               if(this.usuarios.get(i).estaCompanero(userInfoAnterior.getNombre())) {
                   this.usuarios.get(i).actualizarNombreCompanero(
                            userInfoAnterior.getNombre(), userNuevainfo.getNombre());
               }
           }
        }
         
        for(int i=0; i<this.usuarios.size(); i++){
            if(comparaCuentaUsuario(this.usuarios.get(i), userInfoAnterior)){
                this.usuarios.get(i).setNombre(userNuevainfo.getNombre());
                this.usuarios.get(i).setCorreo(userNuevainfo.getCorreo());
                this.usuarios.get(i).setClave(userNuevainfo.getClave());
                this.usuarios.get(i).setSkype(userNuevainfo.getSkype());
                this.usuarios.get(i).setHotmail(userNuevainfo.getHotmail());
                this.usuarios.get(i).setGmail(userNuevainfo.getGmail());
                this.usuarios.get(i).setPais(userNuevainfo.getPais());
                this.usuarios.get(i).setTelefono(userNuevainfo.getTelefono());
                this.usuarios.get(i).setPuesto(userNuevainfo.getPuesto());
                this.usuarios.get(i).setFechaIngreso(userNuevainfo.getFechaIngreso());
            }
        }
    }
    
    private boolean comparaCuentaUsuario(Usuario user, Usuario user2){
        if (user.getCorreo().equals(user2.getCorreo())){
            return true;
        }
        if (user.getClave().equals(user2.getClave())){
            return true;
        }
        return false;
    }
    private boolean comparaNombreUsuario(Usuario user, Usuario user2){
        if (user.getNombre().equals(user2.getNombre())){
            return true;
        }
        return false;
    }
    private boolean comparaCorreoUsuario(Usuario user, Usuario user2){
        if (user.getCorreo().equals(user2.getCorreo())){
            return true;
        }
        return false;
    }
    
    public void enviarPublicacion(Publicacion p){
        //Ya que este método para saber cual usuario modificó la info debe comparar con la informacion
        //anterior ya que pudo haber cambiado el correo o la clave, así que requiere el anterior
        //Se puede usar para modificar otras informaciones
        notificarPublicacion(new EventoActualizacionPerfilUsuario(p));
        this.publicaciones.add(p);
        Usuario auxUsuarioEnviaPublicacion=this.getUsuarioNombre(p.getNombrePublicador());
        for(int i=0; i<this.usuarios.size(); i++){
            
            //agrega la publicacion al perfil de sus compañeros
            for(int j=0; j<auxUsuarioEnviaPublicacion.getCompaneros().size(); j++){
                Usuario auxUsuarioRecibePublicacion=this.getUsuarioNombre(auxUsuarioEnviaPublicacion.getCompaneros().get(j));
                
                if(comparaNombreUsuario(this.usuarios.get(i), auxUsuarioRecibePublicacion)){
                    this.usuarios.get(i).addPublicacion(p);
                } 
            }
            //Se agrega tambien al perfil de quien lo envia
            if(comparaNombreUsuario(this.usuarios.get(i), auxUsuarioEnviaPublicacion)){
                this.usuarios.get(i).addPublicacion(p);
            }
            
        }
    }
    
    @Override
    public void notificarEdicionPerfilUsuario(EventoActualizacionPerfilUsuario erp) {
        for(int i=0; i<this.observadores.size(); i++ ){
            this.observadores.get(i).manejarEdicionPerfil(erp);
        }
    }
    @Override
    public void notificarAdicionCompanero(EventoActualizacionPerfilUsuario erp) {
        for(int i=0; i<this.observadores.size(); i++ ){
            this.observadores.get(i).manejarAdicionCompanero(erp);
        }
    }

    @Override
    public void agregarObservadorActualizacionPerfil(ObservadorActualizacionPerfilUsuario orp){
        this.observadores.add(orp);
    }

    @Override
    public void notificarPublicacion(EventoActualizacionPerfilUsuario erp) {
        for(int i=0; i<this.observadores.size(); i++ ){
            this.observadores.get(i).manejarPublicacion(erp);
        }
    }
    
    @Override
    public void removerObservadorActualizacionPerfil(ObservadorActualizacionPerfilUsuario orp) {
        this.observadores.remove(orp);
    }
    
    public void exportarATexto(File archivo){
        PersistibleTexto pt=new PersistenciaTexto();
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < usuarios.size(); i++) {
            if (i > 0) {
                sb.append(SEPARADOR_USUARIO);
            }

            Usuario user = usuarios.get(i);
            sb.append(user.getNombre());
            sb.append(SEPARADOR_ATRIBUTOS_USUARIO);
            sb.append(user.getCorreo());
            sb.append(SEPARADOR_ATRIBUTOS_USUARIO);
            sb.append(user.getClave());
            sb.append(SEPARADOR_ATRIBUTOS_USUARIO);
            sb.append(user.getSkype());
            sb.append(SEPARADOR_ATRIBUTOS_USUARIO);
            sb.append(user.getGmail());
            sb.append(SEPARADOR_ATRIBUTOS_USUARIO);
            sb.append(user.getHotmail());
            sb.append(SEPARADOR_ATRIBUTOS_USUARIO);
            sb.append(user.getPais());
            sb.append(SEPARADOR_ATRIBUTOS_USUARIO);
            sb.append(user.getTelefono());
            sb.append(SEPARADOR_ATRIBUTOS_USUARIO);
            sb.append(user.getPuesto());
            sb.append(SEPARADOR_ATRIBUTOS_USUARIO);
            sb.append(user.getFechaIngreso());
            sb.append(SEPARADOR_ATRIBUTOS_USUARIO);
            sb.append(user.getUrlAvatar());
            
        }
        
        pt.guardarHaciaArchivo(archivo, sb.toString());
    }
    public void importarDesdeTexto(File archivo) {
        PersistibleTexto pt = new PersistenciaTexto();

        String resultado = pt.leerDesdeArchivo(archivo);
        String[] usuarioLectura = resultado.split(SEPARADOR_USUARIO);

        for (String usuariosLectura : usuarioLectura) {
            contadorUsuarios++;
            String[] datosUsuario = usuariosLectura.split(SEPARADOR_ATRIBUTOS_USUARIO);
            Usuario user=new Usuario();
            user.setNombre(datosUsuario[POSICION_NOMBRE]);
            user.setCorreo(datosUsuario[POSICION_CORREO]);
            user.setClave(datosUsuario[POSICION_CLAVE]);
            user.setSkype(datosUsuario[POSICION_SKYPE]);
            user.setGmail(datosUsuario[POSICION_GMAIL]);
            user.setHotmail(datosUsuario[POSICION_HOTMAIL]);
            user.setPais(datosUsuario[POSICION_PAIS]);
            user.setTelefono(datosUsuario[POSICION_TELEFONO]);
            user.setPuesto(datosUsuario[POSICION_PUESTO]);
            user.setFechaIngreso(datosUsuario[POSICION_FECHA_INGRESO]);
            user.setUrlAvatar(datosUsuario[POSICION_IMAGEN]);
            if(!estaUsuarioNombre(user.getNombre())) {
                agregarUsuario(user);
            } 
        }
    }
    
    public void exportarATextoCompaneros(File archivo){
       if(this.contadorUsuarios>usuarios.size())
       {
        PersistibleTexto pt=new PersistenciaTexto();
        StringBuilder sb = new StringBuilder();
        int aux=0;
        for (int i = 0; i < usuarios.size(); i++) {
            
            
            if(usuarios.get(i).getCompaneros().size()>0){
                if (aux > 0) {
                    sb.append(SEPARADOR_COMPANEROS_USUARIO);
                }
                Usuario user = usuarios.get(i);
                sb.append(user.getNombre());
                for (int j = 0; j < user.getCompaneros().size(); j++) {
                    sb.append(SEPARADOR_NOMBRES_COMPANEROS_USUARIO);
                    sb.append(user.getCompaneros().get(j));
                }
                aux++;
            }
        }
        pt.guardarHaciaArchivo(archivo, sb.toString());
       }
    }
            
    public void importarDesdeTextoCompaneros(File archivo) {
        PersistibleTexto pt = new PersistenciaTexto();

        String resultado = pt.leerDesdeArchivo(archivo);
        String[] companeroLectura = resultado.split(SEPARADOR_COMPANEROS_USUARIO);
        for (int i=0; i<companeroLectura.length;i++) {
            for(int j=0; j<companeroLectura.length;j++){
                if("*".equals(companeroLectura[j])){
                    j=companeroLectura.length;
                }
                if(!"+".equals(companeroLectura[j])&&!"*".equals(companeroLectura[j])){
                    agregarCompaneros(companeroLectura[i],companeroLectura[j]);
                }
            }
        }
    }
      public void exportarATextoPublicacion(File archivo){
        PersistibleTexto pt=new PersistenciaTexto();
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < publicaciones.size(); i++) {
            if (i > 0) {
                sb.append(SEPARADOR_PUBLICACION);
            }

            Publicacion publicacion = publicaciones.get(i);
            sb.append(publicacion.getFecha());
            sb.append(SEPARADOR_ATRIBUTOS_PUBLICACION);
            sb.append(publicacion.getNombrePublicador());
            sb.append(SEPARADOR_ATRIBUTOS_PUBLICACION);
            sb.append(publicacion.getTextoPublicacion());
            
        }
        
        pt.guardarHaciaArchivo(archivo, sb.toString());
    }
    public void importarDesdeTextoPublicaciones(File archivo) {
        PersistibleTexto pt = new PersistenciaTexto();

        String resultado = pt.leerDesdeArchivo(archivo);
        if(archivo.canRead()){
        String[] publicacionLectura = resultado.split(SEPARADOR_PUBLICACION);
         for (int i=0; i<publicacionLectura.length;i++) { 
                Publicacion publicacion=new Publicacion();
                publicacion.setFecha(publicacionLectura[i]);
                publicacion.setNombrePublicador(publicacionLectura[i+2]);
                publicacion.setPublicacion(publicacionLectura[i+4]);
                if(!estaPublicacion(publicacion)) {
                 enviarPublicacion(publicacion);
             }
                i=i+5;
         }
        }
    }
    
    public void mostrarNombreUsuarios(){
        for(int i=0; i<this.usuarios.size(); i++){
            System.out.println(this.usuarios.get(i).getNombre());
        }
    }
}

