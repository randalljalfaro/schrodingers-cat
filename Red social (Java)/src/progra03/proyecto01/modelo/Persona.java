package progra03.proyecto01.modelo;



public class Persona {
    
    String nombre, pais, telefono;
    
    public Persona(){
        this("Vacio","Vacio","Vacio");
    }
    public Persona(String nombre, String pais, String telefono){
        this.nombre=nombre;
        this.pais=pais;
        this.telefono=telefono;
    }
    
    public void setNombre(String nombre){
        this.nombre=nombre;
    }
    public String getNombre(){
        return this.nombre;
    }
    public void setPais(String pais){
        this.pais=pais;
    }
    public String getPais(){
        return this.pais;
    }
    public void setTelefono(String telefono){
        this.telefono=telefono;
    }
    public String getTelefono(){
        return this.telefono;
    }
}
