package progra03.proyecto01.modelo;

import java.util.ArrayList;

public class Usuario extends Empleado {
    String correo, clave, gmail, hotmail, skype, urlAvatar;
    ArrayList<String> companeros;
    ArrayList<Publicacion> publicaciones;
    
    public Usuario(){
        this("Vacio","Vacio","Vacio","Vacio","Vacio","images/H2.jpg");
    }
    public Usuario(String correo, String clave, String gmail, String hotmail, String skype, String urlAvatar){
        this.clave=clave;
        this.correo=correo;
        this.gmail=gmail;
        this.hotmail=hotmail;
        this.skype=skype;
        this.companeros=new ArrayList<>();
        this.publicaciones=new ArrayList<>();
        this.urlAvatar=urlAvatar;
    }
    
    public void setPuesto(String puesto){
        this.puesto=puesto;
    }
    public String getPuesto(){
        return this.puesto;
    }
    
    public void setClave(String clave){
        this.clave=clave;
    }
    public String getClave(){
        return this.clave;
    }
    
    public void setCorreo(String correo){
        this.correo=correo;
    }
    public String getCorreo(){
        return this.correo;
    }
    
    public void setGmail(String gmail){
        this.gmail=gmail;
    }
    public String getGmail(){
        return this.gmail;
    }
    
    public void setHotmail(String hotmail){
        this.hotmail=hotmail;
    }
    public String getHotmail(){
        return this.hotmail;
    }

    public void setUrlAvatar(String urlAvatar){
        this.urlAvatar=urlAvatar;
    }
    public String getUrlAvatar(){
        return this.urlAvatar;
    }
    
    public void setSkype(String skype){
        this.skype=skype;
    }
    public String getSkype(){
        return this.skype;
    }
    
    public void addCompanero(String e){
        this.companeros.add(e);
    }
    public void removeCompanero(String e){
        this.companeros.remove(e);
    }
    public void actualizarNombreCompanero(String nombreAnterior, String nombreModificado){
        this.companeros.remove(nombreAnterior);
        this.companeros.add(nombreModificado);
    }
    
    public void removePublicacion(Publicacion p){
        this.publicaciones.remove(p);
    }
    public void addPublicacion(Publicacion p){
        this.publicaciones.add(p);
    }
    public void setCompaneros(ArrayList<String> c){
        this.companeros=c;  
    }
    public ArrayList<String> getCompaneros(){
        return this.companeros;  
    }
    public ArrayList<Publicacion> getPublicaciones(){
        return this.publicaciones;  
    }
    public boolean estaCompanero(String nombre){
        for(int i=0; i<this.companeros.size(); i++){
            if(this.companeros.get(i).equals(nombre)){
                return true;
            }
        }
        return false;
    }
}
