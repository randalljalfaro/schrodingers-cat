package progra03.proyecto01.modelo;



public class Empleado extends Persona{
    String puesto, fechaIngreso;
    
    public Empleado(){
        this("Vacio","Vacio","Vacio","Vacio","Vacio");
    }
    public Empleado(String nombre, String pais, String telefono, String puesto, String fechaIngreso) {
        this.nombre=nombre;
        this.pais=pais;
        this.telefono=telefono;
        this.puesto=puesto;
        this.fechaIngreso=fechaIngreso;
    }
    
    public void setFechaIngreso(String fechaIngreso){
        this.fechaIngreso=fechaIngreso;
    }
    public String getFechaIngreso(){
        return this.fechaIngreso;
    }
    
    
}
