
package progra03.persistencia.texto;

import java.io.File;

public interface PersistibleTexto {
    public String leerDesdeArchivo(File file);

    public void guardarHaciaArchivo(File file, String texto);

    public void adjuntarHaciaArchivo(File file, String texto);
}
