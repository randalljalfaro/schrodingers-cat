package progra03.persistencia.texto;
import java.io.*;
import java.util.*;
public class PersistenciaTexto implements PersistibleTexto{
    public PersistenciaTexto() {
    }

    @Override
    public String leerDesdeArchivo(File file) {
        Scanner scanner = null;

        try {
            scanner = new Scanner(file);
            StringBuilder sb = new StringBuilder();

            while (scanner.hasNext()) {
                sb.append(scanner.nextLine());
                sb.append("\n");
            }

            return sb.toString();
        } catch (FileNotFoundException ex) {
            throw new RuntimeException(ex);
        } finally {
            if(scanner != null){
                scanner.close();
            }
        }
    }

    @Override
    public void guardarHaciaArchivo(File file, String texto) {

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(file);
            fos.write(texto.getBytes());
            fos.flush();
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException ex) {
                    throw new RuntimeException(ex);
                }
            }
        }
    }

    @Override
    public void adjuntarHaciaArchivo(File file, String texto) {

        FileWriter fileWriter = null;

        try {
            fileWriter = new FileWriter(file, true);
            fileWriter.write(texto);
            fileWriter.flush();
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        } finally {
            if(fileWriter != null){
                try {
                    fileWriter.close();
                } catch (IOException ex) {
                    throw new RuntimeException(ex);
                }
            }
        }
    }
}
