:echo Arith Simplification
:echo START
4*x +6*x - 5*x
:echo --> 5*x
((x+1)^2) - 2*x
:echo --> x^2 + 1 (formula notable)
:echo END
