
:echo Derivates and Arith Simplification
Derivates and Arith Simplification
:echo START
START
!x.log(sin(x))
\x.(1.0/sin(x)*cos(x))
:echo --> \x.(cos(x) / sin(x))
--> \x.(cos(x) / sin(x))
!x.log(sin(y))
\x.x
:echo --> \x.0 (la expresion no depende de x)
--> \x.0 (la expresion no depende de x)
:echo END
END