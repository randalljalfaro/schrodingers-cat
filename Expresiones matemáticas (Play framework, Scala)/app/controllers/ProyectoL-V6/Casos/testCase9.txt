:echo Regla Cadena
:echo START
def f = \x.log(1/(x^2))
def fprime = !x.(f x)
fprime
:echo --> \x.-2/x
:echo END
