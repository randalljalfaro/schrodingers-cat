package eif400.model
import eif400.reduction._
sealed trait Expr

trait Applicable{
   val left:Expr
   val right:Expr
   def operate(left:Double, right:Double):Expr
   def default(left:Expr, right:Expr):Expr
   def app(left: Expr, right:Expr):Expr = (left, right) match{
       case (Num(vl), Num(vr)) => operate(vl, vr)
	   case _ => default(left, right)
   }
   def app:Expr = app(left, right)
}

trait SingleApplicable{
   val arg:Expr
   def operate(left:Double):Expr
   def default(left:Expr):Expr
   def app(left: Expr):Expr = left match{
       case Num(vl) => operate(vl)
	   case _ => default(left)
   }
   def app:Expr = app(arg)
}


case class Lambda(arg: Var, body: Expr) extends Expr
case class Var(name: String) extends Expr
case class Num(value: Double) extends Expr

object Zero extends Num(0)
object One  extends Num(1)
object Two  extends Num(2)

case class Apply(fun: Expr, arg: Expr) extends Expr
case class Def(name: Var, value: Expr) extends Expr

case class Sum(left: Expr, right: Expr) extends Expr with Applicable{
   def operate(x:Double, y:Double):Expr = Num(x+y)
   def default(left: Expr, right: Expr) = (left, right) match{
        case (Zero, x)    => x
	    case (x, Zero)    => x
	    //Factor com_n
		
	    case (Mult(w,x), Mult(y,z)) if (x==z) => Mult(Sum(w,y).app,x).app
	    case (x, Mult(y,z)) if (x==z) 				=> Mult(Sum(y,One).app,x).app
	    case (a, b) if(a==b)     			 		=> Mult(Two,a).app
	    
		case (x, Neg(y))		  => Minus(x,y).app
	    case _         			  => Sum(left, right)
   	}
}

case class Minus(left: Expr, right:Expr) extends Expr with Applicable{
     def operate(x:Double, y:Double):Expr = Num(x-y)
	 def default(left: Expr, right: Expr) = (left, right) match{
	   case (Zero, x)    => x
	   case (x, Zero)    => x
	   //Factor com_n
	   case (Mult(x,w), Mult(y,z)) if (w==z) => Mult(Sum(x,Neg(y).app).app,z).app
	   case (Mult(x,w), z) if (w==z)         => Mult(Sum(x,Neg(One)).app,z).app
	   case (x, Mult(y,z)) if (x==z) 		 => Mult(Sum(Neg(y).app,One).app,z).app
	   case (x, y) if (y==x) => Zero
	   
	   case _         => Sum(left, Neg(right))
   }
}
//def iguales(x:Expr

case class Mult(left: Expr, right:Expr) extends Expr with Applicable{

   def operate(x:Double, y:Double):Expr = Num(x*y)
   def default(left: Expr, right: Expr) = (left, right) match{
	   case (Neg(x), Neg(y))=> Mult(x,y).app
	   case (Neg(x),y )=> Neg(Mult(x,y).app)
	   case (x, Neg(y))=> Neg(Mult(x,y).app)
	   case (x, One) => x
	   case (One, x) => x
	   case (x, Zero) => Zero
	   case (Zero, x) => Zero
	   case (x,y) if (x==y) =>Pow(x,Two).app
	   //Formula notable
	   case (Sum(x,w), Minus(y,z)) if (x==y && w==z) => Sum(Pow(x,Two).app,Neg(Pow(w,Two).app).app).app
	   case (Sum(x,w), Sum(y,Neg(z))) if (x==y && w==z) => Sum(Pow(x,Two).app,Neg(Pow(w,Two).app).app).app
	   //Distributividad
	   case (Sum(x,w), Sum(y,z)) => Sum(Mult(x,y).app,Mult(w,z).app).app
	   
       case _         => Mult(left, right)
   }
}
case class Div(left: Expr, right:Expr) extends Expr with Applicable{
   def operate(x:Double, y:Double):Expr = Num(x/y)
   def default(left: Expr, right: Expr) = (left, right) match{
		case (Pow(x,Num(p)) ,Pow(y,Num(q))) if(x==y) => if (p>q) Pow(x,Num(p-q)).app else Div(One,Pow(x,Num(q-p)).app)
		case (x , y)	if(x==y) => One
		case _                => Div(left, right)
	}
}

case class Pow(left: Expr, right: Expr) extends Expr with Applicable{
   def operate(x:Double, y:Double):Expr = Num(Math.pow(x,y))
   def default(left: Expr, right: Expr) = (left, right) match{
       case (Zero, x)      => Zero
	   case (x, Zero)      => One
	   case (x, One)	   => x
	   case (One,x)		   => One
	   case (Sum(x,Neg(y)), Two)=> Sum(Sum(Pow(x,Two).app,Neg(Mult(Two,Mult(x,y).app).app).app).app,Pow(y,Two).app).app
	   case (Sum(x,y), Two)     => Sum(Sum(Pow(x,Two).app,Mult(Two,Mult(x,y).app).app).app,Pow(y,Two).app).app
	   // Mas reglas aca
	   case _         => Pow(left, right)
   }
}
case class Neg(arg:Expr) extends Expr with SingleApplicable{
   def operate(x:Double):Expr = Num(-x)
   def default(arg:Expr) = arg match{
       case Num(x) => Num(-x)
	   case Neg(x) => x
       case _           => Neg(arg)
   }
}

case class Derivate(arg: Var, body:Expr) extends Expr

case class Closure(arg:Var, body: Expr, scope:ExprScope) extends Expr

class Primitive( val name:Var, val arg:Expr) extends Expr

case class Log(e:Expr) extends Primitive(Var("log"), e) with SingleApplicable{
   def operate(x:Double):Expr = Num(Math.log(x))
   def default(arg:Expr) = arg match{
		case Div(a @ _, b @ _)	=> Sum(Log(a).app,Neg(Log(b).app)).app 
		case Mult(a @ _, b @ _)	=> Sum(Log(a).app,    Log(b).app ).app
		case x			=> Log(arg)
    }
}

case class Sin(e:Expr) extends Primitive(Var("sin"), e) with SingleApplicable{
   def operate(x:Double):Expr = Num(Math.sin(x))
   def default(arg:Expr) = Sin(arg)
}
case class Cos(e:Expr) extends Primitive(Var("cos"), e) with SingleApplicable{
   def operate(x:Double):Expr = Num(Math.cos(x))
   def default(arg:Expr) = Cos(arg)
}
case class Error(e:Expr) extends Primitive(Var("error"), e) with SingleApplicable{
   def operate(x:Double):Expr = this
   def default(arg:Expr) = Error(arg)
}
object Primitive{
   val ERROR=(x:Expr) => Error(x)
   private val primitivesMap = Map[String, Expr => Expr](
                     "log" -> {Log(_:Expr)},
		     "sin" -> {Sin(_:Expr)},
		     "cos" -> {Cos(_:Expr)}
   )
   private def map(s:String) = primitivesMap.getOrElse(s, ERROR)
   def unapply(e:Expr):Option[(Expr, Expr)]= e match{
     case p:Primitive => Some((p.name, p.arg))
	 case _           => None
   }
   def apply(f:String, e:Expr):Expr= (map(f))(e) 
}
trait PrettyPrinter {
  def apply(expr: Expr): String = expr match {
    case Def(name, value)  => p"def $name=$value"
    case Lambda(arg,body)  => p"\\$arg.$body"
    case Closure(arg,expr,_) => expr match{
					case Var(x) => p"\\$arg.x"
					case Num(x) => p"\\$arg.x"
					case _	    => p"\\$arg.($expr)"
				}
    case Apply(fun, arg)   => p"($fun $arg)"
    case Var(name)         => s"$name"
    case Neg(exp)          => exp match{
				 case a @ Sum(_,_)  => p"-($a)"
				 case a @ Minus(_,_)=> p"-($a)"
				 case a @ _         => p"-$a"
				}
    case Num(value)        => s"$value"
    case Sum(l, r)         => p"$l+$r"
    case Minus(l, r)       => p"$l-$r"
    case Mult(l, r)        => (l , r) match {
				 case (x@ Sum(_,_), z@ Sum(_,_))	=> p"($x)*($z)"
				 case (x@ Sum(_,_), z)	=> p"($x)*$z"
				 case (z, x@ Sum(_,_))	=> p"$z*($x)"
				 case (x, y)		=> p"$x*$y"
			  }
   case Div(l, r)         => (l , r) match {
				 case (x@ Mult(_,_), z@ Mult(_,_))  => p"($x)/($z)"
				 case (x@ Sum(_,_),z@ Sum(_,_))	    => p"($x)/($z)"
				 case (x@ Mult(_,_), z)	=> p"($x)/$z"
				 case (z, x@ Sum(_,_))	=> p"$z/($x)"
				 case (z, x@ Mult(_,_))	=> p"$z/($x)"
				 case (x@ Sum(_,_), z)	=> p"($x)/$z"
				 case (x, y)		=> p"$x/$y"
			 }
   case Derivate(Var(x), exp)   => exp match{
				     case a @ Sum(_,_)  => s"!$x" + p".($a)"
				     case a @ Minus(_,_)=> s"!$x" + p".($a)"
				     case a @ _         => s"!$x" + p".$a"
				}
   case Pow(l, r)               => p"($l)^($r)"
   case Sin(x)		        => p"sin($x)"
   case Cos(x)		        => p"cos($x)"
   case Log(x)		        => p"log($x)"
   case x                       => s"=> $x]"
  }
 
  implicit class PrettyPrinting( val sc: StringContext) {
    def p(args: Expr*) = sc.s((args map apply):_*)
  }
 
}


