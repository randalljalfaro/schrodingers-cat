package eif400.parser 

import eif400.model._

import scala.util.parsing.combinator.syntactical.StdTokenParsers
import scala.util.parsing.combinator.lexical.StdLexical
import scala.util.parsing.input._
import scala.collection.immutable.PagedSeq

import scala.xml.Elem
import scala.xml.EntityRef

class LambdaParser extends StdTokenParsers {
  type Tokens = StdLexical
  val lexical = new StdLexical
  lexical.delimiters ++= Seq("%", "^", "+", 
                              "*", "-", "/", 
                              "!", "=", "\\", 
							  ".", "(", ")", " ")
  lexical.reserved.add("def")
  lexical.reserved.add("log")
  lexical.reserved.add("sin")
  lexical.reserved.add("cos")
  
  def parse(source: String): ParseResult[Expr] = {
	  val tokens = new lexical.Scanner(source)
	  phrase(stmt)(tokens)
  }
  def parse(in: java.io.Reader): ParseResult[Expr] ={
    val source = new PagedSeqReader(PagedSeq.fromReader(in))
	val tokens = new lexical.Scanner(source)
    phrase(stmt)(tokens)
  }
  
  def stmt:Parser[Expr]  = define | expr
  
  def define:Parser[Expr]= "def" ~> variable ~ "=" ~ expr ^^
                           {case v ~ "=" ~ e => Def(v, e)}
						   
  def expr:Parser[Expr]  = lambda | application | exprArith | derivate | parent
  
  def lambda             =  "\\"  ~> variable ~ "." ~ expr ^^ 
							{ case v ~ "." ~ e  => Lambda(v, e) }
  def derivate             =  "!"  ~> variable ~ "." ~ expr ^^ 
							{ case v ~ "." ~ e  => Derivate(v, e) }						
							
  def application        = "(" ~> aexpr  <~ ")" ^^ 
							{ case a  => a }
  def parent :Parser[Expr]  = "(" ~> expr  <~ ")" ^^ 
							{ case a  => a }
							
  def variable           =  ident ^^ Var
  
  def number             =  numericLit ^^ {case x => Num(x.toInt)}
  
  def numberFloat        =  numericLit~"."~numericLit ^^ {case x~"."~y =>
														y match{	
															case z if (z.toInt==(0).toInt||z.toDouble==(0.0).toDouble)=>Num(x.toInt)
															case _ => Num((x.toDouble+Math.pow(10,-1*y.length).toDouble*y.toDouble).toDouble)
														}
													}
  
  def negNumber          =  "-" ~> numericLit ^^ {case x => Neg(Num(x.toInt))}
  
  def negVar 			 =  "-" ~> ident ^^ {case x => Neg(Var(x))}
  
  def negPrimitive       =  "-" ~> "(" ~> exprArith <~ ")" 
  
  def neg:Parser[Expr]    = negNumber | negVar
  
  def aexpr = (expr ~ expr) ^^ {case left ~ right => Apply(left, right)}
  
  def exprArith:Parser[Expr] = monom ~ (("+" |"-") ~ monom).* ^^
                               {
							     case m ~ lf   => lf.foldLeft(m){
								   (z:Expr, h:(String ~ Expr)) => 
								    h match{
								     case ("+" ~ m) => Sum(z, m)
								     case ("-" ~ m) => Sum(z, Neg(m))
									 //case ("-" ~ m) => Minus(z, m)
								   }
							     }
								 
							   }
  def monom:Parser[Expr] = factor ~ rep(("*"|"/"|"^") ~ factor) ^^
                            {
							     case m ~ lf   => lf.foldLeft(m){
								   (z:Expr, h:(String ~ Expr)) => h match{
									 case ("^" ~ m) => Pow(z, m)
								     	 case ("*" ~ m) => Mult(z, m)
									 case ("/" ~ m) => Div(z, m)
									 // otros casos aca / y ^
									 
								   }
							     }
							}
	def factor:Parser[Expr] = neg | variable | numberFloat | number | primitive | parens
	
	
	def parens             = "(" ~> exprArith <~ ")"
	
	def primitive[Expr]    = log | sin | cos
	
	def log				   = ("log") ~ "(" ~ exprArith ~ ")" ^^{
	    case (fun ~ "(" ~ e ~ ")" )=> Primitive(fun, e)
	}
	
	def sin				   = ("sin") ~ "(" ~ exprArith ~ ")" ^^{
	    case (fun ~ "(" ~ e ~ ")" )=> Primitive(fun, e)
	}
	def cos				   = ("cos") ~ "(" ~ exprArith ~ ")" ^^{
	    case (fun ~ "(" ~ e ~ ")" )=> Primitive(fun, e)
	}
	
}

