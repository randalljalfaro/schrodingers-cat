package eif400.reduction
import eif400.model._
import scala.xml.Elem
import scala.xml.EntityRef

object ToMathML{
	def convertir(expr:Expr):Elem={<math>
									{expr match{
										case Var(x)   => <mi>{x.toString}</mi></mrow>
										case Num(x)   => <mrow><mn>{x.toString}</mn></mrow>
										case Pow(x,y) =><mrow>
															<msup>
																<mrow>{convertir(x)}</mrow>
																<mrow>{convertir(y)}</mrow>
															</msup>
														</mrow>
										case Sum(l,r) => <mrow>
															{convertir(l)} 
															<mo>+</mo>
															{convertir(r)}
														</mrow>
										case Minus(l,r) => <mrow>
															{convertir(l)} 
															<mo>-</mo>
															{convertir(r)}
														</mrow>
										case Mult(l,r)=>  <mrow>
															{convertir(l)} 
															 <mo>&InvisibleTimes; </mo>
															{convertir(r)}
															</mrow>
										case Div(l,r)=>  <mrow>
															<mfrac linethickness="2">
															<mrow>{convertir(l)}</mrow>
															<mrow>{convertir(r)}</mrow>
															</mfrac>
														</mrow>
										case Sin(x)         =>  <mrow>
																	<mi>sin</mi>
																	<mfenced open="(" close=")"  separators="&nbsp;"> 
																	{convertir(x)}
																	</mfenced> 
																   </mrow>
										case Cos(x)         =>  <mrow>
																	<mi>cos</mi>
																	<mo> ( </mo>
																	{convertir(x)}
																	<mo> ) </mo>
																   </mrow>
										case Neg(x) =>		 <mrow>
																<mo>-</mo>
																	{convertir(x)} 							
																</mrow>
										 case Lambda(arg,body)  => <mrow>
																	 <mi>&#x3bb;</mi>
																	 <mi>{convertir(arg)}</mi>
																	 <mo>.</mo>
																	{convertir(body)}
																 </mrow>
																	
										/*case Closure(arg,expr,_) => expr match{
													case _	    => {convertir(expr)}
												}	*/
										case Apply(fun, arg)   => <mrow>
																	<mfenced open="(" close=")"  separators="&nbsp;"> 
																	 <mi>{convertir(fun)}</mi>
																	{convertir(arg)}
																	</mfenced> 
																</mrow>	
										case Def(x, e)=> <mrow>							
															<mi>{convertir(x)}</mi>
															<mo>=</mo>
															{convertir(e)}
														</mrow>
										case Derivate(Var(x), exp)   => exp match{
													 case a @ Sum(_,_)  => <mrow><mi>f</mi><mo>=</mo>{convertir(a)}</mrow>				 
																			
													 case a @ Minus(_,_)=> <mrow><mi>f</mi><mo>=</mo>{convertir(a)}</mrow>					 
																			
													 case a @ _         => <mrow><mi>f</mi><mo>=</mo>{convertir(a)}</mrow>					 
																				
											}
										case Log(x)		   => <mrow><mi>log</mi>
															  <mfenced open="(" close=")"  separators="&nbsp;"> 
															  {convertir(x)}</mfenced></mrow>		
														
										case _ 	      => <mi>falta caso</mi>
														
										}
										</math>
									}
	}
	

}
