package eif400.model

class Scope[S, T](parent:Scope[S, T] = null){
    type HashMap[S, T] = collection.mutable.HashMap[S, T]
    private val map = new HashMap[S, T]()
    def +(p:(S, T)) = {map.+=(p); this}
	def -(k:S) = {map-=(k); this}
	def isBound(k:S):Boolean =
	    (map.isDefinedAt(k), isTop) match{
		  case (true, _) => true
		  case (_, true) => false
		  case (_, false) => parent.isBound(k) 
		}
	def apply(k:S):T =
	   (map.isDefinedAt(k), isTop) match{
		  case (true, _) => map.apply(k)
		  case (false, false) => parent.apply(k)
          case (_, _) => map.apply(k)		  
		}
	def get(k:S) = apply(k)
	def isTop = parent==null 
    def push = new Scope[S,T](this)
    def pop  ={ if (isTop) throw new Exception("Scope is Empty")
               else parent } 
	override def toString = (map.toString)+"^"+(if (!isTop) (parent.toString) else "")
    			   
}
class ExprScope(p:ExprScope=null) extends Scope[Var, Expr](p){
    override def apply(k:Var):Expr = if (isBound(k)) super.apply(k)
	                                else k
	override def push = new ExprScope(this)
	override def +(p:(Var, Expr)) = {super.+(p); this}
}