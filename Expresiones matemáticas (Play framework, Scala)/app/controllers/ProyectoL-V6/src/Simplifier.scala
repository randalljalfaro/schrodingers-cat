package eif400.reduction
import eif400.model._

object Simplification extends PrettyPrinter{
	def simplify(expr:Expr, s:ExprScope):Expr= {
	expr match{
		case x:Var             => s(x)
		case Lambda(x, b)      => simplify(Closure(x, b, s),s)
		case Closure(x, b, sc) => Closure(x, simplify(b, sc), sc)
		case Apply(f, x)       => (simplify(f, s), simplify(x, s)) match{
			 case (Closure(v, b, sc), sx) => simplify(b, (sc.push) + (v -> sx))
			 case (sf, sx) => Apply(sf, sx)
		}
		case Def(x, e)          => {s+(x -> simplify(e, s)); x}
		case a @ Sum(l,r)       => agruparArit(listaExpr(l,r,s),Sum(_,_).app,s).reverse.sortWith(compara).
						foldLeft(Sum(Zero,Zero).app) {(expr:Expr, res:Expr) =>Sum(expr,res).app}
		
		case a @ Minus(l,r)     =>  a.app(simplify(l, s), simplify(r, s))
		case a @ Mult(l,r)      =>  agruparArit(listaMult(l,r,s),Mult(_,_).app,s).sortWith(compara).
						foldLeft(Mult(One,One).app){(expr:Expr, res:Expr) =>Mult(expr,res).app}
		case a @ Div(l,r)       =>  a.app(simplify(l, s), simplify(r, s))
		case a @ Pow(l,r)       =>  a.app(simplify(l, s), simplify(r, s))
		
		case a @ Log(x)         =>  a.app(simplify(x, s))
		case a @ Sin(x)         =>  a.app(simplify(x, s))
		case a @ Cos(x)         =>  a.app(simplify(x, s))
		case a @ Neg(x)		    =>  a.app(simplify(x, s))
		case a @ Derivate(x, e) =>  simplify(derivate(x, simplify(e,s), s),s)
		case x => x
		}
	}
	def compara(expr:Expr, expr2:Expr)=(expr, expr2) match {
	    case (Num(_),_)          =>true
		case (Var(x),Var(y))     =>( x compareToIgnoreCase y ) < 0
		case (Var(x),Mult(_,Var(y)))  =>( x compareToIgnoreCase y ) < 0
		case (Var(_),Num(_))          =>false
		case (Var(_),_)          =>true
		case (_,_)=> false
	}
	def listaMult(l:Expr, r:Expr, s:ExprScope):List[Expr] = (simplify(l,s), simplify(r,s)) match {
			case (Mult(x,y), z) => simplify(z,s)::listaMult(x,y,s)
			case (x, Mult(y,z)) => simplify(x,s)::listaMult(y,z,s)
			case (x , y) 		=> simplify(x,s)::simplify(y,s)::Nil
		}
	
	def listaExpr(l:Expr, r:Expr, s:ExprScope):List[Expr] = (l, r) match {
			case (Sum(x,y), z) => simplify(z,s) match{
				case Sum(a,b)     =>listaExpr(a, b, s):::listaExpr(x,y,s)
				case Neg(Sum(a,b))=>listaExpr(Neg(a), Neg(b), s):::listaExpr(x,y,s)
				case z		  =>z::listaExpr(x,y,s)
				}
			case (x , y) 		=> (simplify(x,s),simplify(y,s)) match {
				case (Sum(a,b), Sum(c,d)) => listaExpr(c, d, s):::listaExpr(a, b, s):::Nil
				case (Sum(a,b), z)        => z::listaExpr(a, b, s):::Nil
				case (z, Sum(c,d))        => listaExpr(c, d, s):::z::Nil
				case (Neg(Sum(a,b)), Neg(Sum(c,d))) => listaExpr(Neg(c), Neg(d), s):::listaExpr(Neg(a), Neg(b), s):::Nil
				case (Neg(Sum(a,b)), z)   => z::listaExpr(Neg(a), Neg(b), s):::Nil
				case (z, Neg(Sum(c,d)))   => listaExpr(Neg(c), Neg(d), s):::z::Nil
				case (a, b)		  => b::a::Nil
									}
		}
	
	
	def agruparArit(l:List[Expr],p:(Expr,Expr)=> Expr, s:ExprScope):List[Expr] = {
		def f(e:Expr,res:List[Expr]):List[Expr] ={
		  (simplify(e,s),res) match{
		  
		    //factor común entre multiplicaciones
			case (z @ Mult(Num(_),b), (x @ Mult(Num(_),c))::y)      if (b==c) => p(z,x)::y
			case (z @ Neg(Mult(Num(_),b)), (x @ Mult(Num(_),c))::y) if (b==c) => p(x,z)::y
			case (z @ Mult(Num(_),b), (x @ Neg(Mult(Num(_),c)))::y) if (b==c) => p(z,x)::y
			
			case (z @ Mult(r,b), (x @ Mult(q,c))::y)      if (b==c&&r==q) => p(z,x)::y
			case (z @ Neg(Mult(r,b)), (x @ Mult(q,c))::y) if (b==c&&r==q) => p(x,z)::y
			case (z @ Mult(r,b), (x @ Neg(Mult(q,c)))::y) if (b==c&&r==q) => p(z,x)::y
		    
			//factor común de una mult y otra cosa
			case (z @ Mult(_,b@Var(_)), (x@Var(_))::y) if (b==x)        => p(x,z)::y
			case (z @ Mult(_,b@Var(_)), (x@ Neg(c@Var(_)))::y) if (b==c)=> p(z,x)::y
			case (z @ Neg(Mult(_,b@Var(_))), (x@Var(_))::y) if (b==x)   => p(x,z)::y
			
			case (z@Neg(b@Var(_)), (x @ Mult(_,c@Var(_)))::y) if (b==c) => p(x,z)::y
			case (z , (x @ Neg(Mult(_,c@Var(_))))::y) if (z==c)         => p(z,x)::y
		
			//Iguales
			case (z, x::y) if (z==x)                         => p(z,x)::y
			case (z@Neg(b), x::y) if (b==x)                  => p(x,z)::y
			case (z, (x@ Neg(c))::y) if (z==c)               => p(z,x)::y
			
			case (z @ Num(_), (x @ Num(_))::y)               => p(z,x)::y
			case (z @ Neg(Num(_)), (x @ Num(_))::y)          => p(z.app,x)::y
			case (z @ Num(_), (x @ Neg(Num(_)))::y)          => p(z,x.app)::y
			case (z @ Neg(Num(_)), (x @ Neg(Num(_)))::y)     => p(z.app,x.app)::y

			//Seguir buscando en la lista del "res" si es 
			// no pegó con nada y la lista tiene algo más
			case (z , m::t)					=> m::f(z,t)
			//si llega a acá, no pegó con nada en la lista "res"
			//Agrega la expresion en la lista
			case (z, Nil)   				=> z::res
			}
		}
		l.foldLeft(List[Expr]()) {(res:List[Expr],expr:Expr)=>f(expr,res)}
	}

	
	def derivate(v:Var, e:Expr, s:ExprScope)= e match{
	    case Closure(x, b, sc) =>{ 
		     val sx = (sc.push)+(x -> x)
		     Closure(x, simplify(derivateExpr(x, b, sx), sx), sc)
		}
	   case _ => {
		     val sv = (s.push)+(v -> v)
	             Closure(v, simplify(derivateExpr(v, e, sv), s), s)
		}
		
	}
	def derivateExpr(x:Var, e:Expr, s:ExprScope):Expr= e match{
	    case y:Var =>{
		   val sy = s(y)
		   if (sy == x) One
		   else if (sy == y) Zero
		   else derivateExpr(x, sy, s)
		}
	    case l @ Lambda(_,_)	=> derivateExpr(x, simplify(l,s), s)  
	    case Closure(y, b, sc)  => Closure(y, derivateExpr(x, b, (sc.push)+(y->Zero)), sc)
		
		case Apply(f, g)        => derivateExpr(x, simplify(Apply(f,g),s), s)
        //case Apply(f, g)        => Mult(Apply(derivateExpr(x, f, s), g), derivateExpr(x, g, s))
	    case Sum(l, r)          => Sum(derivateExpr(x, l, s), derivateExpr(x, r, s))
	    case Mult(l , r)        => Sum(Mult(l, derivateExpr(x, r, s)), Mult(derivateExpr(x, l, s), r))
		//case Div(l , r)			=> 
		//Derivada funcion potencial
		case Pow(l,r@Num(_))	    => chainRule(x, (z:Expr) => Pow(simplify(Mult(r,z),s),Minus(r,One).app).app, l,s)
		case Pow(l,r@Neg(Num(_)))	=> chainRule(x, (z:Expr) => Pow(simplify(Mult(r,z),s),Minus(r,One).app).app, l,s)
		
        case p:Primitive         => derivatePrim(x, p, s)
		case Neg(p)              => Neg(derivateExpr(x, p, s))
	    case _                   => Zero
	}
	def derivatePrim(x:Var, e:Primitive, s:ExprScope):Expr= e match{
	    case Sin(y @ _)	 => chainRule(x, Cos(_), y, s)
	    case Cos(y @ _)	 => chainRule(x, (z:Expr) => Neg(Sin(z)), y, s)
	    case Log(y @ _)	 => chainRule(x, Div(One, _), y, s)
        case _               => Zero
	}

	def chainRule(x:Var, p:Expr => Expr, e:Expr, s:ExprScope):Expr= e match{
	    case y if x==y =>   p(x)
	    case _         =>   simplify(Mult(simplify(p(e),s), simplify(derivateExpr(x, e, s),s)), s)
	}
}
