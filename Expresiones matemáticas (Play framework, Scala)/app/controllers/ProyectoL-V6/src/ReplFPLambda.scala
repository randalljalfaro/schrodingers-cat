package eif400.repl

import eif400.model._
import eif400.parser._
import eif400.reduction._
object ReplLambda extends PrettyPrinter{ 
  val name="Repl Lambda"
  val version="0.1"
  val dirMem = "app"+java.io.File.separator+"controllers"+java.io.File.separator+
			"ProyectoL-V6"+java.io.File.separator+"memoria.txt"
  
  val Echo = """^ *:echo (.*)$""".r
  
  def logo(name:String, version:String){
    println(s"*** $name version $version ***")
  }
  def end(name:String){
    println(s"*** $name exits ***")
  }
  def exec(cmd : String){
    cmd match{
	   case ":exit" | ":quit" =>{
	    end(name)
	    sys.exit(0)
	   }
	   case x => {println(s"$x invalid input");}
	}
  }
  def main(args: Array[String]){
    val txtReg= """^(.*).txt""".r
	val QuitCmd = """ *\:(?:quit|exit) *""".r
	val ListCmd = """:list *(?:(\d+)?(?: *.. *(\d+))?)?""".r
	val scope = new ExprScope
	
	if (args.length==1){ 
	  val filename=args(0)
	  Console.setIn( new java.io.BufferedReader( new java.io.FileReader(filename)))
	  Console.setOut( new java.io.PrintStream(filename+"_out.txt"))
	}
	
	logo(name, version)
	
    while ( true ) {
      var line = readLine("? ")
	  line match{
	    case null | QuitCmd()  => exec(":quit")
		case _ =>println(leerLinea(line,scope))
		}
	}
  }
  
  def fileSimplify(dir:String){
    val scope = new ExprScope
	val texto=scala.io.Source.fromFile(dir).getLines.toList.
			foldLeft(""){(x:String,y:String)=>x+"\n"+y+"\n"+leerLinea(y,scope)}
	val out=scalax.io.Resource.fromOutputStream(new java.io.FileOutputStream(dir+"_out.txt"))
	out.write(texto)
  }
  
  import scala.xml.Elem
  import scala.xml.EntityRef
  object NoneId extends xml.dtd.PublicID("", ""){
   override def toString = ""
  }
  def toStringXml(html:Elem)={
    val w = new java.io.StringWriter()
    xml.XML.write(w, html, "UTF-8", xmlDecl = false, doctype = 
            xml.dtd.DocType("html", NoneId, Nil))
    w.toString
   }
  def crearPaginaHTML(nombre:String, sHTML:String)={
	import java.io._
	val writer = new PrintWriter( new File ("public/"+nombre+".html"))
        writer.write(sHTML)
        writer.close()
  }
 
 
 def HTMLFile(x:Int, y:Int, dir:String)= 
					<html>
						<head>
							<title>Testing HTML Scala</title>
						</head>
						<body>
							{	import java.io._
								val parser = new LambdaParser
								import parser.{Success, NoSuccess}
								val regL="""^ *\?(.*)$""".r
								(x, y) match {
									case ( x , y ) if (y>0&&x<=y) =>((scala.io.Source.fromFile(dir).getLines.toList.filter(_!="$").
																	foldLeft(List[String]()){(x:List[String],y:String)=> y match{
																				case regL(z)=>z::x
																				case Echo(_)=> x
																				case _=>y::x
																		}
																	}.reverse.splitAt(x)._2).splitAt(y-x+1)._1).map(
																			(x:String)=>parser.parse(x) match {
																				case Success(expr, rest) => <mrow>
																											<br/>
																												{eif400.reduction.ToMathML.convertir(expr)}
																											</mrow>
																				case failure:NoSuccess	 => <mrow>
																											<br/>
																												{x}
																											</mrow>
																			})
									case ( _ , _ ) =>
											scala.io.Source.fromFile(dir).getLines.toList.filter(_!="$").
																	foldLeft(List[String]()){(x:List[String],y:String)=> y match{
																				case regL(z)=>z::x
																				case Echo(_)=> x
																				case _=>y::x
																		}
																	}.reverse.map(
																	(x:String)=>parser.parse(x) match {
																		case Success(expr, rest)  => <mrow>
																										<br/>
																										{eif400.reduction.ToMathML.convertir(expr)}
																									 </mrow>
																		case failure:NoSuccess=>  <mrow>
																									<br/>
																									{x}
																									</mrow>
																	})
										}
								
							}
						</body>
					</html>
					
  def leerLinea(line:String, s:ExprScope):String={
	
	val scope = s match {
		case null => new ExprScope
		case _	  => s
	}
	val parser = new LambdaParser
	
	import parser.{Success, NoSuccess}
	import java.io._
	
	val toMathML = """^ *:toMathML *output=(.*) *, *range=(\d+)? *.. *(\d+)?$""".r
	line match{
		case Echo (mostrar) => mostrar
		case toMathML (nombre, x, y ) => {
				(x, y) match {
					case ( null , null )=> crearPaginaHTML(nombre, toStringXml(HTMLFile( 0 , 0 , dirMem)))
					case ( null , y)    => crearPaginaHTML(nombre, toStringXml(HTMLFile( 0 , y.toInt , dirMem)))
					case (x , null )    => crearPaginaHTML(nombre, toStringXml(HTMLFile( x.toInt , 0 , dirMem)))
					case (a,b)	        => crearPaginaHTML(nombre, toStringXml(HTMLFile( x.toInt , y.toInt, dirMem)))
				}
				nombre+".html"
			   
		 }
		 
		case _ => parser.parse(line) match {
			case Success (expr, rest)     => {
			   val mem=new FileWriter(dirMem,true)
			   mem.write("$"+"\n")
			   mem.write(p"${expr}".toString+"\n")
			   if (!rest.atEnd) "Ignoring rest of input"
			   val simp=Simplification.simplify(expr, scope)
			   val resultado = p"${simp}".toString
	           mem.write(resultado+"\n")
			   mem.write("$"+"\n")
	           mem.close()
			   resultado
			}
			case failure: NoSuccess   => failure.toString
		 }
		}
   }
 }
