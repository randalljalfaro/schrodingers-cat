package controllers
import play.api._
import play.api.mvc._
import play.api.mvc.BodyParsers.parse._
import eif400.repl._
import java.io._
object Application extends Controller {
  def index = Action {
		Ok(views.html.index("Scala Lecter"))
  }
  
  def lecterCall = Action(parse.tolerantText){ request =>
		val reg="""^( *:toMathML .*)""".r
		request.body match {
			case reg(r)	=> Ok("http://localhost:9000/assets/"+ReplLambda.leerLinea(r, null ))
			case r		=> Ok(ReplLambda.leerLinea(r, null ))
		}
		
	}
	
def uploadFile = Action(parse.multipartFormData) { request =>{
	 val f=request.body.file("fileUpload")
	 val txtReg= """^(.*).txt""".r
	 val dir ="app"+File.separator+"controllers"+File.separator+
					"ProyectoL-V6"+File.separator+"Casos"+File.separator
	 f.map { a =>
			val repl=ReplLambda
			val name = a.filename
			val contentType = a.contentType.get
			//Se guarda Case en el directorio
			a.ref.moveTo(new File(dir+name), true )
			
			val nombrePag= name match{
				case txtReg(x) => x
				case 	x	 => x
			}
			//Se usa el File que ya se copi_ en el server
			repl.fileSimplify(dir+name)
			//Se usa el File que da como resultado
			repl.crearPaginaHTML(nombrePag, repl.toStringXml(repl.HTMLFile( 0 , 0 , dir + name + "_out.txt" )))
		}.getOrElse {
			Redirect(routes.Application.index)
		}
	  val nombre= f.head.filename match{
			case txtReg(x) => x
			case 	x	 => x
	  }
	  Ok(views.html.index("http://localhost:9000/assets/"+nombre+".html"))
	 }
	}
}
