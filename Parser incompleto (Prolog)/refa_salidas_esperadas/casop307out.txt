DFA before simplification.Name=casop307
.Start=q0
.Final States:
...Final=q4
.Transitions:
...(q0/a) -> q1
...(q0/b) -> q4
...(q1/a) -> q2
...(q1/b) -> q1
...(q2/a) -> q3
...(q2/b) -> q2
...(q3/a) -> q3
...(q3/b) -> q1
...(q4/a) -> q4
...(q4/b) -> q4
q1 reaches q3
q1 reaches q2
q1 reaches q1
DFA after simplification.Name=casop307
.Start=q0
.Final States:
...Final=q4
.Transitions:
...(q0/b) -> q4
...(q4/a) -> q4
...(q4/b) -> q4
