Before renaming
.Name=caso06
.Start=a1_b11_0
.Final States:
...Final=a1_f
...Final=b1_f
.Transitions:
...(a1_0/a) -> a1_f
...(a1_b11_0/a) -> a1_f
...(a1_b11_0/b) -> b1_f
...(b1_0/b) -> b1_f
After renaming
.Name=n_caso061
.Start=s0
.Final States:
...Final=s1
...Final=s2
.Transitions:
...(s0/a) -> s1
...(s0/b) -> s2
