% salida de re_to_fa(a+b)
:-['../src/re/re', '../src/fa/fa_simplifier'].

:- fa_new([name=caso06, start=a1_b11_0, finals=[a1_f, b1_f],
           trans=[
             (a1_0/a)->a1_f,
             (a1_b11_0/a)->a1_f,
             (a1_b11_0/b)->b1_f,
             (b1_0/b)->b1_f
           ]]).
testcase06:-
   told,
   tell('./testcases/casop06out.txt'),
   format('Before renaming~n',[]),
   Name=caso06,
   fa_print(Name),
   format('After renaming~n',[]),
   fa_rename(Name, T),
   fa_print(T),
   told.
:-testcase06.