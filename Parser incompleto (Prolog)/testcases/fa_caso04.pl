% Author: Carlos Loria-Saenz
% Date: 01/10/2013

:-['./src/re/re', './src/fa/fa_simplifier'].


test_case_re_to_fa_simplify(N, RE, F) :-
      % Sets output to file based on N
      concat_atom(['testcases/', N, 'out.txt'], Out),
      open(Out, write, OutStream),
      current_output(Cout),
      format('*** Starts Test Case ~a (output in ~a) ***~n',[N, Out]),
      set_output(OutStream),
      format('*** Starts RE ~w Test Case ~a ***~n',[RE, N]),
      %
      reset_gensym,
      re_to_fa(RE, F),
      fa_print(F),
      fa_inactive_show(F),
      fa_unreachable_show(F),
      fa_simplify_show(F),
      %
      % Close file and Restores old Out
      format('*** End of Test Case ~a ***~n',[N]),
      close(OutStream),
      set_output(Cout),
      format('*** End of RE ~w Test Case ~a (output in ~a) ***~n',[RE, N, Out]).

:- test_case_re_to_fa_simplify(caso04, a, _).