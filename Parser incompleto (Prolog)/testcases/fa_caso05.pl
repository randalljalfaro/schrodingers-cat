% Author: Carlos Loria-Saenz
% Date: 01/10/2013
% salida de re_to_fa(a+b)
:-['../src/re/re', '../src/fa/fa_simplifier'].

:- fa_new([name=caso05, start=a1_b11_0, finals=[a1_f, b1_f],
           trans=[
             (a1_0/a) -> a1_f,
             (a1_b11_0/a) -> a1_f,
             (a1_b11_0/b) -> b1_f,
             (b1_0/b) -> b1_f
           ]]).

:- test_case_nfa_dfa_simplify(caso05, _).