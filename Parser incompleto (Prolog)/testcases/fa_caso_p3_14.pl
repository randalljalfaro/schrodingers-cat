% Author: Carlos Loria-Saenz
% Date: 20/10/2013
:-['./src/fa/fa_simplifier'].
:- fa_new([name=caso14, start=q0, finals=[q2],
           trans=[
		   
             (q0/a)->q1,
             (q0/b)->q0,
			 (q0/b)->q2,
			 
             (q1/a)->q0,
             (q1/a)->q3,
			 (q1/b)->q1,
			 (q1/b)->q2,
			 
			 (q2/a)->q3
			 
           ]]).

%test_caso01_1 :-
 %  fa_run_show(caso14, [a,a,b,b,b]).
:- test_case_nfa_dfa_simplify(caso14,_).
%:- told,
%   pwd,
%   tell('./testcases/caso14.out.txt'),
%   fa_print(caso14),
%   test_caso01_1,
%   told.
   
   
   