% OTRO TIPO
% MUTANDO EN FAX!


re_to_fa((X&Y), FN) :- re_to_fa(X, FAX),
					   re_to_fa(Y, FAY),
					   link_finals_to_start(FAX, FAY, FAX).					   
					   
					   
link_finals_to_start(X, Y, N):-
	fa_undo_finals(N),
	fa_start(Y, SY),
	fa_final_states(X, LSX),
	forall(member(Z, LSX), (forall(fa_transition(X, Q/S->Z),
		(
			fa_del_transition(N, Q/S->Z),
			fa_add_transition(N, Q/S->SY))))
		),
	fa_transitions(Y, LTY),
	fa_add_transitions(N, LTY),
	fa_final_states(Y, LFY),
	fa_add_finals(N, LFY).