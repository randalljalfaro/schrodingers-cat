% Author: Carlos Loria-Saenz
% Date: 01/10/2013
:-['./src/fa/fa_simplifier'].

:- fa_new([name=caso03, start='A', finals=['E'],
           trans=[
             ('A'/a)->'B',
             ('A'/b)->'C',
             ('B'/a)->'B',
             ('B'/b)->'D',
             ('C'/a)->'B',
             ('C'/b)->'C',
             ('D'/a)->'B',
             ('D'/b)->'E',
             ('E'/a)->'B',
             ('E'/b)->'C'
           ]]).

:- test_case_nfa_dfa_simplify(caso03, _).