:- fa_new([name=caso07, start=q0, finals=[q4],
           trans=[
             (q0/b)->q4,
             (q0/a)->q1,
             (q1/b)->q1,
             (q1/a)->q2,
			 (q2/b)->q2,
			 (q2/a)->q3,
			 (q3/a)->q3,
			 (q3/b)->q1,
			 (q4/a)->q4,
			 (q4/b)->q4
           ]]).


testcase07 :- pwd,
   told,
   tell('./testcases/caso07out.txt'),
   format('DFA before simplification',[]),
   Name=caso07,
   fa_print(Name),
   fa_reachable(Name, q1, LR),
   forall(member(Q, LR), format('~s reaches ~s~n', [q1, Q])),
   fa_simplify_inactive(Name),
   format('DFA after simplification',[]),
   fa_print(Name),
   told.
:-  testcase07.
   
   