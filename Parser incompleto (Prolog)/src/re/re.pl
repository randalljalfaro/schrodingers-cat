% Author:Carlos Loria-Saenz
% Author:Carlos Loria-Saenz
% Para uso exclusivo de: https://sites.google.com/site/una2loriacarlos/
% Date: 10/2013

%

% http://www.swi-prolog.org/pldoc/doc_for?object=op/3

:- op(200,yf, '*').
:- op(200,yf, '+').
:- op(200,yf, '?').
:- op(500,xfy, '&').

re_to_fa(A, FA) :- atomic(A),!,
                   fa_new_name([A], FA),
                   fa_new_state_name([FA,0], Q0),
                   fa_new_state_name([FA,f], Qf),
                   fa_new([ name=FA,
                            start=Q0,
                            finals=[Qf],
                            trans=[
                              Q0/A->Qf
                            ]
                           ]). 		

re_to_fa((X)+, FN) :-  re_to_fa(X, FAX),
					   re_to_fa(X, FAY),
					   fa_new_state_name([FAX,FAY,0], Q0),
					   fa_new_name([FAX,FAY,0], FN),
					   fa_new([ name=FN,
					  		 start=Q0,
					  		 finals=[],
					  		 trans=[]
					  	   ]),
					   fa_link_plus_temp(FAX, FAY,FN).

re_to_fa((X)*, FN) :- re_to_fa(X, FAX),
					  fa_new_state_name([FAX,0], Q0),
					  fa_new_name([FAX,0], FN),
					  fa_new([ name=FN,
					  		 start=Q0,
					  		 finals=[],
					  		 trans=[]
					  	   ]),
					  fa_add_final(FAX, Q0),
					  fa_link_finals_frontend(FAX, FN).
				   
re_to_fa((X+Y), FN) :- re_to_fa(X, FAX),
					   re_to_fa(Y, FAY),
					   fa_new_state_name([FAX,FAY,0], Q0),
					   fa_new_name([FAX,FAY,0], FN),
					   fa_new([name=FN,
                               start=Q0,
                               finals=[],
                               trans=[]
                              ]),
					  fa_link_frontend(FAX, FAY, FN).
					  

re_to_fa((X)?, FAX) :- re_to_fa(X, FAX),
					   fa_start(FAX, SX),
					   fa_add_final(FAX, SX).
				   
re_to_fa((X&Y), FN) :- re_to_fa(X, FAX),
					    re_to_fa(Y, FAY),
						fa_start(FAX, SX),
						fa_new_name([FAX,FAY,0], FN),
					    fa_new([name=FN,
                               start=SX,
                               finals=[],
                               trans=[]
                              ]),
					    fa_link_front_start(FAX, FAY, FN).
                           
re_to_fa(_, _) :- throw('case of re_to_fa not implemented!').

 % RE= ((a&b)*), %((a+b)&c),
 % (a+(b+c)?)+
test_re(F) :- RE=(a+b)*, 
             format('*** Testing RE->NFA: (~w) ***~n',[RE]),
             reset_gensym,
             re_to_fa(RE, F),
             fa_print(F).
             % fa_inactive_show(F),
             % fa_unreachable_show(F),
             % fa_simplify_show(F).
                      
                      