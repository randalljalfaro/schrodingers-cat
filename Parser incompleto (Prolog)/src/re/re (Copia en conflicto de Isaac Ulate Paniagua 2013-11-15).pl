% Author:Carlos Loria-Saenz
% Para uso exclusivo de: https://sites.google.com/site/una2loriacarlos/
% Date: 10/2013

%

% http://www.swi-prolog.org/pldoc/doc_for?object=op/3

:- op(200,yf, '*').
:- op(200,yf, '+').
:- op(200,yf, '?').
%:- op(400,xfx, '+').
:- op(500,xfy, '&').

re_to_fa(A, FA) :- atomic(A),!,
                   fa_new_name([A], FA),
                   fa_new_state_name([FA,0], Q0),
                   fa_new_state_name([FA,f], Qf),
                   fa_new([ name=FA,
                            start=Q0,
                            finals=[Qf],
                            trans=[
                              Q0/A->Qf
                            ]
                           ]).
%re_to_fa((X+Y), FAS) :- re_to_fa(X, FAX),
%					  re_to_fa(Y, FAY),
%					  fa_new_name([FAX,FAY], FAS),
%					  fa_new_state_name([FAS,0], Q0),
%					  fa_final_states(FAX, FEXL),
%					  fa_final_states(FAY, FEYL),
%					  append(FEXL, FEYL, L),
%					  fa_new([name=FAS,
%                              start=Q0,
%                              finals=L,
%                              trans=[]
%                             ]),
%					  fa_link_one_start(FAS, FAX),
%					  fa_link_one_start(FAS, FAY).

re_to_fa((X)*, FAX) :- re_to_fa(X, FAX),
					 fa_start(FAX, SA),
					 fa_add_final(FAX, SA),
					 forall(fa_transition(FAX, (_/S)->Q),
					 fa_add_transition(FAX,(Q/S)->Q)).
					 

re_to_fa((X)?, FAX) :- re_to_fa(X, FAX),
					 fa_start(FAX, SA),
					 fa_add_final(FAX, SA),
					 forall(fa_transition(FAX, (_/S)->Q),
							fa_add_transition(FAX,(Q/S)->Q)).
					 
re_to_fa((X)+, FAX) :- re_to_fa(X, FAX),
					   forall(fa_transition(FAX, (_/S)->Q),
							fa_add_transition(FAX,(Q/S)->Q)).

re_to_fa((X&Y), FAA) :- re_to_fa(X, FAX),
					    re_to_fa(Y, FAY),
					    fa_new_name([FAX,FAY], FAA),
					    fa_new_state_name([FAA,0], Q0),
					    fa_new([name=FAA,
                                 start=Q0,
                                 finals=[],
                                 trans=[]
                               ]),
						fa_link_one_start(FAA,FAX),
						% fa_link_not_ends_not_start(FAA,FAX),
						% fa_link_ends_to_start(FAA, FAX, FAY),
						%forall(fa_final(FAY, T),
						%	fa_add_final(FAA, T)),
						fa_link_first_fa(FAA, FAX),
						fa_link_ends_to_start(FAA, FAY).
                           
re_to_fa(_, _) :- throw('case of re_to_fa not implemented!').

 % RE= ((a&b)*), %((a+b)&c),
 % (a+(b+c)?)+
test_re(F) :- RE=(a+b)*, 
             format('*** Testing RE->NFA: (~w) ***~n',[RE]),
             reset_gensym,
             re_to_fa(RE, F),
             fa_print(F).
             % fa_inactive_show(F),
             % fa_unreachable_show(F),
             % fa_simplify_show(F).
                      
                      