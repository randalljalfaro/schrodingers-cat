:-[fa, fa_reachable].

fa_inactive(F, X):-
   fa_reachable(F, X, RX),
   fa_final_states(F, LF),
   not(member(X, LF)),
   intersection(LF, RX, []).

fa_inactive_states(F, I) :-
   fa_states(F, S),
   findall(X, (member(X, S), fa_inactive(F, X)), L),
   sort(L, I).
   
fa_simplify_inactive(F) :-
    fa_inactive_states(F, I),
    forall((member(X, I),fa_transition(F, (_/_)->X)),
            fa_del_transition(F, (_/_)->X)
    ).
	
fa_inactive_show(F) :-
    format('~nInactive States: ~a~n',[F]),
    fa_inactive_states(F, I),
    forall(member(X, I), format('...Inactive: ~a~n', [X])).
    

