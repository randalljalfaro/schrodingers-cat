% Author:Carlos Loria-Saenz
% Para uso exclusivo de: https://sites.google.com/site/una2loriacarlos/
% Date: 01/10/2013


partition(_, [], [], []).
partition(G,[X|R], YR, NR):-
partition(G, R, Y, N),
 (apply(G,[X])->(YR=[X |Y],NR=N);(NR=[X |Y],YR=Y)).
 
 zip([], [], []).
 zip([X|RX], [Y|RY], [[X,Y]|RXY]):-zip(RX, RY, RXY).
 
 zip_withindex([],[]):- !.
 zip_withindex(L, LI):- length(L,N),
						N1 is N-1,
						numlist(0, N1, R),
						zip(L, R, LI).