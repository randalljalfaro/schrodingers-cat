% Author: Carlos Loria-Saenz loriacarlos@gmail.com
% Para uso exclusivo de: https://sites.google.com/site/una2loriacarlos/
% Date: 01/11/2011

:-[fa].
%Entrada: N el id de un NFA
%Salida:  DN el id de un dfa equivalente a N
%PASOS (recuerde que los estados de DN serán listas de estados de N)
%0. Crear un FA vacio dando un nombre para DN y siendo [N] su estado inicial 
%1. Inicializar una pila P (listas de estados visitados en DFS)
%2. Poner el estado inicial de [N] de DN en P (los estados son listas de estados)
%3. Repetir lo siguiente mientras P no esté vacía:
%   3.1 Sea Q el top de P (pop it)
%   3.2 Para cada simbolo S de N:
%       3.2.1 Sea LQ la lista de estados que salen de N viendo S
%       3.2.2 Si LQ NO está en P (no visitado) ponga LQ en P (push it)
%       3.2.3 Cree en DN una transicion Q/S->LQ
%       3.2.4 Si hay en LQ un estado final de N hacerlo final en DN

%Rcuerde manejar las lista de estados adecuadmante porque por ejemplo [q1, q3, q0,q0, q3] representa el mismo estado que [q0, q1, q3]].

fa_nfa_to_dfa(N, D):-
   fa_start(N,SN),
   dfs_visit(N, [SN], [], L),
   concat_atom([N,dfa],A),
   D=A,
   fa_add_start(D,SN),
   nfa_to_dfa(N,L,D).
   
   
nfa_to_dfa(N,LS,DN):- forall(member(X,LS),
							( fa_transition_symbols(N,X,LT),
							  forall(member(Y,LT),fa_cut_duplicate(N,X,Y,DN)))).
							  

								 
add_if_final(N,X,DN):- fa_final(N,X),!,
					  fa_add_final(DN,X).
add_if_final(_,_,_).

fa_cut_duplicate(N,X,Y,DN):- findall(X/Y->Q, fa_transition(N, X/Y->Q), LS),
							 length(LS, K),
							 K is 1 ,!,
							 forall(member(A/B->C, LS), 
								(add_if_final(N,A,DN),
								 add_if_final(N,C,DN),
								 fa_add_transition(DN,A/B->C))).

fa_cut_duplicate(N,X,Y,DN):- fa_transition_to(N,X,Y,LT),
							fa_new_state_name(LT,S),
							forall(fa_transition( N, X/Y->Q), 
								fa_add_transition(DN, Q/Y->S)).

%fa_cut_duplicate(N,X,Y,DN):-fa_num_transition_symbol(N,X,Y,K),
%							findall(X,fa_transition(N,X/Y->_),LTX),
	%						forall(member(X/Y->Q,LTX),fa_add_transition(DN,X/Y->Q)).