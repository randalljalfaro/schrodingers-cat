% Autor: Carlos Loria-Saenz
% Fecha: 11/10/2013

:-[fa_nfa_dfa, fa_reachable, fa_inactive, fa_util,'./src/re/re'].


fa_simplify_show(F):-
	%Corregir***
   fa_inactive_show(F),
   fa_unreachable_show(F),
   format('Before Simplifying: ~a ~n',[F]),
   fa_print(F),
   fa_simplify_inactive(F),
   fa_simplify_unreachable(F),
   format('After  Simplifying: ~a ~n',[F]),
   fa_rename(F,RF),
   fa_print(RF).

fa_nfa_to_dfa_show(N, F) :-
      format('NFA: ~a before NFA->DFA~n',[N]),
      fa_print(N),
	  fa_nfa_to_dfa(N, F),
      format('NFA: ~a after  NFA->DFA is ~a~n',[N, F]),
      fa_print(F).
	  
test_case_nfa_dfa_simplify(N, F) :-
      % Sets output to file based on N
      concat_atom(['testcases/', N, 'out.txt'], Out),
      open(Out, write, OutStream),
      current_output(Cout),
      format('*** Starts Test Case ~a (output in ~a) ***~n',[N, Out]),
      set_output(OutStream),
       format('*** Starts Test Case ~a ***~n',[N]),
      fa_nfa_to_dfa_show(N, F),
      fa_simplify_show(F),
      % Close file and Restores old Out
      format('*** End of Test Case ~a ***~n',[N]),
      close(OutStream),
      set_output(Cout),
      format('*** End of Test Case ~a (output in ~a) ***~n',[N, Out]).

test_case_re_to_fa_simplify(N, RE, F) :-
      % Sets output to file based on N
      concat_atom(['testcases/', N, 'out.txt'], Out),
      open(Out, write, OutStream),
      current_output(Cout),
      format('*** Starts Test Case ~a (output in ~a) ***~n',[N, Out]),
      set_output(OutStream),
      format('*** Starts Test Case ~a ***~n',[N]),
	  re_to_fa(RE, M),
	  fa_print(M),
      fa_nfa_to_dfa_show(M, F),
      fa_simplify_show(F),
      % Close file and Restores old Out
      format('*** End of Test Case ~a ***~n',[N]),
      close(OutStream),
      set_output(Cout),
      format('*** End of Test Case ~a (output in ~a) ***~n',[N, Out]).
	  
make_new_state_name(Q,N,Sep,QN):-
	concat_atom([Q,N], Sep, QN).
	
make_new_state_name(N,QN):-
	make_new_state_name('s', N,'',QN).

:- dynamic fa_renamed/2.

fa_rename(F, NF):-
	fa_start(F, Start),
	fa_reachable(F,Start,LR),
	reverse(LR, LRR),
	zip_withindex(LRR,LRI),
	retractall(fa_renamed(_,_)),
	forall(member([Q,N], LRI),
			(make_new_state_name(N,QN),
				assert(fa_renamed(Q, QN)))),
    fa_new_name([n,F],NF),
	fa_start(F, Start),
	fa_renamed(Start, NStart),
	findall(NQ, (fa_final(F, Q),
		fa_renamed(Q,NQ)), NFinales),
	findall(NQ/S->NQ1, (fa_transition(F, (Q/S)->Q1),
						fa_renamed(Q, NQ),
						fa_renamed(Q1, NQ1)),
				NTrans),		
	fa_new([name=NF,start=NStart, finals=NFinales, trans=NTrans]).