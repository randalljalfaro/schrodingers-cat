% Author:Carlos Loria-Saenz
% Para uso exclusivo de: https://sites.google.com/site/una2loriacarlos/
% Date: 01/10/2013

:-[fa, fa_util].
:- dynamic group/2.
:- dynamic group_state/3.
new_group_name(N, NG) :-
   concat_atom([N, g],'_', Base),
   gensym(Base, NG).
add_group(N, G) :-
  assert(group(N, G)).
del_group(N, G) :-
  rectract(group(N, G)).
add_state_to_group(N, Q, G) :-
  assert(group_state(N,Q,G)).
del_state_from_group(N,Q,G) :-
  retract(group_state(N,Q,G)).
groups(N, LG) :-
  findall(G, group(N, G), LG).
init_groups(N, LG):-
  forall(member(LQ, LG),new_group(N,LQ)).
new_group(N,LQ):-
  new_group_name(N, GN),
  add_group(N, GN),
  forall(member(Q, LQ),
         add_state_to_group(N,Q,GN)).
new_group(N,G,LQ):-
     new_group_name(N, GN),
     add_group(N, GN),
     forall(member(Q, LQ),
            (del_state_from_group(N,Q,G),
             add_state_to_group(N,Q,GN))).
delete_groups(N) :-
     retractall(group(N,_)),
     retractall(group_state(N,_,_)).
print_group(N,G) :-
 findall(Q,group_state(N,Q,G), LQ),
 format('Name:~a~n',[G]),
 forall(member(Q, LQ), format('~w ', [Q])),
 nl.
print_groups(N) :-
 findall(G,group(N,G), LG),
 format('Groups:~an~n',[N]),
 forall(member(G, LG), print_group(N,G)),
 nl.
     
transition_to_group(N, Q/S->G):-
      fa_transition(N,Q/S->QN),
      group_state(N,QN,G).
states_to_groups(N, Q, LS, LGQ) :-
    findall(S/G,(member(S, LS), transition_to_group(N,Q/S->G)), LG),
    sort(LG, LGQ).
% Q1 and Q2 are equivalent if for every S they lead to states in same group
fa_eq(N, Q1, Q2) :-
    fa_symbols(N, LS),
    states_to_groups(N, Q1, LS, LG1),
    states_to_groups(N, Q2, LS, LG1).

% Select group where state Q belongs
find_state_group(N, Q, G) :- group_state(N,Q,G).
fa_group_members(N, G, GM) :-
  findall(Q,find_state_group(N, Q, G), GM).

%Select state Q and partition group on Q: equivalent (Eq)to Q and the rest (Neq)
fa_partition_one_group(_, [], [], []).
fa_partition_one_group(_, [Q], [Q], []) :- !.
fa_partition_one_group(N, [Q|RG], [Q|Eq], Neq) :-
    partition(fa_eq(N, Q), RG, Eq, Neq).

%Partition each group in a list of groups
fa_partition_groups(N, [], NG):- %end rule
   groups(N, NG).
fa_partition_groups(N, [[]|RL], NG):- !, %skip empty groups if any
   fa_partition_groups(N, RL, NG).
fa_partition_groups(N, [[_]|RL], NG):- !, %pass singleton [M]
   fa_partition_groups(N, RL, NG).
fa_partition_groups(N, [G|RG], NG) :-
     fa_group_members(N, G, GM),
     fa_partition_one_group(N, GM, GEq, GNeq),
     (GNeq=[_|_]->
       new_group(N, G, GEq);
       true),
     fa_partition_groups(N, RG, NG).

no_group_change(G, NG) :-
  length(G, K), length(NG, K).
fa_partition(N, G) :-
  reset_gensym,
  delete_groups(N),
  fa_final_not_final_states(N, F, NF),
  IP=[F, NF],
  init_groups(N, IP), groups(N,IG),
  fa_partition_loop(N, IG, G).
fa_partition_loop(N, CG, G) :-
  print_groups(N),
  fa_partition_groups(N, CG, NG),
  (no_group_change(CG, NG)->
        %then NG is already the final partition and we are done
        G=NG
        ;% else
        % Start new partition cycle.
        fa_partition_loop(N, NG, G)
  ).

  
nfa_dfa_groups_print(F) :-
 format('Groups after NFA->DFA for: ~a~n', [F]),
 forall(nfa_state_name(LQ, QN),
        format('...~a = ~w~n',[QN, LQ])).
		
		
%todo: fa_build
fa_build_fa(N, E) :-
  throw(fa_error(unimplemented, [fa_build_fa, N, E])).
fa_minimize(N) :-
    fa_partition(N, G), %G is the partition to equivalent states groups
    % A new FA must be built using E...
    fa_build_fa(N, G).



    