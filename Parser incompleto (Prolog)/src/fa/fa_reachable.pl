:-[fa].

fa_unreachable_states(F, LU):-
    fa_start(F, Start),
    fa_reachable(F, Start, LR),
	fa_states(F, States),
	subtract(States, LR, LU).

fa_simplify_unreachable(F) :-
	fa_unreachable_states(F, UStates),
	forall((member(X, UStates),fa_transition(F, (X/_)->_)),
            fa_del_transition(F, (X/_)->_)
    ).


fa_unreachable_show(F) :-
    format('~nUnreachable States: ~a~n',[F]),
    fa_unreachable_states(F, I),
    forall(member(X, I), format('...Unreachable: ~a~n', [X])),
	format('~n',[]).
	
fa_reachable(N, Q, LR)	:- dfs_visit(N, [Q], [], LR).

dfs_visit(_, [], V, V).
dfs_visit(N, [Q|NV], V, LR) :- member(Q, V),!,
						      dfs_visit(N, NV, V, LR).
							  
dfs_visit(N, [Q|NV], V, LR) :- 
				findall(QN, fa_transition(N, (Q/_)->QN), LQN),
			   append(LQN, NV, NNV),
			   dfs_visit(N, NNV, [Q|V], LR).
   
