% Author: Carlos Loria-Saenz loriacarlos@gmail.com
% Para uso exclusivo de: https://sites.google.com/site/una2loriacarlos/
% Date: 10/2013
% Objetivos
%  Introducir operaciones de BD en prolog
%  Modelar un objeto en Prolog
%  Ilustrar algoritmos sobre expresiones
%  Evaluar el paradigma logico
% Espacio de Tareas Posibles
% DFA
%  *Corrida
%  *Analisis
%    *nodeterminismo (DFA o NFA)
%    *estados inalcanzables
%  *Eliminacion de codigo muerto
% RE
%  *Modelo usando expresiones de prolog
%  *Conversion RE -> NFA
% NFA
%  *Corrida  usando backtracking de prolog
%  *Conversi�n  nfa -> dfa
%  *Minimizacion
%  *Determinacion
%

%:- use_module(library(apply)).

% Modelando un DFA
% dynamic le dice al compilador que se puede agregar/borrar reglas
:- dynamic fa_start/2.
:- dynamic fa_final/2.
:- dynamic fa_transition/2.

% Operaciones de un fa
fa_add_start(N, Q)    :- not(fa_start(N, _)),!,
                         assert(fa_start(N, Q)).
fa_add_start(N, Q)    :- throw(fa_error(duplicated_starts, N, Q)).

fa_change_start(N, Q) :- retract(fa_start(N,_)),
                         assert(fa_start(N,Q)).
                         
fa_add_final(N, Q)    :- assert(fa_final(N, Q)).
fa_undo_final(N, Q)   :- retract(fa_final(N,Q)).

fa_undo_finals(N):- retractall(fa_final(N,_)).

fa_add_finals(N, LF):-
 forall(member(X, LF), fa_add_final(N, X)).

fa_add_transition(N, T) :- T=((_/_)->_),
                           assert(fa_transition(N, T)).
fa_add_transitions(N, LT):-
 forall(member(X, LT), fa_add_transition(N, X)).

fa_del_transition(N, T) :- retract(fa_transition(N, T)).


fa_new_name(LN, N) :-
  %sort(LN,LS),
  concat_atom(LN, '_', A), %cambiar por atomic_list_concat!
  gensym(A, N).
  
fa_new_state_name(LN, N) :-
  concat_atom(LN, '_', N).
  
% Constructores
% Construye a partir de Tfa un termino de FA
fa_new(Tfa):-
    Tfa=[name=N, start=Q, finals=F, trans=T],!,
    fa_delete(N),
    fa_add_start(N, Q),
    fa_add_finals(N, F),
    fa_add_transitions(N, T).
fa_new(L) :- throw(fa_error(invalid_new, L)).



% A partir de LN una lista de FAs
fa_new_from(LN, N) :-
   fa_new_name(LN , N),
   fa_delete(N),
   fa_new_state_name([N,0] , Q),
   fa_add_start(N, Q),
   forall((member(X, LN),fa_transitions(X, T)),
           fa_add_transitions(N, T)),
           
   forall((member(X, LN),fa_final_states(X, T)),
           fa_add_finals(N, T)).

fa_link_sequential(FAB, LN) :-
   LN=[A, B],
   fa_new_name(LN , FAB),
   fa_delete(FAB),
   fa_start(A, SA),
   fa_add_start(FAB, SA),
   forall((member(X, LN),fa_transition(X, T)),
           fa_add_transition(FAB, T)),

   forall(fa_final(B, T),
           fa_add_final(FAB, T)).
      

	  
fa_link_star(FA, A) :-
   fa_new_name([A] , FA),
   fa_delete(FA),

   fa_new_state_name([FA,0] , SFA),
   fa_add_start(FA, SFA),
   %Copy Transitions
   forall( fa_transition(A, T),
           fa_add_transition(FA, T)),
   %Link SFA to old entries in FA
   fa_start(A, SA),
   forall( fa_transition(A, (SA/I->X)),
           fa_add_transition(FA, (SFA/I->X))),
   % Finals
   forall(fa_final(A, T),
          fa_add_final(FA, T)),
   fa_add_final(FA, SFA),
   
   % Loop transitions
   fa_final_states(FA, FAfinals),
   forall( fa_transition(A, (SA/I->X)),
           forall(member(F, FAfinals),
                  fa_add_transition(FA, (F/I->X))
                  )
   )
   .
% Destructor de FAs
fa_delete(N) :-
   retractall(fa_start(N,_)),
   retractall(fa_final(N,_)),
   retractall(fa_transition(N,_)).
% para saber si el FA es NFA
fa_is_nfa(N) :-
   fa_transition(N, Q/S->QN), %transicion en FA N en estado Q viendo S  hacia QN
   fa_transition(N, Q/S->QM), %transicion en FA N en estado Q viendo S  hacia QM
   QN \= QM.                  % con QN y QM siendo distintos s�mbolos
                              %(\= es la negaci�n de =)
   
%Impresion de FA
fa_print(N) :-
  fa_start(N, A),
  format('.Name=~a~n', [N]),
  format('.Start=~a~n', [A]),
  fa_final_states(N, F),
  format('.Final States:~n',[]),
  forall(member(X, F),
         format('...Final=~a~n', [X])),
  fa_transitions(N, T),
  format('.Transitions:~n',[]),
  forall(member(Q/S->QN, T),
         format('...(~a/~a) -> ~a~n', [Q,S,QN])).


% Accesores de estados
fa_symbols(N, V) :-
   findall(S, fa_transition(N, (_/S->_)), LS),
   sort(LS, V).
fa_special_states(N, L) :-
   findall(Q, (fa_start(N,Q);fa_final(N,Q)), QF),
   sort(QF, L).
fa_final_states(N, L) :-
   findall(Q, fa_final(N,Q), QF),
   sort(QF, L).
fa_states(N, L) :-
   fa_start(N, S0),
   fa_final_states(N, QF),
   findall([Q, QN], fa_transition(N, (Q/_->QN)), QS),
   flatten(QS, QSF),
   append([S0|QF], QSF, QSR),
   sort(QSR, L).
fa_transition_symbols(N, S, LS) :-
   findall(A, fa_transition(N, S/A->_), LS).
fa_transition_to(N, S,A, LS) :-
   findall(Q, fa_transition(N, S/A->Q), LS).
fa_num_states(N, K) :-
   fa_states(N, L), length(L, K).
fa_transitions(N, L) :-
   findall(Q, fa_transition(N,Q), QF),
   sort(QF, L).

fa_final_not_final_states(N, F, NF):-
  fa_states(N, L),fa_final_states(N, F),
  findall(X, (member(X, L), not(member(X, F))), NF).
%
% Utilerias para RE

fa_link_one_start(N, A):-
   fa_start(N, SN),
   fa_start(A, SA),
   forall(fa_transition(A, (SA/X)->Q),
          fa_add_transition(N,(SN/X)->Q)).
fa_link_starts(N, L):-
   maplist(fa_link_one_start(N), L).
   
fa_link_ends(N, A, B):-
   fa_start(A, SA),
   fa_start(B, SB),
   forall((fa_final(A, SFA), fa_transition(A, (X/I)->SFA)),
           fa_add_transition(N,(X/I)->SB)
   ),
   (fa_final(A, SA)-> forall(fa_transition(B, (SB/I)->X),
                             fa_add_transition(N,(SA/I)->X));
                      true
   ).  

fa_link_frontend(X, Y, N):-
	fa_start(X, SX),
	fa_start(N, SN),
	fa_start(Y, SY),
	forall(fa_transition(X, SX/S->Q), 
		fa_add_transition(N, SN/S->Q)),
	forall(fa_transition(Y, SY/S->Q), 
		fa_add_transition(N, SN/S->Q)),
	fa_transitions(X, LTX),
	fa_transitions(Y, LTY),
	append(LTX, LTY, LTN),
	fa_add_transitions(N, LTN),
	fa_final_states(X, LFX),
	fa_final_states(Y, LFY),
	append(LFX, LFY, LFN),
	fa_add_finals(N, LFN).

fa_link_finals_frontend(X, N) :-
	fa_start(N, SN),
	fa_start(X, SX),
	forall(fa_transition(X, SX/S->Q), 
			( forall(fa_final(X, F), 
				forall(fa_transition(X, SX/S->_),
					fa_add_transition(N, F/S->Q))
			))),
	forall(fa_transition(X, SX/S->Q),
			fa_add_transition(N, SN/S->Q)),
	fa_transitions(X, LTX),
	fa_add_transitions(N, LTX),
	fa_final_states(X, LFX),
	fa_add_finals(N, LFX).
	
fa_link_plus_temp(X,Y,N):-
	fa_start(N, S),
	% fa_new_state_name([Y,0], S),
	forall(fa_final(X,F),
		forall(fa_transition(X, A/B->F), fa_add_transition(N, A/B->S))),
    fa_start(Y,SY),
	forall(fa_transition(Y, SY/T->Q), fa_add_transition(N, S/T->Q)),
	forall(fa_transition(Y, SY/T->Q), 
		forall(fa_final(Y, F), fa_add_transition(N, F/T->Q))),
	fa_transitions(Y, LY),
	fa_transitions(X, LX),
	append(LY,LX,LN),
	fa_final_states(Y, LFY),
	fa_add_finals(N, LFY),
	fa_add_final(N,S),
	fa_add_transitions(N, LN),
	fa_start(X, SX),
	fa_change_start(N, SX). % aca el start es el de or de X
	
fa_link_front_start(X, Y, N) :-	  	
	fa_start(X, SX),
	fa_start(N, SN),
	fa_start(Y, SY),
	forall(fa_transition(X, SX/S->_), 
		fa_add_transition(N, SN/S->SY)),
	fa_transitions(Y, LTY),
	fa_transitions(X, LTX),
	append(LTX, LTY, LTN),
	fa_add_transitions(N, LTN),
	fa_final_states(Y, LFY),
	fa_add_finals(N, LFY).
	
%---------------------------------------------------
% Corrida
% N el FA, T la hilera de entrada (como lista de simbolos)
dfa_run(N, T) :- dfa_run(N, T, accept). % acepta por exito
% R el resultado accept � reject
dfa_run(N, T, R) :- fa_start(N, Q),
                    dfa_loop(N, Q, T, R).
% El ciclo del DFA
dfa_loop(N, Q, [], accept) :- fa_final(N, Q).
dfa_loop(N, Q, [S|T], R) :-
   fa_transition(N, (Q/S)->QN),
   fa_report(N, Q, S, QN),
   dfa_loop(N, QN, T, R), !.  %Aca garantiza determinismo.
dfa_loop(_, _, _, reject).

% un log de por donde va pasando
fa_report(N, Q, S, QN) :-
    format('~a: ~a/~a -> ~a~n', [N, Q, S, QN]).
    
fa_run_show(N, T) :-
     format('*** ~a starts! ***~n', [N]),
     dfa_run(N, T, R),
     format('*** ~a ~aed! ***~n', [N, R]).
