"""
 Sample Python/Pygame Programs
 Simpson College Computer Science
 http://programarcadegames.com/
 http://simpson.edu/computer-science/
 
 Vídeo explicativo: http://youtu.be/O4Y5KrNgP_c
"""
 
import pygame
import random
from pyTEC.boton import *
#--- Constantes Globales ---
NEGRO    = (   0,   0,   0)
BLANCO    = ( 255, 255, 255)
VERDE    = (   0, 255,   0)
ROJO      = ( 255,   0,   0)
 
largo_pantalla  = 1200//2
alto_pantalla = 670

class Nave(pygame.sprite.Sprite):
    """ Esta clase representa al protagonista. """
    def __init__(self, path):
        pygame.sprite.Sprite.__init__(self) 
        self.image = pygame.image.load(path+"\\pyRacerSpace\\images\\naveEspacial2.png")
        self.image = pygame.transform.scale(self.image, (70,100))
        #self.image.fill((0,0,255))
        self.rect = self.image.get_rect()
        self.posAltura = 2
        self.posAncho = 1
        self.movX = 0
        self.movY = 0
        self.poder = 0
        self.path = path
 
    def update(self):
        """ Actualiza la posición del protagonista. """
        altura = int(((abs(4-self.posAltura))**3.5)*(50/100) + 50)
        ancho = int(((abs(4-self.posAltura))**3.5)*(80/100) + 80)
        self.image = pygame.image.load(self.path+"\\pyRacerSpace\\images\\naveEspacial2.png")
        self.image = pygame.transform.scale(self.image, (ancho,altura))
        self.rect = self.image.get_rect()
        self.rect.x = ((largo_pantalla//4)*self.posAncho)-(largo_pantalla//16)+self.movX
        self.rect.y = alto_pantalla - 100

    def moverDerecha(self):
        if self.posAncho == 1 or self.posAncho == 2:
            self.posAncho += 1
            self.movX = 20
        else:
            self.movX = 0

    def moverIzquierda(self):
        if self.posAncho == 3 or self.posAncho == 2:
            self.posAncho -= 1
            self.movX = -20
        else:
            self.movX = 0

    def moverArriba(self):
        if self.posAltura == 3 or self.posAltura == 2:
            self.posAltura -= 1
            self.movY = 40
        else:
            self.movY = 0

    def moverAbajo(self):
        if self.posAltura == 1 or self.posAltura == 2:
            self.posAltura += 1
            self.movY = -40
        else:
            self.movY = 0
 
