import urllib.parse, urllib.request
import sys
import json
headers = {"Content-type": "application/x-www-form-urlencoded",
           "Accept": "text/plain"}

direccion_afuera = "http://ic-itcr.ac.cr:2005"
direccion_local = "http://172.19.127.50:2005"
direccion = direccion_afuera
PYGAME_CODE = 1

def PETICION_SERVER(tipo, direccion, ruta,
                    headers, params, esperaJSON):
    try:
        data = urllib.parse.urlencode(params)
        data = data.encode('utf-8')
        req = urllib.request.Request(direccion+ruta, data)
        f = urllib.request.urlopen(req).read()
        if esperaJSON:
            #print("R: ",f.decode("utf8"))
            return json.loads(f.decode("utf8"))
        return f.decode("utf8")
    except:
        print ("Unexpected error:", sys.exc_info()[0])
        raise


parametros = {
    "CREAR_PARTIDA":{"messageType": "Conection",
                     "gameCode": PYGAME_CODE,
                     "playerName": 0,
                     "gameSession" : 0},
    "COMPROBAR_UNION":{"messageType": "Check",
                       "gameSession" : 0},
    "UNIRSE_PARTIDA":{"messageType": "Conection",
                      "gameCode": PYGAME_CODE,
                      "playerName": 0,
                      "gameSession" : 0},
    "EMPEZAR_PARTIDA":{"messageType": "Create",
                       "gameSession" : 0},
    "ESTADO_JUEGO":{"messageType": "Get",
                    "gameSession" : 0},
    "ACTUALIZAR_ESTADO":{"messageType": "Update",
                         "gameSession" : 0,
                         "playerId":0,
                         "elements":[]},
    }

def crearPartidaServer(nombreUsuario):
    params = parametros["CREAR_PARTIDA"]
    params["playerName"] = str(nombreUsuario)
    print("crearPartidaServer: ",params)
    return PETICION_SERVER(
        "POST", direccion, "/gameDoor",
        headers, params, True
        )
#x = crearPartidaServer("u")

def comprobarUnionServer(gameSession):
    params = parametros["COMPROBAR_UNION"]
    params["gameSession"] = gameSession
    return PETICION_SERVER(
        "POST", direccion, "/gameDoor",
        headers, params, True
        )

def unirseAPartidaServer(idUsuario, gameSession):
    params = parametros["UNIRSE_PARTIDA"]
    params["playerName"] = idUsuario
    params["gameSession"] = gameSession
    print("unirseAPartidaServer: ",params)
    return PETICION_SERVER(
        "POST", direccion, "/gameDoor",
        headers, params, True
        )
#y = unirseAPartidaServer("hgbjh", x["gameSession"])

def empezarPartidaServer(gameSession):
    params = parametros["EMPEZAR_PARTIDA"]
    params["gameSession"] = gameSession
    return PETICION_SERVER(
        "POST", direccion, "/gameDoor",
        headers, params, False
        )

def estadoJuegoServer(gameSession):
    params = parametros["ESTADO_JUEGO"]
    params["gameSession"] = gameSession
    print("estadoJuegoServer: ",params)
    return PETICION_SERVER(
        "POST", direccion, "/gameDoor",
        headers, params, True
        )
#print(estadoJuegoServer(x["gameSession"]))

def actualizarEstadoServer(gameSession, tipoJugador, elementos):
    params = parametros["ACTUALIZAR_ESTADO"]
    params["gameSession"] = gameSession
    params["playerId"]=tipoJugador
    params["elements"]=elementos
    print("actualizarEstadoServer: ",params)
    return PETICION_SERVER(
        "POST", direccion, "/gameDoor",
        headers, params, False
        )

import threading
import time
import pygame
 
 
class ThreadServerClient(object):
    def __init__(self, app, accion, juego, interval=0.333):
        print("************************")
        self.interval = interval
        self.app = app
        self.juego = juego
        self.accion = accion
##        self.thread = threading.Thread(target=self.run, args=())
##        self.thread.daemon = True   # Daemonize thread                             

    def comprobarUnion(self):
        def run(self):
            puntos = "..."
            i=3
            updown = False
            print(self.app.estadoJuego)
            while self.app.estadoJuego == "ESPERANDO_UNION":
                data = comprobarUnionServer(self.app.gameSession)
                pConfig = self.app.PantallaConfigPartida
                print("ESPERANDO_UNION: ",data)
                if data["message"]=="Acepted":
                    print(str(data))
                    self.app.estadoJuego = "ESPERANDO_EMPEZAR"
                    pConfig.btnUsuarioInv.texto.valor = str(data["otherPlayers"][0]["name"])
                    self.app.aliasContra = data["otherPlayers"][0]["name"]
                else:
                    if i==0:
                        updown = False
                    elif i==3:
                        updown = True
                    if not updown:
                        i+=1
                    else:
                        i-=1
                    pConfig.btnUsuarioInv.texto.valor = str("Esperando "+puntos[:i])
                
                time.sleep(self.interval)
        self.thread = threading.Thread(target=run, args=(self,))
        return self.thread
        
    def estadoJuego(self):
        def run(self):
            while self.app.estadoJuego == "JUGANDO":
                print("ACTUALIZANDO")
                self.juego.data = estadoJuegoServer(self.app.gameSession)["elements"]
                #print("DATA: ",self.juego.data)
                time.sleep(self.interval)
        self.thread = threading.Thread(target=run, args=(self,))
        return self.thread
