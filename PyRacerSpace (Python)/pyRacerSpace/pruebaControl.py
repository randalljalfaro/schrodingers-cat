import serial, threading, time
def run():
    ser = serial.Serial(
        port="COM8",
        baudrate=9600
    )
    last_received = ""
    while True:
        ser.write(str("S").encode())
        last_received = str(ser.readline().decode("utf-8"))[:2]
        print("*****",last_received,"******")
        time.sleep(0.1)
    ser.close()
    
threading.Thread(target=run).start()
