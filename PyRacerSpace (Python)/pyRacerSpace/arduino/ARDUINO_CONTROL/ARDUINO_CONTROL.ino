int pinAr = 2;
int pinAb = 3;
int pinDer = 4;
int pinIzq = 5;
int valueAr = 0;
int valueAb = 0;
int valueDer = 0;
int valueIzq = 0;
int inputChar= 0; 
void setup() {
  Serial.begin(9600);   //iniciar puerto serie
  pinMode(pinAr, INPUT);  //definir pin como entrada
  pinMode(pinAb, INPUT);  //definir pin como entrada
  pinMode(pinDer, INPUT);  //definir pin como entrada
  pinMode(pinIzq, INPUT);  //definir pin como entrada
  pinMode(13, OUTPUT);
  digitalWrite(pinAr, HIGH);
  digitalWrite(pinAb, HIGH);
  digitalWrite(pinDer, HIGH);
  digitalWrite(pinIzq, HIGH);
}
 
void loop(){
  valueAr = digitalRead(pinAr);  //lectura digital de pin
  valueAb = digitalRead(pinAb);  //lectura digital de pin
  valueDer = digitalRead(pinDer);  //lectura digital de pin
  valueIzq = digitalRead(pinIzq);  //lectura digital de pin
  digitalWrite(13, HIGH);
  
  //mandar mensaje a puerto serie en función del valor leido
  if (valueAr == LOW) {
      Serial.println("ARRIBA");
  }
  if (valueAb == LOW) {
      Serial.println("ABAJO");
  }
  if (valueDer == LOW) {
      Serial.println("DERECHA");
      digitalWrite(13, LOW);
  }
  if (valueIzq == LOW) {
      Serial.println("IZQUIERDA*");
      digitalWrite(13, HIGH);
  }
  while (Serial.available() > 0) {
      inputChar = (char)Serial.read();
      Serial.print("RECIBIDO");
      Serial.println(inputChar);
      if (inputChar=='0'){
        digitalWrite(13, LOW);
      }
      if (inputChar=='1'){
        digitalWrite(13, HIGH);
      }
  }
  delay(100);
}
