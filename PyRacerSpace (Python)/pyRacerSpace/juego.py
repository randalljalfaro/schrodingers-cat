"""
 Sample Python/Pygame Programs
 Simpson College Computer Science
 http://programarcadegames.com/
 http://simpson.edu/computer-science/
 
 Vídeo explicativo: http://youtu.be/O4Y5KrNgP_c
"""
 
import pygame
from serial import *
import random
import json
import threading
from pyTEC.boton import *
from pyRacerSpace.nave import *
from pyRacerSpace.obstaculo import *
from pyRacerSpace.conexion import *
#--- Constantes Globales ---
NEGRO    = (   0,   0,   0)
BLANCO    = ( 255, 255, 255)
VERDE    = (   0, 255,   0)
ROJO      = ( 255,   0,   0)
 
largo_pantalla  = 1200
alto_pantalla = 670
conteo = 0
class Juego(object):
    """ Esta clase representa una instancia del juego. Si necesitamos reiniciar el juego,
        solo tendríamos que crear una nueva instancia de esta clase. """
 
    # --- Atributos de la clase. 
    # En este caso, todos los datos necesarios 
    # para ejecutar nuestro juego.
     
    # Lista de sprites
    bloque_lista = None
    listade_todoslos_sprites = None
    protagonista = None
    ganador = False
    ganadorNombre = "*****"
    # Otros datos    
    game_over = False
    fin = False
    puntuacion = 0
    tiempo = 0
    data = []
     
    # --- Métodos de la clase
    # Disponemos el juego
    def __init__(self, DIR_APP, img_fondo,
                 gameSession, tipoJugador,
                 idJugador, pantalla, alias,
                 aliasContra):
        pygame.display.set_mode((largo_pantalla,alto_pantalla))
        self.puntuacion = 0
        self.vidas = 1
        self.tipoJugador = tipoJugador
        self.game_over = False
        self.DIR_APP = DIR_APP
        self.img_fondo = img_fondo
        self.gameSession = gameSession
        self.idJugador = idJugador
        self.alias = alias
        self.aliasContra = aliasContra
         
        # Creamos la lista de sprites
        self.bloque_lista = pygame.sprite.Group()
        self.listade_todoslos_sprites = pygame.sprite.Group()
        
        # Creamos al protagonista
        self.nave = Nave(DIR_APP)
        self.listade_todoslos_sprites.add(self.nave)
        self.naveContrincante = Nave(DIR_APP)
        self.naveContrincante.posAncho = 5
        self.listade_todoslos_sprites.add(self.naveContrincante)
        self.pantalla = pantalla

        self.imageFondo = pygame.image.load(self.img_fondo).convert_alpha()
        self.imageFondo = pygame.transform.scale(self.imageFondo, (largo_pantalla,alto_pantalla))
        
        self.btnPtj = Boton(pantalla)
        self.btnPtj.setPosicion(5,5)
        self.btnPtj.setTamano(150,25)
        self.btnVidas = Boton(pantalla)
        posX2 = largo_pantalla-self.btnVidas.ancho+10
        self.btnVidas.setPosicion(5,self.btnPtj.altura+10)
        self.btnVidas.setTamano(300,25)
        self.btnPtj2 = Boton(pantalla)
        self.btnPtj2.setPosicion(posX2,0)
        self.btnPtj2.setTamano(150,25)
        self.btnPtj2.setTexto("0")
        self.btnVidas2 = Boton(pantalla)
        self.btnVidas2.setPosicion(posX2-150,self.btnPtj.altura+5)
        self.btnVidas2.setTamano(300,25)
        self.btnVidas2.setTexto("Poder: ")
        self.btnPtj2.setTexto("Monedas: ")
        self.btnTiempo = Boton(pantalla)
        ancho = largo_pantalla//2 - self.btnTiempo.ancho//2
        self.btnTiempo.setPosicion(ancho,2*(self.btnPtj.altura+5))
        self.btnTiempo.setTamano(150,25)
        
    #******************************************************************
    def procesos_de_eventos(self):
        """ Procesa todos los eventos. Devuelve un "True" si precisamos
            cerrar la ventana. """
        for evento in pygame.event.get():
            if evento.type == pygame.QUIT:
                self.fin = True
                return True
            if evento.type == pygame.KEYDOWN:
                if evento.key == pygame.K_RIGHT:
                    self.nave.moverDerecha()
                if evento.key == pygame.K_LEFT:
                    self.nave.moverIzquierda()
                if evento.key == pygame.K_UP:
                    self.nave.moverArriba()
                if evento.key == pygame.K_DOWN:
                    self.nave.moverAbajo()
                def run(self):
                    actualizarEstadoServer(
                        self.gameSession,
                        self.tipoJugador,
                        [{"type":"Player",
                          "id":self.tipoJugador,
                          "posX":self.nave.posAncho-1,
                          "coins":self.puntuacion,
                          "time":self.tiempo,
                          "level":self.nave.posAltura-1,
                          "power":self.nave.poder
                          }]
                        )
                threading.Thread(target=run, args=(self,)).start()
        return False

    #****************************************************************** 
    def logica_de_ejecucion(self):
        """
        Este método se ejecuta para cada fotograma. 
        Actualiza posiciones y comprueba colisiones.
        """
        if self.vidas <= 0:
            self.game_over = True
            self.fin = True
            self.vidas=0
        if not self.game_over:
            # Mueve todos los sprites
            self.listade_todoslos_sprites.update()
            # Observa por si el bloque protagonista ha colisionado con algo.
            lista_impactos_bloques = pygame.sprite.spritecollide(
                self.nave,
                self.bloque_lista,
                False
            )
            tipos = [
                "OBSTACULO_ARRIBA",
                "OBSTACULO_ABAJO",
                "OBSTACULO_TOTAL",
                "ARMADURA",
                "IMAN",
                "MONEDA"
            ]
            # Comprueba la lista de colisiones..
            choque = False
            bloque = None
            for bloque in lista_impactos_bloques:
                choqueArriba = (bloque.tipo==tipos[0] and self.nave.posAltura==1)
                choqueAbajo = (bloque.tipo==tipos[1] and self.nave.posAltura==3)
                choqueTotal = (bloque.tipo==tipos[2])
                choque = (choqueArriba or choqueAbajo or choqueTotal)
                if choque:
                    pygame.mixer.Sound(self.DIR_APP+"\\pyRacerSpace\\sonidos\\rock_effect.wav").play()
                    bloque.kill()
                    self.vidas-=1
                    if self.nave.poder == 2:
                       self.nave.poder=0 
                elif bloque.tipo=="IMAN":
                    self.nave.poder = 1
                    bloque.kill()
                elif bloque.tipo=="ARMADURA":
                    self.vidas+=1
                    self.nave.poder = 2
                    bloque.kill()
                elif bloque.tipo=="MONEDA":
                    pygame.mixer.Sound(self.DIR_APP+"\\pyRacerSpace\\sonidos\\coin_effect.wav").play()
                    self.puntuacion += 1
                    bloque.kill()
            if self.nave.poder == 1:
                for sp in self.bloque_lista.sprites():
                    a = (sp.rect.y>(alto_pantalla-300))
                    b = (sp.tipo=="MONEDA")
                    if  a and b:
                        pygame.mixer.Sound(self.DIR_APP+"\\pyRacerSpace\\sonidos\\coin_effect.wav").play()
                        self.puntuacion += 1
                        sp.kill()
        i=0
        if self.tipoJugador==0:
            i=1
        if self.data!=[] and self.data[i]!=None:
            dataNave = self.data[i]
            naveContra = self.naveContrincante
            naveContra.posAncho = dataNave["posX"]+4+1
            naveContra.posAltura = dataNave["level"]+1
            """print("*****naveContra*****",naveContra)
            print("*****",self.data[i])"""

    #******************************************************************             
    def display_frame(self):
        """ Muestra todo el juego sobre la pantalla. """
        pantalla = self.pantalla
        poderes = [
            "", "Colector de monedas", "Escudo"
            ]
        i=0
        if self.tipoJugador==0:
            i=1
        if self.data!=[] and self.data[i]!=None:
            dataNave = self.data[i]
            self.btnVidas2.setTexto("Poder: "+str(poderes[dataNave["power"]]))
            self.btnPtj2.setTexto("Monedas: "+str(dataNave["coins"]))
        self.btnPtj.setTexto("Monedas: "+str(self.puntuacion))
        self.btnVidas.setTexto("Poder: "+str(poderes[self.nave.poder]))
        self.btnTiempo.setTexto("Tiempo: "+str(self.tiempo))
        pantalla.fill(BLANCO)

        if self.ganador:
            #fuente = pygame.font.Font("Serif", 25)
            fuente = pygame.font.SysFont("serif", 30)
            win = self.DIR_APP+"\\pyRacerSpace\\images\\wingame.png"
            wingame = pygame.image.load(win)
            imagen = pygame.transform.scale(wingame,(largo_pantalla,alto_pantalla))
            pantalla.blit(imagen,(0,0))
            btnWIN = Boton(pantalla)
            btnWIN.setPosicion(largo_pantalla//2-100,alto_pantalla//2+100)
            btnWIN.setTamano(200,40)
            btnWIN.setTexto(self.ganadorNombre)
            btnWIN.pintar()
            
        elif self.game_over:
            #fuente = pygame.font.Font("Serif", 25)
            fuente = pygame.font.SysFont("serif", 25)
            over = self.DIR_APP+"\\pyRacerSpace\\images\\gameover.gif"
            gameover = pygame.image.load(over)
            imagen = pygame.transform.scale(gameover,(largo_pantalla,alto_pantalla))
            pantalla.blit(imagen,(0,0))
            
        if not self.game_over and not self.ganador  and not pantalla.get_locked():
            image = pygame.image.load(self.img_fondo)
            image = pygame.transform.scale(image, (largo_pantalla,alto_pantalla))
            pantalla.blit(image,[0,0])
        if not self.game_over and not self.ganador  and not pantalla.get_locked():
            try:
                self.listade_todoslos_sprites.draw(pantalla)
            except:
                e = sys.exc_info()[0]
                print(str(e))
        """if not self.game_over and not pantalla.get_locked():
            print("PINTAR"""
        self.btnPtj.pintar()
        self.btnVidas.pintar()
        self.btnTiempo.pintar()
        self.btnPtj2.pintar()
        self.btnVidas2.pintar()
        pygame.display.flip()

def playSound(path):
    pygame.mixer.music.load(path)
    pygame.mixer.music.set_volume(0.3)
    pygame.mixer.music.play()
    
def leerComando(juego, rs):
    def run(juego):
        nave = juego.nave
        ser = rs.ser
        last_received = ""
        while not juego.fin:
            last_received = str(ser.readline().decode("utf-8"))[:2]
            print("*****",last_received,"******")
            if str(last_received)=="B1":
                nave.moverArriba()
            elif str(last_received)=="B2":
                nave.moverDerecha()
            elif str(last_received)=="B3":
                nave.moverAbajo()
            elif str(last_received)=="B4":
                nave.moverIzquierda()
            time.sleep(0.07)
        #ser.close()
    #run(ser)
    threading.Thread(target=run, args=(juego,)).start()
    
def getEle(numFrame, elementos):
    eles = []
    elesOtros = []
    for e in elementos:
        if e["time"]==numFrame:
            eles=eles+[e]
        else:
            elesOtros = elesOtros+[e]
    return (eles, elesOtros)

def crearElemento(juego, key, DIR_APP, ambiente):
    fondo = {
        "Mundo Volcanico":0,
        "Mundo Helado":1,
        "Mundo Tropical":2,
        "Espacio":3
        }
    imgs =[
        [
            DIR_APP+"\\pyRacerSpace\\images\\fireball.png",
            DIR_APP+"\\pyRacerSpace\\images\\copoNieve.png",
            DIR_APP+"\\pyRacerSpace\\images\\tronco.png",
            DIR_APP+"\\pyRacerSpace\\images\\estrella.png"
            ],
        [
            DIR_APP+"\\pyRacerSpace\\images\\lava.png",
            DIR_APP+"\\pyRacerSpace\\images\\cumuloHielo.png",
            DIR_APP+"\\pyRacerSpace\\images\\ramaAb.png",
            DIR_APP+"\\pyRacerSpace\\images\\asteroide.png"
            ],
        [
            DIR_APP+"\\pyRacerSpace\\images\\sol.png",
            DIR_APP+"\\pyRacerSpace\\images\\iceberg.png",
            DIR_APP+"\\pyRacerSpace\\images\\arbol.png",
            DIR_APP+"\\pyRacerSpace\\images\\saturno2.png"
            ],
        DIR_APP+"\\pyRacerSpace\\images\\escudo2.png",
        DIR_APP+"\\pyRacerSpace\\images\\iman.png",
        DIR_APP+"\\pyRacerSpace\\images\\moneda.png"
        ]
    tipos = [
        "OBSTACULO_ARRIBA",
        "OBSTACULO_ABAJO",
        "OBSTACULO_TOTAL",
        "ARMADURA",
        "IMAN",
        "MONEDA"
        ]
    posX = key["posX"]
    objectType = int(key["objectType"])
    print(str(key))
    bloque = None
    fondoAmbiente = fondo[ambiente]
    if objectType<4:
        bloque = Bloque(posX+1, 1,
                        imgs[objectType-1][fondoAmbiente],
                        tipos[objectType-1],
                        {})
    else:
        bloque = Bloque(posX+1, 1,
                        imgs[objectType-1],
                        tipos[objectType-1],
                        {})
    juego.bloque_lista.add(bloque)
    juego.listade_todoslos_sprites.add(bloque)
    
def keyFrameSegundo(elementos):
    keyFrameDict = {}
    for i in range(130):
        eAux = getEle(i, elementos)
        elementos = eAux[1]
        keyFrameDict[i] = eAux[0]
    return keyFrameDict

def ejecutarJuego(DIR_APP, img_fondo,
                  gameSession, tipoJugador,
                  idJugador, rs, ambiente,
                  alias, aliasContra):
    
    
    """Función principal del programa. """
    # Iniciamos Pygame y disponemos la ventana
    dimensiones = [largo_pantalla, alto_pantalla]
    pantalla = pygame.display.set_mode(dimensiones)
    pygame.display.set_caption("JUGANDO")
    pygame.mouse.set_visible(False)
    
                
    juego = Juego(DIR_APP, img_fondo,
                  gameSession, tipoJugador,
                  idJugador, pantalla, alias,
                  aliasContra)
    playSound(DIR_APP+"\\pyRacerSpace\\sonidos\\Your_Voice_Is_American.wav")
    
    
  
    MARCO_SEGUNDO = 60
    medir_segundo = 0
    
    # Crea los objetos y dispone los datos
    hecho = False
    reloj = pygame.time.Clock()
    elementos = estadoJuegoServer(gameSession)["elements"][2:]
    
    listaKeyFrames = keyFrameSegundo(elementos)
    """print(DIR_APP+"\\pyRacerSpace\\data"+str(tipoJugador)+".txt")
    outfile = open(DIR_APP+"\\pyRacerSpace\\data"+str(tipoJugador)+".txt", 'w')
    for key in listaKeyFrames:
        outfile.write(str(key)+": "+str(listaKeyFrames[key])+"\n")
    outfile.close()
    
    for i in listaKeyFrames:
        print(i, ": ",listaKeyFrames[i],"\n")
    """
    # Dibuja el fotograma actual
    def procesoLogicaEventos(j):
        j.logica_de_ejecucion()

    i = 1
    #thread.daemon = True
    hiloRevisarEstado = ThreadServerClient(rs, "", juego).estadoJuego()
    keyFrame = 0
    rs.estadoJuego = "JUGANDO"
    #while
    hiloRevisarEstado.start()
    fin = False
    
    if rs.control == True:
        print(rs)
        leerComando(juego, rs)
    while not juego.game_over and not fin:
        #print(juego.data[:2])
        if (i/0.5)//60 == 129:
            juego.ganador = True
            if juego.data!=[] and juego.data[0]==None and juego.data[1]==None:
                juego.ganadorNombre = "EMPATE"
            elif juego.data!=[] and juego.data[0]==None:
                juego.ganadorNombre = aliasContra
            elif juego.data!=[] and juego.data[1]==None:
                juego.ganadorNombre = alias
            elif juego.data!=[] and juego.data[1]["coins"]==juego.data[0]["coins"]:
                juego.ganadorNombre = "EMPATE"
            elif juego.data!=[] and juego.data[1]["coins"]>juego.data[0]["coins"]:
                juego.ganadorNombre = aliasContra
            elif juego.data!=[]:
                juego.ganadorNombre = alias
            else:
                juego.ganadorNombre = "*******"
        elif i%60==0:
            keyFrame = (i/0.5)//60
            #print("-",keyFrame,": ",listaKeyFrames[int(keyFrame)],"\n")
            for key in listaKeyFrames[int(keyFrame)]:
                crearElemento(juego, key, DIR_APP, ambiente)
            juego.tiempo = i //60
##            print("DATA: ",juego.data[:2])
        elif i%30==0:
            keyFrame = (i/0.5)//60
            #print("*",keyFrame,": ",listaKeyFrames[int(keyFrame)],"\n")
            for key in listaKeyFrames[int(keyFrame)]:
                crearElemento(juego, key, DIR_APP, ambiente)
            juego.tiempo = i //60

        fin = juego.procesos_de_eventos()
        
        if not juego.ganador:
            # Actualiza las posiciones de los objetos y comprueba colisiones
            threading.Thread(target=procesoLogicaEventos, args=(juego,)).start()
        
        juego.display_frame()
        
        # Hace una pausa hasta el siguiente fotograma
        reloj.tick_busy_loop(MARCO_SEGUNDO)
        
        i+=1
    rs.estadoJuego = "TERMINADO"
    pygame.mouse.set_visible(True)
    
    
    
         

