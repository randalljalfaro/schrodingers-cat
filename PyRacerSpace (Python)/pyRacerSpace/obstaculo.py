"""
 Sample Python/Pygame Programs
 Simpson College Computer Science
 http://programarcadegames.com/
 http://simpson.edu/computer-science/
 
 Vídeo explicativo: http://youtu.be/O4Y5KrNgP_c
"""
import threading
import pygame
import random
from pyTEC.boton import *
#--- Constantes Globales ---
NEGRO    = (   0,   0,   0)
BLANCO    = ( 255, 255, 255)
VERDE    = (   0, 255,   0)
ROJO      = ( 255,   0,   0)
 
largo_pantalla  = 1200//2
alto_pantalla = 670
# --- Clases ---
 
class Bloque(pygame.sprite.Sprite):
    """ Este clase representa aun sencillo bloque que es recogido por el protagonista. """
     
    def __init__(self, posAncho, posAltura,
                 ruta_img, tipo, attr):
        """ Constructor; crea la imagen del bloque. """
        pygame.sprite.Sprite.__init__(self) 
        self.image = pygame.image.load(ruta_img)
        self.image = pygame.transform.scale(self.image, (1,1))
        self.rect = self.image.get_rect()
        #self.image.fill((255,0,0))
        #self.image.set_colorkey((255,255,255))
        self.angle = 0
        self.width = 0.1
        self.height = 0.1
        corrimiento = 150
        tipos = [
            "OBSTACULO_ARRIBA",
            "OBSTACULO_ABAJO"
        ]
        if tipos[0]==tipo:
            corrimiento = 0
        elif tipos[1]==tipo:
            corrimiento = 300
        
        self.rect.x = ((largo_pantalla//4)*posAncho)-(largo_pantalla//16)
        self.rect.y = (alto_pantalla//4)*posAltura + corrimiento
        self.y = alto_pantalla//10
        self.ruta_img = ruta_img
        self.posAncho = posAncho
        self.posAltura = posAltura
        self.tipo = tipo
        self.factor = 1
        self.attr = attr
        
    def update(self):
        """ Se le llama automáticamente cuando necesitamos mover el bloque. """
        self.image = pygame.image.load(self.ruta_img)
        #self.image.fill((255,0,0))
        self.image = pygame.transform.rotate(self.image, self.angle)
        self.image = pygame.transform.scale(self.image, (int(self.height),int(self.width)))
        #self.image = pygame.transform.rotozoom(self.image, self.angle, 1 - 1/self.height)
        self.updateValores()
    
    def updateValores(self):
        if alto_pantalla<self.rect.y:
            self.kill()
        else:
            self.rect.y = int(self.y)
        self.rect.height = int(self.height)
        self.rect.width = int(self.width)
        self.angle+=5
        if self.height<200 and self.width<150:
            self.height+= 2.7**self.factor
            self.width+= 2.7**self.factor
            self.factor = 1.3**self.factor
        self.y+=(0.3*self.height)
          
