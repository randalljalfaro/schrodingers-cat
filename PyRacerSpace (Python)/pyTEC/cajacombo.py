from pyTEC.componente import *
from pyTEC.boton import *
from util import *
import pygame
from pygame import *
vacia = lambda l : l==[]

class CajaCombo(Componente):
    
    #   Inicializa las variables internas y configura al CajaCombo
    #   Entrada: Objeto de CajaCombo, Objeto de pantalla de pygame
    def __init__(combo, pantalla, opciones):
        Componente.__init__(combo, pantalla)
        combo.desplegado = False
        combo.elegido = ""
        combo.initSubcomponentes(opciones)
        
    def initSubcomponentes(combo, opciones):
        def nuevoElegido(item):
            combo.desplegado = not combo.desplegado
            combo.elegido=item.texto.valor
            combo.actualizado = True
            
        def f_creaItems(opc, subcomps):
            item = Boton(combo.pantalla)
            item.setTexto(opc)
            item.click = nuevoElegido
            item.setTamano(item.ancho*2, 30)
            item.texto.setTamano(23)
            return subcomps+[item]

        combo.items = Util().foldLeft(opciones, f_creaItems, [])
        if combo.items!=[]:
            btnAux = Boton(combo.pantalla)
            btnAux.setTexto(opciones[0])
            nuevoElegido(btnAux)
            combo.desplegado = False

    def pintarElegido(combo):
        def f_elegido(subcomp, res):
            if res==False and subcomp.texto.valor==combo.elegido:
                return subcomp
            return res
        itemEleg = Util().foldLeft(combo.items, f_elegido, False)
        itemEleg.setPosicion(combo.x, combo.y+5)
        itemEleg.setTamano(combo.ancho, 30)
        combo.setSubcomponentes([itemEleg])
        combo.subcomps[0].pintar()

    def pintarPliegue(combo):
        combo.setSubcomponentes(combo.items)
        
        def f_filtro(subcomp, opcPliegue):
            if subcomp.texto.valor == combo.elegido:
                return opcPliegue
            return opcPliegue+[subcomp]
        opcsPliegue = Util().foldLeft(combo.items, f_filtro, [])
        
        def f_pintar(subcomp, nivel):
            h = subcomp.altura
            subcomp.setTamano(combo.ancho, 30)
            subcomp.setPosicion(combo.x, combo.y+h*nivel+5)
            subcomp.pintar()
            return nivel+1
        Util().foldLeft(opcsPliegue, f_pintar, 1)

    def pintarVacia(combo):
        btnVacio = Boton(combo.pantalla)
        btnVacio.setTexto("--Vacía--")
        btnVacio.setPosicion(combo.x, combo.y)
        btnVacio.setTamano(combo.ancho, 30)
        btnVacio.pintar()
        
    def pintar(combo):
        if combo.desplegado:
            combo.pintarElegido()
            combo.pintarPliegue()
        elif combo.items!=[]:
            combo.pintarElegido()
        else:
            combo.pintarVacia()
        
   
