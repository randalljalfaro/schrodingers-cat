from pyTEC.componente import *
import os
#clase de la configuracion de botones
class Boton(Componente):
    rutaImg = os.path.dirname(os.path.realpath(__file__))+"\\botones\\botonGris.png"
    #   Configura e inicializa el botón
    #   Entrada: Objeto del botón, Objeto de pantalla de pygame
    def __init__(btn, pantalla):
        Componente.__init__(btn, pantalla)
        btn.fondo.setImagen(btn.rutaImg)
        btn.setTamano(200, 50)
        btn.fondo.setBorde(5, 8)
