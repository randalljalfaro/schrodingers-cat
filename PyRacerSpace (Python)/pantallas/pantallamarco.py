import pygame
from pyTEC.boton import *
from pyTEC.etiqueta import *
from pyTEC.cajacombo import *
from util import *
from pantallas.pantalla import *

class PantallaConfigPartida(Pantalla):
    #   Configura e inicializa la pantalla del marco
    #   Entrada: Objeto de la pantalla, Objeto pantalla de pygame,
    #       ancho, altura, ruta para imágen de fondo
    def __init__(p, pantalla, ancho, altura, rutaImg, ruta1, ruta2, ruta3, alias):
        #FUuuuuuusión de Pantalla y PantallaOpcionesMarco
        Pantalla.__init__(p, pantalla, ancho, altura, rutaImg)
        p.ruta1 = ruta1
        p.ruta2 = ruta2
        p.ruta3 = ruta3
        p.rutaImg = rutaImg
        p.alias = alias
        p.initComponentes()
        
    #   Inicializa los componentes de la aplicación que van
    #   a estar en pantalla de opciones de marco
    #   Entrada: Objeto de la pantalla de opciones de marco
    def initComponentes(p):
        lineax1 = 70
        lineax2 = 500
        lineax3 = 1000
        lineay1 = 50
        lineay2 = 150
        lineay3 = 250
        lineay5 = 550
        p.btnAtras = Boton(p.pantalla)
        p.btnEmpezarPartida = Boton(p.pantalla)

        p.btnAtras.setPosicion(lineax1,lineay1)
        p.btnEmpezarPartida.setPosicion(lineax1,lineay2)
        
        p.btnAtras.setTamano(150,40)
        p.btnEmpezarPartida.setTamano(150,40)

        items = ["Espacio","Mundo Volcanico",
                 "Mundo Helado","Mundo Tropical"]
        p.comboEscenarios = CajaCombo(p.pantalla, items)
        p.comboEscenarios.setPosicion(lineax2,lineay3+50)
        
        p.btnAtras.setTexto("Atrás")
        p.btnEmpezarPartida.setTexto("Empezar Partida")

        p.btnAtras.texto.setTamano(30)
        p.btnEmpezarPartida.texto.setTamano(30)

        p.etqID = Etiqueta(p.pantalla)
        p.etqID.setTexto("ID de partida")
        p.etqID.texto.setTamano(30)
        p.etqID.texto.setColor((50,250,250))
        p.etqID.setPosicion(lineax3-60,lineay1-30)
        
        p.btnID = Boton(p.pantalla)
        p.btnID.setPosicion(lineax3-120,lineay1)
        p.btnID.setTamano(200,30)

        p.etqAlias = Etiqueta(p.pantalla)
        p.etqAlias.setTexto("Bienvenido "+p.alias)
        p.etqAlias.texto.setTamano(30)
        p.etqAlias.texto.setColor((50,250,250))
        p.etqAlias.setPosicion(lineax2,lineay1)

        p.etqUsuarioInv = Etiqueta(p.pantalla)
        p.etqUsuarioInv.setTexto("Usuario invitado")
        p.etqUsuarioInv.texto.setTamano(30)
        p.etqUsuarioInv.texto.setColor((50,250,250))
        p.etqUsuarioInv.setPosicion(lineax3-60,lineay1+40)
        
        p.btnUsuarioInv = Boton(p.pantalla)
        p.btnUsuarioInv.setPosicion(lineax3-120,lineay1+70)
        p.btnUsuarioInv.setTamano(200,30)
        p.btnUsuarioInv.setTexto("Esperando...")
        
        p.etqEscenario = Etiqueta(p.pantalla)
        p.etqEscenario.setTexto("Escoge un escenario")
        p.etqEscenario.texto.setTamano(30)
        p.etqEscenario.texto.setColor((50,250,250))
        p.etqEscenario.setPosicion(lineax2,lineay3)

        p.btnControl = Boton(p.pantalla)
        p.btnControl.setPosicion(lineax2,lineay3+200)
        p.btnControl.setColor((200,10,10))
        p.btnControl.setTamano(200,30)
        p.btnControl.setTexto("Control apagado")

        p.btnSalir = Boton(p.pantalla)
        p.btnSalir.setPosicion(lineax3-50,lineay5)
        p.btnSalir.setTamano(150,40)
        p.btnSalir.texto.setTamano(30)
        p.btnSalir.setTexto("   Salir")
        
        p.componentes = [p.btnAtras, p.btnEmpezarPartida,
                         p.btnID, p.etqID,p.btnSalir,
                         p.etqUsuarioInv, p.btnUsuarioInv,
                         p.etqEscenario, p.comboEscenarios,
                         p.etqAlias, p.btnControl]
        
    def cambiarFondo(p,escenario):
        if escenario == "Espacio":
            p.setImagenFondo(p.rutaImg)
        elif escenario=="Mundo Volcanico":
            p.setImagenFondo(p.ruta1)
        elif escenario=="Mundo Helado":
            p.setImagenFondo(p.ruta2)
        elif escenario=="Mundo Tropical":
            p.setImagenFondo(p.ruta3)
        
        
        
    #   Pinta subcomponentes y fondo de la Pantalla
    def pintar(p):
        if p.comboEscenarios.actualizado:
            p.cambiarFondo(p.comboEscenarios.elegido)
            p.comboEscenarios.actualizado = False
        p.pintarSinComponentes()
        super().pintarComps()

            
    #   Pinta el marco sin botones, para cuando se debe guardar el marco
    def pintarSinComponentes(p):
        pygame.display.set_mode((p.ancho,p.altura))
        super().pintarFondo()
        
