import pygame, util
from pyTEC.boton import *
from pyTEC.etiqueta import *
from pyTEC.componente import *
from util import *
from pantallas.pantalla import *

class PantallaOpcionesMarco(Pantalla):
    #   Configura e inicializa la pantalla de opciones de marco
    #   Entrada: Objeto de la pantalla, Objeto pantalla de pygame,
    #       ancho, altura, ruta para imágen de fondo
    def __init__(p, pantalla, ancho, altura, rutaImg):
        #FUuuuuuusión de Pantalla y PantallaOpcionesMarco
        Pantalla.__init__(p, pantalla, ancho, altura,rutaImg)
        p.initComponentes()

    #   Inicializa los componentes de la aplicación que van
    #   a estar en pantalla de opciones de marco
    #   Entrada: Objeto de la pantalla de opciones de marco
    def initComponentes(p):
        util = Util()
        lineax1 = 70
        lineax2 = 1000
        lineax3 = 400
        lineay1 = 50
        lineay2 = 150
        lineay3 = 255
        lineay4 = 320
        lineay5 = 550

        p.etqNombre = Etiqueta(p.pantalla)
        p.etqNombre.texto.setTamano(30)
        p.etqNombre.texto.setColor((50,50,250))
        p.etqNombre.setTexto("Ingresa tu Alias")
        p.etqNombre.setPosicion(lineax2,lineay1+50)

        p.btnCrearPartida = Boton(p.pantalla)
        p.btnCrearPartida.setPosicion(lineax1,lineay1+100)
        p.btnCrearPartida.setTexto(" Crear Partida")
        p.btnCrearPartida.setTamano(150,40)

        p.btnUnirse = Boton(p.pantalla)
        p.btnUnirse.setPosicion(lineax1,lineay2+100)
        p.btnUnirse.setTexto(" Unir a Partida")

        p.btnAlias = Boton(p.pantalla)
        p.btnAlias.setPosicion(lineax2-150,lineay1+90)
        p.btnAlias.setTamano(330,30)
        p.btnAlias.editable = True

        p.btnSalir = Boton(p.pantalla)
        p.btnSalir.setPosicion(lineax2,lineay5+50)
        p.btnSalir.setTamano(150,40)
        p.btnSalir.texto.setTamano(30)
        p.btnSalir.setTexto("      Salir")

        #p.etqGame = Etiqueta(p.pantalla)
        #p.etqGame.texto.setTamano(80)
        #p.etqGame.texto.setColor((50,250,250))
        #p.etqGame.setTexto("R A C E R S   S P A C E")
        #p.etqGame.setPosicion(lineax3,lineay1)
        #print(pygame.font.get_fonts())

        
        p.recargarComponentes()
    
    
    def recargarComponentes(p):
        p.componentes = [p.btnCrearPartida,p.btnSalir,
                         p.btnUnirse,p.etqNombre, p.btnAlias]
        


    
