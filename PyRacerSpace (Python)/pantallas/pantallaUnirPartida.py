import pygame
from pyTEC.boton import *
from pyTEC.etiqueta import *
from util import *
from pantallas.pantalla import *

class PantallaUnir(Pantalla):
    def __init__(p, pantalla, ancho, altura, rutaImg, alias):
        Pantalla.__init__(p, pantalla, ancho, altura, rutaImg)
        p.alias = alias
        p.initComponentes()

    def initComponentes(p):
        lineax1 = 70
        lineax2 = 1000
        lineax3 = 500
        lineay1 = 50
        lineay2 = 150
        lineay3 = 255
        lineay4 = 320
        lineay5 = 600

        p.btnSalir = Boton(p.pantalla)
        p.btnSalir.setPosicion(lineax2,lineay5)
        p.btnSalir.setTamano(150,40)
        p.btnSalir.texto.setTamano(30)
        p.btnSalir.setTexto("      Salir")

        p.btnAtras = Boton(p.pantalla)
        p.btnAtras.setPosicion(lineax1,lineay1)
        p.btnAtras.setTamano(150,40)
        p.btnAtras.setTexto("      Atrás")
        p.btnAtras.texto.setTamano(30)
        
        p.etqID = Etiqueta(p.pantalla)
        p.etqID.texto.setTamano(30)
        p.etqID.texto.setColor((0,0,150))
        p.etqID.setTexto("Ingrese el ID de la partida")
        p.etqID.setPosicion(lineax3+30,lineay3)

        p.etqERROR = Etiqueta(p.pantalla)
        p.etqERROR.texto.setTamano(30)
        p.etqERROR.texto.setColor((0,0,150))
        p.etqERROR.setPosicion(lineax3+30,lineay3+150)

        p.btnID = Boton(p.pantalla)
        p.btnID.setPosicion(lineax3+30,lineay3+50)
        p.btnID.setTamano(300,40)
        p.btnID.editable = True

        p.etqAlias = Etiqueta(p.pantalla)
        p.etqAlias.setPosicion(lineax3,lineay1)
        p.etqAlias.texto.setColor((50,250,250))
        p.etqAlias.texto.setTamano(30)
        p.etqAlias.setTexto("Bienvenido "+p.alias)
        

        p.btnUnirse = Boton(p.pantalla)
        p.btnUnirse.setPosicion(lineax3+50,lineay4+50)
        p.btnUnirse.setTamano(200,30)
        p.btnUnirse.setTexto(" Unirse a la partida")

        p.componentes = [p.etqID, p.btnID,p.btnUnirse,
                         p.btnAtras, p.btnSalir, p.etqAlias,
                         p.etqERROR]


    def pintar(p):
        p.pintarSinComponentes()
        super().pintarComps()



    def pintarSinComponentes(p):
        pygame.display.set_mode((p.ancho,p.altura))
        super().pintarFondo()
