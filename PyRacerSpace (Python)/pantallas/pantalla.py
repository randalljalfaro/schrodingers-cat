from util import *
import pygame
vacia = lambda l : l==[]
class Pantalla:
    #   Configura e inicializa la pantalla de opciones de marco
    #   Entrada: Objeto de la pantalla, Objeto pantalla de pygame,
    #       ancho, altura, ruta para imágen de fondo
    def __init__(p, pantalla, ancho, altura, rutaImg):
        p.pantalla = pantalla
        p.setTamano(ancho, altura)
        p.setImagenFondo(rutaImg)
        p.setComponentes([])

    #   Establece el ancho y altura de la pantalla
    #   Entrada: Objeto de pantalla, ancho, altura
    def setTamano(p, ancho, altura):
        p.ancho = ancho
        p.altura = altura
        p.actualizado = True

    #   Establece cuales son los componentes visibles de la pantalla
    #   Entrada: Objeto de pantalla
    def setComponentes(p, comps):
        p.componentes = comps

    #   Establece la ruta de la imágen de fondo,ç
    #   carga la imágen y la redimensiona
    #   Entrada: Objeto de pantalla
    def setImagenFondo(p, ruta):
        p.rutaFondo = ruta
        if ruta != "":
            imagen = pygame.image.load(ruta)
            p.imagen = pygame.transform.scale(imagen,(p.ancho, p.altura))
            p.img_rect = p.imagen.get_rect()

    def setColorFondo(p, color):
        p.rutaFondo = ruta
        if p.ruta != "":
            imagen = pygame.image.load(ruta)
            p.imagen = pygame.transform.scale(imagen,(p.ancho, p.altura))
            p.img_rect = p.imagen.get_rect()

    #   Llama a las funciónes que pintan el fondo de la pantalla
    #   y los componentes visibles
    #   Entrada: Objeto de pantalla
    def pintar(p):
        pygame.display.set_mode((p.ancho,p.altura))
        p.pintarFondo()
        p.pintarComps()

    #   Pinta la imágen de fondo en la pantalla
    #   Entrada: Objeto de pantalla
    def pintarFondo(p):
        if p.rutaFondo!="" and p.actualizado:
            p.imagen = pygame.transform.scale(p.imagen,(p.ancho, p.altura))
            p.img_rect = p.imagen.get_rect()
            p.actualizado = False
        if p.rutaFondo!="":
             p.pantalla.blit(p.imagen, p.img_rect)

    #   Pinta los componentes visibles de la pantalla
    #   Entrada: Objeto de pantalla
    def pintarComps(p):
        Util().paraTodos(p.componentes, lambda c: c.pintar())
        
