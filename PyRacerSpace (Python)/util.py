vacia = lambda l: l==[]
vaciaL = vacia
vaciaC = lambda c: c=="" 
esLista = lambda l: type(l)==list
esCadena = lambda c:type(c)==str
BLANCO = (255,255,255)
class Util:
    #funcion que cuenta los elementos de la lista o cadena de entrada
    #entradas: una lista
    #salidas: cantidad de elementos de la lista
    def largoArreglo(util, arreglo):
        if vaciaL(arreglo) or vaciaC(arreglo):
            return 0
        return 1 + largoArreglo(arreglo[1:])
    #funcion que evalua el tipo de objeto y lo envia a largoArreglo si objeto es
    #una lista o una cadena
    #entradas:un objeto
    #salidas: si el objeto es una lista o una cadena lo envia a largoArreglo
    def largo(util, objeto):
        if esLista(objeto) or esCadena(objeto):
            return util.largoArreglo(objeto)
        return -1
    #funcion que evalua los elementos comps y valores de acuerdo a la funcionde entrada
    #entradas: dos listas y una funcion
    #salidas: las listas evaluadas por medio de la funcion de entrada
    def modificar(util, comps, valores, f):
        if not vacia(comps) and not vacia(valores):
            f(comps[0], valores[0])
            util.modificar(comps[1:], valores[1:], f)

    def buscar(util, lista, f, criterio):
        if not vacia(lista):
            if f(lista[0], criterio):
                return lista[0]
            return util.buscar(lista[1:], f, criterio)
        return False
    
    def filtrar(util, lista, f, todos):
        if not vacia(lista):
            if f(lista[0]) and todos:
                return util.filtrar(util, lista[1:], f, todos)
            elif f(lista[0]):
                return lista[1:]
            return [lista[0]]+util.filtrar(lista[1:], f, todos)
        return []

    def paraTodos(util, lista, f):
         if not vacia(lista):
             f(lista[0])
             return util.paraTodos(lista[1:], f)

    def esta(util, lista, elem):
        if not vacia(lista)and lista[0]!=elem:
            return util.esta(lista[1:], elem)
        elif not vacia(lista):
            return True
        return False
    
    def foldLeft(util,lista, f, resultado):
        if lista!=[]:
            return util.foldLeft(lista[1:], f, f(lista[0], resultado))
        return resultado
            
