import pygame, os
from pygame import *
from manejadordeeventos import *
from pantallas.pantallaprincipal import *
from pantallas.pantallamarco import *
from pantallas.pantallaUnirPartida import *
from pyRacerSpace.juego import *
from pyRacerSpace.conexion import *
import threading
import time
x = 20
y = 40
os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (x,y)
path = os.path.dirname(os.path.realpath(__file__))
#*************************************************************************
#   Clase de la configuración de la aplicación Frameaker
class ConfigRS:
    FOTOGPORSEG = 10
    ALTURA = 670
    ANCHO = 1200
    FIN = False
    TITULO = "PyRacers Space"
    FONDO = BLANCO
    DIR_APP = os.path.dirname(os.path.realpath(__file__))
    Espacio = DIR_APP+'\\images\\universo HD.jpg'
    mundoVolcanico = DIR_APP+'\\images\\volcanes.jpeg'
    mundoHelado = DIR_APP+'\\images\\hielonave.jpg'
    mundoTropical = DIR_APP+'\\images\\Tropical.jpg'
    asteroides = DIR_APP+'\\images\\asteroides.jpg'
    nave = DIR_APP+'\\images\\nave.jpg'
    navespacial = DIR_APP+'\\images\\navespacial.png'
    titulo = DIR_APP+'\\images\\Imagen1.png'
    
    
    #Se encarga de configurar los parametros con que inicia la aplicación
    def __init__(config, rs):
        pygame.display.set_caption(config.TITULO)
        rs.pantalla = pygame.display.set_mode([config.ANCHO,config.ALTURA])
        rs.pantalla.fill(BLANCO)
        pygame.display.set_icon(pygame.image.load(config.navespacial))
        #Reloj para actualizar pantalla
        rs.reloj = pygame.time.Clock()

#*************************************************************************
#   Clase de la aplicación principal
class PyRacersSpace(ManejadorEventos):
    #   Función que inicializa la configuración de la aplicación
    #   Entrada: Objeto del manejador de eventos
    def __init__(rs):
        #Inicia la biblioteca
        pygame.init()
        pygame.mixer.init()
        
        #Fuuuuusión de Manejador de eventos y app 
        ManejadorEventos.__init__(rs)
        
        #Configuración de la aplicación
        rs.config = ConfigRS(rs)
        rs.estadoJuego=""
        rs.alias = ""
        rs.aliasContra = None
        rs.gameSession = 0
        rs.tipoJugador = 0
        try:
            rs.ser = Serial(
                port="COM8",
                baudrate=9600,
                timeout=1
            )
        except:
            rs.msj = "No hay dispositivo en el COM8"
            print(rs.msj)
        
        rs.pantallaPrincipal = PantallaOpcionesMarco(
            rs.pantalla, rs.config.ANCHO,
            rs.config.ALTURA, rs.config.Espacio)
        
        #PantallaPrincipal inicia por default
        rs.activarPantallaPrincipal()
        pygame.display.set_mode((rs.config.ANCHO,rs.config.ALTURA))

        def eventoSalir(pantallaOpcionesMarco):
            rs.config.FIN = True
        rs.pantallaPrincipal.btnSalir.click = eventoSalir

        rs.irPrincipal = lambda btnAtras: rs.activarPantallaPrincipal()
        playSound(rs.config.DIR_APP+"\\pyRacerSpace\\sonidos\\Your_Voice_Is_American.wav")

    #   Función principal que se mantiene ejecutando
    #   para revisar eventos, responder a ellos
    #   y además actualizar la pantalla
    #   Entrada: Objeto del manejador de eventos
    
    def ejecutar(rs):
        while not rs.config.FIN:
            rs.manejaEventos()
            if rs.pantallaActivada == "PANTALLA_PRINCIPAL":
                rs.comps = rs.pantallaPrincipal.componentes
                rs.imagen = pygame.image.load(rs.config.titulo)
                rs.pantallaPrincipal.pintar()
                rs.pantalla.blit(rs.imagen,(190,-50))
            elif rs.pantallaActivada == "PANTALLA_CONFIG_PARTIDA":
                rs.comps = rs.PantallaConfigPartida.componentes
                rs.PantallaConfigPartida.pintar()
            elif rs.pantallaActivada == "PANTALLA_UNIRSE_PARTIDA":
                rs.comps = rs.pantallaUnir.componentes
                rs.pantallaUnir.pintar()
            elif rs.pantallaActivada=="PANTALLA_JUEGO":
                rs.empezarJuego()
            pygame.display.update()
            rs.reloj.tick(rs.config.FOTOGPORSEG)
        pygame.quit()
        rs.ser.close()

        
    #   Activa la pantallla principal y registra la
    #   función para cargar una imágen
    #   Entrada: Objeto del manejador de eventos
    def activarPantallaPrincipal(rs):
        pPrin = rs.pantallaPrincipal
        #crearPartidaServer
        rs.pantallaActivada = "PANTALLA_PRINCIPAL"
        
        def unirPartida(btnUnirse):
            alias = pPrin.btnAlias.texto.valor
            if alias!="":
                rs.tipoJugador = 0
                rs.alias = alias
                rs.activarPantallaUnir(alias)
        pPrin.btnUnirse.click = unirPartida

        def crearPartida(btnCrearPartida):
            alias = pPrin.btnAlias.texto.valor
            if alias!="":
                data = crearPartidaServer(alias)
                rs.alias = alias
                rs.tipoJugador = 1
                rs.gameSession = int(data["gameSession"])
                rs.idJugador = int(data["id"])
                #print(data)
                rs.activarPantallaConfig(True,alias)
        pPrin.btnCrearPartida.click = crearPartida


    def activarPantallaUnir(rs,alias):
        pPrin = rs.pantallaPrincipal
        rs.pantallaUnir = PantallaUnir(
            rs.pantalla, rs.config.ANCHO,
            rs.config.ALTURA, rs.config.nave,alias)
        pUnir = rs.pantallaUnir 
        
        rs.pantallaActivada ="PANTALLA_UNIRSE_PARTIDA"
        pUnir.btnAtras.click = rs.irPrincipal

        def unirseAPartida(btnUnirse):
            idUnirPart = pUnir.btnID.texto.valor
            if idUnirPart!="":
                try:
                    data = unirseAPartidaServer(rs.alias, int(idUnirPart))
                    if data["message"]!="Busy":
                        print(data)
                        rs.gameSession = int(idUnirPart)
                        rs.idJugador = int(data["id"])
                        rs.activarPantallaConfig(False, rs.alias)
                        pConfig = rs.PantallaConfigPartida
                        pConfig.btnUsuarioInv.texto.valor = str(data["otherPlayers"][0]["name"])
                        print("unirseAPartida: ",data)
                    else:
                        pUnir.etqERROR.setTexto("Sesión ocupada")
                except:
                    pUnir.etqERROR.setTexto("Sesión inválida")
        pUnir.btnUnirse.click = unirseAPartida
        
        def eventoSalir(pantallaOpcionesMarco):
            rs.config.FIN = True
        pUnir.btnSalir.click = eventoSalir

 
    def activarPantallaConfig(rs, anfitrion,alias):
        pPrin = rs.pantallaPrincipal
        rs.anfitrion = anfitrion
        rs.estadoJuego = "ESPERANDO_UNION"
        rs.PantallaConfigPartida = PantallaConfigPartida(
            rs.pantalla, rs.config.ANCHO,
            rs.config.ALTURA, rs.config.asteroides,
            rs.config.mundoVolcanico,rs.config.mundoHelado,
            rs.config.mundoTropical,alias)
        pConf = rs.PantallaConfigPartida 
        pConf.btnID.texto.valor = str(rs.gameSession)
        rs.pantallaActivada ="PANTALLA_CONFIG_PARTIDA"
        pConf.btnAtras.click = rs.irPrincipal

        global encender_apagar
        encender_apagar = "N"
        rs.control = False
        def encenderControl(btnControl):
            global encender_apagar
            ser = rs.ser
            print(ser)
            if encender_apagar == "N":
                encender_apagar = "S"
                for i in range(10):
                    ser.write(str(encender_apagar).encode())#)
                    time.sleep(0.1)
                btnControl.setColor((10,200,10))
                btnControl.texto.valor = "Control encendido"
                rs.control = True
            else:
                encender_apagar = "N"
                for i in range(10):
                    ser.write(str(encender_apagar).encode())#)
                    time.sleep(0.1)
                btnControl.setColor((200,10,10))
                btnControl.texto.valor = "Control apagado"
                rs.control = False
        pConf.btnControl.click = encenderControl
        
        def eventoSalir(pantallaOpcionesMarco):
            rs.config.FIN = True
        pConf.btnSalir.click = eventoSalir
        
        if anfitrion:
            hiloCompUnion = ThreadServerClient(rs, "", False).comprobarUnion()
            hiloCompUnion.start()
            pConf.btnEmpezarPartida.click = rs.empezarJuego
        else:
            btnEmpezar= pConf.btnEmpezarPartida
            btnEmpezar.texto.valor = "Esperando inicio de partida"
            btnEmpezar.pintar()
            def run(): 
                while estadoJuegoServer(rs.gameSession)["elements"][2:]==[]:
                    time.sleep(0.1)
                rs.pantallaActivada="PANTALLA_JUEGO"
            threading.Thread(target=run).start()

    def empezarJuego(rs, btnEmpezarPartida=None):
        pConf = rs.PantallaConfigPartida 
        fondo = {
            "Mundo Volcanico":rs.config.mundoVolcanico,
            "Mundo Helado":rs.config.mundoHelado,
            "Mundo Tropical":rs.config.mundoTropical,
            "Espacio":rs.config.asteroides
            }
        img_fondo = fondo[pConf.comboEscenarios.elegido]
        if rs.anfitrion:
            empezarPartidaServer(rs.gameSession)
        ejecutarJuego(rs.config.DIR_APP, img_fondo,
                      rs.gameSession, rs.tipoJugador,
                      rs.idJugador, rs,
                      pConf.comboEscenarios.elegido,
                      rs.alias, rs.aliasContra)
        
def playSound(path):
    pygame.mixer.music.load(path)
    pygame.mixer.music.set_volume(0.3)
    pygame.mixer.music.play()
    
RacerSpace = PyRacersSpace()
RacerSpace.ejecutar()
