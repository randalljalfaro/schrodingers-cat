import pygame, os
from pygame import *
class ManejadorDeMarcos:
    
    
    #entradas:la variable para la pantalla
    def __init__(mm, pantalla, pantallaMarco):
        mm.pantalla = pantalla
        mm.pantallaMarco = pantallaMarco
        mm.coordenada = (0,0)
        mm.DIR_ABS = os.path.dirname(os.path.realpath(__file__))
        mm.colores = {
            "Rojo":(255,0,0),
            "Verde":(0,255,0),
            "Azul":(0,0,255),
            "Amarillo":(255,255,0),
            "Morado":(74,35,100),
            "Celeste":(12,183,242),
        }
        mm.imgFiguras = {"Cuadrado":"\\figuras\\cuadrado.png",
                        "Triángulo":"\\figuras\\triangulo.png",
                        "Rectángulo":"\\figuras\\rectangulo.png",
                        "Rombo":"\\figuras\\rombo.png",
                        "Hexágono":"\\figuras\\hexagono.png",
                        "Círculo":"\\figuras\\circulo.png",
                        "Estrella":"\\figuras\\estrella1.png"}

    def pintar(mm):
        print(mm.figura)
        if mm.figura=="Cuadrado" or mm.figura=="Rectángulo" :
            pygame.draw.polygon(mm.pantalla,mm.color,mm.puntos,mm.borde)
        elif mm.figura=="Círculo":
            img_rect = mm.imgFigura.get_rect()
            mm.pantalla.blit(mm.imgFigura, img_rect)
            pygame.draw.circle(mm.pantalla,mm.color, mm.puntos,
                               mm.radio, mm.borde)
        else:
            img_rect = mm.imgFigura.get_rect()
            mm.pantalla.blit(mm.imgFigura, img_rect)
            pygame.draw.polygon(mm.pantalla,mm.color,mm.puntos,mm.borde)
            
            

    #funciones donde creamos cada figura de acuerdo a lo elegido en la
    #opcion anterior
    #salidas:la figura exacta de lo elegido en la funcion anterior
    def Triangulo(mm, base, altura, color, borde):
        mm.figura="Triángulo"
        mm.borde = borde
        mm.color = mm.colores[color]
        mm.puntos =[[base/2,0],[0,altura],[base,altura]]
        #Marco
        mm.imgFigura = pygame.image.load(mm.DIR_ABS+mm.imgFiguras[mm.figura])
        mm.imgFigura = pygame.transform.scale(mm.imgFigura,(base, altura))
        mm.pintar()
        
    def Cuadrado(mm, lado, color, borde):
        mm.figura="Cuadrado"
        mm.borde = borde
        mm.color = mm.colores[color]
        mm.puntos = [[0,0],[lado,0],[lado,lado],[0,lado]]
        mm.pintar()
        
    def Rectangulo(mm, base, altura, color, borde):
        mm.figura="Rectángulo"
        mm.borde = borde
        mm.color = mm.colores[color]
        mm.puntos = [[0,0],[base,0],[base,altura],[0,altura]]
        mm.pintar()
        
    def Rombo(mm, Diagonal, diagonal, color, borde):
        mm.figura="Rombo"
        mm.borde = borde
        mm.color = mm.colores[color]
        mm.puntos = [[0,Diagonal/2],[diagonal/2,0],
                     [diagonal,Diagonal/2],[diagonal/2,Diagonal]]
        #Marco
        mm.imgFigura = pygame.image.load(mm.DIR_ABS+mm.imgFiguras[mm.figura])
        mm.imgFigura = pygame.transform.scale(mm.imgFigura,(diagonal, Diagonal))
        mm.pintar()
        
    def Hexagono(mm,lado,color,borde):
        mm.figura="Hexágono"
        mm.borde = borde
        mm.color = mm.colores[color]
        mm.puntos = [[0,lado/4],[0,lado/2+lado/4],[lado/2,lado],
                     [lado,lado/2+lado/4],[lado,lado/2-lado/4],[lado/2,0]]
        #Marco
        mm.imgFigura = pygame.image.load(mm.DIR_ABS+mm.imgFiguras[mm.figura])
        mm.imgFigura = pygame.transform.scale(mm.imgFigura,(lado, lado))
        mm.pintar()
        
        
    def Circulo(mm,radio,color,borde):
        print("*", radio)
        mm.figura="Círculo"
        mm.borde = borde
        mm.color = mm.colores[color]
        mm.radio = radio//2
        mm.puntos = (int(radio/2),int(radio/2))
        #Marco
        mm.imgFigura = pygame.image.load(mm.DIR_ABS+mm.imgFiguras[mm.figura])
        mm.imgFigura = pygame.transform.scale(mm.imgFigura,(radio, radio))
        print("*", radio)
        mm.pintar()
        

    def Estrella(mm,lado,color,borde):
        mm.figura="Estrella"
        mm.borde = borde
        mm.color = mm.colores[color]
        mm.puntos = [[lado/2,0],[lado/2+lado/8.5,lado/2-lado/8.5],
                     [lado,lado/2-lado/9],[lado/2+lado/5,lado/2+lado/8],
                     [lado/2+lado/3.25,lado],[lado/2,lado/2+lado/3.5],
                     [lado/2-lado/3.25,lado/2+lado/2],[lado/2-lado/5,lado/2+lado/8],
                     [0,lado/2-lado/9],
                     [lado/2-lado/8.5,lado/2-lado/8.5]]
        #Marco
        mm.imgFigura = pygame.image.load(mm.DIR_ABS+mm.imgFiguras[mm.figura])
        mm.imgFigura = pygame.transform.scale(mm.imgFigura,(lado, lado))
        mm.pintar()
    
