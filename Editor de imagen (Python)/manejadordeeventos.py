import pygame
from pygame import *
vacia = lambda l: l==[]

#clase de la configuracion de los eventos
class ManejadorEventos:
    def __init__(me):
        me = me

    #   Pide a pygame lista de eventos,
    #   luego la envía a revisar por el ManejadorEventos
    #   Entradas: Objeto del manejador de eventos
    def manejaEventos(me):
        me.revisarEventos(pygame.event.get())

    #   Recorre una lista de eventos
    #   Por ahora se le da importancia a:
    #       .click del mouse
    #       .cerrado del programa
    #   Entradas: Objeto del manejador de eventos, Lista de eventos
    def revisarEventos(me, eventos):
        if not vacia(eventos):
            evento = eventos[-1]
            #print(evento.type)
            if evento.type == pygame.MOUSEBUTTONDOWN:
                #print(evento.pos)
                me.enClick(evento.pos, me.comps)
            elif evento.type == pygame.QUIT:
                me.config.FIN = True
            return me.revisarEventos(eventos[:-1])

    #   Revisa en cual de los componentes se dio el click
    #   y si encuenta algun componente en esas coordenadas y
    #   si ese componente tiene registrada una función click, la ejecuta
    #   Entradas:   .Objeto del manejador de eventos
    #               .Tupla con las coordenas en que se dio el click
    #               .Lista de componentes que tenga la app
    def enClick(fm, pos, comps):
        if not vacia(comps):
            c = comps[-1]
            x = pos[0]
            y = pos[1]
            #print(c.x ," - ", c.y, " * ",c.ancho ," - ", c.altura, " ** ", pos)
            estaDentroX = (c.x <= x <= (c.x + c.ancho))
            estaDentroY = (c.y <= y <= (c.y + c.altura))

            #Se le da prioridad a los subcomponentes
            #print(c.subcomps)
            #print(c)
            #print(c.subcomps)
            fm.enClick(pos, c.subcomps)

            if estaDentroX and estaDentroY:
                ejecucion = False
                try:
                    c.click(c)
                except AttributeError:
                    print("Componente no tiene eventos registrados")
            fm.enClick(pos, comps[:-1])
