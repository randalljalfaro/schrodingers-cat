from util import *
import pygame
from pygame import *

class Componente:
    #   Inicializa las variables internas y configura al componentes
    #   Entrada: Objeto de pantalla de pygame
    def __init__(comp, pantalla):
        comp.pantalla = pantalla
        #Configuración por default
        comp.setTamano(200, 20)
        comp.setPosicion(0, 0)
        comp.texto = Texto(comp)
        comp.fondo = Fondo(comp)
        comp.setColor((160,160,160))
        comp.texto.setPosicion(8, 2)
        comp.setSubcomponentes([])
        comp.setFondo(True)

    #   Establece cuales son los subcomponentes
    #   Entrada:    .Objeto del componente
    #               .Lista con componentes
    def setSubcomponentes(comp, subcomps):
        comp.subcomps = subcomps

    #   Establece la altura y el ancho
    #   Entrada:    .Objeto del componente
    #               .Altura .Ancho
    def setTamano(comp, ancho, altura):
        comp.ancho = ancho
        comp.altura = altura

    #   Establece la posición en el eje x y el y
    #   Entrada:    .Objeto del componente
    #               .Posición en x .Posición en y    
    def setPosicion(comp, x, y):
        comp.x = x
        comp.y = y

    #   Establece el color de fondo
    #   Entrada:    .Objeto del componente
    #               .Color
    def setColor(comp, color):
        comp.fondo.setColor(color)

    #   Establece el texto que aparecerá sobre el componente
    #   Entrada:    .Objeto del componente
    #               .Texto
    def setTexto(comp, texto):
        comp.texto.setValor(texto)

    #   Pinta al componente (fondo, texto)
    #   Entrada:    .Objeto del componente
    def pintar(comp):
        if comp.pintaFondo:
            comp.fondo.pintar()
        comp.texto.pintar()

    #   Establece si el fondo se debe pintar
    #   Entrada:    .Objeto del componente
    #               .Booleano  
    def setFondo(comp, activo):
        comp.pintaFondo = activo

   



#****************************************************************************
class Texto:
    #   Inicializa las variables internas y configura al texto
    #   Entrada: Objeto de texto, Componente
    def __init__(txt, comp):
        txt.comp = comp
        txt.setColor((0,0,0))
        txt.setTamano(12)
        txt.setValor("")
        txt.setFuente("dfkaisb")
        
    #   Establece el color de la letra
    #   Entrada:    .Objeto del texto
    #               .Color
    def setColor(txt, color):
        txt.color = color

    #   Establece la altura y el ancho
    #   Entrada:    .Objeto del texto
    #               .Altura .Ancho
    def setTamano(txt, tamano):
        txt.tamano = tamano

    #   Establece el tipo de fuente para el texto
    #   Entrada:    .Objeto del texto
    #               .Tipo de fuente
    def setFuente(txt, fuente):
        txt.fuente = fuente

    #   Establece la posición en el eje x y el eje y,
    #   Estos ejes son relativos, dependen del componente
    #   Entrada:    .Objeto del texto
    #               .Posición en x .Posición en y    
    def setPosicion(txt, xRel, yRel):
        txt.x = xRel
        txt.y = yRel

    #   Establece la cadeba de texto
    #   Entrada:    .Objeto del texto
    #               .Texto
    def setValor(txt, val):
        txt.valor = val

    #   Pinta el texto, dependiendo de la configuración
    #   Entrada:    .Objeto del texto
    def pintar(txt):
        x = txt.comp.x
        y = txt.comp.y
        fuente = pygame.font.SysFont(txt.fuente, txt.tamano)
        textoRender = fuente.render(txt.valor, 6, txt.color)
        txt.comp.pantalla.blit(textoRender,(x+txt.x, y+txt.y))
        #print(comp.pygame.font.get_fonts())





#****************************************************************************       
class Fondo:
    #   Inicializa las variables internas y configura al fondo
    #   Entrada: Objeto de fondo, Componente
    def __init__(f, comp):
        f.comp = comp
        f.img = ""
        f.setPosicion(1,1)
        f.setBorde(2,2)
        f.setColor((126,126,126))

    #   Establece la posición en el eje x y el y
    #   Estos ejes son relativos, dependen del componente
    #   Entrada:    .Objeto del fondo
    #               .Posición en x .Posición en y  
    def setPosicion(txt, xRel, yRel):
        txt.x = xRel
        txt.y = yRel

    #   Establece que tan redondo son los bordes del fondo
    #   Entrada:    .Objeto del fondo
    #               .Redondeo en x .Redondeo en y  
    def setBorde(fondo, xr, yr):
        fondo.xr = xr
        fondo.yr = yr

    #   Establece el color de fondo
    #   Entrada:    .Objeto del fondo
    #               .Color
    def setColor(fondo, color):
        fondo.color = color
        fondo.img=""

    #   Establece la imagen de fondo
    #   Entrada:    .Objeto del fondo
    #               .Ruta de la imágen
    def setImagen(fondo, img):
        fondo.img = pygame.image.load(img)

    #   Pinta el fondo, puede pintar una imagen,
    #   el fondo dibujado o ambos
    #   Entrada:    .Objeto del fondo
    def pintar(f):
        comp = f.comp
        clip = comp.pantalla.get_clip()
        pantalla = comp.pantalla
        color = f.color
        rect = pygame.Rect(comp.x, comp.y, comp.ancho, comp.altura)
        f.inflate(pantalla, clip, rect, color, 0 , f.xr, f.yr)
        f.ellipse(pantalla, clip, rect, color, 0 , f.xr, f.yr)
        pantalla.set_clip(clip)
        if f.img!="":
            f.pintarImagen()

    #   Pinta  una imagen de fondo
    #   Entrada:    .Objeto del fondo
    def pintarImagen(f):
        imgRect = f.img.get_rect()
        imgRect.x = f.comp.x + 1
        imgRect.y = f.comp.y + 1
        an = int(f.comp.ancho)-2
        al = int(f.comp.altura)-2
        f.img = pygame.transform.scale(f.img, (an, al))
        f.comp.pantalla.blit(f.img, imgRect)
        
    #   Necesarias para pintar fondo con bordes redondos
    #   Entradas:   .Objeto del fondo .Objeto de pantalla de pygame,
    #       .clip .Objeto con info de posición .Color .Ancho
    #       .Redondeo en x .Redondeo en y
    def inflate(f, pantalla, clip, rect, color, width, xr, yr):
        width = 0
        pantalla.set_clip(clip.clip(rect.inflate(0, -yr*2)))
        pygame.draw.rect(pantalla, color, rect.inflate(1-width,0), width)
        pantalla.set_clip(clip.clip(rect.inflate(-xr*2, 0)))
        pygame.draw.rect(pantalla, color, rect.inflate(0,1-width), width)

    #   Necesarias para pintar fondo con bordes redondos
    #   Entradas:   .Objeto del fondo .Objeto de pantalla de pygame,
    #       .clip .Objeto con info de posición .Color .Ancho
    #       .Redondeo en x .Redondeo en y
    def ellipse(f, pantalla, clip, rect, color, width, xr, yr):
        pantalla.set_clip(clip.clip(rect.left, rect.top, xr, yr))
        pygame.draw.ellipse(pantalla, color, pygame.Rect(rect.left, rect.top, 2*xr, 2*yr), width)
        pantalla.set_clip(clip.clip(rect.right-xr, rect.top, xr, yr))
        pygame.draw.ellipse(pantalla, color, pygame.Rect(rect.right-2*xr, rect.top, 2*xr, 2*yr), width)
        pantalla.set_clip(clip.clip(rect.left, rect.bottom-yr, xr, yr))
        pygame.draw.ellipse(pantalla, color, pygame.Rect(rect.left, rect.bottom-2*yr, 2*xr, 2*yr), width)
        pantalla.set_clip(clip.clip(rect.right-xr, rect.bottom-yr, xr, yr))
        pygame.draw.ellipse(pantalla, color, pygame.Rect(rect.right-2*xr, rect.bottom-2*yr, 2*xr, 2*yr), width)

