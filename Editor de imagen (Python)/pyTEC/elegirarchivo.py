import pygame, pyTEC.componente, os
from pygame import *
from util import *
from pyTEC.componente import *
from pyTEC.cajacombo import *
from pyTEC.etiqueta import *

#Lambdas y funciones para validar
vacia = lambda l:l == []

EXT_IMG = [".jpg", ".png", ".jpeg"]
DIR = lambda d: os.path.isdir(d)
FILE = lambda f: not os.path.isdir(f)
def esImagen(ruta):
    ext = os.path.splitext(ruta)[1]
    return Util().esta(EXT_IMG, ext)

class ElegirArchivo(Componente):
    #   Inicializa las variables internas y configura al ElegirArchivo
    #   Entrada: Objeto de ElegirArchivo, Objeto de pantalla de pygame
    def __init__(elAr, pantalla):
        elAr.directorio = ""
        elAr.ruta = os.path.abspath(os.curdir)
        elAr.initComponentes(pantalla)
        Componente.__init__(elAr, pantalla)
        elAr.fondo.setBorde(2,2)
        elAr.setTamano(350, 80)
        elAr.registrarEventos()
        elAr.setSubcomponentes([elAr.txtDirs, elAr.txtArchivos,
                                elAr.txtRuta,elAr.btnCargarDir,
                                elAr.comboArchivos,elAr.comboDirs])
        #print(elAr.subcomps)

    #   Inicializa los subcomponentes de la aplicación que van
    #   a estar dentro del componente ElegirArchivo
    #   Entrada: Objeto de ElegirArchivo, Objeto Pantalla de pygame
    def initComponentes(elAr, pantalla):
        elAr.txtDirs = Etiqueta(pantalla)
        elAr.txtDirs.setTexto("Directorios: ")
        

        dirs = elAr.filtroArchivos(os.listdir(elAr.ruta), DIR)+["**Atrás**"]
        elAr.comboDirs = CajaCombo(pantalla, dirs)
        #elAr.comboDirs.setItems(dirs)
        
        elAr.btnCargarDir = Boton(pantalla)
        elAr.btnCargarDir.setTamano(25, elAr.btnCargarDir.altura)
        elAr.btnCargarDir.setTexto("Ir")

        elAr.txtArchivos = Etiqueta(pantalla)
        elAr.txtArchivos.setTexto("Archivos: ")
        

        files = elAr.filtroArchivos(os.listdir(elAr.ruta), esImagen)
        elAr.comboArchivos = CajaCombo(pantalla, files)
        #elAr.comboArchivos.setItems(files)

        elAr.txtRuta = Etiqueta(pantalla)
        elAr.txtRuta.setTexto("Ruta :" + elAr.ruta)

        
    #   Filtra una lista de archivos, dependiendo de una lambda
    #   Entrada: Objeto de ElegirArchivo, Lista de archivos, Lambda
    def filtroArchivos(elAr, archivos, f):
        if vacia(archivos):
            return []
        archivo = os.path.join(elAr.ruta, archivos[0])
        if f(archivo):
            return [str(archivos[0])] + elAr.filtroArchivos(archivos[1:], f)
        return elAr.filtroArchivos(archivos[1:], f)

    #   Establece la posición en el eje x y el y
    #   Entrada:    .Objeto del ElegirArchivo
    #               .Posición en x .Posición en y 
    def setPosicion(elAr, x, y):
        super().setPosicion(x, y)
        elAr.reposicionar()

    #   Establece la altura y el ancho
    #   Entrada:    .Objeto del ElegirArchivo
    #               .Altura .Ancho
    def setTamano(elAr, ancho, altura):
        super().setTamano(ancho, altura)
        elAr.redimensionar() 

    #   Reposiciona los subcomponentes de acuerdo a ElegirArchivo
    #   Entrada:    .Objeto del ElegirArchivo
    def reposicionar(elAr):
        x = elAr.x
        y = elAr.y
        hCombo = elAr.comboDirs.altura
        aCombo = elAr.comboDirs.ancho
        posiciones = [(x, y),(x, y+hCombo*2+10),
                      (x, y+hCombo),(x+aCombo+200, y + hCombo),
                      (x+100, y+hCombo),(x+100, y+2*hCombo+10)]
        componentes = [elAr.txtRuta, elAr.txtArchivos, elAr.txtDirs,
                       elAr.btnCargarDir,elAr.comboDirs, elAr.comboArchivos]
        modPos = lambda c,pos:c.setPosicion(pos[0], pos[1])
        Util().modificar(componentes, posiciones, modPos)

    #   Redimensiona los subcomponentes de acuerdo a ElegirArchivo
    #   Entrada:    .Objeto del ElegirArchivo
    def redimensionar(elAr):
        hCombo = elAr.comboDirs.altura
        aCombo = elAr.comboDirs.ancho
        elAr.comboDirs.setTamano(aCombo, hCombo)
        elAr.comboArchivos.setTamano(aCombo, hCombo)
        elAr.txtDirs.setTamano(elAr.ancho/2, hCombo)

    #   Pinta fondo y co0mponentes de ElegirArchivo
    #   Entrada:    .Objeto del ElegirArchivo
    def pintar(elAr):
        super().pintar()
        Util().paraTodos(elAr.subcomps, lambda c: c.pintar())

    #   Conecta la ruta con el nombre dle archivo
    #   Entrada:    .Objeto del ElegirArchivo
    #   Salida: Ruta con nombre de archivo
    def rutaAbsElegido(elAr):
        print(str(elAr.ruta +"\\"+ elAr.comboArchivos.elegido))
        return str(elAr.ruta +"\\"+ elAr.comboArchivos.elegido)

    #   Recarga las cajacombo
    #   Entrada:    .Objeto del ElegirArchivo
    def recargar(elAr):
        dirs = elAr.filtroArchivos(os.listdir(elAr.ruta), DIR)+["**Atrás**"]
        files = elAr.filtroArchivos(os.listdir(elAr.ruta), esImagen)
        elAr.comboArchivos.initSubcomponentes(files)
        elAr.comboDirs.initSubcomponentes(dirs)
        elAr.txtRuta.setTexto("Ruta :" + elAr.ruta)

    #   Necesaria para que cuando cambie de directorio,
    #   haga los cambios necesario
    #   Entrada:    .Objeto del ElegirArchivo
    def registrarEventos(elAr):
        def cambiarDirectorio(btnCargar):
            elAr.directorio = elAr.comboDirs.elegido
            if elAr.directorio == "**Atrás**":
                absDir = os.path.abspath(os.curdir)
                ruta, directorio = os.path.split(absDir)
                elAr.directorio = directorio
                elAr.ruta = ruta
                os.chdir("..")
            else:
                print(elAr.ruta+ "\\"+elAr.directorio)
                os.chdir(elAr.ruta+ "\\"+elAr.directorio)
                elAr.ruta = elAr.ruta+ "\\"+elAr.directorio
            elAr.recargar()
        elAr.btnCargarDir.click = cambiarDirectorio
