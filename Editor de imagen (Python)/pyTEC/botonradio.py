from pyTEC.componente import *
from pyTEC.boton import *
#   clase para la configuracion de los boton radio
class BotonRadio(Componente):
    #funcion que configura el tamaño de los botones del BotonRadio
    #entradas: la pantalla a la cual se le agregara al BotonRadio y la biblioteca pygame
    def __init__(btnRadio, pantalla, texto):
        Componente.__init__(btnRadio, pantalla)
        btnRadio.configuracion(texto)

    def configuracion(btnRadio, texto):
        bR = btnRadio
        bR.setTexto(texto)
        bR.texto.setPosicion(bR.x+20, bR.y+3)
        bR.setTamano(100, 20)
        bR.fondo.setBorde(0,0)
        bR.initSubcomponentes()

    #   Inicializa los subcomponentes que pertenecen propiamente
    #   a el BotonRadio    
    #   Entrada: Objeto de BotonRadio
    def initSubcomponentes(btnRadio):
        btnRadio.btnElegido = Boton(btnRadio.pantalla)
        btnRadio.btnElegido.setTamano(10, btnRadio.altura)
        btnRadio.setSubcomponentes([btnRadio.btnElegido])

    #   Pinta los subcomponentes de la CajaSpin
    #   Entrada: Número
    #   Entrada: Objeto de CajaSpin
    def pintar(btnRadio):
        btnRadio.fondo.pintar()
        btnRadio.btnElegido.setPosicion(btnRadio.x, btnRadio.y)
        
        btnRadio.btnElegido.pintar()
        btnRadio.texto.pintar()
