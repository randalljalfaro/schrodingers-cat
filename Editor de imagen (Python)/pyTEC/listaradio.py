from pyTEC.componente import *
from pyTEC.botonradio import *
#clase para la configuracion de la lista de botones radios
class ListaRadio(Componente):
    # inicializa la lista radio, con una pantalla
    def __init__(lR,pantalla, opciones):
        Componente.__init__(lR, pantalla)
        lR.elegido = ""
        lR.fondo.setBorde(2,2)
        lR.setColor((255,255,255))
        lR.initSubcomponentes(opciones)

    #   inicializa los componentes de la aplicacion que van
    #   a estar en la pantalla opciones marco
    # entrada: Objeto de botonradio 
    def initSubcomponentes(lR, opciones):
        def f_cambiaColorElegido(subcomp):
            if subcomp.texto.valor == lR.elegido:
                subcomp.btnElegido.setColor((30,255,30))
            else:
                subcomp.btnElegido.setColor((0,0,0))
        
        def nuevoElegido(comp):
            lR.elegido = comp.texto.valor
            Util().paraTodos(lR.subcomps, f_cambiaColorElegido)
            
        def f_creaRadios(opc, subcomps):
            radio = BotonRadio(lR.pantalla, opc)
            radio.click = nuevoElegido
            return subcomps+[radio]

        subcomps = Util().foldLeft(opciones, f_creaRadios, [])
        lR.setSubcomponentes(subcomps)
        nuevoElegido(lR.subcomps[0])
    # pinta los subcomponentes de listaradio
    # Entradas: un subcomponente y un valor en x y en y   
    def repintar(lR, subcomps, x, y):
        if subcomps!=[]:
            subcomps[0].setPosicion(x+30, y)
            subcomps[0].pintar()
            return lR.repintar( subcomps[1:], x+subcomps[0].ancho+150, y)
    
    def pintar(lR):
        lR.fondo.pintar()
        lR.repintar(lR.subcomps, lR.x, lR.y)
        



    
