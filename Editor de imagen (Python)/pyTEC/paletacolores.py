from pyTEC.componente import *
from pyTEC.boton import *
from pyTEC.etiqueta import *

class PaletaColores(Componente):
    def __init__(p, pantalla, coloresPaletas):
        Componente.__init__(p,pantalla)
        p.etqElegido = Etiqueta(pantalla)
        colores = {
            "Rojo":(255,0,0),
            "Verde":(0,255,0),
            "Azul":(0,0,255),
            "Amarillo":(255,255,0),
            "Morado":(74,35,100),
            "Celeste":(12,183,242),
        }
        def nuevoElegido(paleta):
            p.color = paleta.color
            p.etqElegido.setTexto("Color elegido: "+paleta.color)
            
        def f_creaPaletas(color, subcomps):
            paleta = Boton(p.pantalla)
            paleta.color = color
            paleta.setColor(colores[color])
            paleta.click = nuevoElegido
            paleta.setTamano(30, 30)
            return subcomps+[paleta]

        subcomps = Util().foldLeft(coloresPaletas, f_creaPaletas, [])
        p.color = coloresPaletas[0]
        p.setSubcomponentes(subcomps)
        nuevoElegido(p.subcomps[0])

    def pintar(p):
        p.fondo.pintar()
        gap = 10
        for paleta in p.subcomps:
            #print((p.x+gap, p.y+30))
            paleta.setPosicion(p.x+gap, p.y+30)
            paleta.pintar()
            gap+=50
        p.etqElegido.setPosicion(p.x+2, p.y+2.5)
        p.etqElegido.pintar()
