from pyTEC.componente import *
from pyTEC.boton import *
from pyTEC.etiqueta import *

#clase para la configuracion de la caja spin
class CajaSpin(Componente):
    #funcion que configura el tamaño de los botones de la caja spin
    #entradas: la pantalla a la cual se le agregara la caja spin y la biblioteca pygame
    #salidas: caja spin configurada para sus funciones
    def __init__(spin, pantalla):
        Componente.__init__(spin, pantalla)
        spin.setTamano(100, 20)
        spin.fondo.setBorde(2,2)
        spin.pantalla = pantalla
        spin.valor = 0
        spin.max = 10000
        spin.initSubcomponentes()

    #   Inicializa los componentes de la aplicación que van
    #   a estar en pantalla de opciones de marco
    #   Entrada: Objeto de CajaSpin
    def initSubcomponentes(spin):
        spin.arriba = Boton(spin.pantalla)
        spin.abajo = Boton(spin.pantalla)
        spin.arriba.setTexto("+")
        spin.abajo.setTexto("-")
        spin.abajo.texto.setTamano(14)
        spin.arriba.texto.setTamano(14)
        spin.arriba.fondo.setBorde(2, 2)
        spin.abajo.fondo.setBorde(2, 2)
        spin.abajo.texto.setPosicion(2, -3)
        spin.arriba.texto.setPosicion(2, -3)
        spin.abajo.texto.setColor((255,255,255))
        spin.arriba.texto.setColor((255,255,255))
        largo = spin.altura / 2
        spin.arriba.setTamano(largo, largo)
        spin.abajo.setTamano(largo, largo)
        spin.arriba.setColor((255,0,0))
        spin.abajo.setColor((255,0,255))
        spin.arriba.setColor((0,0,0))
        spin.abajo.setColor((0,0,0))

        #funcion que aumenta el valor de la caja spin
        def arriba(arr):
            if spin.valor<spin.max:
                spin.valor += 1
        #funcion que disminuye el valor de la caja spin
        def abajo(ab):
            if spin.valor>0:
                spin.valor += -1
        spin.arriba.click = arriba
        spin.abajo.click = abajo
        spin.setSubcomponentes([spin.arriba, spin.abajo])
        
    #   Establece el valor numérico del spin
    #   Entrada: Número
    #   Entrada: Objeto de CajaSpin
    def setValor(spin, valor):
        spin.valor = valor
        #funcion que configura la posicion, colores y texto de la caja spin

    def setMax(spin, valor):
        spin.max = valor
        
    #   Pinta los subcomponentes de la CajaSpin
    #   Entrada: Número
    #   Entrada: Objeto de CajaSpin
    def pintar(spin):
        spin.fondo.pintar()
        spin.texto.setValor(str(spin.valor))
        spin.arriba.setPosicion(spin.x + spin.ancho, spin.y)
        spin.abajo.setPosicion(spin.x + spin.ancho, spin.y+10)
        
        spin.arriba.pintar()
        spin.abajo.pintar()
        spin.texto.pintar()
