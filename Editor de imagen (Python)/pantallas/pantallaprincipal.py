import pygame, util
from pyTEC.elegirarchivo import *
from pyTEC.boton import *
from pyTEC.cajacombo import *
from pyTEC.cajaspin import *
from pyTEC.etiqueta import *
from pyTEC.listaradio import *
from pyTEC.paletacolores import *
from util import *
from pantallas.pantalla import *

class PantallaOpcionesMarco(Pantalla):
    #   Configura e inicializa la pantalla de opciones de marco
    #   Entrada: Objeto de la pantalla, Objeto pantalla de pygame,
    #       ancho, altura, ruta para imágen de fondo
    def __init__(p, pantalla, ancho, altura):
        #FUuuuuuusión de Pantalla y PantallaOpcionesMarco
        Pantalla.__init__(p, pantalla, ancho, altura)
        p.initComponentes()
        p.resgistrarEventos()

    #   Inicializa los componentes de la aplicación que van
    #   a estar en pantalla de opciones de marco
    #   Entrada: Objeto de la pantalla de opciones de marco
    def initComponentes(p):
        util = Util()
        lineax1 = 150
        gapX1 = 230
        lineay1 = 120
        lineay2 = 150
        lineay3 = 255
        lineay4 = 320
        lineay5 = 430

        opcRadios = ["Cargar imágen", "Cargar fondo"]
        p.listRadio = ListaRadio(p.pantalla, opcRadios)
        p.listRadio.setPosicion(lineax1+50, lineay1)
        p.listRadio.setTamano(485, p.listRadio.altura)
        
        p.elegirArchivo = ElegirArchivo(p.pantalla)
        p.elegirArchivo.setPosicion(lineax1+30,lineay2)
        a = p.elegirArchivo.ancho
        p.fondo_imagen = p.elegirArchivo
        p.elegirArchivo.setTamano(a*1.7,p.elegirArchivo.altura)

        colores = ["Rojo", "Verde", "Azul", "Amarillo","Morado","Celeste"]
        p.elegirFondo = PaletaColores(p.pantalla, colores)
        p.elegirFondo.setPosicion(lineax1+70,lineay2)
        p.elegirFondo.setTamano(a*1.3,p.elegirArchivo.altura)

        p.etqMarco = Etiqueta(p.pantalla)
        p.etqMarco.setTexto("Generar Marco")
        p.etqMarco.setPosicion(lineax1,lineay3)

        items = ["Cuadrado", "Triángulo", "Rectángulo", "Rombo",
                 "Hexágono", "Círculo", "Estrella"]
        p.comboMarcos = CajaCombo(p.pantalla, items)
        p.comboMarcos.setPosicion(lineax1, lineay3+20)
        
        p.etqGrosor = Etiqueta(p.pantalla)
        p.etqGrosor.setTexto("Grosor de línea")
        p.etqGrosor.setPosicion(lineax1+gapX1+90,lineay3)
        
        p.spinGrosor = CajaSpin(p.pantalla)
        p.spinGrosor.setPosicion(lineax1+gapX1+100,lineay3+24)
        p.spinGrosor.valor = 1
        
        p.etqGenFractal = Etiqueta(p.pantalla)
        p.etqGenFractal.setTexto("Generar Fractal")
        p.etqGenFractal.setPosicion(lineax1,lineay4)


        items = ["Dragón","Sierpinski","Estrella de mar","Palmera"]
        p.comboGenFractal = CajaCombo(p.pantalla, items)
        p.comboGenFractal.setPosicion(lineax1, lineay4+20)
        
        p.etqGrosFractal = Etiqueta(p.pantalla)
        p.etqGrosFractal.setTexto("Grosor de línea (fractal)")
        p.etqGrosFractal.setPosicion(lineax1+gapX1+90,lineay4)
        
        p.spinGrosFractal = CajaSpin(p.pantalla)
        p.spinGrosFractal.setPosicion(lineax1+gapX1+100,lineay4+24)
        p.spinGrosFractal.valor = 1

        p.etqProfFractal = Etiqueta(p.pantalla)
        p.etqProfFractal.setTexto("Profundidad")
        a = p.etqProfFractal.ancho
        p.etqProfFractal.setPosicion(lineax1+a+gapX1+90,lineay4)
        
        p.spinProfFractal = CajaSpin(p.pantalla)
        p.spinProfFractal.setPosicion(lineax1+a+gapX1+100,lineay4+24)
        p.spinProfFractal.valor = 1

        p.etqLargoFrag = Etiqueta(p.pantalla)
        p.etqLargoFrag.setTexto("Largo")
        p.etqLargoFrag.setPosicion(lineax1+a+gapX1+100,lineay4+50)
        
        p.spinLargoFractal = CajaSpin(p.pantalla)
        p.spinLargoFractal.setPosicion(lineax1+a+gapX1+100,lineay4+70)
        p.spinLargoFractal.valor = 1
        
        p.etqColMarcFig = Etiqueta(p.pantalla)
        p.etqColMarcFig.setTexto("Color de marco y figura generada")
        p.etqColMarcFig.setPosicion(lineax1,lineay5)
        
        colores = ["Rojo", "Verde", "Azul", "Amarillo","Morado","Celeste"]
        p.elegirColMarcFig = PaletaColores(p.pantalla, colores)
        p.elegirColMarcFig.setPosicion(lineax1,lineay5+20)
        a = p.elegirColMarcFig.ancho
        h = p.elegirColMarcFig.altura
        p.elegirColMarcFig.setTamano(a+100,h*3.7)

        p.etqDimension = Etiqueta(p.pantalla)
        p.etqDimension.setTexto("Tamaño del marco")
        p.etqDimension.setPosicion(lineax1+gapX1+90,lineay5)
        
        p.spinTamX = CajaSpin(p.pantalla)
        a = p.spinTamX.ancho
        h = p.spinTamX.altura
        p.spinTamX.setTamano(a/2, h)
        p.spinTamX.setPosicion(lineax1+gapX1+100,lineay5+24)
        p.spinTamX.valor = 600

        p.spinTamY = CajaSpin(p.pantalla)
        p.spinTamY.setTamano(a/2, h)
        p.spinTamY.setPosicion(lineax1+gapX1+a+110,lineay5+24)
        p.spinTamY.valor = 600

        p.etqsAng1 = Etiqueta(p.pantalla)
        p.etqsAng1.setTexto("Angulos")
        p.etqsAng1.setPosicion(lineax1+30,lineay4+50)

        p.spinAng1 = CajaSpin(p.pantalla)
        p.spinAng1.setTamano(a/2, h)
        p.spinAng1.setPosicion(lineax1+70,lineay4+70)
        p.spinAng1.valor = 1

        p.spinAng2 = CajaSpin(p.pantalla)
        p.spinAng2.setTamano(a/2, h)
        p.spinAng2.setPosicion(lineax1,lineay4+70)
        p.spinAng2.valor = 1

        p.btnGenImg = Boton(p.pantalla)
        p.btnGenImg.setPosicion(lineax1+gapX1+80,lineay5+24*3)
        p.btnGenImg.setTexto("Generar imagen")

        p.btnSalir = Boton(p.pantalla)
        p.btnSalir.setPosicion(lineax1+gapX1+a+90,lineay5+24*3)
        p.btnSalir.setTexto("Salir")
        
        p.recargarComponentes()


    def resgistrarEventos(p):
        #---------------------------------------------------
        #Intercambio entre escoger color de fondo y
        #elegir una imagen de fondo
        def cambiar_fondo_imagen():
            if p.listRadio.elegido == "Cargar fondo":
                p.fondo_imagen = p.elegirFondo
            else:
                p.fondo_imagen = p.elegirArchivo
            p.recargarComponentes()
        p.listRadio.click = lambda c: cambiar_fondo_imagen()

        #---------------------------------------------------
        #Abrir pantalla secundaria con opciones seleccionadas
        
    def todosLosComponentes(p):
        return [p.listRadio, p.etqMarco,
                 p.etqGrosor, p.etqGenFractal,
                 p.etqGrosFractal, p.spinGrosor,
                 p.spinGrosFractal, p.etqColMarcFig,
                 p.elegirColMarcFig,p.etqDimension,
                 p.etqProfFractal, p.spinProfFractal,
                 p.spinTamX, p.spinTamY,
                 p.btnGenImg, p.btnSalir,
                 p.spinAng1,p.spinAng2,
                 p.etqsAng1,
                 p.etqLargoFrag,p.spinLargoFractal,
                 p.comboGenFractal,p.comboMarcos,
                 p.fondo_imagen]
    #   Agrega o elimina componentes dependiendo
    #   de cuales sean las opciones del fractal
    def recargarComponentes(p):
        fractal = p.comboGenFractal.elegido
        if (fractal=="Palmera" or fractal=="Estrella de mar"):
            p.componentes = p.todosLosComponentes()
        elif fractal=="Pi":
            p.componentes = p.todosLosComponentes()[:16]+p.todosLosComponentes()[21:]
        else:
            p.componentes = p.todosLosComponentes()[:16]+p.todosLosComponentes()[19:]
    #   Recarga los componentes dependiendo de
    #   la figura elegida
    def recargaOpciones(p):
        figura = p.comboMarcos.elegido
        if figura=="Círculo":
            p.etqDimension.setTexto("Radio")
        elif figura=="Cuadrado":
            p.etqDimension.setTexto("Lado")
        elif figura=="Triángulo":
            p.etqDimension.setTexto("Base              Altura")
        elif figura=="Rectángulo":
            p.etqDimension.setTexto("Base              Altura")
        elif figura=="Rombo":
            p.etqDimension.setTexto("Diagonal mayor    Diagonal menor")
        elif figura=="Hexágono":
            p.etqDimension.setTexto("Lado")
        elif figura=="Estrella":
            p.etqDimension.setTexto("Lado")
        
            
    def pintar(p):
        #Mantener recargado la opción elegida entre color de fondo y
        #una imagen de fondo
        p.listRadio.click(p.listRadio)
        #Pinta a los componentes que tenga registrados
        super().pintar()

    
