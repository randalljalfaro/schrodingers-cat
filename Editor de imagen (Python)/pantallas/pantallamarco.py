import pygame
from pyTEC.boton import *
from pyTEC.etiqueta import *
from util import *
from math import pi, cos, sin
from pantallas.pantalla import *
from manejadordemarcos import *
from manejadordefractales import *

class PantallaMarco(Pantalla):
    #   Configura e inicializa la pantalla del marco
    #   Entrada: Objeto de la pantalla, Objeto pantalla de pygame,
    #       ancho, altura, ruta para imágen de fondo
    def __init__(p, pantalla, ancho, altura):
        #FUuuuuuusión de Pantalla y PantallaOpcionesMarco
        Pantalla.__init__(p, pantalla, ancho, altura)
        p.initComponentes()
        p.mm = ManejadorDeMarcos(pantalla, p)
        p.mf = ManejadorDeFractales(pantalla)
        p.devolver = False
        p.pausa = False
        
    #   Inicializa los componentes de la aplicación que van
    #   a estar en pantalla de opciones de marco
    #   Entrada: Objeto de la pantalla de opciones de marco
    def initComponentes(p):
        p.etqExito = Etiqueta(p.pantalla) 
        p.btnAtras = Boton(p.pantalla)
        p.btnGuardar = Boton(p.pantalla)
        p.btnPausa = Boton(p.pantalla)
        p.btnPlay = Boton(p.pantalla)
        
        p.etqExito.setPosicion(10, p.altura-50)
        p.btnAtras.setPosicion(10,10)
        p.btnGuardar.setPosicion(70,10)
        p.btnPausa.setPosicion(10,50)
        p.btnPlay.setPosicion(70,50)
        
        p.btnAtras.setTamano(60,20)
        p.btnGuardar.setTamano(60,20)
        p.btnPausa.setTamano(60,20)
        p.btnPlay.setTamano(60,20)

        p.btnAtras.setTexto("Atrás")
        p.btnGuardar.setTexto("Guardar")
        p.btnPausa.setTexto("Pausa")
        p.btnPlay.setTexto("Play")
        
        p.componentes = [p.etqExito, p.btnAtras, p.btnGuardar,
                         p.btnPausa, p.btnPlay]
        
    #   Pinta subcomponentes y fondo de la Pantalla
    def pintar(p):
        p.pintarSinComponentes()
        super().pintarComps()

    #   funcion que hace que el fractal se devuelva
    #   y se aumente dependiendo de la profundidad,
    #   tambien pausa y da play a la creacion del fractal
    def ajustarProf(p):
        if not p.pausa and p.mf.prof<p.mf.maxProf and not p.devolver:
            p.mf.prof+=1
        elif not p.pausa and p.mf.prof>0 and p.devolver:
            p.mf.prof-=1
        elif not p.pausa:
            p.devolver = not p.devolver
            
    #   Pinta el marco sin botones, para cuando se debe guardar el marco
    def pintarSinComponentes(p):
        pygame.display.set_mode((p.ancho,p.altura))
        p.ajustarProf()
        super().pintarFondo()
        p.mf.pintar()
        p.mm.pintar()
        
