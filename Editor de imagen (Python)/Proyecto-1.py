import pygame, os
from pygame import *
from manejadordeeventos import *
from pantallas.pantallaprincipal import *
from pantallas.pantallamarco import *
from math import pi


#*************************************************************************
#   Clase de la configuración de la aplicación Frameaker
class ConfigFM:
    FOTOGPORSEG = 10
    ALTURA = 650
    ANCHO = 1000
    FIN = False
    TITULO = "Frame Maker Advanced"
    FONDO =BLANCO
    DIR_APP = os.path.dirname(os.path.realpath(__file__))
    IMAGENFONDO = DIR_APP+'\\images\\marco.png'
    
    #Se encarga de configurar los parametros con que inicia la aplicación
    def __init__(config, fm):
        pygame.display.set_caption(config.TITULO)
        fm.pantalla = pygame.display.set_mode([config.ANCHO,config.ALTURA])
        fm.pantalla = pygame.display.set_mode((0,0))
        fm.pantalla.fill(BLANCO)
        #Reloj para actualizar pantalla
        fm.reloj = pygame.time.Clock()

#*************************************************************************
#   Clase de la aplicación principal
class FrameMaker(ManejadorEventos):
    IMGFONDOPRUEBA = "C:\\Users\Public\\Documents\\Proyecto-1v3\\images\\Dark Side of the moon.png"
    #   Función que inicializa la configuración de la aplicación
    #   Entrada: Objeto del manejador de eventos
    def __init__(fm):
        #Inicia la biblioteca
        pygame.init()
        #Fuuuuusión de Manejador de eventos y app 
        ManejadorEventos.__init__(fm)
        #Configuración de la aplicación
        fm.config = ConfigFM(fm)

        
        fm.pantallaOpcionesMarco = PantallaOpcionesMarco(
            fm.pantalla, fm.config.ANCHO,
            fm.config.ALTURA)
        fm.pantallaOpcionesMarco.setImagenFondo(
            fm.config.IMAGENFONDO
            )
        #PantallaPrincipal inicia por default
        fm.activarPantallaPrincipal()

        def eventoSalir(pantallaOpcionesMarco):
            fm.config.FIN = True
        fm.pantallaOpcionesMarco.btnSalir.click = eventoSalir


    #   Función principal que se mantiene ejecutando
    #   para revisar eventos, responder a ellos
    #   y además actualizar la pantalla
    #   Entrada: Objeto del manejador de eventos
    def ejecutar(fm):
        prof = 0
        devolver = False
        while not fm.config.FIN:
            fm.manejaEventos()
            if fm.pantallaActivada == "PANTALLA_OPCIONES_MARCO":
                fm.comps = fm.pantallaOpcionesMarco.componentes
                fm.pantallaOpcionesMarco.recargaOpciones()
                fm.pantallaOpcionesMarco.pintar()
            elif fm.pantallaActivada == "PANTALLA_MARCO":
                fm.comps = fm.pantallaMarco.componentes
                fm.pantallaMarco.pintar()

                
            pygame.display.update()
            fm.reloj.tick(fm.config.FOTOGPORSEG)
            
        pygame.quit()

    #   Activa la pantallla principal y registra la
    #   función para cargar una imágen
    #   Entrada: Objeto del manejador de eventos
    def activarPantallaPrincipal(fm):
        pygame.display.set_mode((fm.config.ANCHO,fm.config.ALTURA))
        pPrin = fm.pantallaOpcionesMarco
        
        def cargarImagen(btnGenImg):
            if pPrin.listRadio.elegido!="Cargar fondo":
                FILE = lambda f: os.path.isfile(f)
                if FILE(pPrin.elegirArchivo.rutaAbsElegido()):
                    fm.activarPantallaSecundaria()
            else:
                fm.activarPantallaSecundaria()
                    
        pPrin.btnGenImg.click = cargarImagen
        fm.pantallaActivada = "PANTALLA_OPCIONES_MARCO"

    #   Activa la pantallla secundaria y registra la
    #   función para devolverse de pantalla
    #   y la de guardar una imágen
    #   Entrada: Objeto del manejador de eventos
    def activarPantallaSecundaria(fm):
        print("activarPantallaSecundaria")
        pygame.display.set_mode((fm.config.ANCHO,fm.config.ALTURA))
        pPrin = fm.pantallaOpcionesMarco
        fm.pantallaMarco = PantallaMarco(
            fm.pantalla, fm.config.ANCHO,
            fm.config.ALTURA)
        FILE = lambda f: os.path.isfile(f)
        print(pPrin.listRadio.elegido)
        if pPrin.listRadio.elegido!="Cargar fondo":
            fm.pantallaMarco.setImagenFondo(
                pPrin.elegirArchivo.rutaAbsElegido()
                )
        else:
            fm.pantallaMarco.setColorFondo(
                pPrin.elegirFondo.color
                )
            
        def eventoGuardar(comp):
            pMarco = fm.pantallaMarco
            nom_ext = os.path.basename(pMarco.rutaFondo)
            nombre, ext = os.path.splitext(nom_ext)
            pMarco.pintarSinComponentes()
            i=0
            dirImg = fm.config.DIR_APP +"\\marcos\\"+nombre+str(i)+".jpg"
            while(os.path.isfile(dirImg)):
                i+=1
                dirImg = fm.config.DIR_APP +"\\marcos\\"+nombre+str(i)+".jpg"
            pygame.image.save(fm.pantalla,
                              fm.config.DIR_APP +"\\marcos\\"+nombre+str(i)+".jpg")
        fm.pantallaMarco.btnGuardar.click = eventoGuardar
        
        
        eventoAtras = lambda btnAtras: fm.activarPantallaPrincipal()
        fm.pantallaMarco.btnAtras.click = eventoAtras

        def eventoPausa(btnPausa):
            fm.pantallaMarco.pausa = True
        fm.pantallaMarco.btnPausa.click = eventoPausa

        def eventoPlay(btnPlay):
            fm.pantallaMarco.pausa = False
        fm.pantallaMarco.btnPlay.click = eventoPlay
        
        figura = pPrin.comboMarcos.elegido
        fractal = pPrin.comboGenFractal.elegido
        borde = pPrin.spinGrosor.valor
        color = pPrin.elegirColMarcFig.color
        prof = pPrin.spinProfFractal.valor
        grosor = pPrin.spinGrosFractal.valor
        largo = pPrin.spinLargoFractal.valor
        ang1 = pPrin.spinAng1.valor
        ang2 = pPrin.spinAng2.valor
        tamx = pPrin.spinTamX.valor
        tamy = pPrin.spinTamY.valor
        if figura=="Triángulo":
            fm.pantallaMarco.mm.Triangulo(tamx,tamy,color, borde)
            fm.pantallaMarco.setTamano(tamx,tamy)
        elif figura=="Cuadrado":
            fm.pantallaMarco.mm.Cuadrado(tamx,color, borde)
            fm.pantallaMarco.setTamano(tamx,tamx)
        elif figura=="Rectángulo":
            fm.pantallaMarco.mm.Rectangulo(tamx, tamy, color, borde)
            fm.pantallaMarco.setTamano(tamx,tamy)
        elif figura=="Rombo":
            fm.pantallaMarco.mm.Rombo(tamx,tamy,color, borde)
            fm.pantallaMarco.setTamano(tamx,tamy)
        elif figura=="Hexágono":
            fm.pantallaMarco.mm.Hexagono(tamx,color,borde)
            fm.pantallaMarco.setTamano(tamx,tamy)
        elif figura=="Estrella":
            fm.pantallaMarco.mm.Estrella(tamx,color,borde)
            fm.pantallaMarco.setTamano(tamx,tamy)
        elif figura=="Círculo":
            fm.pantallaMarco.mm.Circulo(tamx,color,borde)
            fm.pantallaMarco.setTamano(tamx,tamy)

        if fractal=="Palmera":
            fm.pantallaMarco.mf.FractalPalmera(prof,color, tamx//2, tamy,
                                               -ang1, ang2, largo,
                                               grosor,-90)
        elif fractal=="Estrella de mar":
            fm.pantallaMarco.mf.FractalEstrellaDeMar(prof,color,tamx//2, tamy//2,
                                                     -ang1, -ang2, largo,
                                                     grosor,-90)
        elif fractal=="Dragón":
            fm.pantallaMarco.mf.FractalDragon(prof, color,tamx//4, tamy//2,
                                              largo, grosor)
        elif fractal=="Sierpinski":
            fm.pantallaMarco.mf.FractalSierpinski(prof,color, tamx, tamy, grosor)
            
        fm.pantallaActivada = "PANTALLA_MARCO"

frMaker = FrameMaker()
frMaker.ejecutar()

