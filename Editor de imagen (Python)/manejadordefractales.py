import pygame, os
from pygame import *
from pyTEC.componente import *
from math import pi,cos,sin
import sys
sys.setrecursionlimit(100000)
##   clase para manejar los fractales    ##
class ManejadorDeFractales:

    def __init__(mf,pantalla):
        mf.pantalla = pantalla
        mf.maxProf = 0
        mf.prof = 0
        mf.coordenada = (0,0)
        mf.colores = {
            "Rojo":(255,0,0),
            "Verde":(0,255,0),
            "Azul":(0,0,255),
            "Amarillo":(255,255,0),
            "Morado":(74,35,100),
            "Celeste":(12,183,242),
        }

    def pintar(mf):
        if mf.fractal=="Palmera":
            mf.pintarFractalPalmera(mf.prof,mf.color, mf.x, mf.y,
                                mf.ang1, mf.ang2, mf.largo, 
                                mf.grosor, mf.primAng)
        elif mf.fractal=="Estrella de mar":
            rotacion = 360/5
            mf.pintarFractalPalmera(mf.prof,mf.color, mf.x, mf.y,
                                mf.ang1, mf.ang2, mf.largo, 
                                mf.grosor, mf.primAng+rotacion)
            mf.pintarFractalPalmera(mf.prof,mf.color, mf.x, mf.y,
                                mf.ang1, mf.ang2, mf.largo, 
                                mf.grosor, mf.primAng+rotacion*2)
            mf.pintarFractalPalmera(mf.prof,mf.color, mf.x, mf.y,
                                mf.ang1, mf.ang2, mf.largo, 
                                mf.grosor, mf.primAng+rotacion*3)
            mf.pintarFractalPalmera(mf.prof,mf.color, mf.x, mf.y,
                                mf.ang1, mf.ang2, mf.largo, 
                                mf.grosor, mf.primAng+rotacion*4)
            mf.pintarFractalPalmera(mf.prof,mf.color, mf.x, mf.y,
                                mf.ang1, mf.ang2, mf.largo, 
                                mf.grosor, mf.primAng+rotacion*5)
        elif mf.fractal=="Dragón":
            mf.pintarFractalDragon(mf.x, mf.y, mf.color,
                                   mf.movs[:mf.prof], "I",
                                   mf.largo, mf.grosor)
        elif mf.fractal=="Sierpinski":
            mf.pintarFractalSierpinski(mf.prof, mf.color,
                                       mf.p1, mf.p2, mf.p3,
                                       mf.grosor)
#-----------------------------------------------------#
##  Funciones de cada fractal con sus formulas  ##
    

    def FractalPalmera(mf, prof,color, x, y, ang1, ang2, largo,
                       grosor,primAng):
        mf.fractal="Palmera"
        mf.prof = prof
        mf.maxProf = prof
        mf.color = color
        mf.x = x
        mf.y = y
        mf.ang1 = ang1
        mf.ang2 = ang2
        mf.largo = largo
        mf.grosor = grosor
        mf.primAng = primAng
        
        
    def FractalEstrellaDeMar(mf, prof,color, x, y, ang1, ang2, largo,
                       grosor,primAng):
        mf.FractalPalmera(prof, color, x, y, ang1, ang2,largo, 
                          grosor, primAng)
        mf.fractal="Estrella de mar"
        
    def pintarFractalPalmera(mf, prof, color, x, y, ang1, ang2, largo,
                             grosor, primAng):
        if prof!=0:
            if grosor>1:
                grosor-=1
            
            grados_a_radianes = pi/180
            nuevoX = x + (cos(primAng*grados_a_radianes)*largo)
            nuevoY = y + (sin(primAng*grados_a_radianes)*largo)
            
            pygame.draw.line(mf.pantalla, mf.colores[color],
                             (x, y),(nuevoX, nuevoY), grosor)
            
            mf.pintarFractalPalmera(prof-1,color, nuevoX, nuevoY,
                                  ang1, ang2, largo,
                                    grosor,primAng+ang1)
            
            mf.pintarFractalPalmera(prof-1,color, nuevoX, nuevoY,
                                  ang1, ang2, largo,
                                    grosor,primAng+ang2)
    
        
   

    def FractalDragon(mf, prof,color, x, y, largo, grosor):
        def sust_inv(lista, listasust_inv):
            if lista!=[]:
                ele = "D"
                if lista[0]=="D":
                    ele = "I"
                return sust_inv(lista[1:], [ele]+listasust_inv)
            return listasust_inv
        
        def puntosDragon(profPts, puntos):
            if profPts==prof:
                return puntos
            elif profPts==0:
                return puntosDragon(profPts+1, [])
            elif profPts==1:
                return puntosDragon(profPts+1, ["I"])
            puntos = puntos+["I"]+sust_inv(puntos,[])
            return puntosDragon(profPts+1, puntos)
        mf.movs = puntosDragon(0, [])
        mf.x = x
        mf.y = y
        mf.maxProf = len(mf.movs)-1
        mf.color = color
        mf.largo=largo
        mf.grosor = grosor
        mf.fractal="Dragón"
        
    def pintarFractalDragon(mf, x, y, color, movs, orient, largo, grosor):
        def orientacionI():
            if movs[0] == "I":
               pygame.draw.line(mf.pantalla, mf.colores[color],
                             (x, y),(x, y-largo), grosor)
               mf.pintarFractalDragon(x, y-largo, color, movs[1:], "Ab",
                                largo, grosor)
            elif movs[0] == "D":
               pygame.draw.line(mf.pantalla, mf.colores[color],
                             (x, y),(x, y+largo), grosor)
               mf.pintarFractalDragon(x, y+largo, color, movs[1:], "Ar", largo, grosor)
        def orientacionD():
            if movs[0] == "I":
                pygame.draw.line(mf.pantalla, mf.colores[color],
                             (x, y),(x, y+largo), grosor)
                mf.pintarFractalDragon(x, y+largo, color, movs[1:], "Ar",largo, grosor)
            elif movs[0] == "D":
                pygame.draw.line(mf.pantalla, mf.colores[color],
                             (x, y),(x, y-largo), grosor)
                mf.pintarFractalDragon(x, y-largo, color, movs[1:], "Ab",largo, grosor)
        def orientacionAr():
            if movs[0] == "I":
                pygame.draw.line(mf.pantalla, mf.colores[color],
                             (x, y),(x+largo, y), grosor)
                mf.pintarFractalDragon(x+largo, y, color, movs[1:], "I",largo, grosor)
            elif movs[0] == "D":
                pygame.draw.line(mf.pantalla, mf.colores[color],
                             (x, y),(x-largo, y), grosor)
                mf.pintarFractalDragon(x-largo, y, color, movs[1:], "D",largo, grosor)
        def orientacionAb():
            if movs[0] == "I":
                pygame.draw.line(mf.pantalla, mf.colores[color],
                             (x, y),(x-largo, y), grosor)
                mf.pintarFractalDragon(x-largo, y, color, movs[1:], "D",largo, grosor)
            elif movs[0] == "D":
                pygame.draw.line(mf.pantalla, mf.colores[color],
                             (x, y),(x+largo, y), grosor)
                mf.pintarFractalDragon(x+largo, y, color, movs[1:], "I",largo, grosor)
        mover = {
            "I":orientacionI,
            "D":orientacionD,
            "Ar":orientacionAr,
            "Ab":orientacionAb,
            }
        if movs!=[]:
            mover[orient]()
            
    def FractalSierpinski(mf, prof,color, x, y, grosor):
        mf.maxProf=prof
        mf.prof=prof
        mf.p1 = {"x":0, "y":y}
        mf.p2 = {"x":x//2, "y":0}
        mf.p3 = {"x":x, "y":y}
        mf.color = color
        mf.grosor = grosor
        mf.fractal="Sierpinski"

    def pintarFractalSierpinski(mf,prof,color, p1, p2, p3, grosor):
        def mediatriz(p1, p2):
            return {"x":((p1["x"]+p2["x"])//2),
                    "y":((p1["y"]+p2["y"])//2)}
        if prof <=1 :
            pygame.draw.line(mf.pantalla, mf.colores[color],
                             (p1["x"], p1["y"]),(p2["x"], p2["y"]),
                             grosor)
            pygame.draw.line(mf.pantalla, mf.colores[color],
                             (p2["x"], p2["y"]),(p3["x"], p3["y"]),
                             grosor)
            pygame.draw.line(mf.pantalla, mf.colores[color],
                             (p3["x"], p3["y"]),(p1["x"], p1["y"]),
                             grosor)
        else:
            p4 = mediatriz(p1, p2)
            p5 = mediatriz(p2, p3)
            p6 = mediatriz(p3, p1)
            mf.pintarFractalSierpinski(prof-1, color, p1, p4, p6, grosor)
            mf.pintarFractalSierpinski(prof-1, color, p4, p2, p5, grosor)
            mf.pintarFractalSierpinski(prof-1, color, p6, p5, p3, grosor)
    
